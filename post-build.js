let pack = require('./dist/package.json');
pack['peerDependencies'] = pack.dependencies;
delete pack.dependencies;
delete pack.devDependencies;
require('fs').writeFileSync(require('path').resolve(__dirname, './dist/package.json'), JSON.stringify(pack, null, '\t'));
