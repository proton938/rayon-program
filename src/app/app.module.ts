import * as angular from 'angular';
import {AppComponent} from './app.component';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BlockUIModule} from 'ng-block-ui';
import {states} from './states.config';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {UIRouterModule} from '@uirouter/angular';
import {ProgramCardComponent} from './components/card/card.component';
import {NewProgramComponent} from './components/new-program/new-program.component';
import {WidgetsModule} from '@reinform-cdp/widgets';
import {ShowcaseBuilderModule} from '@reinform-cdp/showcase-builder';
import {FormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {CdpBpmComponentsModule} from '@reinform-cdp/bpm-components';
import {
  BsDropdownModule,
  CarouselModule,
  ModalModule,
  PaginationModule,
  PopoverModule,
  TabsModule
} from 'ngx-bootstrap';
import {DemoTaskComponent} from './components/tasks/demo-task.component';
import {ToastrModule} from 'ngx-toastr';
import {SearchResourceModule, SolarResourceServiceProvider} from '@reinform-cdp/search-resource';
import {ProgramCardHeaderComponent} from './components/card/header/program-card-header.component';
import {RayonProgramResourceService} from './services/rayon-program-resource.service';
import {RayonHelperService} from './services/rayon-helper.service';
import {NgSelectModule} from '@ng-select/ng-select';
import {FileResourceModule, FileResourceServiceProvider} from '@reinform-cdp/file-resource';
import {NgArrayPipesModule} from 'angular-pipes';
import {TextMaskModule} from 'angular2-text-mask';
import {RayonProgramShowcaseComponent} from './components/showcase/showcase.component';
import {ShowcaseService} from './components/showcase/showcase.service';
import {SolrDocumentBuilder} from './models/showcase/builders/SolrDocumentBuilder';
import {RayonProgramSolrDocumentBuilder} from './models/showcase/builders/RayonProgramSolrDocumentBuilder';
import {RayonProgramCommonSearchShowcaseComponent} from './components/showcase/showcase-common-search.component';
import {RayonProgramShowcaseFilterComponent} from './components/showcase/showcase-filter.component';
import {RayonProgramJsonEditorComponent} from './components/card/json-editor/json-editor.component';
import {RayonRequestResourceService} from './services/rayon-request-resource.service';
import {RayonNsiService} from './services/rayon-nsi.service';
import {NewRequestComponent} from './components/new-request/new-request.component';
import {RayonTaskInfoComponent} from './components/tasks/task-info/task-info.component';
import {RayonObjectTaskInfoComponent} from './components/tasks/object-task-info/object-task-info.component';
import {ObjectListFormComponent} from './components/forms/object-list-form/object-list-form.component';
import {TaskPrepareObjectList} from './components/tasks/prepare-object-list/prepare-object-list.component';
import {TaskAdjustObjectInformation} from './components/tasks/adjust-object-information/adjust-object-information.component';
import {MapModule} from '@reinform-cdp/map';

import {FormObjectComponent} from './components/form-object/form-object.component';
import {FinanceFormComponent} from './components/forms/finance/finance-form.component';
import {RayonPassportResourceService} from './services/rayon-passport-resource.service';
import {RayonPassportService} from './services/rayon-passport.service';
import {RayonRequestService} from './services/rayon-request.service';
import {TaskAgreeObjectList} from './components/tasks/agree-object-list/agree-object-list.component';
import {ProgramCardMainComponent} from './components/card/tabs/main/card-main.component';
import {ProgramCardMainViewComponent} from './components/card/tabs/main/card-main-view.component';
import {ProgramCardMainEditComponent} from './components/card/tabs/main/card-main-edit.component';
import {ProgramCardProcessComponent} from './components/card/tabs/process/card-process.component';
import {ProgramCardLogComponent} from './components/card/tabs/log/card-log.component';
import {ProgramCardLogWorksComponent} from './components/card/tabs/log/card-log-works.component';
import {ProgramCardJsonComponent} from './components/card/tabs/json/card-json.component';
import {ProgramFormObjectComponent} from './components/forms/object/form-object.component';
import {FilterPipe} from './pipes/filter.pipe';
import {ProgramFormObjectParamsZuComponent} from './components/forms/object/form-object-params-zu.component';
import {ProgramFormObjectParamsComponent} from './components/forms/object/form-object-params.component';
import {ProgramFormObjectFeaturesComponent} from './components/forms/object/form-object-features.component';
import {ProgramFormObjectMaterialsComponent} from './components/forms/object/form-object-materials.component';
import {ProgramCardWorksViewComponent} from './components/card/tabs/works/card-works-view.component';
import {ProgramCardWorksComponent} from './components/card/tabs/works/card-works.component';
import {InputDecimalComponent} from './components/forms/fields/input-decimal.component';
import {MoneyPipe} from './pipes/money.pipe';
import {ProgramCardInstructionsComponent} from './components/card/tabs/instructions/card-instructions.component';
import {FormWorkTypeComponent} from './components/forms/work-type/form-work-type.component';
import {NewWorkTypeComponent} from './components/new-work-type/new-work-type.component';
import {FormWorkTypeFinanceComponent} from './components/forms/work-type/form-work-type-finance.component';
import {FormWorkTypeFinanceByYearComponent} from './components/forms/work-type/form-work-type-finance-by-year.component';
import {RayonWorkTypeService} from './services/rayon-work-type.service';
import {FormWorkTypeEditModalComponent} from './components/forms/work-type/form-work-type-edit-modal.component';
import {SafePipe} from './pipes/safe.pipe';
import {ScriptService} from './services/script.service';
import {ProgramCardConsiderationComponent} from './components/card/tabs/consideration/card-consideration.component';
import {PanoramComponent} from './components/panoram/panoram.component';
import {ProgramCardObjectsComponent} from './components/card/tabs/objects/card-objects.component';
import {RayonLinkResourceService} from './services/rayon-link-resource.service';
import {ProgramCardObjectsOptionalComponent} from './components/card/tabs/objects/card-objects-optional.component';
import {RayonBtiService} from './services/rayon-bti.service';
import {FormCoordinatesModalComponent} from './components/forms/coordinates/form-coordinates-modal.component';
import {RayonGeoService} from './services/rayon-geo.service';
import {FormDataPublicationComponent} from './components/forms/start-process/data-publication/form-data-publication.component';
import {ProgramFormObjectDominantComponent} from './components/forms/object/dominant/form-object-dominant.component';
import {ProgramFormObjectDominantMaterialsComponent} from './components/forms/object/dominant/form-object-dominant-materials.component';
import {NewProgramDominantComponent} from './components/new-program/new-program-dominant.component';
import {ProgramCardDominantComponent} from './components/card/dominant/card-dominant.component';
import {ProgramCardDominantMainEditComponent} from './components/card/dominant/tabs/main/card-dominant-main-edit.component';
import {ProgramCardDominantMainViewComponent} from './components/card/dominant/tabs/main/card-dominant-main-view.component';
import {ProgramCardDominantHeaderComponent} from './components/card/dominant/header/program-card-dominant-header.component';
import {ProgramCardDominantMainComponent} from './components/card/dominant/tabs/main/card-dominant-main.component';
import {ProgramCardDominantObjectsComponent} from './components/card/dominant/tabs/objects/card-dominant-objects.component';
import {ProgramShowcaseDominantComponent} from './components/showcase/dominant/showcase-dominant.component';
import {ProgramFormObjectCommercialComponent} from './components/forms/object/commercial/form-object-commercial.component';
import {NewProgramCommercialComponent} from './components/new-program/new-program-commercial.component';
import {ProgramCardCommercialHeaderComponent} from './components/card/commercial/header/program-card-commercial-header.component';
import {ProgramCardCommercialComponent} from './components/card/commercial/card-commercial.component';
import {ProgramCardCommercialMainComponent} from './components/card/commercial/tabs/main/card-commercial-main.component';
import {ProgramCardCommercialMainViewComponent} from './components/card/commercial/tabs/main/card-commercial-main-view.component';
import {ProgramCardCommercialMainEditComponent} from './components/card/commercial/tabs/main/card-commercial-main-edit.component';
import {ProgramCardCommercialObjectsComponent} from './components/card/commercial/tabs/objects/card-commercial-objects.component';
import {ProgramShowcaseCommercialComponent} from './components/showcase/commercial/showcase-commercial.component';
import {ProgramFormObjectCommercialMaterialsComponent} from './components/forms/object/commercial/form-object-commercial-materials.component';
import {ObjectToArchiveBtnComponent} from './components/buttons/object-to-archive-btn/object-to-archive-btn.component';
import {ProgramShowcaseEventsComponent} from './components/showcase/events/showcase-events.component';
import {ProgramShowcaseArchiveComponent} from './components/showcase/archive/showcase-archive.component';
import {PublicationRequestComponent} from './components/publication-request/publication-request.component';
import {RayonPublicationService} from './services/rayon-publication.service';
import {PublicationRequestHeaderComponent} from './components/publication-request/header/publication-request-header.component';
import {PublicationRequestMainComponent} from './components/publication-request/tabs/main/publication-request-main.component';
import {RayonPublicationRequestJsonEditorComponent} from './components/publication-request/json-editor/json-editor.component';
import {ProgramPublicationRequestJsonComponent} from './components/publication-request/tabs/json/publication-request-json.component';
import {ProgramPublicationRequestLogComponent} from './components/publication-request/tabs/log/publication-request-log.component';
import {ProgramPublicationRequestProcessComponent} from './components/publication-request/tabs/process/publication-request-process.component';
import {ProgramShowcaseObjectsComponent} from './components/showcase/objects/showcase-objects.component';
import {PublicationRequestDocumentsComponent} from './components/publication-request/tabs/main/publication-request-documents.component';
import {TaskModerateData} from './components/tasks/moderate-data/moderate-data.component';
import {TaskModerateDataMosRu} from './components/tasks/moderate-data-mos-ru/moderate-data-mos-ru.component';
import {ProgramCardArchiveComponent} from './components/card/archive/card-archive.component';
import {ProgramCardArchiveHeaderComponent} from './components/card/archive/header/card-archive-header.component';
import {ProgramCardArchiveMainComponent} from './components/card/archive/tabs/main/card-archive-main.component';
import {ProgramCardArchiveMainEditComponent} from './components/card/archive/tabs/main/card-archive-main-edit.component';
import {ProgramCardArchiveMainViewComponent} from './components/card/archive/tabs/main/card-archive-main-view.component';
import {ProgramCardArchiveObjectsComponent} from './components/card/archive/tabs/objects/card-archive-objects.component';
import {ProgramCardArchiveObjectsOptionalComponent} from './components/card/archive/tabs/objects/card-archive-objects-optional.component';
import {NewProgramArchiveComponent} from './components/new-program/new-program-archive.component';
import {ProgramFormObjectArchiveComponent} from './components/forms/object/archive/form-object-archive.component';
import {ProgramFormObjectArchiveMaterialsComponent} from './components/forms/object/archive/form-object-archive-materials.component';
import {ProgramFormObjectArchiveFeaturesComponent} from './components/forms/object/archive/form-object-archive-features.component';
import {ObjectEditBtnComponent} from './components/buttons/object-edit-btn/object-edit-btn.component';
import {RayonProgramBtnService} from './services/rayon-program-btn.service';
import {ObjectAdjustmentBtnComponent} from './components/buttons/object-adjustment-btn/object-adjustment-btn.component';
import {ObjectMatrixService} from './services/object-matrix.service';
import {WorkMatrixService} from './services/work-matrix.service';
import {FormCreateObjectComponent} from './components/forms/start-process/create-object/form-create-object.component';
import {TaskFillFormForCreatingObjectComponent} from './components/tasks/fill-form-for-creating-object/fill-form-for-creating-object.component';
import {FormCreateObjectDominantComponent} from './components/forms/start-process/create-object-dominant/form-create-object-dominant.component';
import {TaskFillFormForCreatingUniqueObjectComponent} from './components/tasks/fill-form-for-creating-unique-object/fill-form-for-creating-unique-object.component';
import {TaskApproveData} from './components/tasks/approve-data/approve-data.component';
import {ApproveDataTable} from './components/tasks/approve-data/approve-data-table.component';
import {ProgramCardDecisionsComponent} from './components/card/tabs/decisions/card-decisions.component';
import {FormRenameFileModalComponent} from './components/forms/rename-file/form-rename-file-modal.component';
import {RayonSchemaService} from './services/rayon-schema.service';
import {PlanResourceService} from './services/plan-resource.service';
import {FormPlanComponent} from './components/forms/plan/form-plan.component';
import {NewPlanComponent} from './components/new-plan/new-plan.component';
import {CardPlanComponent} from './components/card/plan/card-plan.component';
import {CardPlanMainComponent} from './components/card/plan/tabs/main/card-plan-main.component';
import {CardPlanMainEditComponent} from './components/card/plan/tabs/main/card-plan-main-edit.component';
import {CardPlanMainViewComponent} from './components/card/plan/tabs/main/card-plan-main-view.component';
import {NewPlanModalComponent} from './components/new-plan/new-plan-modal.component';
import {CardPlanLogComponent} from './components/card/plan/tabs/log/card-plan-log.component';
import {CardPlanProcessComponent} from './components/card/plan/tabs/process/card-plan-process.component';
import {CardPlanJsonComponent} from './components/card/plan/tabs/json/card-plan-json.component';
import {CardPlanVersionsComponent} from './components/card/plan/tabs/versions/card-plan-versions.component';
import {ProgramSpinnerComponent} from './components/common/spinner/program-spinner.component';
import {CardPlanHeaderComponent} from './components/card/plan/header/card-plan-header.component';
import {CardPlanObjectsComponent} from './components/card/plan/tabs/objects/card-plan-objects.component';
import {PlanObjectsOptionalComponent} from './components/card/plan/tabs/objects/plan-objects-optional.component';
import {PlanObjectsFilterComponent} from './components/card/plan/tabs/objects/plan-objects-filter.component';
import {ApprovePlanBtnComponent} from './components/buttons/approve-plan-btn/approve-plan-btn.component';
import {PublicMosComponent} from './components/forms/public-mos/public-mos.component';
import {FormHeadingUkkComponent} from './components/forms/heading-ukk/heading-ukk.component';
import {ObjectHeaderLabelsComponent} from './components/card/header/object-header-labels.component';
import {SolrMediatorService} from './services/solr-mediator.service';
import {CommonQueryService} from './services/common-query.service';
import {ProgramCardAerialPhotoComponent} from './components/card/tabs/aerial-photo/card-aerial-photo.component';
import {ProgramCardAerialPhotoViewComponent} from './components/card/tabs/aerial-photo/card-aerial-photo-view.component';
import {ProgramCardAerialPhotoEditComponent} from './components/card/tabs/aerial-photo/card-aerial-photo-edit.component';
import {DroneService} from './services/drone.service';
import {ChangeRequestBtnComponent} from './components/buttons/change-request-btn/change-request-btn.component';
import {ChangeRequestService} from './services/change-request.service';
import {TaskPrepAppObjWorkChangesComponent} from './components/tasks/change-request/prep-app-obj-work-changes/prep-app-obj-work-changes.component';
import {ChangeRequestDataModalComponent} from './components/buttons/change-request-btn/change-request-data-modal.component';
import {TaskPrepAppObjectWorkChangesComponent} from './components/tasks/change-request/prep-app-object-work-changes/prep-app-object-work-changes.component';
import {TaskApprAppObjWorkChangesComponent} from './components/tasks/change-request/appr-app-obj-work-changes/appr-app-obj-work-changes.component';
import {TaskReviewRequestResultsComponent} from './components/tasks/change-request/review-request-results/review-request-results.component';
import {StarComponent} from './components/common/star/star.component';
import {CardChangeRequestComponent} from './components/card/change-request/card.component';
import {HeaderChangeRequestComponent} from './components/card/change-request/header/header.component';
import {CardChangeRequestMainComponent} from './components/card/change-request/tabs/main/main.component';
import {CardChangeRequestJsonComponent} from './components/card/change-request/tabs/json/json.component';
import {CardChangeRequestLogComponent} from './components/card/change-request/tabs/log/log.component';
import {CardChangeRequestProcessComponent} from './components/card/change-request/tabs/process/process.component';
import {TaskApprAppObjectWorkChangesComponent} from './components/tasks/change-request/appr-app-object-work-changes/appr-app-object-work-changes.component';
import {TaskReviewRequestWorkResultsComponent} from './components/tasks/change-request/review-request-work-results/review-request-work-results.component';
import {TaskChangeRequestInfoComponent} from './components/tasks/change-request/info.component';
import {LabelsComponent} from './components/common/labels/labels.component';
import {ClickOutsideModule} from 'ng-click-outside';
import {LoadCatalogComponent} from './components/load-catalog/load-catalog.component';
import {EhdService} from './services/ehd.service';
import {LinkPlanWorkObjectComponent} from './components/link-plan-work-object/link-plan-work-object.component';
import {ProcessResourceService} from './services/process-resource.service';
import {LinkResultModalComponent} from './components/link-plan-work-object/link-result-modal.component';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    LaddaModule,
    ToastrModule.forRoot(),
    BlockUIModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    UIRouterUpgradeModule.forRoot({states: states}),
    UIRouterModule,
    WidgetsModule.forRoot(),
    CdpBpmComponentsModule.forRoot(),
    SearchResourceModule.forRoot(),
    NgSelectModule,
    FileResourceModule.forRoot(),
    NgArrayPipesModule,
    TextMaskModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot(),
    CarouselModule.forRoot(),
    ShowcaseBuilderModule,
    MapModule
  ],
  declarations: [
    FilterPipe,
    MoneyPipe,
    SafePipe,
    InputDecimalComponent,
    AppComponent,
    StarComponent,
    ProgramSpinnerComponent,
    ProgramCardComponent,
    ProgramCardHeaderComponent,
    ProgramCardMainComponent,
    ProgramCardMainViewComponent,
    ProgramCardMainEditComponent,
    ProgramCardProcessComponent,
    ProgramCardLogComponent,
    ProgramCardLogWorksComponent,
    ProgramCardJsonComponent,
    ProgramFormObjectComponent,
    ProgramFormObjectParamsComponent,
    ProgramFormObjectParamsZuComponent,
    ProgramFormObjectFeaturesComponent,
    ProgramFormObjectMaterialsComponent,
    ProgramCardWorksComponent,
    ProgramCardWorksViewComponent,
    ProgramCardInstructionsComponent,
    ProgramCardObjectsComponent,
    ProgramCardObjectsOptionalComponent,
    ProgramCardDecisionsComponent,
    RayonProgramShowcaseComponent,
    NewProgramComponent,
    DemoTaskComponent,
    RayonProgramCommonSearchShowcaseComponent,
    RayonProgramShowcaseFilterComponent,
    RayonProgramJsonEditorComponent,
    NewRequestComponent,
    RayonTaskInfoComponent,
    ObjectListFormComponent,
    TaskPrepareObjectList,
    TaskAgreeObjectList,
    TaskAdjustObjectInformation,
    TaskModerateData,
    TaskModerateDataMosRu,
    TaskFillFormForCreatingObjectComponent,
    TaskFillFormForCreatingUniqueObjectComponent,
    TaskApproveData,
    TaskChangeRequestInfoComponent,
    TaskPrepAppObjWorkChangesComponent,
    TaskApprAppObjWorkChangesComponent,
    TaskPrepAppObjectWorkChangesComponent,
    TaskReviewRequestResultsComponent,
    TaskApprAppObjectWorkChangesComponent,
    TaskReviewRequestWorkResultsComponent,
    ApproveDataTable,
    FormObjectComponent,
    FinanceFormComponent,
    FormWorkTypeComponent,
    NewWorkTypeComponent,
    FormWorkTypeFinanceComponent,
    FormWorkTypeFinanceByYearComponent,
    FormWorkTypeEditModalComponent,
    ProgramCardConsiderationComponent,
    PanoramComponent,
    FormDataPublicationComponent,
    RayonObjectTaskInfoComponent,
    FormCoordinatesModalComponent,
    ProgramFormObjectDominantComponent,
    ProgramFormObjectDominantMaterialsComponent,
    NewProgramDominantComponent,
    ProgramCardDominantHeaderComponent,
    ProgramCardDominantComponent,
    ProgramCardDominantMainComponent,
    ProgramCardDominantMainEditComponent,
    ProgramCardDominantMainViewComponent,
    ProgramCardDominantObjectsComponent,
    ProgramShowcaseDominantComponent,
    ProgramFormObjectCommercialComponent,
    ProgramFormObjectCommercialMaterialsComponent,
    NewProgramCommercialComponent,
    ProgramCardCommercialHeaderComponent,
    ProgramCardCommercialComponent,
    ProgramCardCommercialMainComponent,
    ProgramCardCommercialMainViewComponent,
    ProgramCardCommercialMainEditComponent,
    ProgramCardCommercialObjectsComponent,
    ProgramShowcaseCommercialComponent,
    ObjectToArchiveBtnComponent,
    ObjectEditBtnComponent,
    ObjectAdjustmentBtnComponent,
    ProgramShowcaseEventsComponent,
    ProgramShowcaseObjectsComponent,
    ProgramShowcaseArchiveComponent,
    PublicationRequestComponent,
    PublicationRequestHeaderComponent,
    PublicationRequestMainComponent,
    RayonPublicationRequestJsonEditorComponent,
    ProgramPublicationRequestJsonComponent,
    ProgramPublicationRequestLogComponent,
    ProgramPublicationRequestProcessComponent,
    PublicationRequestDocumentsComponent,
    ProgramCardArchiveComponent,
    ProgramCardArchiveHeaderComponent,
    ProgramCardArchiveMainComponent,
    ProgramCardArchiveMainEditComponent,
    ProgramCardArchiveMainViewComponent,
    ProgramCardArchiveObjectsComponent,
    ProgramCardArchiveObjectsOptionalComponent,
    NewProgramArchiveComponent,
    ProgramFormObjectArchiveComponent,
    ProgramFormObjectArchiveMaterialsComponent,
    ProgramFormObjectArchiveFeaturesComponent,
    FormCreateObjectComponent,
    FormCreateObjectDominantComponent,
    FormRenameFileModalComponent,
    FormPlanComponent,
    NewPlanComponent,
    NewPlanModalComponent,
    CardPlanComponent,
    CardPlanHeaderComponent,
    CardPlanMainComponent,
    CardPlanMainViewComponent,
    CardPlanMainEditComponent,
    CardPlanLogComponent,
    CardPlanProcessComponent,
    CardPlanJsonComponent,
    CardPlanVersionsComponent,
    CardPlanObjectsComponent,
    PlanObjectsOptionalComponent,
    PlanObjectsFilterComponent,
    ApprovePlanBtnComponent,
    ChangeRequestBtnComponent,
    ChangeRequestDataModalComponent,
    PublicMosComponent,
    FormHeadingUkkComponent,
    ObjectHeaderLabelsComponent,
    ProgramCardAerialPhotoComponent,
    ProgramCardAerialPhotoViewComponent,
    ProgramCardAerialPhotoEditComponent,
    CardChangeRequestComponent,
    HeaderChangeRequestComponent,
    CardChangeRequestMainComponent,
    CardChangeRequestJsonComponent,
    CardChangeRequestLogComponent,
    CardChangeRequestProcessComponent,
    ObjectHeaderLabelsComponent,
    LabelsComponent,
    LoadCatalogComponent,
    LinkPlanWorkObjectComponent,
    LinkResultModalComponent
  ],
  bootstrap: [AppComponent],
  exports: [AppComponent, CdpBpmComponentsModule],
  entryComponents: [FormObjectComponent, FormWorkTypeEditModalComponent, FormCoordinatesModalComponent,
    FormDataPublicationComponent, FormRenameFileModalComponent, NewPlanModalComponent, ChangeRequestDataModalComponent,
    LinkResultModalComponent]
})
export class Program {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: Program,
      providers: [
        RayonProgramResourceService,
        RayonRequestResourceService,
        RayonPassportResourceService,
        RayonNsiService,
        RayonHelperService,
        RayonPassportService,
        RayonRequestService,
        ShowcaseService,
        SolrDocumentBuilder,
        RayonProgramSolrDocumentBuilder,
        RayonWorkTypeService,
        ScriptService,
        RayonWorkTypeService,
        RayonLinkResourceService,
        RayonBtiService,
        RayonGeoService,
        RayonPublicationService,
        RayonProgramBtnService,
        ObjectMatrixService,
        WorkMatrixService,
        RayonSchemaService,
        PlanResourceService,
        SolrMediatorService,
        CommonQueryService,
        DroneService,
        ChangeRequestService,
        ClickOutsideModule,
        EhdService,
        ProcessResourceService
      ]
    };
  }
}

export const ng1Module = angular.module('program', ['cdp.bpm.components', 'cdp.core', 'cdp.file.resource']);

ng1Module.config(['cdpFileResourceServiceProvider', (fileResourceServiceProvider: FileResourceServiceProvider) => {
  fileResourceServiceProvider.root = '/filestore/v1';
}]);

ng1Module.config(['cdpSolarResourceServiceProvider', (cdpSolarResourceServiceProvider: SolarResourceServiceProvider) => {
  cdpSolarResourceServiceProvider.root = '/app/mr/search';
}]);
