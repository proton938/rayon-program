import './styles.scss';
import 'jquery';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {SkeletonModule} from '@reinform-cdp/skeleton';
import {setAngularJSGlobal, UpgradeModule} from '@angular/upgrade/static';
import * as angular from 'angular';
import {FormsModule} from '@angular/forms';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {UrlService} from '@uirouter/core';
import {ng1RoutesConfig, ng6RoutesConfig} from './states.config';
import {DemoRootComponent} from './demo-root/demo-root.component';
import {Program} from '../app.module';
import {CoreModule} from '@reinform-cdp/core';
import {CdpBpmComponentsModule, TaskWrapperComponent} from '@reinform-cdp/bpm-components';
import 'metismenu';

setAngularJSGlobal(angular);
declare let document: any;

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    UpgradeModule,
    UIRouterUpgradeModule.forRoot({states: [].concat(ng6RoutesConfig, [
      {
        name: 'app.execution',
        url: '/execution/:system/:taskId',
        component: TaskWrapperComponent
      }
    ])}),
    SkeletonModule.forRoot(),
    Program.forRoot(),
    CdpBpmComponentsModule.forRoot(),
    CoreModule.forRoot('MR', '/main/', 'Мой район', '/app/mr/bpm')
  ],
  providers: [],
  declarations: [DemoRootComponent],
  entryComponents: [DemoRootComponent]
})
export class DemoModule {
  constructor(private upgrade: UpgradeModule) {
  }

  ngDoBootstrap() {
    this.upgrade.bootstrap(document.body, ['program.demo'], {strictDi: false});
  }
}

// можно подключить для отладки cdp.skeleton - каркас приложения
const appDemo = angular.module('program.demo', [
  'ui.router',
  'ui.router.upgrade',
  'cdp.skeleton',
  'cdp.bpm.components',
  'program'
]);

appDemo.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', ng1RoutesConfig]);

// техническая секция, чтоб все работало с hybrid-router
// подождать пока инициализируется роутер
appDemo.config(['$urlServiceProvider', ($urlService: UrlService) => $urlService.deferIntercept()]);
