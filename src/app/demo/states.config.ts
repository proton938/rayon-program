import {NgHybridStateDeclaration} from '@uirouter/angular-hybrid';
import * as angular from 'angular';

export const ng6RoutesConfig: NgHybridStateDeclaration[] = [
];


export function ng1RoutesConfig($stateProvider: any, $urlRouterProvider: any, $locationProvider: angular.ILocationProvider) {
  $locationProvider.hashPrefix('');
  $urlRouterProvider.otherwise(($injector) => {
    const $state = $injector.get('$state');
    $state.go('app.object.new');
  });
}
