import {Component, Input} from "@angular/core";
import {ChangeRequest} from "../../../models/change-request/ChangeRequest";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {RayonProgram} from "../../../models/program/RayonProgram";
import {compact} from 'lodash';
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {RayonWork} from "../../../models/work/RayonWork";
import {RayonWorkTypeService} from "../../../services/rayon-work-type.service";

@Component({
  selector: 'task-change-request-info',
  templateUrl: './info.component.html'
})
export class TaskChangeRequestInfoComponent {
  isLoading: boolean = true;
  work: RayonWork;

  @Input() request: ChangeRequest;
  @Input() obj: RayonProgram;
  @Input() cardLink: boolean = false;

  constructor(private programService: RayonProgramResourceService,
              private workService: RayonWorkTypeService,
              private helper: RayonHelperService) {}

  ngOnInit() {
    if (this.request.modifiedTab === 'WORK') {
      this.work = this.workService.getInstance(JSON.parse(this.request.document.jsonOld));
    }

    if (!this.obj) {
      switch (this.request.modifiedTab) {
        case 'OBJ':
          this.obj = this.programService.getInstance(JSON.parse(this.request.document.jsonOld));
          this.isLoading = false;
          break;
        case 'WORK':
          this.programService.get(JSON.parse(this.request.document.jsonOld).work.objectId).subscribe(obj => {
            this.obj = this.programService.getInstance(obj);
            this.isLoading = false;
          });
          break;
      }
    } else {
      this.isLoading = false;
    }
  }

  getOldInfo(): string {
    let r: string[] = [];
    r.push(this.helper.getField(this.obj, 'object.name'));
    r.push(this.helper.getField(this.obj, 'address.prefect.name').join(', '));
    r.push(this.helper.getField(this.obj, 'address.district.name').join(', '));
    r.push(this.helper.getField(this.obj, 'address.address'));
    return compact(r).join(', ');
  }
}
