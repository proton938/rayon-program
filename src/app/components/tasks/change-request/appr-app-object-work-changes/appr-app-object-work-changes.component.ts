import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {fromJson, toJson} from '@uirouter/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {SessionStorage} from '@reinform-cdp/security';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {RayonHelperService} from '../../../../services/rayon-helper.service';
import {find, concat, some} from 'lodash';
import {ChangeRequestService} from '../../../../services/change-request.service';
import {copy} from 'angular';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {ChangeRequest} from '../../../../models/change-request/ChangeRequest';
import {StateService} from '@uirouter/core';
import {RayonProgramResourceService} from '../../../../services/rayon-program-resource.service';
import {ChangeRequestCoordinator} from '../../../../models/change-request/ChangeRequestCoordinator';
import {RayonProgram} from '../../../../models/program/RayonProgram';
import {formatDate} from '@angular/common';
import {RayonWorkWrapper} from '../../../../models/work/RayonWorkWrapper';
import {isBoolean, isNumber, last} from 'lodash';
import {RayonWork} from '../../../../models/work/RayonWork';
import {RayonWorkTypeService} from '../../../../services/rayon-work-type.service';
import {Observable, forkJoin, from, of} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {ExFileType} from '@reinform-cdp/widgets';
import {RayonWorkProgress} from '../../../../models/work/RayonWorkProgress';
import {RayonWorkProgressDocument} from '../../../../models/work/RayonWorkProgressDocument';
import {FileResourceService} from '@reinform-cdp/file-resource';

@Component({
  selector: 'task-appr-app-object-work-changes',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './appr-app-object-work-changes.component.html',
  styleUrls: [
    './appr-app-object-work-changes.component.scss',
    '../change-request.scss'
  ]
})
export class TaskApprAppObjectWorkChangesComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption = '';
  taskId = '';
  dicts: any = {};
  saving = false;
  finishing = false;
  validate = false;
  vars: any = {};

  request: ChangeRequest;
  copyRequest: ChangeRequest;
  documentOld: RayonWorkWrapper = new RayonWorkWrapper();
  document: RayonWorkWrapper = new RayonWorkWrapper();
  obj: RayonProgram;
  progress: RayonWorkProgress = new RayonWorkProgress();
  emptyProgress: RayonWorkProgress = new RayonWorkProgress();
  progressOld: RayonWorkProgress = new RayonWorkProgress();
  progressFiles: any[] = [];
  progressOldFiles: any[] = [];
  files: any[] = [];
  requestFiles: any[] = [];
  extraFiles: any[] = [];

  documentOldProgress: RayonWorkProgress[] = [];
  emptyDocumentsExecution: RayonWorkProgressDocument[] = [new RayonWorkProgressDocument()];

  @BlockUI('block-ui-task') blockUI: NgBlockUI;

  constructor(public helper: RayonHelperService,
              private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private session: SessionStorage,
              private solrMediator: SolrMediatorService,
              private service: ChangeRequestService,
              private programService: RayonProgramResourceService,
              private workService: RayonWorkTypeService,
              private state: StateService,
              private nsi: NsiResourceService,
              private fileResourceService: FileResourceService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      if (response.variables) { response.variables.forEach(v => this.vars[v.name] = v.value); }

      const dicts$ = this.nsi.getDictsFromCache([
        'mr_program_WorkStatuses',
        'mr_program_approveMunicipalStatus',
        'mr_program_approveAgStatus',
        'mr_program_ResponsibleOrganizations',
        'mr_program_StatePrograms',
        'mr_program_StateCustomers',
        'mr_instruction_SupportTypes',
        'mr_program_reasonForRejecting',
        'mr_program_WorkDocTypes',
        'mr_program_approveDocStatus',
        'mr_program_WorkDocTypesCanceled'
      ]);
      const request$ = this.service.get(this.vars['EntityIdVar']);

      forkJoin([dicts$, request$]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = response[1];
        this.copyRequest = copy(this.request);

        if (!this.request.coordinator) { this.request.coordinator = new ChangeRequestCoordinator(); }

        if (this.request.document) {
          if (this.request.document.jsonOld) {
            this.documentOld.build(JSON.parse(this.request.document.jsonOld));
            if (this.documentOld.work.progress.length) { this.progressOld = last(this.documentOld.work.progress); }
          }
          if (this.request.document.jsonNew) {
            this.document.build(JSON.parse(this.request.document.jsonNew));
            if (this.document.work.progress.length) { this.progress = last(this.document.work.progress); }
            if (some(this.progress.documentsExecution, i => !i.type)) {
              this.progress.documentsExecution = this.progress.documentsExecution.filter(i => i.type);
            }
          }
        }

        this.makeDicts();

        forkJoin([this.getFiles(this.documentOld.work.folderId), this.getFiles(this.request.folderId)]).subscribe(res => {
          this.files = res[0];
          this.requestFiles = res[1];
          if (this.request.files && this.request.files.length) {
            this.extraFiles = this.request.files.map(id => find(this.requestFiles, f => f.idFile === id));
          }
          this.updateProgressFiles();
          this.updateDocumentOldProgress();
          this.loading = LoadingStatus.SUCCESS;
        }, error => {
          this.loading = LoadingStatus.ERROR;
        });
      });
    });
  }

  makeDicts() {
    this.dicts.statuses = this.dicts['mr_program_WorkStatuses'].map(d => this.helper.convertToNsi(d));
    this.dicts.approveMunicipalStatus = this.dicts['mr_program_approveMunicipalStatus'].map(d => this.helper.convertToNsi(d));
    this.dicts.approveAg = this.dicts['mr_program_approveAgStatus'].map(d => this.helper.convertToNsi(d));
    this.dicts.responsibleOrganizations = this.dicts['mr_program_ResponsibleOrganizations'].map(d => this.helper.convertToNsi(d));
    this.dicts.years = this.helper.makeYearDictionary();
    this.dicts.budgetList = [
      {code: true, name: 'Предусмотрено'},
      {code: false, name: 'Не предусмотрено'}
    ];
    this.dicts.statePrograms = this.dicts['mr_program_StatePrograms']
      .map(d => this.helper.convertToNsi(d)).map(d => this.helper.convertToNsi(d));
    this.dicts.stateCustomers = this.dicts['mr_program_StateCustomers'].map(d => this.helper.convertToNsi(d));
    this.dicts.supportTypes = this.dicts['mr_instruction_SupportTypes'].map(d => this.helper.convertToNsi(d));
    this.dicts.rejectReason = this.dicts['mr_program_reasonForRejecting'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocTypes = this.dicts['mr_program_WorkDocTypes'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocTypesCanceled = this.dicts['mr_program_WorkDocTypesCanceled'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocStatuses = this.dicts['mr_program_approveDocStatus'].map(d => this.helper.convertToNsi(d));

    this.dicts.workExDocTypes = {
      done: this.dicts.workDocTypes,
      psd_done: this.dicts.workDocTypes,
      canceled: this.dicts.workDocTypesCanceled
    };
  }

  updateProgressFiles() {
    if (!this.progressOld.documentsExecution) { this.progressOld.documentsExecution = []; }
    if (this.documentOld.work.progress.length !== this.document.work.progress.length) {
      this.progressFiles = last(this.document.work.progress).documentsExecution.map(i => {
        return i.files.map(id => find(this.files, f => f.idFile === id));
      });
      this.progressOldFiles = [];
    } else if (this.documentOld.work.progress.length) {
      this.progressFiles = this.document.work.progress[this.documentOld.work.progress.length - 1].documentsExecution.map(i => {
        return i.files.map(id => find(this.files, f => f.idFile === id));
      });
      this.progressOldFiles = this.progressOld.documentsExecution.map(i => {
        return i.files.map(id => find(this.files, f => f.idFile === id));
      });
    }
  }

  updateDocumentOldProgress() {
    this.documentOldProgress = copy(this.documentOld.work.progress);
    this.documentOldProgress.push(new RayonWorkProgress());
  }

  isNewProgress(): boolean {
    return this.document.work.status
      && (!this.documentOld.work.status || this.document.work.status.code !== this.documentOld.work.status.code);
  }

  isShow(fieldName: string, index?: number, subIndex?: number): boolean {
    let r = true;
    const getField = (path: string) => {
      return [
        this.helper.getField(this.documentOld.work, path),
        this.helper.getField(this.document.work, path)
      ];
    };
    let field: any;
    switch (fieldName) {
      case 'progress.documentsExecution.status':
        field = getField('progress').map(f => {
          let r = this.helper.getField(f[index], 'documentsExecution.status');
          if (r) { r = r[subIndex]; }
          return r;
        });
        if (!field[0] && !field[1]) { field = [0, 0]; }
        index = null;
        break;
      case 'progress.documentsExecution':
        field = getField(fieldName);
        if (!field[0][index]) { field[0][index] = []; }
        break;
      default:
        field = getField(fieldName);
        break;
    }
    if (field && isNumber(index) && field.length) {
      r = (!!field[0][index] || !!field[1][index]) && toJson(field[0][index]) !== toJson(field[1][index]);
    } else if (field.length) { r = toJson(field[0]) !== toJson(field[1]); }
    return r;
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  getFiles(folderId: string = this.request.folderId): Observable<any[]> {
    return folderId
      ? this.helper.getFiles(folderId).pipe(map(files => files.map(file => {
        return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
          false, file.dateCreated, file.fileType, file.mimeType);
      })))
      : from(of([]));
  }

  // #ISMR-2983 Удалить из alfresco файлы из jsonOld, которые не фигурируют в jsonNew
  removeIrrelevantFiles(): Observable<any> {
    const filesOld = concat([], ...this.helper.getField(this.documentOld.work, 'progress.documentsExecution.files'));
    const filesNew = concat([], ...this.helper.getField(this.document.work, 'progress.documentsExecution.files'));
    const requests$ = this.files
      .filter(f => some(filesOld, i => i === f.idFile) && !some(filesNew, i => i === f.idFile))
      .map(f => this.fileResourceService.deleteFile(f.idFile));
    if (requests$.length) {
      return forkJoin(requests$);
    }
    return from(of('ok'));
  }

  prepareData() {
    this.request.coordinator.login = this.session.login();
    this.request.coordinator.fio = this.session.fullName();
    if (!!this.request.coordinator.comment || !!this.request.coordinator.reasonForRejecting) {
      this.request.coordinator.date = new Date();
    }
  }

  isValid(rework: boolean = false, reject: boolean = false): boolean {
    let r = true;
    if (rework) {
      r = !!this.request.coordinator.comment;
      if (!r) { this.helper.error('Поле "Комментарий" обязательно для заполнения!'); }
    }
    if (reject) {
      r = !!this.request.coordinator.reasonForRejecting;
      if (!r) { this.helper.error('Поле "Причина отклонения" обязательно для заполнения!'); }
    }
    return r;
  }

  makeVars(rework: boolean = false, reject: boolean = false): ITaskVariable[] {
    const r: ITaskVariable[] = [];
    r.push({name: 'IsRework', value: rework});
    if (!rework) { r.push({name: 'IsReject', value: reject}); }
    return r;
  }

  finishTask(vars: ITaskVariable[] = this.makeVars()) {
    this.activityResourceService.finishTask(parseInt(this.taskId, 0), vars).then(response => {
      this.helper.success('Задача успешно завершена!');
      this.blockUI.stop();
      (<any>window).location = '/main/#/app/tasks';
    }).catch(error => {
      console.error(error);
      this.helper.error('Ошибка при завершении задачи!');
      this.blockUI.stop();
    });
  }

  finish(rework: boolean = false, reject: boolean = false) {
    let diff: any[] = [];
    const runFinishing = () => {
      if (rework || reject) {
        this.finishTask(this.makeVars(rework, reject));
      } else {
        this.updateRealDocumentAndFinishTask();
      }
    };
    this.validate = true;
    if (this.isValid(rework, reject)) {
      this.prepareData();
      this.blockUI.start();
      diff = this.service.diff(this.copyRequest, copy(this.request));
      if (diff.length) {
        this.service.patch(this.request.documentId, diff).subscribe(() => runFinishing());
      } else { runFinishing(); }
    }
  }

  updateRealDocumentAndFinishTask() {
    const id: string = this.documentOld.work.documentId;
    const ok = () => {
      this.removeIrrelevantFiles().subscribe(() => {
        this.finishTask();
      }, error => {
        console.log('ERR: ', error);
        this.finishTask();
      });
    };
    this.document.work.correctionReason = 'Изменения осуществлены на основании заявки №' + this.request.documentNumber
      + ' в рамках бизнес-процесса';
    this.workService.get(id).subscribe(res => {
      const old: RayonWork = this.workService.getInstance(res);
      const next: RayonWork = copy(this.document.work);
      // resolve progress list (#ISMR-3019)
      this.workService.resolveProgress(this.documentOld.work, next, old);
      const diff: any[] = this.workService.diff(old, next);
      if (diff.length) {
        this.workService.patch(id, diff, 'Заявка на изменения №' +
          this.request.documentNumber + ' от ' +
          formatDate(this.request.documentDate, 'dd.MM.yyyy', 'en')).subscribe(() => {
          ok();
        }, error => {
          this.helper.error(error);
          this.blockUI.stop();
        });
      } else {
        ok();
      }
    }, error => {
      this.helper.error(error);
      this.blockUI.stop();
    });
  }

  save() {
    const ok = () => {
      this.helper.success('Документ сохранен!');
      this.blockUI.stop();
      (<any>window).location = '/main/#/app/tasks';
    };
    this.prepareData();
    this.blockUI.start();
    const diff: any[] = this.service.diff(this.copyRequest, copy(this.request));
    if (diff.length) {
      this.service.patch(this.request.documentId, diff).subscribe(() => ok());
    } else { ok(); }
  }
}
