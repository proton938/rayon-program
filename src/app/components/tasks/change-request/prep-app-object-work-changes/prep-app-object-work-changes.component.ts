import {Component, OnInit} from '@angular/core';
import {copy} from 'angular';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService} from '@reinform-cdp/bpm-components';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {SessionStorage} from '@reinform-cdp/security';
import {find, compact, some, isBoolean, concat, last, every} from 'lodash';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {RayonHelperService} from '../../../../services/rayon-helper.service';
import {ChangeRequest} from '../../../../models/change-request/ChangeRequest';
import {Observable, forkJoin, from, of} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {ChangeRequestService} from '../../../../services/change-request.service';
import {RayonWork} from '../../../../models/work/RayonWork';
import {RayonWorkTypeService} from '../../../../services/rayon-work-type.service';
import {RayonProgramResourceService} from '../../../../services/rayon-program-resource.service';
import {RayonWorkWrapper} from '../../../../models/work/RayonWorkWrapper';
import {RayonProgram} from '../../../../models/program/RayonProgram';
import {StateService, toJson, fromJson} from '@uirouter/core';
import {formatDate} from '@angular/common';
import {RayonWorkProgress} from '../../../../models/work/RayonWorkProgress';
import {RayonWorkProgressDocument} from '../../../../models/work/RayonWorkProgressDocument';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {ExFileType} from '@reinform-cdp/widgets';
import {RayonWorkUser} from '../../../../models/work/RayonWorkUser';
import {compare} from 'fast-json-patch';

@Component({
  selector: 'task-prep-app-object-work-changes',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './prep-app-object-work-changes.component.html',
  styleUrls: [
    './prep-app-object-work-changes.component.scss',
    '../change-request.scss'
  ]
})
export class TaskPrepAppObjectWorkChangesComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption = '';
  taskId = '';
  vars: any = {};
  dicts: any = {};
  saving = false;
  finishing = false;
  validate = false;

  request: ChangeRequest;
  copyRequest: ChangeRequest;
  obj: RayonProgram;
  document: RayonWorkWrapper = new RayonWorkWrapper();
  documentOld: RayonWorkWrapper = new RayonWorkWrapper();
  progress: RayonWorkProgress = new RayonWorkProgress();
  progressOld: RayonWorkProgress = new RayonWorkProgress();
  emptyProgress: RayonWorkProgress = new RayonWorkProgress();
  progressFiles: any[] = [];
  progressOldFiles: any[] = [];
  files: any[] = [];
  requestFiles: any[] = [];
  extraFiles: any[] = [];

  documentOldProgress: RayonWorkProgress[] = [];
  emptyDocumentsExecution: RayonWorkProgressDocument[] = [new RayonWorkProgressDocument()];

  @BlockUI('block-ui-task') blockUI: NgBlockUI;

  constructor(private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private session: SessionStorage,
              private state: StateService,
              private service: ChangeRequestService,
              private workService: RayonWorkTypeService,
              private programService: RayonProgramResourceService,
              private nsi: NsiResourceService,
              private fileResourceService: FileResourceService,
              public helper: RayonHelperService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      if (response.variables) { response.variables.forEach(v => this.vars[v.name] = v.value); }

      const dicts$ = this.nsi.getDictsFromCache([
        'mr_program_WorkStatuses',
        'mr_program_approveMunicipalStatus',
        'mr_program_approveAgStatus',
        'mr_program_ResponsibleOrganizations',
        'mr_program_StatePrograms',
        'mr_program_StateCustomers',
        'mr_instruction_SupportTypes',
        'mr_program_WorkDocTypes',
        'mr_program_approveDocStatus',
        'mr_program_WorkDocTypesCanceled'
      ]);
      const request$ = this.service.get(this.vars['EntityIdVar']);

      forkJoin([dicts$, request$]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = response[1];
        this.copyRequest = copy(this.request);

        this.makeDicts();

        if (this.request.document) {
          if (this.request.document.jsonOld) {
            this.documentOld.build(JSON.parse(this.request.document.jsonOld));
            if (!this.documentOld.work.progress) { this.documentOld.work.progress = []; }
            if (this.documentOld.work.progress.length) { this.progressOld = last(this.documentOld.work.progress); }
          }
          if (this.request.document.jsonNew) {
            this.document.build(JSON.parse(this.request.document.jsonNew));
            if (!this.document.work.progress) { this.document.work.progress = []; }
            if (!this.document.work.progress.length) {
              const progressItem: RayonWorkProgress = new RayonWorkProgress();
              if (this.document.work.status) { progressItem.stage = copy(this.document.work.status); }
              this.document.work.progress.push(progressItem);
            }
            if (this.document.work.progress.length) { this.progress = last(this.document.work.progress); }
            /*if (this.document.work.finance.length) {
              this.document.work.finance.forEach((f, i) => {
                if (f.byYear && f.byYear.length) {
                  this.byYearChange(f.byYear, this.documentOld.work.finance[i].byYear.length);
                }
              });
            }*/
          }
        }

        const obj$ = this.programService.get(this.documentOld.work.objectId);
        const files$ = this.getFiles(this.documentOld.work.folderId);
        const requestFiles$ = this.getFiles(this.request.folderId);

        forkJoin([obj$, files$, requestFiles$]).subscribe(res => {
          this.obj = this.programService.getInstance(res[0]);
          this.files = res[1];
          this.requestFiles = res[2];
          if (this.request.files && this.request.files.length) {
            this.extraFiles = this.request.files.map(id => find(this.requestFiles, f => f.idFile === id));
          }

          this.updateProgressFiles();
          this.updateDocumentOldProgress();

          this.loading = LoadingStatus.SUCCESS;

        }, error => {
          this.loading = LoadingStatus.ERROR;
        });
      });
    });
  }

  getMosStatus(code: string): string {
    let r = '';
    switch (code) {
      case 'prior':
        r = 'Предварительная проработка вопроса';
        break;
      case 'psd':
      case 'torgi_psd':
      case 'contract_psd':
      case 'psd_done':
        r = 'Ведется проектирование';
        break;
      case 'smr':
      case 'torgi_smr':
      case 'contract_smr':
        r = 'Ведутся работы';
        break;
      case 'done':
        r = 'Работы завершены';
        break;
      case 'cancel':
        r = '';
        break;
    }
    return r;
  }

  makeDicts() {
    this.dicts.statuses = this.dicts['mr_program_WorkStatuses'].filter(i => i.code !== 'canceled').map(d => this.helper.convertToNsi(d));
    this.dicts.approveMunicipalStatus = this.dicts['mr_program_approveMunicipalStatus'].map(d => this.helper.convertToNsi(d));
    this.dicts.approveAg = this.dicts['mr_program_approveAgStatus'].map(d => this.helper.convertToNsi(d));
    this.dicts.responsibleOrganizations = this.dicts['mr_program_ResponsibleOrganizations'].map(d => this.helper.convertToNsi(d));
    this.dicts.years = this.helper.makeYearDictionary();
    this.dicts.budgetList = [ {code: true, name: 'Предусмотрено'}, {code: false, name: 'Не предусмотрено'} ];
    this.dicts.statePrograms = this.dicts['mr_program_StatePrograms'].map(d => this.helper.convertToNsi(d));
    this.dicts.stateCustomers = this.dicts['mr_program_StateCustomers'].map(d => this.helper.convertToNsi(d));
    this.dicts.supportTypes = this.dicts['mr_instruction_SupportTypes'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocTypes = this.dicts['mr_program_WorkDocTypes'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocTypesCanceled = this.dicts['mr_program_WorkDocTypesCanceled'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocStatuses = this.dicts['mr_program_approveDocStatus'].map(d => this.helper.convertToNsi(d));

    this.dicts.workExDocTypes = {
      done: this.dicts.workDocTypes,
      psd_done: this.dicts.workDocTypes,
      canceled: this.dicts.workDocTypesCanceled
    };
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  isNewProgress(): boolean {
    return this.document.work.status
      && (!this.documentOld.work.status || this.document.work.status.code !== this.documentOld.work.status.code);
  }

  getFiles(folderId: string = this.request.folderId): Observable<any[]> {
    return folderId
      ? this.helper.getFiles(folderId).pipe(map(files => files.map(file => {
        return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
          false, file.dateCreated, file.fileType, file.mimeType);
      })))
      : from(of([]));
  }

  onChangeWorkStatus() {
    const equalize = () => {
      if (this.documentOld.work.progress.length !== this.document.work.progress.length) {
        if (!this.documentOld.work.progress.length) {
          this.document.work.progress = [new RayonWorkProgress()];
        } else {
          this.document.work.progress.splice(this.documentOld.work.progress.length);
        }
      }
    };
    if (this.document.work.progress && this.document.work.progress.length && !this.isNewProgress()) {
      equalize();
      this.progress = last(this.document.work.progress);
    } else {
      this.progress = new RayonWorkProgress();
      this.progress.stage = this.document.work.status || null;
      equalize();
      this.document.work.progress.push(this.progress);
      const currentDate: Date = new Date();
      currentDate.setHours(0, 0, 0);
      if (compare(this.document.work.progress, this.documentOld.work.progress).length) {
        this.progress.executor = new RayonWorkUser();
        this.progress.executor.login = this.session.login() || '';
        this.progress.executor.fio = this.session.fullName() || '';
        this.progress.executor.date = currentDate;
        this.progress.executor.organization = this.session.departmentFullName() || '';
      }
      this.addProgressExDoc();
    }
    if (!this.progress.documentsExecution) {
      this.progress.documentsExecution = [];
    }

    if (this.document.work.status) {
      this.document.work.mos.status = this.getMosStatus(this.document.work.status.code);
    }
    this.updateProgressFiles();
  }

  updateProgressFiles() {
    if (!this.progressOld.documentsExecution) { this.progressOld.documentsExecution = []; }
    if (this.documentOld.work.progress.length !== this.document.work.progress.length) {
      this.progressFiles = last(this.document.work.progress).documentsExecution.map(i => {
        return compact(i.files.map(id => find(this.files, f => f.idFile === id)));
      });
      this.progressOldFiles = [];
    } else if (this.documentOld.work.progress.length) {
      this.progressFiles = this.document.work.progress[this.documentOld.work.progress.length - 1].documentsExecution.map(i => {
        return compact(i.files.map(id => find(this.files, f => f.idFile === id)));
      });
      this.progressOldFiles = this.progressOld.documentsExecution.map(i => {
        return compact(i.files.map(id => find(this.files, f => f.idFile === id)));
      });
    }
  }

  addProgressExDoc(context?: any) {
    if (!context) { context = last(this.document.work.progress); }
    context.documentsExecution.push(new RayonWorkProgressDocument());
    this.progressFiles.push([]);
  }

  removeProgressExDoc(index: number, context?: any) {
    if (!context) { context = last(this.document.work.progress); }
    context.documentsExecution.splice(index, 1);
    this.progressFiles.splice(index, 1);
  }

  isMandatory(fieldName: string, context: any): boolean {
    let r = false;
    switch (fieldName) {
      case 'executionFactStart':
        const oldStatus = this.documentOld.work && this.documentOld.work.status && this.documentOld.work.status.code
          ? this.documentOld.work.status.code
          : null;
        if (oldStatus && context.stage.code !== oldStatus) {
          // r = true;
          // r = context.stage.code !== 'smr';
          r = false;
        }
        break;
      case 'executionFactFinish':
      case 'documentsExecution':
        if (context.stage.code === 'done') { r = true; }
        break;
    }
    return r;
  }

  isDisabled(fieldName: string, context: any = this.documentOld.work) {
    let r = false;
    let field: any;
    switch (fieldName) {
      case 'organization':
      case 'executionStart':
      case 'executionFinish':
      case 'budget':
      case 'stateProgram':
      case 'stateCustomer':
      case 'general':
      case 'financed':
      case 'additional':
        r = !!this.obj.aipId;
        break;
      case 'year':
      case 'total':
        field = this.helper.getField(context, fieldName);
        r = !!this.obj.aipId || !field;
        break;
    }
    return r;
  }

  isShow(fieldName: string, context: any = this.documentOld.work): boolean {
    let r = true;
    let field: any;
    const needApproval: boolean = this.vars['ApprovalNeededVar'];
    const typeCode = this.helper.getField(this.documentOld.work, 'type.code');
    const kindCode = this.helper.getField(this.obj, 'object.kind.code');

    switch (fieldName) {
      case 'status':
        field = this.helper.getField(context, 'status.code');
        r = !!field;
        if (r) {
          r = needApproval
            ? field === 'done' || field === 'canceled'
            : field !== 'done' && field !== 'canceled';
        } else {
          r = !needApproval;
        }
        break;
      case 'approveMunicipalDep':
        field = this.helper.getField(context, 'approveMunicipalDep.code');
        if (r) { r = needApproval ? (!!field && field !== 'neprov') : (!field || field === 'neprov'); }
        if (r) { r = typeCode === 'accomplishment' && kindCode === 'YARD'; }
        break;
      case 'approveAg':
        field = this.helper.getField(context, 'approveAg.code');
        r = needApproval
          ? (!!field && field !== 'neprov')
          : (!field || field === 'neprov');
        break;
      case 'risk':
      case 'additional.required':
        r = needApproval;
        break;
      case 'budget':
        field = this.helper.getField(context, fieldName);
        r = needApproval && isBoolean(field);
        break;
      case 'byYear':
        field = this.helper.getField(context, fieldName);
        r = needApproval && !!field && !!field.length;
        break;
      case 'progress.bargainNumberEaist':
        field = this.helper.getField(context, 'status.code');
        r = field === 'torgi_psd' || field === 'torgi_smr';
        break;
      case 'progress.registryNumberEaist':
        field = this.helper.getField(context, 'status.code');
        r = field === 'contract_psd' || field === 'contract_smr';
        break;
      case 'progress.itemNumber':
        field = this.helper.getField(context, 'status.code');
        r = field === 'torgi_psd' || field === 'torgi_smr' || field === 'contract_psd' || field === 'contract_smr';
        break;
      case 'progress.executionFactStart':
        field = this.helper.getField(context, 'status.code');
        r = field === 'psd' || field === 'smr';
        break;
      case 'progress.executionFactFinish':
        field = this.helper.getField(context, 'status.code');
        r = field === 'psd_done' || field === 'done';
        break;
      case 'progress.documentsExecution':
        field = this.helper.getField(context, 'status.code');
        r = field === 'psd_done' || field === 'done' || field === 'canceled';
        break;
      case 'progress.comment':
        field = this.helper.getField(context, 'status .code');
        r = field === 'canceled';
        break;
      case 'progress.reason':
        // field = this.helper.getField(context, 'status.code');
        r = false;
        break;
      case 'workDocStatus':
        field = this.helper.getField(context, 'type.code');
        r = field === 'building' || field === 'reconstruction';
        break;
      case 'extraFiles':
        r = needApproval;
        break;
      default:
        field = this.helper.getField(context, fieldName);
        if (this.helper.isEmptyObject(field)) { field = null; }
        r = needApproval && !!field;
        break;
    }
    return r;
  }

  onRemoveFile(file) {
    const disabledIds: string[] = concat([], ...this.helper.getField(this.documentOld.work, 'progress.documentsExecution.files'));
    if (file && file.idFile && !some(disabledIds, d => d === file.idFile)) {
      this.fileResourceService.deleteFile(file.idFile);
    }
  }

  onRemoveExtraFile(file) {
    if (file && file.idFile) {
      this.fileResourceService.deleteFile(file.idFile);
    }
  }

  filesOnChange() {
    last(this.document.work.progress).documentsExecution.forEach((doc, index) => {
      doc.files = this.progressFiles[index].map(i => i.idFile);
    });
  }

  extraFilesOnChange() {
    this.request.files = this.extraFiles.map(i => i.idFile);
  }

  updateYears(type: string) {

  }

  isValid(): boolean {
    let r = true;
    const doc: any = this.document.work;
    const progress: any = last(doc.progress);
    if (r && this.isShow('status')) {              r = !!this.helper.getField(doc, 'status'); }
    if (r && this.isShow('approveMunicipalDep')) { r = !!this.helper.getField(doc, 'approveMunicipalDep'); }
    if (r && this.isShow('approveAg')) {           r = !!this.helper.getField(doc, 'approveAg'); }
    if (r && this.isShow('organization')) {        r = !!this.helper.getField(doc, 'organization'); }
    if (r && this.isShow('execution.start')) {     r = !!this.helper.getField(doc, 'execution.start'); }
    if (r && this.isShow('execution.finish')) {    r = !!this.helper.getField(doc, 'execution.finish'); }
    if (r && this.isShow('progress.executionFactStart', doc)
          && this.isMandatory('executionFactStart', progress)) {
      r = !!this.helper.getField(progress, 'executionFactStart');
    }
    if (r && this.isShow('progress.executionFactFinish', doc)
          && this.isMandatory('executionFactFinish', progress)) {
      r = !!this.helper.getField(progress, 'executionFactFinish');
    }
    if (r && this.isShow('progress.documentsExecution', doc)
          && this.isMandatory('documentsExecution', progress)) {
      const stageDocs: any[] = this.helper.getField(progress, 'documentsExecution');
      r = !!stageDocs && !!stageDocs.length && every(stageDocs, i => {
        return !!i.type && !!i.date && !!i['number'] && i.files.length;
      });
    }
    return r;
  }

  isEmptyAloneProgress() {
    let r = false;
    if (this.document.work.progress && this.document.work.progress.length === 1) {
      const progressItem: RayonWorkProgress = new RayonWorkProgress();
      if (this.document.work.status) { progressItem.stage = copy(this.document.work.status); }
      r = toJson(this.document.work.progress[0]) === toJson(progressItem);
    }
    return r;
  }

  prepareData(finish: boolean = false) {
    // if isNewProgress - item before the last must be equal old:
    if (this.documentOld.work.progress.length
      && this.document.work.progress.length
      && this.documentOld.work.progress.length < this.document.work.progress.length) {
      this.document.work.progress[this.document.work.progress.length - 2] = copy(last(this.documentOld.work.progress));
    }

    if (this.isEmptyAloneProgress()) {
      this.document.work.progress = [];
    }

    this.request.document.jsonNew = toJson(this.document.min());
    if (finish) { this.request.coordinator = null; }

    // this.prepareByYear();
  }

  toApprove() {
    this.validate = true;
    if (this.isValid()) {
      if (this.hasDocumentChanges()) {
        this.prepareData(true);
        this.blockUI.start();
        const diff: any[] = this.service.diff(this.copyRequest, copy(this.request));
        if (diff.length) {
          this.service.patch(this.request.documentId, diff).subscribe(res => {
            this.finishTask();
          });
        } else {
          this.finishTask();
        }
      } else {
        this.helper.error('Изменения отсутствуют! Отправка на согласование невозможна!');
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  toFinish() {
    this.validate = true;
    if (this.isValid()) {
      this.prepareData();
      this.blockUI.start();
      const diff: any[] = this.service.diff(this.copyRequest, copy(this.request));
      if (diff.length) {
        this.service.patch(this.request.documentId, diff).subscribe(res => {
          this.patchWorkAndFinishTask();
        }, error => {
          this.helper.error(error);
          this.blockUI.stop();
        });
      } else {
        this.patchWorkAndFinishTask();
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  patchWorkAndFinishTask() {
    const id: string = this.documentOld.work.documentId;
    this.workService.get(id).subscribe(res => {
      const old: RayonWork = this.workService.getInstance(res);
      const next: RayonWork = copy(this.document.work);
      // resolve progress list (#ISMR-3019)
      this.workService.resolveProgress(this.documentOld.work, next, old);
      next.mos.status = this.getMosStatus(next.status.code);
      const diff: any[] = this.workService.diff(old, next);
      if (diff.length) {
        this.workService.patch(id, diff, 'Заявка на изменения №' +
          this.request.documentNumber + ' от ' +
          formatDate(this.request.documentDate, 'dd.MM.yyyy', 'en')).subscribe(() => {
          this.finishTask();
        }, error => {
          this.helper.error(error);
          this.blockUI.stop();
        });
      } else {
        this.finishTask();
      }
    }, error => {
      this.helper.error(error);
      this.blockUI.stop();
    });
  }

  save() {
    const ok = () => {
      this.helper.success('Документ сохранен!');
      // this.state.reload(this.state.current);
      this.goToCard();
      this.blockUI.stop();
    };
    // this.validate = true;
    // if (this.isValid()) {
    this.prepareData();
    this.blockUI.start();
    const model = copy(this.request);
    const diff: any[] = this.service.diff(this.copyRequest, model);
    if (diff.length) {
      this.service.patch(this.request.documentId, diff).subscribe(ok);
    } else { ok(); }
    // } else {
    //   this.helper.error('Не заполнены обязательные поля!');
    // }
  }

  finishTask() {
    this.activityResourceService.finishTask(parseInt(this.taskId, 0), []).then(response => {
      this.helper.success('Задача успешно завершена!');
      this.blockUI.stop();
      // (<any>window).location = '/main/#/app/tasks';
      this.goToCard();
    }).catch(error => {
      console.error(error);
      this.helper.error('Ошибка при завершении задачи!');
      this.blockUI.stop();
    });
  }

  hasDocumentChanges(): boolean {
    const fields: string[] = ['/work/additional/required'];
    return !!this.workService.diff(copy(this.documentOld.work), copy(this.document.work)).filter((d: any) => {
      if (some(fields, f => d.path === f) && d.value === false && !this.helper.getField(this.documentOld, d.path)) {
        return false;
      }
      return true;
    }).length;
  }

  updateDocumentOldProgress() {
    this.documentOldProgress = copy(this.documentOld.work.progress);
    this.documentOldProgress.push(new RayonWorkProgress());
  }

  goToCard() {
    this.state.go('app.object.card.works', {
      id: this.document.work.objectId,
      mode: 'view',
      workId: this.document.work.documentId
    });
  }

  /*byYearChange(context: RayonWorkFinanceByYear[] = [], max?: number) {
    if (!context) { return; }
    context.slice().map((item, index) =>  !item.year && item.total === '' ? index : null)
      .reverse().forEach(i => {
        if (isNumber(i)) { context.splice(i, 1); }
    });
    if (max && context.length === max) { return; }
    const lastItem = last(context);
    if ((lastItem && (lastItem.year || lastItem.total !== '')) || !context.length) {
      context.push(new RayonWorkFinanceByYear());
    }
  }*/

  /*prepareByYear() {
    this.document.work.finance.forEach(f => {
      f.byYear = f.byYear.filter(b => b.year || b.total !== '');
    });
  }*/

  get twiceProgressIndexes(): number[] {
    const r: number[] = [this.documentOld.work.progress.length - 1];
    if (this.isNewProgress()) { r.push(this.document.work.progress.length - 1); }
    return r;
  }
}
