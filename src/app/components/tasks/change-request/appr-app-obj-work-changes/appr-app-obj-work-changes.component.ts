import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {fromJson, toJson} from '@uirouter/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {SessionStorage} from '@reinform-cdp/security';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {RayonHelperService} from '../../../../services/rayon-helper.service';
import {find, isArray} from 'lodash';
import {ChangeRequestService} from '../../../../services/change-request.service';
import {Observable, forkJoin, from, of} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {copy} from 'angular';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {ChangeRequest} from '../../../../models/change-request/ChangeRequest';
import {RayonProgramWrapper} from '../../../../models/program/RayonProgramWrapper';
import {StateService} from '@uirouter/core';
import {RayonProgramResourceService} from '../../../../services/rayon-program-resource.service';
import {ChangeRequestCoordinator} from '../../../../models/change-request/ChangeRequestCoordinator';
import {RayonProgram} from '../../../../models/program/RayonProgram';
import {formatDate} from '@angular/common';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {ExFileType} from '@reinform-cdp/widgets';

@Component({
  selector: 'task-appr-app-obj-work-changes',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './appr-app-obj-work-changes.component.html',
  styleUrls: [
    './appr-app-obj-work-changes.component.scss',
    '../change-request.scss'
  ]
})
export class TaskApprAppObjWorkChangesComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption = '';
  taskId = '';
  dicts: any = {};
  saving = false;
  finishing = false;
  validate = false;

  request: ChangeRequest;
  copyRequest: ChangeRequest;
  documentOld: RayonProgramWrapper = new RayonProgramWrapper();
  document: RayonProgramWrapper = new RayonProgramWrapper();

  files: any[] = [];
  extraFiles: any[] = [];

  @BlockUI('block-ui-task') blockUI: NgBlockUI;

  constructor(public helper: RayonHelperService,
              private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private session: SessionStorage,
              private solrMediator: SolrMediatorService,
              private service: ChangeRequestService,
              private programService: RayonProgramResourceService,
              private state: StateService,
              private nsi: NsiResourceService,
              private fileResourceService: FileResourceService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const requestId = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      });

      const dicts$ = this.nsi.getDictsFromCache([
        'Prefect',
        'District',
        'mr_program_ObjectForms',
        'mr_program_ObjectIndustries',
        'mr_program_ObjectCategories',
        'mr_program_ResponsibleOrganizations',
        'mr_program_OwnershipForms',
        'mr_program_dataSource',
        'mr_program_reasonForRejecting']);
      const request$ = this.service.get((<any>requestId).value);

      forkJoin([dicts$, request$]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = response[1];
        this.copyRequest = copy(this.request);

        if (!this.request.coordinator) { this.request.coordinator = new ChangeRequestCoordinator(); }

        this.makeDicts();

        if (this.request.document) {
          if (this.request.document.jsonOld) { this.documentOld.build(JSON.parse(this.request.document.jsonOld)); }
          if (this.request.document.jsonNew) { this.document.build(JSON.parse(this.request.document.jsonNew)); }
        }
        this.getAddressList();
        this.getOldAddressList();

        this.getFiles().subscribe(res => {
          this.files = res;
          if (this.request.files && this.request.files.length) {
            this.extraFiles = this.request.files.map(id => find(this.files, f => f.idFile === id));
          }
        });

        this.loading = LoadingStatus.SUCCESS;
      });
    });
  }

  makeDicts() {
    this.dicts.prefects = this.dicts.Prefect;
    this.dicts.districts = this.dicts.District;
    this.dicts.industries = this.dicts['mr_program_ObjectIndustries'];
    this.dicts.kinds = this.dicts['mr_program_ObjectCategories'];
    this.dicts.organizations = this.dicts['mr_program_ResponsibleOrganizations'];
    this.dicts.rejectReason = this.dicts['mr_program_reasonForRejecting'].map(d => this.helper.convertToNsi(d));
  }

  getFiles(): Observable<any[]> {
    return this.request.folderId
      ? this.helper.getFiles(this.request.folderId).pipe(map(files => files.map(file => {
        return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
          false, file.dateCreated, file.fileType, file.mimeType);
      })))
      : from(of([]));
  }

  getValue(context, path) {
    let field = this.helper.getField(context, path);
    if (!field) { field = null; }
    if (isArray(field) && !field.length) { field = null; }
    return field;
  }

  isShow(fieldName: string): boolean {
    let r = true;
    const getField = (path: string) => {
      return [
        toJson(this.getValue(this.documentOld.obj, path)),
        toJson(this.getValue(this.document.obj, path))
      ];
    };
    let field: any[] = [];
    switch (fieldName) {
      case 'prefect':
        field = getField('address.prefect');
        break;
      case 'district':
        field = getField('address.district');
        break;
      case 'objectForm':
        field = getField('objectForm.code');
        break;
      case 'addressId':
        field = getField('address.addressId');
        break;
      case 'address':
        field = getField('address.address');
        break;
      case 'industry':
        field = getField('object.industry.code');
        break;
      case 'kind':
        field = getField('object.kind.code');
        break;
      case 'objectName':
        field = getField('object.name');
        break;
      case 'objectDescription':
        field = getField('object.description');
        break;
      case 'organization':
        field = getField('responsible.organization.code');
        break;
      case 'ownership':
        field = getField('object.ownership.code');
        break;
      case 'cadastralNumber':
        field = getField('parameterZu.cadastralNumber');
        break;
      case 'problem':
        field = getField('problem');
        break;
      case 'attraction':
        field = getField('attraction');
        break;
      case 'risk':
        field = getField('risk');
        break;
      case 'comment':
        field = getField('comment');
        break;
    }
    if (field.length) { r = field[0] !== field[1]; }
    return r;
  }

  getAddressList() {
    const addressId: string = this.helper.getField(this.document, 'obj.address.addressId');
    if (addressId) {
      this.solrMediator.getByIds([{ids: [addressId], type: this.solrMediator.types.address}]).subscribe(res => {
        if (res && res[0] && res[0].docs) {
          this.dicts.addressList = res[0].docs.map((d: any) => {
            return {
              code: d.documentId,
              name: d.fullAddress
            };
          });
        }
      }, error => {
        this.dicts.addressList = [];
      });
    } else {
      this.dicts.addressList = [];
    }
  }

  getOldAddressList() {
    const addressId: string = this.helper.getField(this.documentOld, 'obj.address.addressId');
    if (addressId) {
      this.solrMediator.getByIds([{ids: [addressId], type: this.solrMediator.types.address}]).subscribe(res => {
        if (res && res[0] && res[0].docs) {
          this.dicts.addressOldList = res[0].docs.map((d: any) => {
            return {
              code: d.documentId,
              name: d.fullAddress
            };
          });
        }
      }, error => {
        this.dicts.addressOldList = [];
      });
    } else {
      this.dicts.addressOldList = [];
    }
  }

  prepareData() {
    this.request.coordinator.login = this.session.login();
    this.request.coordinator.fio = this.session.fullName();
    if (!!this.request.coordinator.comment || !!this.request.coordinator.reasonForRejecting) {
      this.request.coordinator.date = new Date();
    }
  }

  isValid(rework: boolean = false, reject: boolean = false): boolean {
    let r = true;
    if (rework) {
      r = !!this.request.coordinator.comment;
      if (!r) { this.helper.error('Поле "Комментарий" обязательно для заполнения!'); }
    }
    if (reject) {
      r = !!this.request.coordinator.reasonForRejecting;
      if (!r) { this.helper.error('Поле "Причина отклонения" обязательно для заполнения!'); }
    }
    return r;
  }

  makeVars(rework: boolean = false, reject: boolean = false): ITaskVariable[] {
    const r: ITaskVariable[] = [];
    r.push({name: 'IsRework', value: rework});
    if (!rework) { r.push({name: 'IsReject', value: reject}); }
    return r;
  }

  finishTask(vars: ITaskVariable[] = this.makeVars()) {
    this.activityResourceService.finishTask(parseInt(this.taskId, 0), vars).then(response => {
      this.helper.success('Задача успешно завершена!');
      this.blockUI.stop();
      (<any>window).location = '/main/#/app/tasks';
    }).catch(error => {
      console.error(error);
      this.helper.error('Ошибка при завершении задачи!');
      this.blockUI.stop();
    });
  }

  finish(rework: boolean = false, reject: boolean = false) {
    let diff: any[] = [];
    const runFinishing = () => {
      if (rework || reject) {
        this.finishTask(this.makeVars(rework, reject));
      } else {
        this.updateRealDocumentAndFinishTask();
      }
    };
    this.validate = true;
    if (this.isValid(rework, reject)) {
      this.prepareData();
      this.blockUI.start();
      diff = this.service.diff(this.copyRequest, copy(this.request));
      if (diff.length) {
        this.service.patch(this.request.documentId, diff).subscribe(() => runFinishing());
      } else { runFinishing(); }
    }
  }

  updateRealDocumentAndFinishTask() {
    const id: string = this.documentOld.obj.documentId;
    this.document.obj.correctionReason = 'Изменения осуществлены на основании заявки №'
      + this.request.documentNumber + ' в рамках бизнес-процесса';
    this.programService.get(id).subscribe(res => {
      const old: RayonProgram = this.programService.getInstance(res);
      const next: RayonProgram = copy(this.document.obj);
      const diff: any[] = this.programService.diff(old, next);
      if (diff.length) {
        this.programService.patch(id, diff, 'Заявка на изменения №' +
          this.request.documentNumber + ' от ' +
          formatDate(this.request.documentDate, 'dd.MM.yyyy', 'en')).subscribe(() => {
          this.finishTask();
        }, error => {
          this.helper.error(error);
          this.blockUI.stop();
        });
      } else {
        this.finishTask();
      }
    }, error => {
      this.helper.error(error);
      this.blockUI.stop();
    });
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  save() {
    const ok = () => {
      this.helper.success('Документ сохранен!');
      this.blockUI.stop();
      (<any>window).location = '/main/#/app/tasks';
    };
    this.prepareData();
    this.blockUI.start();
    const diff: any[] = this.service.diff(this.copyRequest, copy(this.request));
    if (diff.length) {
      this.service.patch(this.request.documentId, diff).subscribe(() => ok());
    } else { ok(); }
  }
}
