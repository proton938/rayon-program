import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {fromJson, toJson} from '@uirouter/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {SessionStorage} from '@reinform-cdp/security';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {RayonHelperService} from '../../../../services/rayon-helper.service';
import {find, compact, some, isNumber, last} from 'lodash';
import {ChangeRequestService} from '../../../../services/change-request.service';
import {Observable, forkJoin, from, of} from 'rxjs';
import {copy} from 'angular';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {ChangeRequest} from '../../../../models/change-request/ChangeRequest';
import {StateService} from '@uirouter/core';
import {RayonProgramResourceService} from '../../../../services/rayon-program-resource.service';
import {RayonProgram} from '../../../../models/program/RayonProgram';
import {RayonWorkWrapper} from '../../../../models/work/RayonWorkWrapper';
import {ExFileType} from '@reinform-cdp/widgets';
import {RayonWorkProgress} from '../../../../models/work/RayonWorkProgress';
import {RayonWorkProgressDocument} from '../../../../models/work/RayonWorkProgressDocument';
import {map} from 'rxjs/internal/operators';

@Component({
  selector: 'task-review-request-work-results',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './review-request-work-results.component.html',
  styleUrls: [
    './review-request-work-results.component.scss',
    '../change-request.scss'
  ]
})
export class TaskReviewRequestWorkResultsComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption = '';
  taskId = '';
  dicts: any = {};
  saving = false;
  finishing = false;

  request: ChangeRequest;
  documentOld: RayonWorkWrapper = new RayonWorkWrapper();
  document: RayonWorkWrapper = new RayonWorkWrapper();
  progress: RayonWorkProgress = new RayonWorkProgress();
  emptyProgress: RayonWorkProgress = new RayonWorkProgress();
  progressOld: RayonWorkProgress = new RayonWorkProgress();
  progressFiles: any[] = [];
  progressOldFiles: any[] = [];
  files: any[] = [];

  documentOldProgress: RayonWorkProgress[] = [];
  emptyDocumentsExecution: RayonWorkProgressDocument[] = [new RayonWorkProgressDocument()];

  @BlockUI('block-ui-task') blockUI: NgBlockUI;

  constructor(public helper: RayonHelperService,
              private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private session: SessionStorage,
              private solrMediator: SolrMediatorService,
              private service: ChangeRequestService,
              private programService: RayonProgramResourceService,
              private state: StateService,
              private nsi: NsiResourceService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const requestId = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      });

      const dicts$ = this.nsi.getDictsFromCache([
        'mr_program_WorkStatuses',
        'mr_program_approveMunicipalStatus',
        'mr_program_approveAgStatus',
        'mr_program_ResponsibleOrganizations',
        'mr_program_StatePrograms',
        'mr_program_StateCustomers',
        'mr_instruction_SupportTypes',
        'mr_program_reasonForRejecting',
        'mr_program_WorkDocTypes',
        'mr_program_approveDocStatus',
        'mr_program_WorkDocTypesCanceled'
      ]);
      const request$ = this.service.get((<any>requestId).value);

      forkJoin([dicts$, request$]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = response[1];

        if (this.request.document) {
          if (this.request.document.jsonOld) {
            this.documentOld.build(JSON.parse(this.request.document.jsonOld));
            if (this.documentOld.work.progress.length) { this.progressOld = last(this.documentOld.work.progress); }
          }
          if (this.request.document.jsonNew) {
            this.document.build(JSON.parse(this.request.document.jsonNew));
            if (this.document.work.progress.length) { this.progress = last(this.document.work.progress); }
          }
        }

        this.makeDicts();

        this.getFiles().subscribe(res => {
          this.files = res;
          this.updateProgressFiles();
          this.updateDocumentOldProgress();
          this.loading = LoadingStatus.SUCCESS;
        }, error => {
          this.loading = LoadingStatus.ERROR;
        });
      });
    });
  }

  updateProgressFiles() {
    if (!this.progressOld.documentsExecution) { this.progressOld.documentsExecution = []; }
    if (this.documentOld.work.progress.length !== this.document.work.progress.length) {
      this.progressFiles = last(this.document.work.progress).documentsExecution.map(i => {
        return i.files.map(id => find(this.files, f => f.idFile === id));
      });
      this.progressOldFiles = [];
    } else if (this.documentOld.work.progress.length) {
      this.progressFiles = this.document.work.progress[this.documentOld.work.progress.length - 1].documentsExecution.map(i => {
        return i.files.map(id => find(this.files, f => f.idFile === id));
      });
      this.progressOldFiles = this.progressOld.documentsExecution.map(i => {
        return i.files.map(id => find(this.files, f => f.idFile === id));
      });
    }
  }

  updateDocumentOldProgress() {
    this.documentOldProgress = copy(this.documentOld.work.progress);
    this.documentOldProgress.push(new RayonWorkProgress());
  }

  isNewProgress(): boolean {
    return this.document.work.status
      && (!this.documentOld.work.status || this.document.work.status.code !== this.documentOld.work.status.code);
  }

  makeDicts() {
    this.dicts.statuses = this.dicts['mr_program_WorkStatuses'].map(d => this.helper.convertToNsi(d));
    this.dicts.approveMunicipalStatus = this.dicts['mr_program_approveMunicipalStatus'].map(d => this.helper.convertToNsi(d));
    this.dicts.approveAg = this.dicts['mr_program_approveAgStatus'].map(d => this.helper.convertToNsi(d));
    this.dicts.responsibleOrganizations = this.dicts['mr_program_ResponsibleOrganizations'].map(d => this.helper.convertToNsi(d));
    this.dicts.years = this.helper.makeYearDictionary();
    this.dicts.budgetList = [
      {code: true, name: 'Предусмотрено'},
      {code: false, name: 'Не предусмотрено'}
    ];
    this.dicts.statePrograms = this.dicts['mr_program_StatePrograms']
      .map(d => this.helper.convertToNsi(d)).map(d => this.helper.convertToNsi(d));
    this.dicts.stateCustomers = this.dicts['mr_program_StateCustomers'].map(d => this.helper.convertToNsi(d));
    this.dicts.supportTypes = this.dicts['mr_instruction_SupportTypes'].map(d => this.helper.convertToNsi(d));
    this.dicts.rejectReason = this.dicts['mr_program_reasonForRejecting'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocTypes = this.dicts['mr_program_WorkDocTypes'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocTypesCanceled = this.dicts['mr_program_WorkDocTypesCanceled'].map(d => this.helper.convertToNsi(d));
    this.dicts.workDocStatuses = this.dicts['mr_program_approveDocStatus'].map(d => this.helper.convertToNsi(d));

    this.dicts.workExDocTypes = {
      done: this.dicts.workDocTypes,
      psd_done: this.dicts.workDocTypes,
      canceled: this.dicts.workDocTypesCanceled
    };
  }

  getFiles(): Observable<any[]> {
    return this.documentOld.work.folderId
      ? this.helper.getFiles(this.documentOld.work.folderId).pipe(map(files => files.map(file => {
        return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
          false, file.dateCreated, file.fileType, file.mimeType);
      })))
      : from(of([]));
  }

  getField(context, path): any {
    let r = this.helper.getField(context, path);
    if (r && r.join) { r = compact(r); }
    return r;
  }

  isShow(fieldName: string, index?: number, subIndex?: number): boolean {
    let r = true;
    const getField = (path: string) => {
      return [
        this.getField(this.documentOld.work, path),
        this.getField(this.document.work, path)
      ];
    };
    let field: any;
    switch (fieldName) {
      case 'progress.documentsExecution.status':
        field = getField('progress').map(f => {
          let r = this.helper.getField(f[index], 'documentsExecution.status');
          if (r) { r = r[subIndex]; }
          return r;
        });
        if (!field[0] && !field[1]) { field = [0, 0]; }
        index = null;
        break;
      case 'progress.documentsExecution':
        field = getField(fieldName);
        if (!field[0][index]) { field[0][index] = []; }
        break;
      default:
        field = getField(fieldName);
        break;
    }
    if (field && isNumber(index) && field.length) {
      r = toJson(field[0][index]) !== toJson(field[1][index]);
    } else if (field.length) { r = toJson(field[0]) !== toJson(field[1]); }
    return r;
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  finishTask() {
    this.activityResourceService.finishTask(parseInt(this.taskId, 0), []).then(response => {
      this.helper.success('Задача успешно завершена!');
      this.blockUI.stop();
      (<any>window).location = '/main/#/app/tasks';
    }).catch(error => {
      console.error(error);
      this.helper.error('Ошибка при завершении задачи!');
      this.blockUI.stop();
    });
  }

  finish() {
    this.blockUI.start();
    this.finishTask();
  }
}
