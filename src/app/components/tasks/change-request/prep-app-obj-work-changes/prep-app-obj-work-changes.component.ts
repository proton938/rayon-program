import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus, ExFileType} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {SessionStorage} from '@reinform-cdp/security';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {RayonHelperService} from '../../../../services/rayon-helper.service';
import {find, compact, some, isNull, isUndefined, last} from 'lodash';
import {ChangeRequestService} from '../../../../services/change-request.service';
import {copy} from 'angular';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {ChangeRequest} from '../../../../models/change-request/ChangeRequest';
import {RayonProgramWrapper} from '../../../../models/program/RayonProgramWrapper';
import {Observable, Subject, Subscription, forkJoin, from, of} from 'rxjs';
import {debounceTime, delay, flatMap, map} from 'rxjs/internal/operators';
import {StateService} from '@uirouter/core';
import {RayonProgramResourceService} from '../../../../services/rayon-program-resource.service';
import {ChangeRequestCoordinator} from '../../../../models/change-request/ChangeRequestCoordinator';
import {RayonBtiService} from '../../../../services/rayon-bti.service';
import {FileResourceService} from '@reinform-cdp/file-resource';

@Component({
  selector: 'task-prep-app-obj-work-changes',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './prep-app-obj-work-changes.component.html',
  styleUrls: [
    './prep-app-obj-work-changes.component.scss',
    '../change-request.scss'
  ]
})
export class TaskPrepAppObjWorkChangesComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption = '';
  taskId = '';
  dicts: any = {};
  saving = false;
  finishing = false;
  validate = false;

  selected: any = {
    coordinatorComment: '',
    disabledInfoText: '',
    cadastralNumbers: []
  };

  request: ChangeRequest;
  copyRequest: ChangeRequest;
  documentOld: RayonProgramWrapper = new RayonProgramWrapper();
  document: RayonProgramWrapper = new RayonProgramWrapper();

  files: any[] = [];
  extraFiles: any[] = [];

  isAddressLoading = false;
  private currentAddressFilter: string;
  public addressKeyUp = new Subject<string>();
  private addressSubscription: Subscription;

  @BlockUI('block-ui-task') blockUI: NgBlockUI;

  constructor(public helper: RayonHelperService,
              private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private session: SessionStorage,
              private solrMediator: SolrMediatorService,
              private service: ChangeRequestService,
              private programService: RayonProgramResourceService,
              private state: StateService,
              private nsi: NsiResourceService,
              private bti: RayonBtiService,
              private fileResourceService: FileResourceService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const requestId = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      });

      const dicts$ = this.nsi.getDictsFromCache([
        'Prefect',
        'District',
        'mr_program_ObjectForms',
        'mr_program_ObjectIndustries',
        'mr_program_ObjectCategories',
        'mr_program_ResponsibleOrganizations',
        'mr_program_OwnershipForms',
        'mr_program_dataSource']);
      const request$ = this.service.get((<any>requestId).value);

      forkJoin([dicts$, request$]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = response[1];
        this.copyRequest = copy(this.request);

        this.makeDicts();

        if (this.request.document) {
          if (this.request.document.jsonOld) { this.documentOld.build(JSON.parse(this.request.document.jsonOld)); }
          if (this.request.document.jsonNew) { this.document.build(JSON.parse(this.request.document.jsonNew)); }
          this.cadAction('init');
        }

        this.selected.disabledInfoText = this.getDisabledFieldsInfo();
        this.getOldAddressList();
        if (this.document.obj.address.addressId) { this.getAvailableAddressList(); }
        this.updateDistrictList();

        this.updateCategoriList();

        this.addressSubscription = this.addressKeyUp
          .pipe(
            map((event: any) => event.target.value),
            debounceTime(100),
            flatMap(search => of(search).pipe(delay(100)))
          ).subscribe(value => {
            if (value !== this.currentAddressFilter) { this.getAvailableAddressList(value); }
            this.currentAddressFilter = value;
          });

        this.getFiles().subscribe(res => {
          this.files = res;
          if (this.request.files && this.request.files.length) {
            this.extraFiles = this.request.files.map(id => find(this.files, f => f.idFile === id));
          }
        });

        this.loading = LoadingStatus.SUCCESS;
      });
    });
  }

  makeDicts() {
    this.dicts.prefects = this.dicts.Prefect.map(d => this.helper.convertToNsi(d));
    this.dicts.districts = this.dicts.District.map(d => this.helper.convertToNsi(d));
    this.dicts.objectForms = this.dicts.mr_program_ObjectForms.map(d => this.helper.convertToNsi(d));
    this.dicts.industries = this.dicts['mr_program_ObjectIndustries'].map(d => this.helper.convertToNsi(d));
    this.dicts.kinds = this.dicts['mr_program_ObjectCategories'].map(d => this.helper.convertToNsi(d));
    this.dicts.organizations = this.dicts['mr_program_ResponsibleOrganizations'].map(d => this.helper.convertToNsi(d));
  }

  getDisabledFieldsInfo(): string {
    let r = '';
    const odopmId: string = this.helper.getField(this.documentOld, 'obj.odopm.odopmId');
    const odsId: string = this.helper.getField(this.documentOld, 'obj.odsId');
    const odsOdhId: string = this.helper.getField(this.documentOld, 'obj.odsOdhId');
    const aipId: string = this.helper.getField(this.documentOld, 'obj.aipId');
    if (odopmId) {
      r = 'Заблокированные поля заполняются в автоматическом режиме на основании сведений, загруженных ' +
        'из Единого хранилища данных ЕГАС ОДОПМ. В случае необходимости изменения данных следует произвести ' +
        'соответствующую корректировку в ЕХД ЕГАС ОДОПМ';
    } else if (odsId || odsOdhId) {
      r = 'Заблокированные поля заполняются в автоматическом режиме на основании сведений, ' +
        'загруженных из АСУ ОДС. В случае необходимости изменения данных следует произвести ' +
        'соответствующую корректировку в АСУ ОДС';
    } else if (aipId) {
      r = 'Заблокированные поля заполняются в автоматическом режиме на основании сведений, ' +
        'загруженных из АИС «Строительные инвестиции». В случае необходимости изменения данных ' +
        'следует произвести соответствующую корректировку в АИС «Строительные инвестиции»';
    }
    return r;
  }

  getFiles(): Observable<any[]> {
    return this.request.folderId
      ? this.helper.getFiles(this.request.folderId).pipe(map(files => files.map(file => {
        return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
          false, file.dateCreated, file.fileType, file.mimeType);
      })))
      : from(of([]));
  }

  onRemoveFile(file) {
    if (file && file.idFile) {
      this.fileResourceService.deleteFile(file.idFile);
    }
  }

  filesOnChange() {
    this.request.files = this.extraFiles.map(i => i.idFile);
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  isShow(fieldName: string, context: any = this.documentOld.obj): boolean {
    let r = true;
    let field: any;
    switch (fieldName) {
      case 'prefect':
        field = this.helper.getField(context, 'address.prefect');
        r = !!field && !!field.length;
        break;
      case 'district':
        field = this.helper.getField(context, 'address.district');
        r = !!field && !!field.length;
        break;
      case 'objectForm':
        r = !!this.helper.getField(context, 'objectForm.code');
        break;
      case 'addressId':
        r = !!this.helper.getField(context, 'address.addressId')
          || !!this.helper.getField(context, 'address.address');
        break;
      case 'address':
        r = !this.helper.getField(context, 'address.addressId')
          && !!this.helper.getField(context, 'address.address');
        break;
      case 'industry':
        r = !!this.helper.getField(context, 'object.industry.code');
        break;
      case 'kind':
        r = !!this.helper.getField(context, 'object.kind.code');
        break;
      case 'objectName':
        r = !!this.helper.getField(context, 'object.name');
        break;
      case 'objectDescription':
        r = !!this.helper.getField(context, 'object.description');
        break;
      case 'organization':
        r = !!this.helper.getField(context, 'responsible.organization.code');
        break;
      case 'ownership':
        r = !!this.helper.getField(context, 'object.ownership.code');
        break;
      case 'cadastralNumber':
        field = this.helper.getField(context, 'parameterZu.cadastralNumber');
        r = !!field && !!field.length;
        break;
      case 'comment':
        r = !!this.helper.getField(context, 'comment');
        break;
    }
    return r;
  }

  isDisabled(fieldCode?: string): boolean {
    let r: boolean = !!this.selected.disabledInfoText;
    const aipId: string = this.helper.getField(this.documentOld, 'obj.aipId');

    switch (fieldCode) {
      case 'kind':
        if (r && aipId) { r = false; }
        break;
    }

    return r;
  }

  onChangeAddress() {
    if (this.document.obj.address.addressId) {
      this.document.obj.address.address = find(this.dicts.addressList, i => i.code === this.document.obj.address.addressId).name;
    } else { this.document.obj.address.address = ''; }

    this.getBuildingInfo().subscribe(buildingInfo => {
      this.setObjectParams(buildingInfo);
    });
  }

  clearAddressList() {
    this.dicts.addressList = [];
  }

  onChangePrefect() {
    this.updateDistrictList();
  }

  onChangeObjectForm() {
    const field = this.helper.getField(this.document.obj, 'objectForm.code');
    if (field === 'linear') { this.setObjectParams(); }
  }

  getBuildingInfo(): Observable<any> {
    return Observable.create(observer => {
      if (this.document.obj.itemType && this.document.obj.itemType.code === 'object' && this.document.obj.objectForm.code === 'building'
        && this.document.obj.address.addressId) {
        this.solrMediator.query({page: 0, pageSize: 1, types: [this.solrMediator.types.address],
          query: 'documentId:' + this.document.obj.address.addressId}).subscribe((addRes: any) => {
          if (addRes && addRes.docs && addRes.docs.length) {
            const unoms: string[] = addRes.docs.map(i => i.unomAddress);
            this.bti.findBuildings(unoms).subscribe((res: any) => {
              if (res && res.length === 1) {
                const buildingInfo: any = res[0].building || null;
                observer.next(buildingInfo);
              } else {
                observer.next(null);
              }
              observer.complete();
            });
          } else {
            observer.next(null);
            observer.complete();
          }
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next(null);
        observer.complete();
      }
    });
  }

  setObjectParams(params: any = null) {
    const check = p => params && !isNull(params[p]) && !isUndefined(params[p]);
    this.document.obj.object.areaObject = check('areaObject') ? params.areaObject : '';
    this.document.obj.parameterObject.constructionYear = check('constructionYear') ? params.constructionYear : '';
    this.document.obj.parameterObject.use = check('use') ? params.use : '';
    this.document.obj.parameterObject.floor = check('floor') ? params.floor : '';
    this.document.obj.parameterObject.wallMaterial = check('wallMaterial') ? params.wallMaterial : '';
    this.document.obj.parameterObject.wearRate = check('wearRate') ? params.wearRate : '';
  }

  updateDistrictList() {
    this.dicts.districtsAvailables = [];
    if (this.document.obj.address.prefect && this.document.obj.address.prefect.length) {
      this.dicts.districtsAvailables = this.dicts.District
        .filter(d => this.document.obj.address.prefect.filter(p => p.code === d.perfectId[0].code).length)
        .map(d => this.helper.convertToNsi(d));
      if (this.document.obj.address.district && this.document.obj.address.district.length) {
        this.document.obj.address.district = this.document.obj.address.district
          .filter(i => this.dicts.districtsAvailables.filter(f => f.code === i.code).length);
      }
    } else {
      this.dicts.districtsAvailables = this.dicts.districts;
      this.document.obj.address.district = [];
    }
  }

  addressSearchFn(term: string, item: any) { // Чтобы не фильтровал уже отфильтрованные данные с сервера
    return !!item;
  }

  getAvailableAddressList(searchString?: string) {
    let filter = '';
    let result: any[] = [];
    if ((searchString && searchString.length >= 3) || (!searchString && this.document.obj.address.addressId)) {
      this.isAddressLoading = true;
      if (this.document.obj.address.prefect && this.document.obj.address.prefect.length) {
        filter += (filter ? ' AND ' : '')
          + '(prefectCodeAddress:' + this.document.obj.address.prefect.map(i => i.code).join(' OR prefectCodeAddress:') + ')';
      }
      if (this.document.obj.address.district && this.document.obj.address.district.length) {
        filter += (filter ? ' AND ' : '')
          + '(districtCodeAddress:' + this.document.obj.address.district.map(i => i.code).join(' OR districtCodeAddress:') + ')';
      }
      if (!searchString && this.document.obj.address.addressId) {
        filter += (filter ? ' AND ' : '') + '(documentId:' + this.document.obj.address.addressId + ')';
      } else {
        filter += (filter ? ' AND ' : '') + '(fullAddress:*' + searchString.replace(/\s+/g, '* AND fullAddress:*') + '*)';
      }
      filter += (filter ? ' AND NOT ' : '') + '(isDeletedAddress:true)';
      this.solrMediator.query({page: 0, pageSize: 100, types: [this.solrMediator.types.address], query: filter}).subscribe(response => {
        if (response && response.docs) {
          this.dicts['addressList'] = result = response.docs.map((d: any) => {
            return {
              code: d.documentId,
              name: d.fullAddress
            };
          });
          this.isAddressLoading = false;
        } else {
          this.dicts['addressList'] = result = [];
          this.isAddressLoading = false;
        }
      });
    }
  }

  getOldAddressList() {
    const addressId: string = this.helper.getField(this.documentOld, 'obj.address.addressId');
    if (addressId) {
      this.solrMediator.getByIds([{ids: [addressId], type: this.solrMediator.types.address}]).subscribe(res => {
        if (res && res[0] && res[0].docs) {
          this.dicts.addressOldList = res[0].docs.map((d: any) => {
            return {
              code: d.documentId,
              name: d.fullAddress
            };
          });
        }
      }, error => {
        this.dicts.addressOldList = [];
      });
    } else {
      this.dicts.addressOldList = [];
    }
  }

  updateCategoriList() {
    this.dicts.objectCategories = this.dicts['mr_program_ObjectCategories']
      .filter(this.filterObjectCategories.bind(this))
      .map(d => this.helper.convertToNsi(d));
    if (this.document.obj.object.kind
      && !some(this.dicts.objectCategories.map(i => i.code), i => i === this.document.obj.object.kind.code)) {
      this.document.obj.object.kind = null;
    }
  }

  filterObjectCategories(item) {
    return item.industry && this.document.obj.object.industry
      ? some(item.industry, i => i.code === this.document.obj.object.industry.code)
      : false;
  }

  cadAction(method: string = '', item: any = '') {
    switch (method) {
      case 'init':
        const cads: string[] = this.helper.getField(this.document, 'obj.parameterZu.cadastralNumber') || [];
        this.selected.cadastralNumbers = cads.map(i => {
          return {cad: i};
        });
        if (!!this.selected.cadastralNumbers[this.selected.cadastralNumbers.length - 1]
          || !this.selected.cadastralNumbers.length) { this.cadAction('add'); }
        break;
      case 'add': // Добавление
        this.selected.cadastralNumbers.push({cad: item});
        this.cadAction('sync');
        break;
      case 'sync': // Синхронизация
        this.document.obj.parameterZu.cadastralNumber = compact(this.selected.cadastralNumbers.map(i => i.cad));
        break;
      default: // действия в результате события input
        this.selected.cadastralNumbers = this.selected.cadastralNumbers.filter(i => i.cad !== '');
        if (!!this.selected.cadastralNumbers[this.selected.cadastralNumbers.length - 1]
          || !this.selected.cadastralNumbers.length) { this.cadAction('add'); }
        this.cadAction('sync');
        break;
    }
  }

  prepareData(finish: boolean = false) {
    this.request.document.jsonNew = JSON.stringify(this.document);
    if (finish) { this.request.coordinator = null; }
  }

  isValid(): boolean {
    let r = true;
    const doc: any = this.document.obj;
    if (r && this.isShow('prefect')) {      r = this.isShow('prefect', doc); }
    if (r && this.isShow('district')) {     r = this.isShow('district', doc); }
    if (r && this.isShow('objectForm')) {   r = this.isShow('objectForm', doc); }
    if (r && this.isShow('address') && !doc.address.addressId) {
      r = this.isShow('address', doc);
    }
    if (r && this.isShow('industry')) {     r = this.isShow('industry', doc); }
    if (r && this.isShow('kind')) {         r = this.isShow('kind', doc); }
    if (r && this.isShow('objectName')) {   r = this.isShow('objectName', doc); }
    if (r && this.isShow('organization')) { r = this.isShow('organization', doc); }
    return r;
  }

  save() {
    const ok = () => {
      this.helper.success('Документ сохранен!');
      // this.state.reload(this.state.current);
      this.goToCard();
      this.blockUI.stop();
    };
    this.prepareData();
    this.blockUI.start();
    const model = copy(this.request);
    const diff: any[] = this.service.diff(this.copyRequest, model);
    if (diff.length) {
      this.service.patch(this.request.documentId, diff).subscribe(ok);
    } else { ok(); }
  }

  finishTask() {
    this.activityResourceService.finishTask(parseInt(this.taskId, 0), []).then(response => {
      this.helper.success('Задача успешно завершена!');
      this.blockUI.stop();
      // (<any>window).location = '/main/#/app/tasks';
      this.goToCard();
    }).catch(error => {
      console.error(error);
      this.helper.error('Ошибка при завершении задачи!');
      this.blockUI.stop();
    });
  }

  finish() {
    this.validate = true;
    if (this.isValid()) {
      if (this.hasDocumentChanges()) {
        this.prepareData(true);
        this.blockUI.start();
        const diff: any[] = this.service.diff(this.copyRequest, copy(this.request));
        if (diff.length) {
          this.service.patch(this.request.documentId, diff).subscribe(res => {
            this.finishTask();
          });
        } else {
          this.finishTask();
        }
      } else {
        this.helper.error('Изменения отсутствуют! Отправка на согласование невозможна!');
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  hasDocumentChanges(): boolean {
    const booleanFields: string[] = ['problem', 'attraction'];
    booleanFields.forEach(f => {
      if (!this.documentOld.obj[f] && !this.document.obj[f]) { this.document.obj[f] = this.documentOld.obj[f]; }
    });
    return !!this.programService.diff(this.documentOld.obj, this.document.obj).length;
  }

  goToCard() {
    this.state.go('app.object.card.main', {
      id: this.document.obj.documentId,
      mode: 'view'
    });
  }
}
