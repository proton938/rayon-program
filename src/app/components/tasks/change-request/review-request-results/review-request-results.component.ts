import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {fromJson, toJson} from '@uirouter/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {SessionStorage} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {find, compact, isArray, some} from 'lodash';
import {ChangeRequestService} from "../../../../services/change-request.service";
import {forkJoin, of} from "rxjs/index";
import {copy} from 'angular';
import {SolrMediatorService} from "../../../../services/solr-mediator.service";
import {ChangeRequest} from "../../../../models/change-request/ChangeRequest";
import {RayonProgramWrapper} from "../../../../models/program/RayonProgramWrapper";
import {StateService} from "@uirouter/core";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";

@Component({
  selector: 'task-review-request-results',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './review-request-results.component.html',
  styleUrls: [
    './review-request-results.component.scss',
    '../change-request.scss'
  ]
})
export class TaskReviewRequestResultsComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption: string = '';
  taskId: string = '';
  dicts: any = {};
  saving: boolean = false;
  finishing: boolean = false;

  request: ChangeRequest;
  documentOld: RayonProgramWrapper = new RayonProgramWrapper();
  document: RayonProgramWrapper = new RayonProgramWrapper();

  @BlockUI('block-ui-task') blockUI: NgBlockUI;

  constructor(public helper: RayonHelperService,
              private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private session: SessionStorage,
              private solrMediator: SolrMediatorService,
              private service: ChangeRequestService,
              private programService: RayonProgramResourceService,
              private state: StateService,
              private nsi: NsiResourceService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const requestId = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      });

      const dicts$ = this.nsi.getDictsFromCache([
        'Prefect',
        'District',
        'mr_program_ObjectForms',
        'mr_program_ObjectIndustries',
        'mr_program_ObjectCategories',
        'mr_program_ResponsibleOrganizations',
        'mr_program_OwnershipForms',
        'mr_program_dataSource']);
      const request$ = this.service.get((<any>requestId).value);

      forkJoin([dicts$, request$]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = response[1];

        this.makeDicts();

        if (this.request.document) {
          if (this.request.document.jsonOld) this.documentOld.build(JSON.parse(this.request.document.jsonOld));
          if (this.request.document.jsonNew) this.document.build(JSON.parse(this.request.document.jsonNew));
        }

        this.getAddressList();
        this.getOldAddressList();

        this.loading = LoadingStatus.SUCCESS;
      });
    });
  }

  makeDicts() {
    this.dicts.prefects = this.dicts.Prefect.map(d => this.helper.convertToNsi(d));
    this.dicts.districts = this.dicts.District.map(d => this.helper.convertToNsi(d));
    this.dicts.industries = this.dicts['mr_program_ObjectIndustries'].map(d => this.helper.convertToNsi(d));
    this.dicts.kinds = this.dicts['mr_program_ObjectCategories'].map(d => this.helper.convertToNsi(d));
    this.dicts.organizations = this.dicts['mr_program_ResponsibleOrganizations'].map(d => this.helper.convertToNsi(d));
  }

  getOldInfo(): string {
    let r: string[] = [];
    r.push(this.helper.getField(this.documentOld, 'obj.object.name'));
    r.push(this.helper.getField(this.documentOld, 'obj.address.prefect.name').join(', '));
    r.push(this.helper.getField(this.documentOld, 'obj.address.district.name').join(', '));
    r.push(this.helper.getField(this.documentOld, 'obj.address.address'));
    return compact(r).join(', ');
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  getValue(context, path) {
    let field = this.helper.getField(context, path);
    if (!field) field = null;
    if (isArray(field) && !field.length) field = null;
    return field;
  }

  isShow(fieldName: string): boolean {
    let r: boolean = true;
    let getField = (path: string) => {
      return [
        toJson(this.getValue(this.documentOld.obj, path)),
        toJson(this.getValue(this.document.obj, path))
      ];
    };
    let field: any[] = [];
    switch (fieldName) {
      case 'prefect':
        field = getField('address.prefect');
        break;
      case 'district':
        field = getField('address.district');
        break;
      case 'objectForm':
        field = getField('objectForm.code');
        break;
      case 'addressId':
        field = getField('address.addressId');
        break;
      case 'address':
        field = getField('address.address');
        break;
      case 'industry':
        field = getField('object.industry.code');
        break;
      case 'kind':
        field = getField('object.kind.code');
        break;
      case 'objectName':
        field = getField('object.name');
        break;
      case 'objectDescription':
        field = getField('object.description');
        break;
      case 'organization':
        field = getField('responsible.organization.code');
        break;
      case 'ownership':
        field = getField('object.ownership.code');
        break;
      case 'cadastralNumber':
        field = getField('parameterZu.cadastralNumber');
        break;
      case 'problem':
        field = getField('problem');
        break;
      case 'attraction':
        field = getField('attraction');
        break;
      case 'risk':
        field = getField('risk');
        break;
      case 'comment':
        field = getField('comment');
        break;
    }
    if (field.length) r = field[0] !== field[1];
    return r;
  }

  getAddressList() {
    let addressId: string = this.helper.getField(this.document, 'obj.address.addressId');
    if (addressId) {
      this.solrMediator.getByIds([{ids: [addressId], type: this.solrMediator.types.address}]).subscribe(res => {
        if (res && res[0] && res[0].docs) {
          this.dicts.addressList = res[0].docs.map((d: any) => {
            return {
              code: d.documentId,
              name: d.fullAddress
            };
          });
        }
      }, error => {
        this.dicts.addressList = [];
      });
    } else {
      this.dicts.addressList = [];
    }
  }

  getOldAddressList() {
    let addressId: string = this.helper.getField(this.documentOld, 'obj.address.addressId');
    if (addressId) {
      this.solrMediator.getByIds([{ids: [addressId], type: this.solrMediator.types.address}]).subscribe(res => {
        if (res && res[0] && res[0].docs) {
          this.dicts.addressOldList = res[0].docs.map((d: any) => {
            return {
              code: d.documentId,
              name: d.fullAddress
            };
          });
        }
      }, error => {
        this.dicts.addressOldList = [];
      });
    } else {
      this.dicts.addressOldList = [];
    }
  }

  finishTask() {
    this.activityResourceService.finishTask(parseInt(this.taskId, 0), []).then(response => {
      this.helper.success('Задача успешно завершена!');
      this.blockUI.stop();
      (<any>window).location = '/main/#/app/tasks';
    }).catch(error => {
      console.error(error);
      this.helper.error('Ошибка при завершении задачи!');
      this.blockUI.stop();
    });
  }

  finish() {
    this.blockUI.start();
    this.finishTask();
  }
}
