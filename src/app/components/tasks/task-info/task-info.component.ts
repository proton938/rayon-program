import {Component, Input} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonRequestWrapper} from '../../../models/request/RayonRequestWrapper';

@Component({
  selector: 'task-info',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './task-info.component.html',
  styleUrls: ['./task-info.component.scss']
})
export class RayonTaskInfoComponent {
  @Input() request: RayonRequestWrapper;
  @Input() prefect: string;
  constructor() {
  }
}
