import {Component, OnInit} from '@angular/core';
import {StateService} from '@uirouter/core';
import {ActiveTaskService, ActivityResourceService, ITask} from '@reinform-cdp/bpm-components';
import {delay} from 'rxjs/internal/operators';
import {ToastrService} from 'ngx-toastr';
import {from} from 'rxjs/index';

@Component({
  selector: 'demo-demo-task',
  templateUrl: './demo-task.component.html',
  styleUrls: ['./demo-task.component.scss']
})
export class DemoTaskComponent implements OnInit {
  isLoading: boolean;
  finishing = false;
  task: ITask;

  constructor(private ativityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private stateService: StateService,
              private toastrService: ToastrService) {

    this.isLoading = true;
    this.finishing = false;
  }

  ngOnInit() {
    this.activeTaskService.getTask().pipe(delay(500)).subscribe(task => {
      this.task = task;
      this.isLoading = false;
    });
  }

  finishTask() {
    this.finishing = true;
    from(this.ativityResourceService.finishTask(<any>this.task.id)).pipe(delay(500)).subscribe(() => {
      this.toastrService.success('Задача успешно завершена');
      from([0]).pipe(delay(1000)).subscribe(() => {
        this.finishing = false;
        this.stateService.go('app.user-tasks');
      });
    }, err => {
      console.log(err);
      this.toastrService.error(err.data.exception, 'При завершении задачи произошла ошибка');
      this.finishing = false;
    });
  }

}
