import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {ToastrService} from 'ngx-toastr';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {IRootScopeService, copy} from 'angular';
import {first, find, initial, last, orderBy} from 'lodash';
import {RayonRequestResourceService} from "../../../services/rayon-request-resource.service";
import {RayonRequestWrapper} from "../../../models/request/RayonRequestWrapper";
import {SessionStorage} from "@reinform-cdp/security";
import * as jsonpatch from 'fast-json-patch';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {forkJoin, from} from "rxjs/index";
import {SolrMediatorService} from "../../../services/solr-mediator.service";


@Component({
  selector: 'task-agree-object-list',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './agree-object-list.component.html'
})
export class TaskAgreeObjectList implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption: string = '';
  request: RayonRequestWrapper;
  copyRequest: RayonRequestWrapper;
  taskId: string = '';
  dicts: any = {};
  prefect: any = {};

  selected: any = {
    comment: ''
  };

  @BlockUI('agree-object-list-task') blockUI: NgBlockUI;

  constructor(private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private toastrService: ToastrService,
              @Inject('$rootScope') public rootScope: IRootScopeService,
              private rayonRequestResourceService: RayonRequestResourceService,
              private solrMediator: SolrMediatorService,
              private session: SessionStorage,
              private nsi: NsiResourceService) {
  }

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const requestId = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      });

      const $dicts = this.nsi.getDictsFromCache(['District', 'mr_program_Prefectures', 'mr_program_ObjectIndustries']);
      const $request = this.rayonRequestResourceService.get((<any>requestId).value);

      forkJoin([$dicts, $request]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = new RayonRequestWrapper();
        this.request.build(response[1]);
        this.copyRequest = copy(this.request);
        this.prefect = (<any>first(this.dicts.District.filter(i => i.code === this.request.request.district.code))).perfectId[0];
        this.loading = LoadingStatus.SUCCESS;
      });
    });
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  isValid(type: string): boolean {
    let r: boolean = true;
    if (r && type === 'rework') {
      r = !!this.selected.comment;
      if (!r) this.toastrService.error('Необходимо указать замечание');
    }
    return r;
  }

  makeVars(type: string): ITaskVariable[] {
    let r: ITaskVariable[] = [];
    r.push(<ITaskVariable>{
      name: 'ApprovalApprovedVar',
      value: type === 'agreed'
    });
    return r;
  }

  finish(type: string, vars = this.makeVars(type)) {
    const finishTask: (vars) => void = () => {
      setTimeout(() => {
        this.activityResourceService.finishTask(parseInt(this.taskId, 0), vars).then(response => {
          this.toastrService.success('Задача успешно завершена!');
          this.blockUI.stop();
          this.finishHandler(type);
        }).catch(error => {
          console.error(error);
          this.toastrService.error('Ошибка при завершении задачи!');
          this.blockUI.stop();
        });
      }, 1500);
    };
    if (this.isValid(type)) {
      const id = this.request.request.documentId;
      if (type === 'agreed') this.request.request.done = true;
      if (type === 'rework') {
        this.request.request.review.comment = this.selected.comment;
        this.request.request.review.login = this.session.login();
        this.request.request.review.fio = this.session.fullName();
        this.request.request.review.date = new Date();
        this.request.request.review.date.setHours(0, 0, 0, 0);
      }
      const _left = this.copyRequest.min();
      const _right = this.request.min();
      const diff = jsonpatch.compare(_left, _right);
      if (diff.length > 0) {
        this.blockUI.start();
        setTimeout(() => {
          this.rayonRequestResourceService.patch(id, <any>JSON.stringify(diff)).subscribe(response => {
            this.toastrService.success('Документ успешно сохранен!');
            finishTask(vars);
          }, error => {
            console.error(error);
            this.toastrService.error('Ошибка при сохранении документа!');
            this.blockUI.stop();
          });
        }, 1500);
      } else {
        this.toastrService.warning('В документе изменений нет!');
        this.blockUI.start();
        finishTask(vars);
      }
    }
  }

  finishHandler(type: string) {
    if (type === 'agreed') {
      this.solrMediator.query({
        page: 0,
        pageSize: 1000,
        types: ['REQUEST'],
        query: `(districtCodeRequest:${this.request.request.district.code}) AND (doneRequest:false)`
      }, false).subscribe(result => {
        if (!result.docs || !result.docs.length) {
          this.startProcessFormationPassport();
        } else {
          this.location.back();
        }
      });
    } else {
      this.location.back();
    }
  }

  startProcessFormationPassport() {
    this.solrMediator.query({
      page: 0,
      pageSize: 1000,
      types: ['PASSPORT'],
      query: `(districtCodePassport:${this.request.request.district.code})`
    }, false).subscribe(response => {
      let doc: any = first(response.docs);
      this.activityResourceService.getProcessDefinitions({
        key: 'mrrequest_collectInfoForDistrictsPassport',
        latest: true
      }).then(response => {
        return this.activityResourceService.initProcess({
          processDefinitionId: response.data[0].id,
          variables: [
            {
              'name': 'EntityIdVar', 'value': doc.documentId
            }
          ]
        });
      }).then(() => {
        this.toastrService.success('Процесс успешно запущен!');
        this.location.back();
      });
    });

  }
}
