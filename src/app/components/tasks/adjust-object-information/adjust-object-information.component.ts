import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {StateService, Transition} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {IRootScopeService, copy} from 'angular';
import {first, find, initial, last, orderBy} from 'lodash';
import {RayonRequestResourceService} from "../../../services/rayon-request-resource.service";
import {RayonRequestWrapper} from "../../../models/request/RayonRequestWrapper";
import {SessionStorage} from "@reinform-cdp/security";
import {UserDocumentType} from "@reinform-cdp/core";
import * as jsonpatch from 'fast-json-patch';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {forkJoin, from} from "rxjs/index";
import {RayonProgramResourceService} from '../../../services/rayon-program-resource.service';
import {Observable} from 'rxjs/Rx';
import {FileResourceService} from "@reinform-cdp/file-resource";
import {ExFileType} from "@reinform-cdp/widgets";


import {RayonProgram} from "../../../models/program/RayonProgram";
import {RayonProgramWrapper} from "../../../models/program/RayonProgramWrapper";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SolrMediatorService} from "../../../services/solr-mediator.service";

declare let window;
@Component({
  selector: 'task-adjust-object-information',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './adjust-object-information.component.html'
})
export class TaskAdjustObjectInformation implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption = '';
  document: RayonProgram;
  old: RayonProgram;
  taskId = '';
  files: any = {
    statusquo: [],
    proposedproject: [],
    photobefore: [],
    photoafter: [],
    presentation: [],
    addmaterial: [],
    addmaterialmos: [],
    image: [],
    model: [],
    dominantImage: [],
    photomosru: []
  };
  @BlockUI('adjust-object-information-task') blockUI: NgBlockUI;
  isShowButtons: boolean;
  validate: boolean;
  constructor(private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private fileResourceService: FileResourceService,
              private stateService: StateService,
              private helper: RayonHelperService,
              private location: Location,
              private toastrService: ToastrService,
              @Inject('$rootScope') public rootScope: IRootScopeService,
              private rayonRequestResourceService: RayonRequestResourceService,
              private programService: RayonProgramResourceService,
              private solrMediator: SolrMediatorService,
              private session: SessionStorage,
              private nsi: NsiResourceService) {
  }

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const documentId:any = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      }).value;
      const document$ = this.programService.init(documentId);
      forkJoin([document$])
        .subscribe((response: any) => {
          this.document = response[0];
          this.old = copy(this.document);
          this.getAddress().subscribe(response => {
            this.programService.address = response;
            this.loading = LoadingStatus.SUCCESS;
          });
          this.getFiles();
        });

    });
  }
  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  isValid(): boolean {
    let r: boolean = true;
    if (r) r = !!this.document.address.prefect.length;
    if (r) r = !!this.document.address.district.length;
    if (r) r = !!this.document.itemType;
    if (r && this.document.itemType.code === 'object') r = !!this.document.objectForm;
    if (r && !this.document.address.addressId) r = !!this.document.address.address;
    if (r) r = !!this.document.object.industry;
    if (r) r = !!this.document.object.kind;
    if (r) r = !!this.document.object.name;
    if (r) r = !!this.document.responsible.organization;
    if (r && !!this.document.parameterZu.cadastralNumber.join('').trim().length) r = !!this.document.object.area;
    // if (r && this.document.itemType.code === 'object') r = !!this.document.object.period;

    return r;
  }

  save() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      let model = new RayonProgram();
      model.build(JSON.parse(JSON.stringify(this.document)));
      let doc = model.min();
      let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc});
      if (diff.length) {
        this.programService.patch(this.document.documentId, diff).subscribe((response: RayonProgramWrapper) => {
          this.helper.success('Документ сохранен!');
          this.blockUI.stop();
          this.stateService.reload(this.stateService.current);
        });
      } else {
        this.blockUI.stop();
        this.helper.warning('Нет изменений для сохранения!');
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  finishTask() {
      this.activityResourceService.finishTask(parseInt(this.taskId, 0), []).then(response => {
        this.toastrService.success('Задача успешно завершена!');
        this.blockUI.stop();
        (<any>window).location = '/main/#/app/tasks';
      }).catch(error => {
        console.error(error);
        this.toastrService.error('Ошибка при завершении задачи!');
        this.blockUI.stop();
      });
  }

  finish() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      let model = new RayonProgram();
      model.build(JSON.parse(JSON.stringify(this.document)));
      let doc = model.min();
      let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc});
      if (diff.length) {
        this.programService.patch(this.document.documentId, diff).subscribe((response: RayonProgramWrapper) => {
          this.finishTask();
        });
      } else {
        this.finishTask();
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  showButtons() {
    this.isShowButtons = true;
  }

  getAddress(): Observable<any> {
    return Observable.create(observer => {
      if (this.document.address.addressId) {
        this.solrMediator.query({
          page: 0, pageSize: 1, types: [this.solrMediator.types.address],
          query: 'documentId:' + this.document.address.addressId
        }).subscribe((addRes: any) => {
          if (addRes && addRes.docs && addRes.docs.length && addRes.docs.length === 1) {
            observer.next(addRes.docs[0].fullAddress);
            observer.complete();
          } else {
            observer.next('');
            observer.complete();
          }
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next('');
        observer.complete();
      }
    });
  }

  getFiles() {
    if (this.document && this.document.folderId) {
      from(this.fileResourceService.getFolderContent(this.document.folderId, false)).subscribe(response => {
        if (response) {
          response.forEach(file => {
            if (file && file.fileType && this.files[file.fileType]) {
              this.files[file.fileType].push(ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
                false, file.dateCreated, file.fileType, file.mimeType));
            }
          });
        }
      });
    }
  }
}
