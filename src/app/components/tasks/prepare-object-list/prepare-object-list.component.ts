import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {StateService, Transition} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {IRootScopeService, copy} from 'angular';
import {first, find, initial, last, orderBy} from 'lodash';
import {RayonRequestResourceService} from "../../../services/rayon-request-resource.service";
import {RayonRequestWrapper} from "../../../models/request/RayonRequestWrapper";
import {SessionStorage} from "@reinform-cdp/security";
import {UserDocumentType} from "@reinform-cdp/core";
import * as jsonpatch from 'fast-json-patch';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {forkJoin} from "rxjs/index";


@Component({
  selector: 'task-prepare-object-list',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './prepare-object-list.component.html'
})
export class TaskPrepareObjectList implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption: string = '';
  request: RayonRequestWrapper;
  copyRequest: RayonRequestWrapper;
  taskId: string = '';
  dicts: any = {};
  prefect: any = {};

  @BlockUI('prepare-object-list-task') blockUI: NgBlockUI;

  constructor(private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private stateService: StateService,
              private location: Location,
              private toastrService: ToastrService,
              @Inject('$rootScope') public rootScope: IRootScopeService,
              private rayonRequestResourceService: RayonRequestResourceService,
              private transition: Transition,
              private session: SessionStorage,
              private nsi: NsiResourceService) {
  }

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const requestId = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      });

      const $dicts = this.nsi.getDictsFromCache(['District', 'mr_program_Prefectures', 'mr_program_ObjectIndustries']);
      const $request = this.rayonRequestResourceService.get((<any>requestId).value);

      forkJoin([$dicts, $request]).subscribe((response: any) => {
        this.dicts = response[0];
        this.request = new RayonRequestWrapper();
        this.request.build(response[1]);
        this.copyRequest = copy(this.request);
        this.prefect = (<any>first(this.dicts.District.filter(i => i.code === this.request.request.district.code))).perfectId[0];
        this.loading = LoadingStatus.SUCCESS;
      });

    });
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  isValid(): boolean {
    return true;
  }

  finish(vars = []) {
    const finishTask: (vars) => void = () => {
      setTimeout(() => {
        this.activityResourceService.finishTask(parseInt(this.taskId, 0), vars).then(response => {
          this.toastrService.success('Задача успешно завершена!');
          this.blockUI.stop();
          this.location.back();
        }).catch(error => {
          console.error(error);
          this.toastrService.error('Ошибка при завершении задачи!');
          this.blockUI.stop();
        });
      }, 1500);
    };
    if (this.isValid()) {
      const id = this.request.request.documentId;
      this.request.request.userStartProcess = new UserDocumentType();
      this.request.request.userStartProcess.build({
        login: this.session.login(),
        fio: this.session.fullName(),
        post: this.session.post()
      });
      const _left = this.copyRequest.min();
      const _right = this.request.min();
      const diff = jsonpatch.compare(_left, _right);
      if (diff.length > 0) {
        this.blockUI.start();
        setTimeout(() => {
          this.rayonRequestResourceService.patch(id, <any>JSON.stringify(diff)).subscribe(response => {
            this.toastrService.success('Документ успешно сохранен!');
            finishTask(vars);
          }, error => {
            console.error(error);
            this.toastrService.error('Ошибка при сохранении документа!');
            this.blockUI.stop();
          });
        }, 1500);
      } else {
        // this.toastrService.warning('В документе изменений нет!');
        this.blockUI.start();
        finishTask(vars);
      }
    } else {
      this.toastrService.warning('Не заполнены обязательные поля!');
    }
  }
}
