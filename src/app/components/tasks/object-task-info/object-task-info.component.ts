import {Component, Input} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonProgram} from '../../../models/program/RayonProgram';
import {RayonProgramResourceService} from '../../../services/rayon-program-resource.service';
@Component({
  selector: 'object-task-info',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './object-task-info.component.html',
  styleUrls: ['./object-task-info.component.scss']
})
export class RayonObjectTaskInfoComponent {
  @Input() document: RayonProgram;
  constructor(public programService: RayonProgramResourceService) {
  }
}
