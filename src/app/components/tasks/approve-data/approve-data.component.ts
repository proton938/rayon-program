import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {copy} from 'angular';
import {first, find, findIndex, initial, last, orderBy, compact} from 'lodash';
import {SessionStorage} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {forkJoin, from} from "rxjs/index";
import {SolarResourceService} from "@reinform-cdp/search-resource";
import {RayonPublicationService} from "../../../services/rayon-publication.service";
import {RayonPublication} from "../../../models/publication/RayonPublication";
import {Observable} from "rxjs/Rx";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'task-approve-data',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './approve-data.component.html'
})
export class TaskApproveData {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption: string = '';
  publication: RayonPublication;
  copyPublication: RayonPublication;
  taskId: string = '';
  dicts: any = {};
  saving: boolean = false;
  finishing: boolean = false;

  operatorInfo: string;

  @BlockUI('block-ui-task') blockUI: NgBlockUI;

  constructor(private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private location: Location,
              private helper: RayonHelperService,
              private publicationService: RayonPublicationService,
              private solarResourceService: SolarResourceService,
              private session: SessionStorage,
              private nsi: NsiResourceService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const requestId = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      });

      const $dicts = this.nsi.getDictsFromCache([
        'Prefect',
        'District',
        'mr_program_ObjectIndustries',
        'mr_program_ObjectCategories',
        'mr_program_ResponsibleOrganizations',
        'mr_program_dataSource']);
      const $publication = this.publicationService.get((<any>requestId).value);

      forkJoin([$dicts, $publication]).subscribe((response: any) => {
        this.dicts = response[0];
        this.publication = response[1];
        this.copyPublication = copy(this.publication);
        this.loading = LoadingStatus.SUCCESS;
      });
    });
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  isValid(finish: boolean = false): boolean {
    let r: boolean = true;
    if (finish && this.hasDocsWithoutAgree()) {
      r = false;
      this.helper.error('Не у всех документов проставлен признак согласования');
    }
    return r;
  }

  hasDocsWithoutAgree(): boolean {
    let r: boolean = false;
    this.publication.document.forEach(doc => {
      if (!r && !doc.agreed) r = true;
      if (!r && doc.documentLink && doc.documentLink.length) {
        doc.documentLink.forEach(link => {
          if (!r && !link.agreedLink) r = true;
        });
      }
    });
    return r;
  }

  makeVars(): ITaskVariable[] {
    let r: ITaskVariable[] = [];
    return r;
  }

  getCheckCount(type: string) { // type = 'OBJ' || 'WORK';
    let ids: string[] = [];
    this.publication.document.forEach(i => {
      if (i.type === type && !i.agreed) ids.push(i.id);
      if (i.documentLink && i.documentLink.length) {
        i.documentLink.forEach(t => {
          if (t.typeLink === type && !t.agreedLink) ids.push(t.idLink);
        })
      }
    });
    ids = compact(ids);
    return ids.length;
  }

  get operatorFio(): string {
    let r: string = '';
    if (this.publication && this.publication.executors) {
      let ano = find(this.publication.executors, i => i.role === 'MR_BPM_MODERATOR_ANO');
      if (ano) r = ano.executor.fio;
    }
    return r;
  }

  save(finish: boolean = false): Observable<void> {
    return Observable.create(observer => {
      if (this.isValid(finish)) {
        this.blockUI.start();
        const id = this.publication.documentId;
        const diff = this.publicationService.diff(this.copyPublication, this.publication);
        if (diff.length > 0) {
          this.blockUI.stop();
          setTimeout(() => {
            this.publicationService.patch(id, <any>JSON.stringify(diff)).subscribe(response => {
              this.helper.success('Документ успешно сохранен!');
              // this.publication = response;
              this.copyPublication = copy(response);
              observer.next();
              observer.complete();
            }, error => {
              console.error(error);
              this.helper.error('Ошибка при сохранении документа!');
              this.blockUI.stop();
              observer.error(error);
              observer.complete();
            });
          }, 1500);
        } else {
          this.helper.warning('В документе изменений нет!');
          this.blockUI.stop();
          observer.next();
          observer.complete();
        }
      } else {
        observer.error('Не заполнены обязательные поля');
        observer.complete();
      }
    });
  }

  saveOnly() {
    this.saving = true;
    this.save().subscribe(() => {
      this.saving = false;
    }, error => {
      this.saving = false;
    });
  }

  finish(vars = this.makeVars()) {
    const finishTask: (vars) => void = () => {
      setTimeout(() => {
        this.activityResourceService.finishTask(parseInt(this.taskId, 0), vars).then(response => {
          this.helper.success('Задача успешно завершена!');
          this.blockUI.stop();
          this.finishHandler();
        }).catch(error => {
          console.error(error);
          this.helper.error('Ошибка при завершении задачи!');
          this.blockUI.stop();
        });
      }, 1500);
    };
    this.finishing = true;
    this.save(true).subscribe(() => {
      finishTask(vars);
      this.finishing = false;
    }, error => {
      this.finishing = false;
    });
  }

  finishHandler() {
    this.location.back();
  }
}
