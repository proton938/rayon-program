import {Component, Input} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {RayonWorkTypeService} from "../../../services/rayon-work-type.service";
import {RayonGeoService} from "../../../services/rayon-geo.service";
import {from} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {find, findIndex, compact} from 'lodash';
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {RayonWork} from "../../../models/work/RayonWork";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {SolrMediatorService} from "../../../services/solr-mediator.service";

@Component({
  selector: 'approve-data-table',
  templateUrl: './approve-data-table.component.html'
})
export class ApproveDataTable {
  @Input() documents: any[] = [];
  @Input() selection: boolean;
  @Input() mosRu?: boolean = false;
  @Input() readOnly?: boolean = true;

  isLoading: boolean = true;
  dicts: any = {};
  docs: any[] = [];
  docWorks: any = {};
  gisCollection: any = {};
  docsInfo: any = {};
  isDocsLoading: boolean = false;
  isWorksLoading: boolean = false;
  docsPagination: any = {
    totalItems: 0,
    itemsPerPage: 10,
    currentPage: 1
  };
  linking: any = {};

  @BlockUI('docs-ui-block') blockUI: NgBlockUI;

  constructor(private helper: RayonHelperService,
              private nsi: NsiResourceService,
              private workTypeService: RayonWorkTypeService,
              private geo: RayonGeoService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    this.docsPagination.totalItems = this.documents.length;
    from(this.nsi.getDictsFromCache(['mr_program_requestPublish'])).subscribe(res => {
      this.dicts = res;
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess() {
    this.isLoading = false;
    this.changeDocsPage({page: 1});
    setTimeout(() => {
      this.getObjectOrWork('123');
    }, 1000);
  }

  changeDocsPage($event) {
    this.docsPagination.currentPage = $event.page;
    this.docs = this.documents.filter((item, index) => {
      return index >= ($event.page - 1) * this.docsPagination.itemsPerPage
        && index < ($event.page - 1) * this.docsPagination.itemsPerPage + this.docsPagination.itemsPerPage;
    });
    this.getDocsInfo();
  }

  getDocsInfo() {
    this.blockUI.start();
    let objectIds: string[] = this.docs.filter(i => i.type === 'OBJ' && !this.docsInfo[i.id]).map(i => i.id);
    let workIds: string [] = this.docs.filter(i => i.type === 'WORK').map(i => i.id);

    this.getWorksByIds(workIds).subscribe(ids => {
      if (ids.length) objectIds = objectIds.concat(ids);
      objectIds = compact(objectIds);

      if (objectIds.length) {
        this.isDocsLoading = true;
        this.solrMediator.getByIds([{type: this.solrMediator.types.obj, ids: objectIds}]).subscribe(res => {
          if (res && res[0] && res[0].docs) {
            res[0].docs.forEach((d: any) => {
              this.docsInfo[d.documentId] = d;
            });
            this.getWorks();
            this.isDocsLoading = false;
          }
        }, error => {
          this.blockUI.stop();
        })
      } else {
        this.blockUI.stop();
      }
    });
  }

  getWorks() {
    let queryParts: string[] = [];

    this.docs.forEach(i => {
      switch(i.type) {
        case 'WORK':
          queryParts.push('documentId:' + i.id); // Чтобы показать только его, если вдруг их много привязано к объекту
          break;
        case 'OBJ':
          queryParts.push('objectIdWork:' + i.id);
          break;
      }
    });

    if (queryParts.length) {
      let query = {page: 0, pageSize: 999, types: [this.solrMediator.types.work], query: queryParts.join(' OR ')};
      this.isWorksLoading = true;
      this.solrMediator.query(query).subscribe(res => {
        this.fillDocWorks(res).subscribe(() => {
          this.blockUI.stop();
          this.isWorksLoading = false;
        }, error => {
          this.blockUI.stop();
          this.helper.error(error);
          this.isWorksLoading = false;
        });
      }, error => {
        this.blockUI.stop();
        this.helper.error(error);
        this.isWorksLoading = false;
      });
    } else {
      this.blockUI.stop();
    }
  }

  getDocOrLink(id: string) {
    let doc: any;
    this.docs.forEach(i => {
      if (!doc && i.id === id) doc = i;
      if (!doc && i.documentLink && i.documentLink.length) {
        i.documentLink.forEach(t => {
          if (!doc && t.idLink === id) doc = t;
        });
      }
    });
    return doc;
  }

  getWorksByIds(ids: string[]): Observable<string[]> {
    return Observable.create(observer => {
      if (ids.length) {
        this.solrMediator.getByIds([{type: this.solrMediator.types.work, ids: ids}]).subscribe(res => {
          if (res && res[0] && res[0].docs) {
            res[0].docs.forEach((i: any) => {
              find(this.docs, d => d.id === i.documentId).objectId = i.objectIdWork;
            });
            observer.next(res[0].docs.map((i: any) => i.objectIdWork));
          } else observer.next([]);
          observer.complete();
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  fillDocWorks(solrResponse: any): Observable<void> {
    return Observable.create(observer => {
      if (solrResponse && solrResponse.docs && solrResponse.docs.length) {
        solrResponse.docs.filter(i => this.hasDocOrWork(i.documentId)).forEach((doc: any) => {
          if (!this.docWorks[doc.objectIdWork]) this.docWorks[doc.objectIdWork] = [];
          let workDocIndex: any = findIndex(this.docWorks[doc.objectIdWork], (i: any) => i.documentId === doc.documentId);
          if (!~workDocIndex) {
            this.docWorks[doc.objectIdWork].push(doc);
          } else {
            this.docWorks[doc.objectIdWork][workDocIndex] = doc;
          }
        });
        let ids = solrResponse.docs.map((i: any) => i.documentId);
        this.workTypeService.list(ids).subscribe((works: RayonWork[]) => {
          works.forEach(w => {
            let docWork: any = find(this.docWorks[w.objectId], (i: any) => i.documentId === w.documentId);
            if (docWork) docWork.data = w;
          });
          observer.next();
          observer.complete();
        },error => {
          this.helper.error(error);
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next();
        observer.complete();
      }
    });
  }

  isGeo(id: string): boolean {
    let doc = this.getDocOrLink(id);
    let result: boolean = false;
    let propName: string = 'geo';
    if (doc) {
      if (doc.idLink) propName += 'Link';
      result = doc[propName];
    }
    return result;
  }

  getObjectOrWork(id: string): any {
    let r: any = null;
    this.documents.forEach(doc => {
      if (!r && doc.id === id) r = doc;
      if (!r && doc.documentLink && doc.documentLink.length) {
        doc.documentLink.forEach(link => {
          if (!r && link.idLink === id) r = link;
        });
      }
    });
    return r;
  }

  hasDocOrWork(id: string): boolean {
    let r: boolean = false;
    this.documents.forEach(doc => {
      if (!r && doc.id === id) r = true;
      if (!r && doc.documentLink && doc.documentLink.length) {
        doc.documentLink.forEach(link => {
          if (!r && link.idLink === id) r = true;
        });
      }
    });
    return r;
  }

  hasDocsWithoutAgree(): boolean {
    let r: boolean = false;
    this.documents.forEach(doc => {
      if (!r && !doc.agreed) r = true;
      if (!r && doc.documentLink && doc.documentLink.length) {
        doc.documentLink.forEach(link => {
          if (!r && !link.agreedLink) r = true;
        });
      }
    });
    return r;
  }

  isLink(item): boolean {
    return !!item.idLink;
  }

  isAgreed(id: string): boolean {
    let item = this.getObjectOrWork(id);
    return this.isLink(item) ? item.agreedLink : item.agreed;
  }

  changeAgree(item: any) {
    let agreedProp: string = 'agreed';
    let id: string = item.id || item.idLink;
    if (this.isLink(item)) agreedProp += 'Link';
    this.linking[id] = true;
    item[agreedProp] = !item[agreedProp];
    setTimeout(() => delete this.linking[id], 0);
  }

  agreeAll() {
    this.documents.forEach(doc => {
      doc.agreed = true;
      if (doc.documentLink && doc.documentLink.length) {
        doc.documentLink.forEach(link => {
          link.agreedLink = true;
        });
      }
    });
  }

  getCheckCount(type: string) { // type = 'OBJ' || 'WORK';
    let ids: string[] = [];
    this.documents.forEach(i => {
      if (i.type === type && !i.agreed) ids.push(i.id);
      if (i.documentLink && i.documentLink.length) {
        i.documentLink.forEach(t => {
          if (t.typeLink === type && !t.agreedLink) ids.push(t.idLink);
        })
      }
    });
    ids = compact(ids);
    return ids.length;
  }

  get approveCountObject() {
    return this.getCheckCount('OBJ');
  }

  get approveCountWork() {
    return this.getCheckCount('WORK');
  }
}
