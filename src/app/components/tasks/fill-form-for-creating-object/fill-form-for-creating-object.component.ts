import {Component} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActiveTaskService, ActivityResourceService} from '@reinform-cdp/bpm-components';
import {StateService} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {copy} from 'angular';
import {first, find} from 'lodash';
import * as jsonpatch from 'fast-json-patch';
import {forkJoin, from} from "rxjs/index";
import {RayonProgramResourceService} from '../../../services/rayon-program-resource.service';
import {Observable} from 'rxjs/Rx';
import {FileResourceService} from "@reinform-cdp/file-resource";
import {ExFileType} from "@reinform-cdp/widgets";
import {RayonProgram} from "../../../models/program/RayonProgram";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {RayonProgramWrapper} from "../../../models/program/RayonProgramWrapper";
import {SolrMediatorService} from "../../../services/solr-mediator.service";

@Component({
  selector: 'task-fill-form-for-creating-object',
  templateUrl: './fill-form-for-creating-object.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class TaskFillFormForCreatingObjectComponent {
  loading: LoadingStatus = LoadingStatus.LOADING;
  caption = '';
  document: RayonProgram;
  old: RayonProgram;
  taskId = '';
  files: any = {
    statusquo: [],
    proposedproject: [],
    photobefore: [],
    photoafter: [],
    presentation: [],
    addmaterial: [],
    addmaterialmos: [],
    image: [],
    model: [],
    dominantImage: [],
    photomosru: []
  };
  isShowButtons: boolean = false;
  @BlockUI('block-ui-task') blockUI: NgBlockUI;
  validate: boolean;

  constructor(private activityResourceService: ActivityResourceService,
              private activeTaskService: ActiveTaskService,
              private fileResourceService: FileResourceService,
              private stateService: StateService,
              private helper: RayonHelperService,
              private toastrService: ToastrService,
              private programService: RayonProgramResourceService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    this.activeTaskService.getTask().subscribe(response => {
      this.taskId = response.id;
      this.caption = response.name;
      const documentId:any = find(response.variables, (v) => {
        return v.name === 'EntityIdVar';
      }).value;
      const document$ = this.programService.init(documentId);
      forkJoin([document$])
        .subscribe((response: any) => {
          this.document = response[0];
          this.old = copy(this.document);
          this.getAddress().subscribe(response => {
            this.programService.address = response;
            this.loading = LoadingStatus.SUCCESS;
          });
          this.getFiles();
        });
    });
  }

  showButtons() {
    this.isShowButtons = true;
  }

  isLoaded(): boolean {
    return this.loading === LoadingStatus.SUCCESS;
  }

  isValid(): boolean {
    let r: boolean = true;
    if (r) r = !!this.document.address.prefect.length;
    if (r) r = !!this.document.address.district.length;
    if (r) r = !!this.document.itemType;
    if (r && this.document.itemType.code === 'object') r = !!this.document.objectForm;
    if (r && !this.document.address.addressId) r = !!this.document.address.address;
    if (r) r = !!this.document.object.industry;
    if (r) r = !!this.document.object.kind;
    if (r) r = !!this.document.object.name;
    if (r) r = !!this.document.responsible.organization;
    if (r && !!this.document.parameterZu.cadastralNumber.join('').trim().length) r = !!this.document.object.area;
    return r;
  }

  save() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      let model = new RayonProgram();
      model.build(JSON.parse(JSON.stringify(this.document)));
      let doc = model.min();
      let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc});
      if (diff.length) {
        this.programService.patch(this.document.documentId, diff).subscribe((response: RayonProgramWrapper) => {
          this.stateService.reload(this.stateService.current);
          this.blockUI.stop();
        });
      } else {
        this.blockUI.stop();
        this.helper.warning('Нет изменений для сохранения!');
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  finishTask() {
    this.activityResourceService.finishTask(parseInt(this.taskId, 0), []).then(response => {
      this.toastrService.success('Задача успешно завершена!');
      this.blockUI.stop();
      (<any>window).location = '/main/#/app/tasks';
    }).catch(error => {
      console.error(error);
      this.toastrService.error('Ошибка при завершении задачи!');
      this.blockUI.stop();
    });
  }

  finish() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      let model = new RayonProgram();
      model.build(JSON.parse(JSON.stringify(this.document)));
      let doc = model.min();
      let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc});
      if (diff.length) {
        this.programService.patch(this.document.documentId, diff).subscribe((response: RayonProgramWrapper) => {
          this.finishTask();
        });
      } else {
        this.finishTask();
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  getAddress(): Observable<any> {
    return Observable.create(observer => {
      if (this.document.address.addressId) {
        this.solrMediator.query({
          page: 0, pageSize: 1, types: [this.solrMediator.types.address],
          query: 'documentId:' + this.document.address.addressId
        }).subscribe((addRes: any) => {
          if (addRes && addRes.docs && addRes.docs.length && addRes.docs.length === 1) {
            observer.next(addRes.docs[0].fullAddress);
            observer.complete();
          } else {
            observer.next('');
            observer.complete();
          }
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next('');
        observer.complete();
      }
    });
  }

  getFiles() {
    if (this.document && this.document.folderId) {
      from(this.fileResourceService.getFolderContent(this.document.folderId, false)).subscribe(response => {
        if (response) {
          response.forEach(file => {
            if (file && file.fileType && this.files[file.fileType]) {
              this.files[file.fileType].push(ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
                false, file.dateCreated, file.fileType, file.mimeType));
            }
          });
        }
      });
    }
  }
}
