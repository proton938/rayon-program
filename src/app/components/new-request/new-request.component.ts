import {Component, Inject, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {ActiveTaskService, ActivityResourceService} from '@reinform-cdp/bpm-components';
import {StateService} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {IRootScopeService} from 'angular';
import {RayonRequestResourceService} from '../../services/rayon-request-resource.service';
import {RayonNsiService} from '../../services/rayon-nsi.service';
import * as _ from 'lodash';
import * as angular from 'angular';
import {RayonPassportService} from '../../services/rayon-passport.service';
import {RayonRequestService} from '../../services/rayon-request.service';

@Component({
  selector: 'new-request',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.scss']
})
export class NewRequestComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  @BlockUI('new-request') blockUI: NgBlockUI;

  selectedDistrict: any = null;
  nsiDistricts: any[];
  nsiObjectIndustries: any[];
  nsiPrefectures: any[];
  hasSelectedObjectIndustries = false;

  constructor(private rayonNsiService: RayonNsiService,
              private rayonPassportService: RayonPassportService,
              private rayonRequestService: RayonRequestService,
              private location: Location,
              private toastrService: ToastrService,
              @Inject('$rootScope') private rootScope: IRootScopeService) {
  }

  ngOnInit() {
    this.updateBreadcrumbs();
    this.rayonNsiService.getDictsForCreateNewRequest().subscribe(response => {
      this.nsiDistricts = response.District;
      this.nsiObjectIndustries = response.mr_program_ObjectIndustries;
      this.nsiObjectIndustries.forEach(i => {
        i.checked = false;
        i.organisation = [];
        i.nsiResponsibleOrganizations = angular.copy(response.mr_program_ResponsibleOrganizations);
      });
      this.nsiPrefectures = response.mr_program_Prefectures;

      this.loading = LoadingStatus.SUCCESS;
    }, error => {
      console.log(error);
      this.loading = LoadingStatus.ERROR;
    });
  }

  cancel() {
    this.location.back();
  }

  changeIndustry() {
    this.hasSelectedObjectIndustries = false;
    this.nsiObjectIndustries.forEach(i => {
      if (!i.checked) {
        i.organisation = [];
      } else {
        this.hasSelectedObjectIndustries = true;
      }
    });
  }

  updateBreadcrumbs() {
    if (!this.rootScope['breadcrumbs']) {
      this.rootScope['breadcrumbs'] = [];
    }
    this.rootScope['breadcrumbs'].length = 0;
    this.rootScope['breadcrumbs'].push({
      title: 'Создание заявки на формирование и актуализацию паспортов районов',
      url: null
    });
  }

  buildParams(): any[] {
    const result = [];
    const prefect = _.find(this.nsiPrefectures, p => {
      return _.find(p.prefect, pp => {
        return pp.code === this.selectedDistrict.perfectId[0].code;
      });
    });
    this.nsiObjectIndustries.forEach(industry => {
      if (industry.checked) {
        industry.organisation.forEach(organization => {
          result.push({
            district: this.selectedDistrict,
            industry: industry,
            organization: organization,
            prefecture: false
          });
        });
        result.push({
          district: this.selectedDistrict,
          industry: industry,
          organization: prefect,
          prefecture: true
        });
      }
    });

    return result;
  }

  start() {
    if (this.isValid()) {
      this.blockUI.start();
      this.rayonPassportService.findPassportByDistrictAndCreateIfNeeded(this.selectedDistrict).subscribe(response => {
        this.rayonRequestService.createRequests(this.buildParams()).subscribe(response => {
          if (response) {
            this.blockUI.stop();
            this.cancel();
          } else {
            this.blockUI.stop();
          }
        }, error => {
          this.blockUI.stop();
        });
      }, error => {
        this.blockUI.stop();
      });
    } else {
      this.toastrService.error('Не заполнены обязательные поля!');
    }
  }

  isValid() {
    return (this.selectedDistrict !== null) && this.hasSelectedObjectIndustriesAndPrefectures();
  }

  hasSelectedObjectIndustriesAndPrefectures(): boolean {
    let result = true;
    if (_.find(this.nsiObjectIndustries, i => {
      return i.checked;
    })) {
      this.nsiObjectIndustries.forEach(i => {
        if (i.checked && i.organisation.length === 0) {
          result = false;
        }
      });
    } else {
      result = false;
    }
    return result;
  }
}
