import {Component, OnInit, Inject} from '@angular/core';
import {StateService, Transition} from '@uirouter/core';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {AlertService, ExFileType, LoadingStatus} from "@reinform-cdp/widgets";
import {IRootScopeService, copy, toJson, fromJson} from "angular";
import {RayonProgramResourceService} from "../../services/rayon-program-resource.service";
import * as jsonpatch from 'fast-json-patch';
import {forkJoin, from} from 'rxjs/index';
import {RayonHelperService} from "../../services/rayon-helper.service";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {first, some, map, forIn, find} from 'lodash';
import {SolarResourceService} from "@reinform-cdp/search-resource";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonProgram} from "../../models/program/RayonProgram";
import {RayonProgramWrapper} from "../../models/program/RayonProgramWrapper";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'new-program-commercial',
  templateUrl: './new-program-commercial.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class NewProgramCommercialComponent implements OnInit {

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  documentId: string;
  document: RayonProgram;
  old: RayonProgram;
  validate: boolean = false;
  isShowButtons: boolean;
  files: any = {
    photobefore: [],
    photoafter: [],
    addmaterial: [],
    photomosru: []
  };
  cancelAfterSave: boolean = false;

  @BlockUI('card-main-edit') blockUI: NgBlockUI;

  constructor(private stateService: StateService,
              private location: Location,
              private nsi: NsiResourceService,
              private transition: Transition,
              private programService: RayonProgramResourceService,
              private helper: RayonHelperService,
              private fileResourceService: FileResourceService,
              private alertService: AlertService,
              private solarResourceService: SolarResourceService,
              @Inject('$rootScope') private rootScope: IRootScopeService) {

  }

  ngOnInit() {
    this.documentId = this.transition.params()['id'];



    const document$ = this.programService.init(this.documentId);
    const dicts$ = this.nsi.getDictsFromCache(['DynamicRegistries']);

    forkJoin([document$, dicts$]).subscribe((response: any) => {
      this.document = response[0];
      let breadcrumbs = find(response[1]['DynamicRegistries'], item => item.code === 'MR_PROGRAM_COMMERCIAL');
      this.updateBreadcrumbs(breadcrumbs);
      this.old = copy(response[0]);
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess(): void {
    let sParams: any = this.transition.params();
    if (sParams.prefect || sParams.district || sParams.industry || sParams.itemType)  this.cancelAfterSave = true;
    this.getFiles();
    this.loadingStatus = LoadingStatus.SUCCESS;
  }

  updateBreadcrumbs(breadcrumbs) {
    let showcaseBuilderLink = breadcrumbs.showcaseBuilderLink && breadcrumbs.showcaseBuilderLink[0];
    this.helper.setBreadcrumbs(this.helper.cardBreadcrumbs({
      name: breadcrumbs.name,
      code: showcaseBuilderLink ? showcaseBuilderLink.showcaseCode : 'COMMERCIAL'
    }, this.documentId ? 'Редактирование коммерческого объекта' : 'Создание коммерческого объекта'));
  }

  showButtons() {
    this.isShowButtons = true;
  }

  uploadFileAlert() {
    this.alertService.confirm({
      message: 'Чтобы загрузить файл(ы) необходимо сначала сохранить форму!',
      okButtonText: 'Сохранить',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.save('reload');
    }).catch(error => {
      console.log(error);
    });
  }

  getFiles() {
    if (this.document && this.document.folderId) {
      from(this.fileResourceService.getFolderContent(this.document.folderId, false)).subscribe(response => {
        if (response) {
          response.forEach(file => {
            if (file && file.fileType && this.files[file.fileType]) {
              this.files[file.fileType].push(ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
                false, file.dateCreated, file.fileType, file.mimeType));
            }
          });
        }
      });
    }
  }

  onRemoveFile(file) {
    this.fileResourceService.deleteFile(file.idFile);
  }

  filesOnChange(type: string) {}

  isValid(): boolean {
    let r: boolean = true;
    if (r) r = !!this.document.address.prefect.length;
    if (r) r = !!this.document.address.district.length;
    if (r && !this.document.address.addressId) r = !!this.document.address.address;
    if (r) r = !!this.document.object.industry;
    if (r) r = !!this.document.object.kind;
    if (r) r = !!this.document.object.name;
    if (r) r = !!this.document.rubricator.length;

    return r;
  }

  save(action?: string) {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      if (!this.documentId) {
        this.document.files = [];
        let model = new RayonProgram();
        model.build(fromJson(toJson(this.document)));
        this.programService.create(model.min())
          .subscribe((response: RayonProgramWrapper) => {
            if (response && response.obj && response.obj.documentId) {
              this.helper.success('Объект успешно создан!!');

              this.updateOld(response.obj);

              this.uploadFiles(response.obj.folderId).subscribe(res => {
                if (res.length) { // Если есть файлы, которые были загружены после создания
                  this.helper.getFiles(response.obj.folderId).subscribe(files => {
                    files.forEach(f => {
                      this.document.files.push(this.helper.getShortFileInfo(f));
                    });
                    this.startPatch(action);
                  }, error => {
                    this.helper.error(error);
                  });
                } else { // В противном случае ничего патчить не нужно
                  this.saveResult(response.obj, action);
                  this.blockUI.stop();
                }
              }, error => {
                this.helper.error(error);
              });
            }
          }, error => this.helper.error(error));
      } else {
        this.startPatch(action);
      }
    } else {
      this.helper.error('Не заполнены обязательные поля');
    }
  }

  startPatch(action?: string) {
    let model = new RayonProgram();
    model.build(fromJson(toJson(this.document)));
    let doc = model.min();
    let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc});
    if (diff.length) {
      this.programService.patch(this.documentId, diff).subscribe((response: RayonProgramWrapper) => {
        this.saveResult(response.obj, action);
        this.blockUI.stop();
      });
    } else {
      // Нечего сохранять
      this.saveResult(this.document, action);
      this.blockUI.stop();
    }
  }

  updateOld(doc) {
    if (doc) {
      this.old = new RayonProgram();
      this.old.build(doc);
      this.document = new RayonProgram();
      this.document.build(doc);
      if (!this.documentId) this.documentId = this.document.documentId;
    }
  }

  saveResult(doc: any, action?: string): void {
    switch(action) {
      case 'reload':
        this.updateOld(doc);
        break;

      default:
        if (this.cancelAfterSave) this.cancel();
        else this.stateService.go('app.objectCommercial.card.main', {id: doc.documentId});
        break;
    }
  }

  cancel() {
    this.location.back();
  }

  uploadFiles(folderGuid: string): Observable<any> {
    return Observable.create(observer => {
      let requests$ = [];
      forIn(this.files, (items, fileType) => {
        items.forEach(file => {
          requests$.push(this.uploadFile(file, folderGuid, fileType));
        });
      });
      if (requests$.length) {
        forkJoin(requests$).subscribe(res => {
          this.helper.success('Файлы успешно загружены!');
          observer.next(res);
        }, error => this.helper.error(error), () => observer.complete());
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  uploadFile(file, folderGuid: string, fileType: string): Observable<any> {
    let fd = new FormData();
    fd.append('file', file, file.nameFile);
    if (folderGuid) fd.append('folderGuid', folderGuid);
    if (fileType) fd.append('fileType', fileType);
    fd.append('docEntityID', '-');
    fd.append('docSourceReference', 'TEST-001');
    return from(this.fileResourceService.handleFileUpload(fd));
  }
}
