import {Component, OnInit} from '@angular/core';
import {StateService, Transition} from '@uirouter/core';
import {AlertService, ExFileType, LoadingStatus} from "@reinform-cdp/widgets";
import {copy, toJson, fromJson} from "angular";
import {RayonProgramResourceService} from "../../services/rayon-program-resource.service";
import * as jsonpatch from 'fast-json-patch';
import {forkJoin, from} from 'rxjs/index';
import {RayonHelperService} from "../../services/rayon-helper.service";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonProgram} from "../../models/program/RayonProgram";
import {RayonProgramWrapper} from "../../models/program/RayonProgramWrapper";
import {SessionStorage} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {find} from 'lodash';

@Component({
  selector: 'new-program-archive',
  templateUrl: './new-program-archive.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class NewProgramArchiveComponent implements OnInit {

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  documentId: string;
  document: RayonProgram;
  old: RayonProgram;
  validate: boolean = false;
  isShowButtons: boolean;
  files: any = {
    statusquo: [],
    proposedproject: [],
    photobefore: [],
    photoafter: [],
    presentation: [],
    addmaterial: [],
    addmaterialmos: [],
    image: [],
    model: [],
    dominantImage: [],
    photomosru: []
  };
  cancelAfterSave: boolean = false;

  @BlockUI('card-main-edit') blockUI: NgBlockUI;

  constructor(private stateService: StateService,
              private location: Location,
              private transition: Transition,
              private programService: RayonProgramResourceService,
              private helper: RayonHelperService,
              private fileResourceService: FileResourceService,
              private alertService: AlertService,
              private session: SessionStorage,
              private nsi: NsiResourceService
              ) {

  }

  ngOnInit() {
    this.documentId = this.transition.params()['id'];

    const document$ = this.programService.init(this.documentId);
    const dicts$ = this.nsi.getDictsFromCache(['DynamicRegistries']);

    forkJoin([document$, dicts$]).subscribe((response: any) => {
      this.document = response[0];
      let breadcrumbs = find(response[1]['DynamicRegistries'], item => item.code === 'MR_PROGRAM_ARCHIVE');
      this.updateBreadcrumbs(breadcrumbs);
      this.old = copy(response[0]);
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess(): void {
    let sParams: any = this.transition.params();
    if (sParams.prefect || sParams.district || sParams.industry || sParams.itemType)  this.cancelAfterSave = true;
    this.getFiles();
    this.loadingStatus = LoadingStatus.SUCCESS;
  }

  updateBreadcrumbs(breadcrumbs) {
    let showcaseBuilderLink = breadcrumbs.showcaseBuilderLink && breadcrumbs.showcaseBuilderLink[0];
    this.helper.setBreadcrumbs(this.helper.cardBreadcrumbs({
      name: breadcrumbs.name,
      code: showcaseBuilderLink ? showcaseBuilderLink.showcaseCode : 'ARCHIVE'
    }, this.documentId ? 'Редактирование объекта или мероприятия' : 'Создание объекта или мероприятия'));
  }

  showButtons() {
    this.isShowButtons = true;
  }

  uploadFileAlert() {
    this.alertService.confirm({
      message: 'Чтобы загрузить файл(ы) необходимо сначала сохранить форму!',
      okButtonText: 'Сохранить',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.save('reload');
    }).catch(error => {
      console.log(error);
    });
  }

  getFiles() {
    if (this.document && this.document.folderId) {
      from(this.fileResourceService.getFolderContent(this.document.folderId, false)).subscribe(response => {
        if (response) {
          response.forEach(file => {
            if (file && file.fileType && this.files[file.fileType]) {
              this.files[file.fileType].push(ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
                false, file.dateCreated, file.fileType, file.mimeType));
            }
          });
        }
      });
    }
  }

  onRemoveFile(file) {
    this.fileResourceService.deleteFile(file.idFile);
  }

  filesOnChange(type: string) {}

  isValid(): boolean {
    let r: boolean = true;
    if (!this.session.hasPermission('mr_objectCardMos')) {
      if (r) r = !!this.document.address.prefect.length;
      if (r) r = !!this.document.address.district.length;
      if (r) r = !!this.document.itemType;
      if (r && this.document.itemType.code === 'object') r = !!this.document.objectForm;
      if (r && !this.document.address.addressId) r = !!this.document.address.address;
      if (r) r = !!this.document.object.industry;
      if (r) r = !!this.document.object.kind;
      if (r) r = !!this.document.object.name;
      if (r) r = !!this.document.responsible.organization;
      if (r && !!this.document.parameterZu.cadastralNumber.join('').trim().length) r = !!this.document.object.area;
      // if (r && this.document.itemType.code === 'object') r = !!this.document.object.period;
    }
    return r;
  }

  save(action?: string) {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      let model = new RayonProgram();
      model.build(fromJson(toJson(this.document)));
      let doc = model.min();
      if (!this.documentId) {
        this.programService.create(doc)
          .subscribe((response: RayonProgramWrapper) => {
            if (response && response.obj && response.obj.documentId) {
              this.helper.success('Объект успешно создан!!');
              this.saveResult(response.obj, action);
              this.blockUI.stop();
            }
          }, error => this.helper.error(error));
      } else {
        let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc});
        if (diff.length) {
          this.programService.patch(this.documentId, diff).subscribe((response: RayonProgramWrapper) => {
            this.saveResult(response.obj, action);
            this.blockUI.stop();
          });
        } else {
          // Нечего сохранять
          this.saveResult(this.document, action);
          this.blockUI.stop();
        }
      }
    } else {
      this.helper.error('Не заполнены обязательные поля');
    }
  }

  saveResult(doc: any, action?: string): void {
    switch(action) {
      case 'reload':
        this.old = new RayonProgram();
        this.old.build(doc);
        this.document = new RayonProgram();
        this.document.build(doc);
        this.documentId = this.document.documentId; // чтобы не создавалось 2 одинаковых документа
        break;

      default:
        if (this.cancelAfterSave) this.cancel();
        else this.stateService.go('app.archive.card.main', {id: doc.documentId});
        break;
    }
  }

  cancel() {
    this.location.back();
  }
}
