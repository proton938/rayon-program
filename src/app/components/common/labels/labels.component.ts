import {Component, Input} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {copy} from 'angular';
import {Observable} from "rxjs/Rx";
import {RayonLinkResourceService} from "../../../services/rayon-link-resource.service";
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {some, find, first, last, isArray, isObject, compact, isEmpty} from 'lodash';
import {forkJoin, from, of} from "rxjs/index";
import {map} from "rxjs/internal/operators";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SolrMediatorService} from "../../../services/solr-mediator.service";

@Component({
  selector: 'labels',
  templateUrl: './labels.component.html'
})
export class LabelsComponent {
  loading: boolean = true;
  labels: any[] = [];
  labelSettings: any[] = [];
  labelSettingsDictName: string = 'mr_program_Labels';
  specialLabelsDictName: string = 'mr_program_SpecialLabels';
  specialLabels: any[] = [];
  links: any[] = [];
  linked: any = {};
  linkedReady: any = {};
  linkedCallbacks: any = {};
  dicts: any = {};
  options: any = {
    toArray: s => isArray(s) ? s : s.split(/\s*[^\w\d]\s*/),
    fields: [],
    types: [],
    docTypes: []
  };
  style: any = {
    container: {}
  };

  @Input() document: any;
  @Input() fields: string | string[];
  @Input() types: string | string[];
  @Input() docTypes: string | string[];
  @Input() inline: boolean = false;

  constructor(private nsi: NsiResourceService,
              private programService: RayonProgramResourceService,
              private linkService: RayonLinkResourceService,
              private helper: RayonHelperService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    if (this.inline) this.style.container.display = 'inline-block';
    this.initOptions();
    this.getSettings().subscribe(res => {
      this.labelSettings = res[this.labelSettingsDictName].filter(this.labelFilter.bind(this)).sort((a, b) => a.sortValue - b.sortValue);
      this.specialLabels = res[this.specialLabelsDictName];
      let links$ = some(this.labelSettings, ls => ls && ls.type && ls.type.length && some(ls.type, t => some(this.specialLabels, s => s.code === t.code)))
        ? this.linkService.find(this.document.documentId)
        : from(of([]));
      let dicts$ = some(this.labelSettings, ls => ls && ls.dictionary)
        ? from(this.nsi.getDictsFromCache(this.labelSettings.filter(i => i.dictionary).map(i => i.dictionary)))
        : from(of([]));
      forkJoin([links$, dicts$]).subscribe(res => {
        this.links = (res[0] || []).filter(i => i.link && i.link.status === 'approve');
        this.dicts = res[1] || {};
        this.init();
      }, error => {
        this.helper.error(error);
      });
    });
  }

  initOptions() {
    if (this.fields) this.options.fields = this.options.toArray(this.fields);
    if (this.types) this.options.types = this.options.toArray(this.types);
    if (this.docTypes) this.options.docTypes = this.options.toArray(this.docTypes);
  }

  init() {
    if (this.labelSettings.length) {
      this.labelSettings.forEach(this.labelEachHandler.bind(this));
    } else {
      this.loading = false;
    }
  }

  labelFilter(item: any) {
    let r: boolean = true;
    if (this.options.docTypes.length) {
      r = item.docType && item.docType.length && some(item.docType, d => some(this.options.docTypes, i => i === d.code));
    }
    if (this.options.fields.length) {
      r = item.field &&  some(this.options.fields, i => i === item.field);
    } else if (this.options.types.length) {
      r = item.type &&  some(this.options.types, i => i === item.type);
    }
    return r;
  }

  labelEachHandler(item) {
    if (item) {
      if (item.type && item.type[0]) switch(item.type[0].code) {
        case 'controlInstruction4-18':
          this.initLabelControlInstruction418(item);
          break;
        case 'electionInstruction':
          this.initLabelElectionInstruction(item);
          break;
        case 'decision4-26':
          this.initLabelDecision426(item);
          break;
        case 'activePlan':
          this.initLabelPlanByStatus(item, 'active');
          break;
        case 'archivePlan':
          this.initLabelPlanByStatus(item, 'archive');
          break;
      } else if (item.field) {
        this.initField(item);
      }
    }
  }

  initField(data) {
    let label = this.makeLabelData(data);
    this.labels.push(label);
    let field = this.getField(data.field);
    if (!field || (isObject(field) && isEmpty(field))) {
      label.show = false;
    } else {
      if (data.dictionary) {
        let fieldIsCode: boolean = last(data.field.split(/\.\//)) === 'code';
        let code: string = fieldIsCode ? field : field.code || null;
        let dictRow: any = code ? find(this.dicts[data.dictionary], d => d.code === code) : null;
        if (dictRow) {
          if (dictRow.name) label.name = dictRow.name;
          if (dictRow.color) label.color = dictRow.color;
        } else if (!label.name) {
          label.show = false;
        }
      }
    }
    if (!label.name) label.name = label.code + '(без имени)';
    if (data.lowerCase) label.name = label.name.toLowerCase();
    label.loading = false;
    this.updateGlobalLoading();
  }

  initLabelControlInstruction418(data) {
    let label = this.makeLabelData(data);
    let linkType: string = 'INSTRUCTION';
    this.labels.push(label);
    let update = () => {
      label.show = !!this.linked[linkType] && some(this.linked[linkType], i => i.categoryCodeInstruction === 'control');
      label.loading = false;
      this.updateGlobalLoading();
    };
    this.updateLinked(linkType, update);
  }

  initLabelElectionInstruction(data) {
    let label = this.makeLabelData(data);
    let linkType: string = 'INSTRUCTION';
    this.labels.push(label);
    let update = () => {
      label.show = !!this.linked[linkType] && some(this.linked[linkType], i => i.categoryCodeInstruction === 'election');
      label.loading = false;
      this.updateGlobalLoading();
    };
    this.updateLinked(linkType, update);
  }

  initLabelDecision426(data) {
    let label = this.makeLabelData(data);
    let linkType: string = 'DECISION';
    this.labels.push(label);
    let update = () => {
      label.show = !!this.linked[linkType] && !!this.linked[linkType].length;
      label.loading = false;
      this.updateGlobalLoading();
    };
    this.updateLinked(linkType, update);
  }

  initLabelPlanByStatus(data, status: string) {
    let label = this.makeLabelData(data);
    let linkType: string = 'PLAN';
    this.labels.push(label);
    let update = () => {
      label.show = !!this.linked[linkType] && some(this.linked[linkType], i => i.statusCode === status);
      label.loading = false;
      this.updateGlobalLoading();
    };
    this.updateLinked(linkType, update);
  }

  updateLinked(linkType: string, callback?: () => void) {
    if (!this.links || !linkType) {
      if (callback) callback();
      return;
    }
    let links = this.links.filter(i => this.isLinkType(i, linkType));
    if (links.length) {
      if (!this.linked[linkType]) {
        this.linked[linkType] = [];
        if (callback) this.addLinkedCallback(linkType, callback);
        this.getSolrByIds(linkType).subscribe(res => {
          if (res && res.docs) this.linked[linkType] = res.docs;
          this.runLinkedCallbacks(linkType);
        }, error => {
          this.helper.error(error);
          this.runLinkedCallbacks(linkType);
        });
      } else if (callback) {
        if (this.linkedReady[linkType]) callback();
        else this.addLinkedCallback(linkType, callback);
      }
    } else {
      if (callback) callback();
    }
  }

  addLinkedCallback(linkType: string, callback: () => void) {
    if (!this.linkedCallbacks[linkType]) this.linkedCallbacks[linkType] = [];
    this.linkedCallbacks[linkType].push(callback);
  }

  runLinkedCallbacks(linkType: string) {
    if (this.linkedCallbacks[linkType] && this.linkedCallbacks[linkType].length) {
      this.linkedCallbacks[linkType].splice(0, 1)[0]();
      this.runLinkedCallbacks(linkType);
    } else {
      this.linkedReady[linkType] = true;
    }
  }

  getSolrByIds(type: string): Observable<any> {
    switch(type) {
      case 'PLAN':
        return this.solrMediator.getByIds([{ids: this.getIds(type), type: this.solrMediator.types.plan}]).pipe(map(res => res[0]));
      case 'INSTRUCTION':
        return this.solrMediator.getByIds([{ids: this.getIds(type), type: this.solrMediator.types.instruction}]).pipe(map(res => res[0]));
      case 'DECISION':
        return this.solrMediator.getByIds([{ids: this.getIds(type), type: this.solrMediator.types.decision}]).pipe(map(res => res[0]));
      default:
        return from(of({}));
    }
  }

  getIds(type: string): string[] {
    let r: string[] = [];
    if (type) {
      r = this.links
        .filter(i => i.link.type1 === type || i.link.type2 === type)
        .map(i => find([i.link.id1, i.link.id2], id => id !== this.document.documentId));
    }
    return r;
  }

  makeLabelData(data: any): any {
    return <any>{
      code: data.code,
      name: data.name && data.lowerCase ? data.name.toLowerCase() : data.name,
      color: data.color,
      loading: true,
      show: true,
      showOnReady: data.showOnReady,
      group: first(this.helper.getField(data, 'type.group')) || null
    }
  }

  checkGroups() {
    let groupList = [];
    this.labels.filter(i => i.show).forEach(i => {
      if (i.group) {
        if (some(groupList, g => g === i.group)) {
          i.show = false;
        } else {
          groupList.push(i.group);
        }
      }
    });
  }

  updateGlobalLoading() {
    this.checkGroups();
    setTimeout(() => {
      if (this.loading && !find(this.labels, i => i.loading)) this.loading = false;
    }, 0);
  }

  isShowLabel(label: any): boolean {
    let r: boolean = false;
    r = label.show && !label.loading;
    if (r) r = label.showOnReady || !this.loading;
    return r;
  }

  getField(path: string): any {
    let field: any = copy(this.document);
    let pathParts: string[] = compact(path.split(/[\/\.]/));
    if (some(['obj', 'work', 'plan'], i => i === pathParts[0])) pathParts.splice(0, 1);
    pathParts.forEach(i => {
      if (isArray(field)) {
        field = field.map(f => f && f[i] ? f[i] : null);
      } else {
        field = field[i];
      }
    });
    return field;
  }

  getSettings(): Observable<any> {
    return from(this.nsi.getDictsFromCache([this.labelSettingsDictName, this.specialLabelsDictName]));
  }

  getDemoSettings(): Observable<any> {
    return Observable.create(observer => {
      let r: any[] = [
        {
          code: 'masterPlan', // уникальный код-идентификатор
          name: 'мастер-план', // текст лейбла
          field: '', // Путь к полю в текущем документе, через . или / в качестве разделителя (тип указывать не нужно)
          type: { // Особый тип лейбла (field указывать не нужно)
            code: 'master-plan',
            name: 'Мастер-план'
          },
          docType: { // Тип документа из справочника "DocumentTypes"
            code: 'MR_PROGRAM_OBJECT',
            name: 'Объект по программе "Мой район"'
          },
          color: '',
          dictionary: '',
          showOnReady: true, // Показывать ли лейблы по готовности
          sortValue: '010' // значение сортировки
        }, {
          code: 'workStatus',
          name: 'Статус работы на объекте',
          field: 'status',
          type: '',
          docType: {
            code: 'MR_PROGRAM_WORK',
            name: 'Работа на объекте по программе "Мой район"'
          },
          color: '',
          dictionary: 'mr_program_WorkStatuses',
          showOnReady: true,
          sortValue: '020'
        }, {
          code: 'planStatus',
          name: 'Статус мастер-плана',
          field: 'statusId',
          type: '',
          docType: {
            code: 'MR_PROGRAM_PLAN',
            name: 'Мастер-план'
          },
          color: '',
          dictionary: 'mr_program_planStatus',
          showOnReady: true,
          sortValue: '030'
        }/*,
        {
          code: 'mosRu',
          name: 'mos.ru',
          field: 'mos.public',
          type: '',
          color: 'primary',
          showOnReady: true,
          sortValue: '020'
        },
        {
          code: 'folderId',
          name: 'Alfresco',
          field: 'folderId',
          type: '',
          color: 'primary',
          showOnReady: true,
          sortValue: '005'
        }*/
      ];
      observer.next(r);
      observer.complete();
    });
  }

  isLinkType(link: any, linkType: string): boolean {
    return some([link.link.type1, link.link.type2], t => t === linkType);
  }
}
