import {Component, Input} from "@angular/core";
import {isNumber} from 'lodash';

@Component({
  selector: 'program-spinner',
  templateUrl: './program-spinner.component.html'
})
export class ProgramSpinnerComponent {
  @Input() height: number | string = null;
  @Input() display: string = null;
  @Input() type: string = 'three-bounce';
  style: any = {};
  isShow: boolean = false;

  ngOnInit() {
    if (this.height) this.setHeight();
    if (this.display) this.style.display = this.display;
    this.isShow = true;
  }

  setHeight() {
    switch(this.height) {
      case 'huge':
        this.style.padding = '300px 0';
        break;
      case 'high':
      case 'big':
      case 'large':
        this.style.padding = '150px 0';
        break;
      case 'medium':
        this.style.padding = '100px 0';
        break;
      case 'small':
        this.style.padding = '50px 0';
        break;
    }
    if (isNumber(this.height)) {
      this.style.padding = this.height + 'px 0';
    }
  }
}
