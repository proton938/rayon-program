import {Component} from "@angular/core";
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonPlan} from "../../models/plan/RayonPlan";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {StateService, Transition} from "@uirouter/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {PlanResourceService} from "../../services/plan-resource.service";
import {RayonHelperService} from "../../services/rayon-helper.service";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {SessionStorage} from "@reinform-cdp/security";
import {forkJoin, from} from "rxjs/index";
import {copy, toJson, fromJson} from "angular";
import {forIn, last, some, compact, toNumber} from 'lodash';
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'new-plan',
  templateUrl: './new-plan.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class NewPlanComponent {
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  documentId: string;
  document: RayonPlan;
  prevDoc: RayonPlan;
  old: RayonPlan;
  validate: boolean = false;
  isShowButtons: boolean;
  files: any[] = [];

  @BlockUI('form-block') blockUI: NgBlockUI;

  constructor(private stateService: StateService,
              private location: Location,
              private nsi: NsiResourceService,
              private transition: Transition,
              private planService: PlanResourceService,
              private helper: RayonHelperService,
              private fileResourceService: FileResourceService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    const document$ = this.planService.init(this.documentId);

    forkJoin([document$]).subscribe((response: any) => {
      this.document = response[0];
      this.old = copy(response[0]);
      this.updateBreadcrumbs();
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess(): void {
    this.getFiles();
    this.loadingStatus = LoadingStatus.SUCCESS;
  }

  getFiles() {
    if (this.document && this.document.folderId) {
      this.planService.getAllFiles(this.document.folderId).subscribe(res => {
      }, error => {
        this.helper.error('Ошибка загрузки файлов');
      });
    }
  }

  updateBreadcrumbs() {
    this.helper.setBreadcrumbs([
      {
        title: 'Реестры',
        url: this.helper.registersUrl
      },
      {
        title: 'Мастер-план',
        url: null
      }
    ]);
  }

  showButtons() {
    this.isShowButtons = true;
  }

  isValid(): boolean {
    let r: boolean = true;
    if (r) r = !!this.document.planName;
    if (r) r = !!this.document.prefect;
    if (r) r = !!this.document.planYear;
    return r;
  }

  save() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      if (!this.documentId) {
        this.planService.getVersionsFromSolr(this.document.prefect.code, this.document.planYear).subscribe(s => {
          this.document.planVersion = s && s.docs && s.docs.length
            ? toNumber(last(compact(s.docs.map(i => i.planVersion))) || 0) + 1
            : 1;
          this.document.planNumber = this.planService.makePlanNumber(this.document);
          if (s && s.docs && some(s.docs, i => i.statusCode === 'project')) {
            this.blockUI.stop();
            let planName = '' + this.document.planYear + 'по ' + this.document.prefect.name;
            this.helper.error('Мастер-план ' + planName + ' в статусе "Проект" уже существует!');
          } else {
            this.planService.create(copy(this.document).min()).subscribe(res => {
              if (res && res.documentId) {
                this.helper.success('Мастер-план успешно создан!!');
                this.updateOld(res);
                this.uploadFiles(res.folderId).subscribe(upRes => {
                  if (upRes.length) { // Если есть файлы, которые были загружены после создания
                    this.document.files.push(...upRes.map(u => u.guid));
                    this.startPatch();
                  } else { // В противном случае ничего патчить не нужно
                    this.saveResult(res);
                    this.blockUI.stop();
                  }
                }, error => this.helper.error(error));
              }
            });
          }

        });
      } else {
        // patch
        this.startPatch();
      }
    } else {
      this.helper.error('Не заполнены обязательыне поля!');
    }
  }

  startPatch() {
    let diff: any[] = this.planService.diff(this.old, this.document);
    if (diff.length) {
      this.planService.update(this.documentId, diff).subscribe((res: RayonPlan) => {
        this.saveResult(res);
        this.blockUI.stop();
      });
    } else {
      // Нечего сохранять
      this.saveResult(this.document);
      this.blockUI.stop();
    }
  }

  saveResult(doc) {
    this.stateService.go('app.plan.card.main', {id: doc.documentId});
  }

  cancel() {
    this.location.back();
  }

  updateOld(doc) {
    if (doc) {
      this.old = copy(doc);
      this.document = copy(doc);
      if (!this.documentId) this.documentId = this.document.documentId;
    }
  }

  uploadFiles(folderGuid: string): Observable<any> {
    return Observable.create(observer => {
      let requests$ = [];
      this.files.forEach(file => requests$.push(this.uploadFile(file, folderGuid)));
      if (requests$.length) {
        forkJoin(requests$).subscribe(res => {
          this.helper.success('Файлы успешно загружены!');
          observer.next(res);
        }, error => this.helper.error(error), () => observer.complete());
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  uploadFile(file, folderGuid: string, fileType: string = 'default'): Observable<any> {
    let fd = new FormData();
    fd.append('file', file, file.nameFile);
    if (folderGuid) fd.append('folderGuid', folderGuid);
    if (fileType) fd.append('fileType', fileType);
    fd.append('docEntityID', this.documentId || '-');
    fd.append('docSourceReference', 'TEST-001');
    return from(this.fileResourceService.handleFileUpload(fd));
  }

  isShowBtn(type?: string): boolean {
    return this.session.hasPermission('MR_PLAN_ADD_BUTTON');
  }
}
