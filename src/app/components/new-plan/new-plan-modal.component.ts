import {Component, Input, OnInit} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {RayonPlan} from '../../models/plan/RayonPlan';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {PlanResourceService} from '../../services/plan-resource.service';
import {RayonHelperService} from '../../services/rayon-helper.service';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {SessionStorage} from '@reinform-cdp/security';
import {Observable, forkJoin, from} from 'rxjs';
import {copy, toJson, fromJson} from 'angular';
import {some} from 'lodash';
import {RayonLinkResourceService} from '../../services/rayon-link-resource.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'new-plan-modal',
  templateUrl: './new-plan-modal.component.html'
})
export class NewPlanModalComponent implements OnInit {
  @Input() documentId = '';
  @Input() prevId = '';
  @Input() saveCallback: () => void;

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  document: RayonPlan;
  prevDoc: RayonPlan;
  old: RayonPlan;
  validate = false;
  isShowButtons: boolean;
  files: any[] = [];

  @BlockUI('form-block') blockUI: NgBlockUI;

  constructor(private bsModalRef: BsModalRef,
              private planService: PlanResourceService,
              private helper: RayonHelperService,
              private fileResourceService: FileResourceService,
              private linkService: RayonLinkResourceService,
              private session: SessionStorage) {}

  ngOnInit() {
    const requestList = [];
    if (this.prevId) {
      requestList.push(this.planService.get(this.prevId));
      requestList.push(this.linkService.find(this.prevId));
    } else {
      requestList.push(this.planService.get(this.documentId));
    }

    forkJoin(requestList).subscribe((response: any) => {
      if (this.prevId) {
        this.prevDoc = response[0];
        this.makeDocFromPrev();
      } else {
        this.document = response[0];
        this.old = copy(response[0]);
      }
      console.log('document:', this.document);
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess(): void {
    // this.getFiles();
    this.loadingStatus = LoadingStatus.SUCCESS;
  }

  /*getFiles() {
    if (this.document && this.document.folderId) {
      this.planService.getAllFiles(this.document.folderId).subscribe(res => {
        // debugger;
      }, error => {
        this.helper.error('Ошибка загрузки файлов');
      });
    }
  }*/

  makeDocFromPrev() {
    this.document = copy(this.prevDoc);
    this.document.documentId = '';
    this.document.folderId = '';
    this.document.documentDate = null;
    this.document.planVersion++;
    this.document.planNumber = this.planService.makePlanNumber(this.document);
    this.document.planApproveDate = new Date();
    this.document.planApproveDate.setHours(0, 0, 0);
    this.document.statusId = null;
    this.document.files = [];
  }

  showButtons() {
    this.isShowButtons = true;
  }

  isValid(): boolean {
    let r = true;
    if (r) { r = !!this.document.planName; }
    if (r) { r = !!this.document.prefect; }
    if (r) { r = !!this.document.planYear; }
    return r;
  }

  save() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      if (!this.documentId) {
        this.planService.getVersionsFromSolr(this.document.prefect.code, this.document.planYear).subscribe(s => {
          if (s && s.docs && some(s.docs, i => i.statusCode === 'project')) {
            this.blockUI.stop();
            const planName = '' + this.document.planYear + ' по ' + this.document.prefect.name;
            this.helper.error('Мастер-план ' + planName + ' в статусе "Проект" уже существует!');
          } else {
            this.planService.create(copy(this.document).min()).subscribe(res => {
              if (res && res.documentId) {
                this.helper.success('Мастер-план успешно создан!');
                this.updateOld(res);
                const requests$ = [this.uploadFiles(res.folderId)];
                if (this.prevId) { requests$.push(this.linkService.cloneLinks(this.prevId, res.documentId)); }
                forkJoin(requests$).subscribe(upRes => {
                  if (upRes[0].length) { // Если есть файлы, которые были загружены после создания
                    this.document.files.push(...upRes[0].map(u => u.guid));
                    this.startPatch();
                  } else { // В противном случае ничего патчить не нужно
                    this.saveResult(res);
                    this.blockUI.stop();
                  }
                }, error => this.helper.error(error));
              }
            });
          }
        });

      } else {
        // patch
        this.startPatch();
      }
    } else {
      this.helper.error('Не заполнены обязательыне поля!');
    }
  }

  startPatch() {
    const model = new RayonPlan();
    model.build(fromJson(toJson(this.document)));
    const diff: any[] = this.planService.diff(this.old, model);
    if (diff.length) {
      this.planService.update(this.documentId, diff).subscribe((res: RayonPlan) => {
        this.blockUI.stop();
        this.saveResult(res);
      });
    } else {
      // Нечего сохранять
      this.blockUI.stop();
      this.saveResult(this.document);
    }
  }

  saveResult(doc) {
    console.log(doc);
    if (this.saveCallback) { this.saveCallback(); }
    this.bsModalRef.hide();
  }

  cancel() {
    this.bsModalRef.hide();
  }

  updateOld(doc) {
    if (doc) {
      this.old = copy(doc);
      this.document = copy(doc);
      if (!this.documentId) { this.documentId = this.document.documentId; }
    }
  }

  uploadFiles(folderGuid: string): Observable<any> {
    return Observable.create(observer => {
      const requests$ = [];
      this.files.forEach(file => requests$.push(this.uploadFile(file, folderGuid)));
      if (requests$.length) {
        forkJoin(requests$).subscribe(res => {
          this.helper.success('Файлы успешно загружены!');
          observer.next(res);
        }, error => this.helper.error(error), () => observer.complete());
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  uploadFile(file, folderGuid: string, fileType: string = 'default'): Observable<any> {
    const fd = new FormData();
    fd.append('file', file, file.nameFile);
    if (folderGuid) { fd.append('folderGuid', folderGuid); }
    if (fileType) { fd.append('fileType', fileType); }
    fd.append('docEntityID', this.documentId || '-');
    fd.append('docSourceReference', 'TEST-001');
    return from(this.fileResourceService.handleFileUpload(fd));
  }

  isShowBtn(type?: string): boolean {
    return this.session.hasPermission('MR_PLAN_ADD_BUTTON');
  }

  isLoading(): boolean {
    return this.loadingStatus === LoadingStatus.LOADING;
  }
}
