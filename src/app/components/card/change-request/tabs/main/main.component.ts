import {Component, Input} from "@angular/core";
import {ChangeRequest} from "../../../../../models/change-request/ChangeRequest";
import {ChangeRequestService} from "../../../../../services/change-request.service";
import {RayonProgramResourceService} from "../../../../../services/rayon-program-resource.service";
import {RayonWorkTypeService} from "../../../../../services/rayon-work-type.service";
import {Observable} from "rxjs/Rx";
import {RayonHelperService} from "../../../../../services/rayon-helper.service";
import {compact, find} from 'lodash';
import {RayonProgram} from "../../../../../models/program/RayonProgram";
import {from, of} from "rxjs/index";
import {ExFileType} from "@reinform-cdp/widgets";
import {map} from "rxjs/internal/operators";

@Component({
  selector: 'card-change-request-main',
  templateUrl: './main.component.html'
})
export class CardChangeRequestMainComponent {
  request: ChangeRequest;
  obj: RayonProgram;
  document: any;
  documentOld: any;
  isLoading: boolean = true;
  isError: boolean;

  requestFiles: any[] = [];
  extraFiles: any[] = [];

  constructor(private service: ChangeRequestService,
              private programService: RayonProgramResourceService,
              private workService: RayonWorkTypeService,
              private helper: RayonHelperService) {}

  ngOnInit() {
    this.request = this.service.document;

    this.updateFileList();

    if (this.request.document) {
      if (this.request.document.jsonOld) this.documentOld = JSON.parse(this.request.document.jsonOld);
      if (this.request.document.jsonNew) this.document = JSON.parse(this.request.document.jsonNew);
    }

    if (this.request.modifiedTab === 'WORK') {
      this.programService.get(this.documentOld.work.objectId).subscribe(obj => {
        this.obj = this.programService.getInstance(obj);
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.isError = true;
      });
    } else {
      this.obj = this.programService.getInstance(this.documentOld);
      this.isLoading = false;
    }
  }

  getObjectInfo(): string {
    let r: string[] = [];
    r.push(this.helper.getField(this.obj, 'object.name'));
    r.push(this.helper.getField(this.obj, 'address.prefect.name').join(', '));
    r.push(this.helper.getField(this.obj, 'address.district.name').join(', '));
    r.push(this.helper.getField(this.obj, 'address.address'));
    return compact(r).join(', ');
  }

  updateFileList(): void {
    this.getFiles().subscribe(res => {
      this.requestFiles = res;
      if (this.request.files && this.request.files.length) {
        this.extraFiles = this.request.files.map(id => find(this.requestFiles, f => f.idFile === id));
      }
    });
  }

  getFiles(folderId: string = this.request.folderId): Observable<any[]> {
    return folderId
      ? this.helper.getFiles(folderId).pipe(map(files => files.map(file => {
        return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
          false, file.dateCreated, file.fileType, file.mimeType);
      })))
      : from(of([]));
  }

  /*getDocument(id, type: string): Observable<any> {
    switch(type) {
      case 'OBJ':
        return this.programService.get(id);
      case 'WORK':
        return this.workService.get(id);
    }
  }*/
}
