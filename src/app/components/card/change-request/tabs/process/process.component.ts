import {Component} from "@angular/core";
import {ChangeRequest} from "../../../../../models/change-request/ChangeRequest";
import {ChangeRequestService} from "../../../../../services/change-request.service";

@Component({
  selector: 'card-change-request-process',
  templateUrl: './process.component.html'
})
export class CardChangeRequestProcessComponent {
  document: ChangeRequest;
  modeParam: string;
  isLoading: boolean = true;
  config: any;
  constructor(private service: ChangeRequestService) {}

  ngOnInit() {
    this.document = this.service.document;
    this.config = {
      sysName: 'mrobject',
      linkVarValue: this.document.documentId,
      defaultVars: [{
        name: 'EntityIdVar',
        value: this.document.documentId
      }]
    };
    this.isLoading = false;
  }
}
