import {Component} from "@angular/core";
import {ChangeRequestService} from "../../../../../services/change-request.service";
import {ChangeRequest} from "../../../../../models/change-request/ChangeRequest";

@Component({
  selector: 'card-cahange-request-json',
  templateUrl: './json.component.html'
})
export class CardChangeRequestJsonComponent {
  document: ChangeRequest;

  constructor(public service: ChangeRequestService) {}

  ngOnInit() {
    this.document = this.service.document;
  }
}
