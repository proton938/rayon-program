import {Component} from "@angular/core";
import {ChangeRequest} from "../../../../../models/change-request/ChangeRequest";
import {ChangeRequestService} from "../../../../../services/change-request.service";

@Component({
  selector: 'card-change-request-log',
  templateUrl: './log.component.html'
})
export class CardChangeRequestLogComponent {
  document: ChangeRequest;

  constructor(private service: ChangeRequestService) {}

  ngOnInit() {
    this.document = this.service.document;
  }
}
