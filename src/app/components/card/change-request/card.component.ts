import {Component} from "@angular/core";
import {StateService, Transition} from "@uirouter/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {ChangeRequestService} from "../../../services/change-request.service";
import {ChangeRequest} from "../../../models/change-request/ChangeRequest";
import {formatDate} from '@angular/common';
import {forkJoin, from} from "rxjs/index";
import {find} from 'lodash';

@Component({
  selector: 'card-change-request',
  templateUrl: './card.component.html'
})
export class CardChangeRequestComponent {
  documentId: string;
  document: ChangeRequest;
  dicts: any = {};
  isLoading: boolean = true;
  isError: boolean;
  commonCardState: string = 'app.change-request.card';
  tabs: any[] = [];

  constructor(private transition: Transition,
              private helper: RayonHelperService,
              private state: StateService,
              private session: SessionStorage,
              private nsi: NsiResourceService,
              private service: ChangeRequestService) {}

  ngOnInit() {
    this.documentId = this.transition.params()['id'];

    const doc$ = this.service.init(this.documentId);
    const dicts$ = from(this.nsi.getDictsFromCache([
      'mr_program_requestStatus',
      'DynamicRegistries'
    ]));

    this.tabs = [
      { code: 'main',
        name: 'Карточка',
        state: this.commonCardState + '.main',
        view: true,
        edit: true,
        show: true
      },
      { code: 'json',
        name: 'JSON',
        state: this.commonCardState + '.json',
        view: true,
        edit: true,
        show: true
      },
      { code: 'log',
        name: 'История изменений',
        state: this.commonCardState + '.log',
        view: true,
        edit: true,
        show: true
      },
      { code: 'process',
        name: 'Процесс',
        state: this.commonCardState + '.process',
        view: true,
        edit: true,
        show: true
      }
    ];

    if (!this.tabs.filter(i => i.state === this.state.current.name).length) {
      //This tab on current view-mode not exists
      this.state.go(this.commonCardState + '.main');
    }

    forkJoin([doc$, dicts$]).subscribe(res => {
      this.document = res[0];
      this.dicts = res[1];
      this.updateBreadcrumbs();
      this.isLoading = false;
    }, error => {
      this.helper.error(error);
      this.isLoading = false;
      this.isError = true;
    });
  }

  updateBreadcrumbs() {
    let breadcrumbs = find(this.dicts['DynamicRegistries'], item => item.code === 'MR_PROGRAM_CHANGEREQUEST');
    let showcaseBuilderLink = breadcrumbs.showcaseBuilderLink && breadcrumbs.showcaseBuilderLink[0];
    let name: string = 'Заявка на изменение ' +
      (this.document.modifiedTab === 'WORK' ? 'работы на объекте' : '') +
      (this.document.modifiedTab === 'OBJ' ? 'объекта' : '') +
      ' №' + this.document.documentNumber + ' от ' +
      formatDate(this.document.documentDate, 'dd.MM.yyyy', 'en');
    this.helper.setBreadcrumbs(this.helper.cardBreadcrumbs({
      name: breadcrumbs.name || 'Заявки на корректировку данных об объектах и работах по программе "Мой район"',
      code: showcaseBuilderLink ? showcaseBuilderLink.showcaseCode : 'MR_PROGRAM_CHANGEREQUEST',
      documentCode: 'MR_PROGRAM_CHANGEREQUEST'
    }, name || 'Заявка на изменение'));
  }

  getTab(code: string): any {
    return find(this.tabs, i => i.code === code);
  }

  tabChange(tabState: string): void {
    let tabParams: any = { id: this.documentId };
    this.state.go(tabState, tabParams);
  }

  isActiveTab(tabState: string): boolean {
    return tabState === this.state.current.name;
  }
}
