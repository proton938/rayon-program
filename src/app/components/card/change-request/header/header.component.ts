import {Component, Input} from "@angular/core";
import {ChangeRequest} from "../../../../models/change-request/ChangeRequest";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {from} from "rxjs/index";
import {find} from "lodash";

@Component({
  selector: 'header-card-change-request',
  templateUrl: './header.component.html'
})
export class HeaderChangeRequestComponent {
  h3: string = '';
  dicts: any = {};
  status: any = null;

  @Input() document: ChangeRequest;

  constructor(private nsi: NsiResourceService) {}

  ngOnInit() {
    from(this.nsi.getDictsFromCache(['mr_program_requestStatus'])).subscribe(res => {
      this.dicts = res;
      this.status = this.document && this.document.status
        ? find(this.dicts['mr_program_requestStatus'], i => i.code === this.document.status.code)
        : null;
    });

    switch(this.document.modifiedTab) {
      case 'OBJ':
        this.h3 = 'Заявка на изменение объекта №' + this.document.documentNumber;
        break;
      case 'WORK':
        this.h3 = 'Заявка на изменение работы на объекте №' + this.document.documentNumber;
        break;
    }
  }
}
