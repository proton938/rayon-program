import {Component, OnInit} from '@angular/core';
import {StateService, Transition} from '@uirouter/core';
import {IInfoAboutProcessInit} from '@reinform-cdp/bpm-components';
import {forkJoin} from 'rxjs/index';
import {RayonProgramResourceService} from '../../services/rayon-program-resource.service';
import {SessionStorage} from '@reinform-cdp/security';
import {RayonHelperService} from "../../services/rayon-helper.service";
import {first, find} from "lodash";
import {Observable} from "rxjs/Rx";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {SolrMediatorService} from "../../services/solr-mediator.service";

@Component({
  selector: 'program-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class ProgramCardComponent implements OnInit {

  document: any;
  isLoading: boolean;
  isError: boolean;
  init: IInfoAboutProcessInit;
  documentId: string;
  dictionaries: any = {};
  modeParam: string;
  tabs: any[] = [];
  isObjectAdmin: boolean;
  currentAddress: string = '';
  commonCardState: string = 'app.object.card';

  constructor(private programService: RayonProgramResourceService,
              private helper: RayonHelperService,
              private transition: Transition,
              private stateService: StateService,
              private solrMediator: SolrMediatorService,
              private session: SessionStorage,
              private nsi: NsiResourceService) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    this.modeParam = this.transition.params()['mode'];

    const document$ = this.programService.init(this.documentId);
    const dicts$ = this.nsi.getDictsFromCache(['DynamicRegistries']);

    this.isObjectAdmin = !!~this.session.groups().indexOf('MR_OBJECT_ADMIN');

    this.tabs = [
      { code: 'main',
        name: 'Общая информация',
        state: this.commonCardState + '.main',
        view: true,
        edit: true,
        show: true
      },
      { code: 'works',
        name: 'Работы на объекте',
        state: this.commonCardState + '.works',
        view: true,
        edit: true,
        show: true
      },
      { code: 'aerial-photo',
        name: 'Аэрофотосъемка',
        state: this.commonCardState + '.aerial-photo',
        view: true,
        edit: true,
        show: this.session.hasPermission('mr_objectAeroView')
      },
      { code: 'instructions',
        name: 'Поручения Мэра',
        state: this.commonCardState + '.instructions',
        view: true,
        edit: true,
        show: this.session.hasPermission('mr_objectCardInstruction')
      },
      { code: 'decisions',
        name: 'Решения штабов',
        state: this.commonCardState + '.decisions',
        view: true,
        edit: true,
        show: this.session.hasPermission('MEETING_DECISION_OBJECT_INSTRUCTION_DECISION')
      },
      { code: 'consideration',
        name: 'Рассмотрение',
        state: this.commonCardState + '.consideration',
        view: true,
        edit: true,
        show: false
      },
      { code: 'objects',
        name: 'Объекты',
        state: this.commonCardState + '.objects',
        view: true,
        edit: true,
        show: false
      },
      { code: 'log',
        name: 'История изменений по объекту',
        state: this.commonCardState + '.log',
        view: true,
        edit: true,
        show: this.session.hasPermission('objectCardTab')
      },
      { code: 'log-works',
        name: 'История изменений работ на объекте',
        state: this.commonCardState + '.log-works',
        view: true,
        edit: true,
        show: this.session.hasPermission('mr_program_work_log')
      },
      { code: 'process',
        name: 'Процесс',
        state: this.commonCardState + '.process',
        view: true,
        edit: true,
        show: true
      },
      { code: 'json',
        name: 'JSON',
        state: this.commonCardState + '.json',
        view: true,
        edit: true,
        show: this.session.hasPermission('objectCardTab')
      }
    ];

    if (!this.tabs.filter(i => i.state === this.stateService.current.name).length) {
      //This tab on current view-mode not exists
      this.stateService.go(this.commonCardState + '.main');
    }

    forkJoin([document$, dicts$])
      .subscribe((response: any) => {
        this.document = response[0];
        let breadcrumbsName = find(response[1]['DynamicRegistries'], item => item.code === 'MR_PROGRAM_OBJECT');
        this.updateBreadcrumbs(breadcrumbsName);
        this.initSuccess();
      }, error => {
        this.isError = true;
        this.helper.error(error);
      });
  }

  updateBreadcrumbs(breadcrumbs) {
    let showcaseBuilderLink = breadcrumbs.showcaseBuilderLink && breadcrumbs.showcaseBuilderLink[0];
    this.helper.setBreadcrumbs(this.helper.cardBreadcrumbs({
      name: breadcrumbs.name || 'Объекты',
      code: showcaseBuilderLink ? showcaseBuilderLink.showcaseCode : 'OBJECT'
    }, this.document && this.document.object && this.document.object.name ? this.document.object.name : ''));
  }

  initSuccess() {
    if (this.document.documentId === '03ae67b6-4d3f-4f29-afba-b3cd68807cca'
      || this.document.documentId === 'e29b253a-3e83-46ff-bb89-8e4bf63b0d6e') {
      this.getTab('consideration').show = true;
    }
    this.tabs = this.tabs.filter(i => i.show);
    if (this.document.address.addressId) {
      this.getFullAddress().subscribe(addr => {
        if (addr) this.currentAddress = addr;
        this.isLoading = false;
      });
    } else this.isLoading = false;
  }

  getTab(code: string): any {
    return find(this.tabs, i => i.code === code);
  }

  tabChange(tabState: string): void {
    let tabParams: any = { id: this.documentId };
    if (tabState !== this.stateService.current.name) tabParams.mode = 'view';
    this.stateService.go(tabState, tabParams);
  }

  isActiveTab(tabState: string): boolean {
    return tabState === this.stateService.current.name;
  }

  getFullAddress(): Observable<string> {
    return Observable.create(observer => {
      let requestParams: any = {
        page: 0,
        pageSize: 1,
        types: [this.solrMediator.types.address],
        query: 'documentId:' + this.document.address.addressId
      };
      this.solrMediator.query(requestParams).subscribe(response => {
        observer.next(response && response.docs && response.docs.length ? response.docs[0]['fullAddress'] : '');
        observer.complete();
      }, error => observer.error(error), () => observer.complete());
    });
  }
}
