import {Component, Input, OnInit} from '@angular/core';
import {RayonProgram} from "../../../models/program/RayonProgram";
import {from} from 'rxjs/index';
import {StateService, Transition} from '@uirouter/core';
import {first, some, toString} from 'lodash';
import {AlertService} from "@reinform-cdp/widgets";
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";
import {Observable} from "rxjs/Rx";
import {ActivityResourceService} from "@reinform-cdp/bpm-components";

@Component({
  selector: 'program-card-header',
  templateUrl: './program-card-header.component.html',
  styleUrls: ['./program-card-header.component.scss']
})
export class ProgramCardHeaderComponent implements OnInit {
  documentId: string = 'debug';
  h3: string = '';
  dictionaries: any = {};
  status: any;
  modeParam: string;

  isShow: any = {};
  isMosRu: boolean;
  correcting: boolean = false;

  @Input() document: RayonProgram;
  @Input() address?: string;

  constructor(private transition: Transition,
              private alertService: AlertService,
              private programService: RayonProgramResourceService,
              private stateService: StateService,
              private helper: RayonHelperService,
              private activityService: ActivityResourceService,
              private session: SessionStorage) {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    this.isShow.removeObjectBtn = this.session.hasPermission('objectCardDel');
    this.isShow.archiveObjectBtn = this.session.hasPermission('objectCardArchive');
  }

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    this.modeParam = this.transition.params()['mode'];
    this.h3 = this.getH3();
  }

  getH3(): string {
    let resultParts: string[] = [];
    if (this.document && this.document.address) {
      resultParts.push(this.document.address.prefect.map(i => i.name).join(', '));
      resultParts.push(this.document.address.district.map(i => i.name).join(', '));
      if (this.document.address.addressId) {
        resultParts.push(this.address);
      } else {
        resultParts.push(this.document.address.address);
      }
    }
    return resultParts.filter(i => i).join(', ');
  }

  delete() {
    this.alertService.confirm({
      message: 'Вы действительно хотите удалить данный документ?',
      okButtonText: 'Ок',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.programService.remove([this.documentId]).subscribe(response => {
        this.helper.success('Документ успешно удален');
        this.stateService.go('app.object.list');
      }, error => {
        this.helper.success('Ошибка при удалении документа');
        console.log(error);
      });
    }).catch(error => {
      console.log(error);
    });
  }

  getCurrentStateName(): string {
    return this.stateService.current.name;
  }

  startCorrectProcess() {
    let processId = 'mrobject_ObjectAdjustment';
    this.correcting = true;
    this.getOpenedTasks().subscribe(tasks => {
      if (tasks.length) {
        this.helper.goToTask('app.execution.adjustObjectInformation', tasks[0].id);
        /*this.stateService.go('app.execution.adjustObjectInformation', {
          system: 'mrobject',
          taskId: tasks[0].id,
          systemCode: 'MR'
        });*/
      } else {
        from(this.activityService.getProcessDefinitions({
          keyLike: processId + '%',
          latest: true,
          size: 1
        })).subscribe((def: any) => {
          let defId = def.data[0].id;
          from(this.activityService.initProcess({
            processDefinitionId: defId,
            variables: [
              {name: 'EntityIdVar', value: this.documentId},
              {name: 'ExecutorVar', value: this.session.login()}
            ]
          })).subscribe(processId => {
            this.helper.success('Процесс успешно запущен!');
            from(this.activityService.getTasks({executionId: toString(processId)})).subscribe((res: any) => {
              if (res && res.data && res.data[0]) {
                let taskId = res.data[0].id;
                this.helper.goToTask('app.execution.adjustObjectInformation', taskId);
                /*this.stateService.go('app.execution.adjustObjectInformation', {
                  system: 'mrobject',
                  taskId: taskId,
                  systemCode: 'MR'
                });*/
              }
              this.correcting = false;
            }, error => {
              this.helper.error(error);
              this.correcting = false;
            });
          }, error => {
            this.helper.error(error);
            this.correcting = false;
          });
        }, error => {
          this.helper.error(error);
          this.correcting = false;
        });
      }
    }, error => {
      this.helper.error(error);
      this.correcting = false;
    });
  }

  getOpenedTasks(): Observable<any> {
    return Observable.create(observer => {
      from(this.activityService.getTasksHistory({
        processDefinitionKeyLike: 'mrobject_ObjectAdjustment%',
        processVariables: [{
          name: 'EntityIdVar',
          value: this.documentId,
          operation: 'equals',
          type: 'string'
        }, {
          name: 'ExecutorVar',
          value: this.session.login(),
          operation: 'equals',
          type: 'string'
        }]
      })).subscribe(res => {
        let tasks = [];
        if (res && res.data && res.data.length) {
          tasks = res.data.filter(i => !i.endTime);
        }
        observer.next(tasks);
      }, error => this.helper.error(error), () => {
        observer.complete();
      });
    });
  }
}
