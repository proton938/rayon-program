import {Component, Input} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {copy} from 'angular';
import {Observable} from "rxjs/Rx";
import {RayonLinkResourceService} from "../../../services/rayon-link-resource.service";
import {RayonProgram} from "../../../models/program/RayonProgram";
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {some, find, isArray, isEmpty} from 'lodash';
import {from, of} from "rxjs/index";
import {map} from "rxjs/internal/operators";
import {SolrMediatorService} from "../../../services/solr-mediator.service";

@Component({
  selector: 'object-header-labels',
  templateUrl: './object-header-labels.component.html'
})
export class ObjectHeaderLabelsComponent {
  loading: boolean = true;
  labels: any[] = [];
  labelSettings: any[] = [];
  labelSettingsDictName: string = 'mr_program_objectLabelSettings';
  links: any[] = [];
  linked: any = {};

  @Input() document: RayonProgram;

  constructor(private nsi: NsiResourceService,
              private programService: RayonProgramResourceService,
              private linkService: RayonLinkResourceService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    this.getDemoSettings().subscribe(res => {
      this.labelSettings = res.sort((a, b) => a.sortValue - b.sortValue);
      if (some(this.labelSettings)) {
        this.linkService.find(this.document.documentId).subscribe(res => {
          this.links = res;
          this.init();
        });
      } else {
        this.init();
      }
    });
  }

  init() {
    if (this.labelSettings) {
      this.labelSettings.forEach(this.labelEachHandler.bind(this));
    }
  }

  labelEachHandler(item) {
    if (item) {
      if (item.type) switch(item.type.code) {
        case 'master-plan':
          this.initLabelMasterPlan(item);
          break;
      } else if (item.field) {
        this.initField(item);
      }
    }
  }

  initField(data) {
    let label = this.makeLabelData(data);
    this.labels.push(label);
    let field = this.getField(data.field);
    if (!field  || (isArray(field) && field.length)) {
      label.show = false;
    }
    label.loading = false;
    this.updateGlobalLoading();
  }

  initLabelMasterPlan(data) {
    let label = this.makeLabelData(data);
    let linkType: string = 'PLAN';
    let update = () => {
      if (find(this.linked[linkType], i => i.statusCode === 'active')) label.color = 'primary';
      else if (find(this.linked[linkType], i => i.statusCode === 'archive')) label.color = 'default';
      else label.show = false;
      label.loading = false;
      this.updateGlobalLoading();
    };
    this.labels.push(label);
    if (linkType) {
      if (!this.linked[linkType]) {
        this.linked[linkType] = [];
        this.getSolrByIds(linkType).subscribe(res => {
          if (res && res.docs) this.linked[linkType] = res.docs;
          update();
        })
      } else {
        update();
      }
    }
  }

  getSolrByIds(type: string): Observable<any> {
     switch(type) {
       case 'PLAN':
         return this.solrMediator.getByIds([{ids: this.getIds(type), type: this.solrMediator.types.plan}]).pipe(map(res => res[0]));
       default:
         return from(of({}));
     }
  }

  getIds(type: string): string[] {
    let r: string[] = [];
    if (type) {
      r = this.links
        .filter(i => i.link.type1 === type || i.link.type2 === type)
        .map(i => find([i.link.id1, i.link.id2], id => id !== this.document.documentId));
    }
    return r;
  }

  makeLabelData(data: any): any {
    return {
      code: data.code,
      name: data.name,
      color: data.color,
      loading: true,
      show: true,
      showOnReady: data.showOnReady
    }
  }

  updateGlobalLoading() {
    setTimeout(() => {
      if (this.loading && !find(this.labels, i => i.loading)) this.loading = false;
    }, 0);
  }

  isShowLabel(label: any): boolean {
    let r: boolean = false;
    r = label.show && !label.loading;
    if (r) r = label.showOnReady || !this.loading;
    return r;
  }

  getField(path: string): any {
    let field: any = copy(this.document);
    let pathParts: string[] = path.split(/[\/\.]/);
    pathParts.forEach(i => {
      if (isArray(field)) {
        field = field.map(f => f && f[i] ? f[i] : null);
      } else {
        field = field[i];
      }
    });
    return field;
  }

  getDemoSettings(): Observable<any> {
    return Observable.create(observer => {
      let r: any[] = [
        {
          code: 'masterPlan', // уникальный код-идентификатор
          name: 'мастер-план', // текст лейбла
          field: '', // Путь к полю в текущем документе, через . или / в качестве разделителя (тип указывать не нужно)
          type: { // Особый тип лейбла (field указывать не нужно)
            code: 'master-plan',
            name: 'Мастер-план'
          },
          docType: { // Тип документа из справочника "DocumentTypes"
            code: 'MR_PROGRAM_OBJ',
            name: 'Объект по программе "Мой район"'
          },
          color: '',
          showOnReady: true, // Показывать ли лейблы по готовности
          sortValue: '010' // значение сортировки
        }/*,
        {
          code: 'mosRu',
          name: 'mos.ru',
          field: 'mos.public',
          type: '',
          color: 'primary',
          showOnReady: true,
          sortValue: '020'
        },
        {
          code: 'folderId',
          name: 'Alfresco',
          field: 'folderId',
          type: '',
          color: 'primary',
          showOnReady: true,
          sortValue: '005'
        }*/
      ];
      observer.next(r);
      observer.complete();
    });
  }
}
