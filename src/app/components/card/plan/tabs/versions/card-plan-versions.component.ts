import {Component} from "@angular/core";
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../../../services/plan-resource.service";
import {forkJoin} from "rxjs/index";
import {find, some} from 'lodash';
import {BsModalService} from "ngx-bootstrap";
import {NewPlanModalComponent} from "../../../../new-plan/new-plan-modal.component";
import {RayonHelperService} from "../../../../../services/rayon-helper.service";
import {StateService} from "@uirouter/core";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'card-plan-versions',
  templateUrl: './card-plan-versions.component.html'
})
export class CardPlanVersionsComponent {
  document: RayonPlan;
  versions: any[] = [];
  isLoading: boolean = true;
  isShow: any = {};
  expanded: boolean[] = [];

  constructor(private planService: PlanResourceService,
              private modalService: BsModalService,
              private helper: RayonHelperService,
              private stateService: StateService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.document = this.planService.document;
    this.getVersions();
  }

  getVersions() {
    this.isLoading = true;
    const versions$ = this.planService.getVersionsFromSolr(this.document.prefect.code, this.document.planYear);
    forkJoin([versions$]).subscribe((res: any) => {
      if (res[0] && res[0].docs) {
        this.versions = res[0].docs;
        this.expanded = this.versions.map(i => false);
        this.isShow.add = !find(this.versions, v => v.statusCode === 'project')
          && this.session.hasPermission('MR_PLAN_ADD_BUTTON');
        this.planService.getList(this.versions.map((i: any) => i.sys_documentId))
          .subscribe((listRes: RayonPlan[]) => {
          listRes.forEach(doc => {
            find(this.versions, v => v.sys_documentId === doc.documentId).document = doc;
          });
          this.isLoading = false;
        });
      } else {
        this.isLoading = false;
      }
    });
  }

  addPlan() {
    let modalRef = this.modalService.show(NewPlanModalComponent, {
      class: 'modal-lg',
      initialState: {
        prevId: this.versions[0]['sys_documentId'],
        saveCallback: () => {
          this.helper.success('Успешно!');
          this.getVersions();
        }
      }
    });
  }

  changeVersionExpand(index: number) {
    let v: any = this.versions[index];
    this.expanded[index] = !this.expanded[index];
    if (this.expanded[index] && !v.files && !v.filesLoading) {
      this.getVersionFiles(v.document.folderId, v.document.documentId);
    }
  }

  getVersionFiles(folderId: string, docId: string) {
    if (folderId && docId) {
      let v: any = find(this.versions, v => v.sys_documentId === docId);
      if (v.document && v.document.files && v.document.files.length) {
        v.filesLoading = true;
        this.planService.getAllFiles(folderId).subscribe(files => {
          v.files = files.filter(f => some(v.document.files, i => i === f.idFile));
          v.filesLoading = false;
        }, error => {
          v.filesLoadingError = 'Ошибка загрузки файлов';
        });
      }
    }
  }
}
