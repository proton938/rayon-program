import {Component} from "@angular/core";
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../../../services/plan-resource.service";

@Component({
  selector: 'card-plan-log',
  templateUrl: './card-plan-log.component.html'
})
export class CardPlanLogComponent {
  document: RayonPlan;

  constructor(private planService: PlanResourceService) {}

  ngOnInit() {
    this.document = this.planService.document;
  }
}
