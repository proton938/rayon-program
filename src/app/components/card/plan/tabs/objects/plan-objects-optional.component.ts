import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RayonPlan} from '../../../../../models/plan/RayonPlan';
import {PlanResourceService} from '../../../../../services/plan-resource.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {RayonLinkResourceService} from '../../../../../services/rayon-link-resource.service';
import {RayonHelperService} from '../../../../../services/rayon-helper.service';
import {Observable, forkJoin, from, of} from 'rxjs';
import {isArray, find, some, isEmpty, forIn, assignIn, compact} from 'lodash';
import {RayonWorkTypeService} from '../../../../../services/rayon-work-type.service';
import {copy} from 'angular';
import {SolrMediatorService} from '../../../../../services/solr-mediator.service';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {RayonProgramResourceService} from '../../../../../services/rayon-program-resource.service';
import {IKindRequestParams} from '../../../../../models/common/abstracts/IKindRequestParams';
import {mergeMap} from 'rxjs/internal/operators';
import {CommonQueryService} from '../../../../../services/common-query.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'plan-objects-optional',
  templateUrl: './plan-objects-optional.component.html',
  styleUrls: [
    './plan-objects-optional.component.scss'
  ]
})
export class PlanObjectsOptionalComponent implements OnInit {
  @Input() document: RayonPlan;
  @Input() readOnly: boolean;
  @Output() close = new EventEmitter();
  @Output() edit = new EventEmitter();
  loading = true;
  searchMode = 'types'; // 'types' or 'solr'
  filterCommon: any = {
    placeholder: 'Адрес, наименование или вид объекта или мероприятия, ответственная организация',
    search: '',
    objectTypes: [],
    workTypes: []
  };
  links: any[] = [];
  filter: any = {};
  filterWork: any = {};
  objects: any[] = [];
  pageObjects: any[] = [];
  works: any[] = [];
  showOnlyLinked: boolean;
  isShowContent = true;
  advancedSearch = true;
  advancedSearchExpand = false;
  sections: any[] = [];
  dicts: any = {};
  pagination: any = {
    totalItems: 0,
    itemsPerPage: 10,
    currentPage: 1,
    itemsPerPageList: [10, 20, 50, 100, null]
  };
  joinQueryTmpl = '{!join from=objectIdWork to=sys_documentId fromIndex=mr_work}';

  @BlockUI('block-ui-list') blockUI: NgBlockUI;

  constructor(private planService: PlanResourceService,
              private programService: RayonProgramResourceService,
              private linkService: RayonLinkResourceService,
              private helper: RayonHelperService,
              private solrMediator: SolrMediatorService,
              private workService: RayonWorkTypeService,
              private nsi: NsiResourceService,
              private commonQuery: CommonQueryService) {}

  ngOnInit() {
    this.dicts.loading = true;
    if (this.readOnly) {
      this.filterCommon.placeholder = 'Адрес, наименование или вид объекта или мероприятия, ' +
        'ответственная организация, вид работы';
    }
    from(this.nsi.getDictsFromCache([
      'mr_program_ObjectCategories',
      'mr_program_WorkTypes'
    ])).subscribe(res => {
      this.dicts = res;
      this.dicts.workTypes = [].concat(
        res['mr_program_WorkTypes'].sort(this.sortDict),
        [{name: 'Не определен вид работы', code: 'nowork'}, {name: 'Все', code: 'all'}]
      );
      this.dicts.objectTypes = [].concat(
        res['mr_program_ObjectCategories'].sort(this.sortDict),
        [{name: 'Не определен вид объекта', code: 'noobj'}, {name: 'Все', code: 'all'}]
      );
      this.dicts.loading = false;
    });
    this.linkService.find(this.document.documentId).subscribe(response => {
      this.links = response || [];
      if (this.readOnly) {
        this.updateDataReadOnly();
      } else if (this.loading) {
        this.loading = false;
      }
      // this.changePage({page: 1});
    }, error => {
      this.helper.error(error);
    });
    this.filter = this.filterDefault;
  }

  // каллбек изменения фильтров вида (объекта/работы)
  onChangeTypeFilter(type: string) {
    const filterName: string = {obj: 'objectTypes', work: 'workTypes'}[type];
    const val = this.filterCommon[filterName];
    if (!val.length) { this.pageObjects = []; }
    const specialCodes = val.map(i => i.code).reverse()
      .filter(i => some(['no' + type, 'all'], x => x === i));
    const clearAnotherValues = code => {
      this.filterCommon[filterName] = val.filter(i => i.code === code);
    };
    if (specialCodes.length) { clearAnotherValues(specialCodes[0]); }
    this.searchMode = 'types';
    this.pagination.currentPage = 1;
    this.updateData();
  }

  // Сортировка словарных значений по алфавиту по name
  sortDict(a: any, b: any) {
    if (a.name === b.name) { return 0; }
    return a.name > b.name ? 1 : -1;
  }

  updateData() {
    if (this.searchMode === 'types') {
      const params: IKindRequestParams = {
        kindObject: this.filterCommon.objectTypes.map(i => i.code),
        kindWork: this.filterCommon.workTypes.map(i => i.code),
        page: this.pagination.currentPage - 1,
        pageSize: this.pagination.itemsPerPage,
        sortType: 'DESC'
      };
      if (params.kindObject.length && params.kindWork.length) {
        const ids = this.links.length ? compact(this.links
          .map(i => find([i.link.id1, i.link.id2], r => r !== this.document.documentId))) : [];
        if (this.showOnlyLinked && ids.length) {
          this.updateOnlyLinkedTypes();
        } else {
          this.blockUI.start();
          if (some(params.kindObject, i => i === 'all')) {
            params.kindObject = this.dicts['mr_program_ObjectCategories'].map(i => i.code);
          }
          if (some(params.kindWork, i => i === 'all')) {
            params.kindWork = this.dicts['mr_program_WorkTypes'].map(i => i.code);
          }
          this.programService.findByKinds(params).subscribe(res => {
            this.blockUI.stop();
            this.pagination.totalItems = res.totalCount || 0;
            this.pageObjects = res && res.result && res.result.length ? res.result.map(i => {
              // forge solr fields:
              i.obj.addressObject = this.helper.getField(i.obj, 'address.address');
              i.obj.nameObject = this.helper.getField(i.obj, 'object.name');
              i.obj.kindNameObject = this.helper.getField(i.obj, 'object.kind.name');
              i.obj.organizationNameObject = this.helper.getField(i.obj, 'responsible.organization.name');

              i.obj.works = i.workDocuments ? i.workDocuments.map(w => {
                w.work.typeNameWork = this.helper.getField(w.work, 'type.name');
                w.work.objectIdWork = w.work.objectId;
                return w.work;
              }) : [];
              return i.obj;
            }) : [];
          }, error => {
            console.log(error);
            this.blockUI.stop();
          });
        }

      } else {
        // Недостаточно данных для поиска
      }
    } else {
      this.blockUI.start();
      if (this.pagination.currentPage === 1) {
        this.updateObjectList().subscribe(() => {
          this.updatePageObjects().subscribe(() => {
            this.blockUI.stop();
          }, error => {
            this.helper.error(error);
            this.blockUI.stop();
          });
        }, error => {
          console.log(error);
          this.blockUI.stop();
        });
      } else {
        this.updatePageObjects().subscribe(() => {
          this.blockUI.stop();
        }, error => {
          this.helper.error(error);
          this.blockUI.stop();
        });
      }
    }
  }

  // Update this.pageObjects from this.objects to current page
  updatePageObjects(): Observable<any> {
    const pageNumber = this.pagination.currentPage;
    const perPage = this.pagination.itemsPerPage;
    this.pageObjects = this.readOnly /*|| this.showOnlyLinked*/
      ? this.objects
      : this.objects.slice((pageNumber - 1) * perPage, pageNumber * perPage);
    return Observable.create(obs => {
      const workIds = [];
      this.pageObjects.forEach(p => [].push.apply(workIds, p.works.map(i => i.documentId)));
      if (workIds.length) {
        this.workService.list(workIds).subscribe(res => {
          this.pageObjects.forEach(p => {
            p.works.forEach(w => w = assignIn(w, find(res, f => f.documentId === w.documentId)));
          });
          obs.next(true);
        }, error => {
          obs.error(error);
        }, () => obs.complete());
      } else {
        obs.next(true);
      }
    });
  }

  updateOnlyLinkedTypes() {
    this.blockUI.start();
    const ids = this.links.length ? compact(this.links
      .map(i => find([i.link.id1, i.link.id2], r => r !== this.document.documentId))) : [];
    let objectKinds = this.filterCommon.objectTypes.map(i => i.code);
    let workKinds = this.filterCommon.workTypes.map(i => i.code);
    const params = {
      common: '',
      page: this.pagination.currentPage - 1,
      pageSize: this.pagination.itemsPerPage,
      types: [this.solrMediator.types.obj],
      filterQuery: [],
      query: '(documentId:' + ids.join(' OR documentId:') + ')'
    };
    if (objectKinds.length) {
      if (some(objectKinds, i => i === 'all')) {
        objectKinds = this.dicts['mr_program_ObjectCategories'].map(i => i.code);
      }
      params.query += (params.query ? ' AND ' : '')
        + '(kindCodeObject:(' + objectKinds.join(') OR kindCodeObject:(') + '))';
    }
    if (workKinds.length) {
      if (some(workKinds, i => i === 'all')) {
        workKinds = this.dicts['mr_program_WorkTypes'].map(i => i.code);
      }
      params.filterQuery.push(
        this.joinQueryTmpl
        + (some(workKinds, i => i === 'nowork')
          ? '-typeCodeWork:*'
          : 'typeCodeWork:(' + workKinds.join(' OR ') + ')')
        + (ids.length ? ' AND (sys_documentId:' + ids.join(' OR sys_documentId:') + ')' : '')
      );
    }

    this.solrMediator.query(params).pipe(
      mergeMap(res => {
        this.objects = res && res.docs ? res.docs : [];
        params.types = [this.solrMediator.types.work];
        params.query = this.objects.length
          ? '(objectIdWork:' + this.objects.map(d => d.documentId).join(' OR objectIdWork:') + ')'
          : '';
        if (params.filterQuery.length) {
          params.filterQuery.forEach(i => {
            let value = i.replace(this.joinQueryTmpl, '');
            params.query += (params.query ? ' AND ' : '')
              + (value === 'typeCodeWork:(!*)' ? '-typeCodeWork:*' : value);
          });
        }
        params.filterQuery = [];
        return this.objects.length ? this.solrMediator.query(params) : of({docs: []});
      }),
      mergeMap(res => {
        this.works = res && res.docs ? res.docs : [];
        this.objects.forEach(o => {
          o.works = this.works.filter(w => w.objectIdWork === o.documentId);
        });
        return this.updatePageObjects();
      })
    ).subscribe(res => {
      this.pagination.totalItems = this.objects.length;
      this.blockUI.stop();
    }, error => {
      this.helper.error(error);
      this.blockUI.stop();
    });
  }

  onSelectItemsPerPage() {
    this.changePage({page: 1}, true);
  }

  changePage($event, newSearch?: boolean) {
    this.blockUI.start();
    this.pagination.currentPage = $event.page;
    if (this.readOnly) {
      return this.updateDataReadOnly();
    }/* else if (this.showOnlyLinked) {
      this.updateObjectList().subscribe(() => {
        this.updatePageObjects().subscribe(() => {
          this.blockUI.stop();
        });
      });
      return;
    }*/
    switch (this.searchMode) {
      case 'types':
        this.updateData();
        break;

      default:
        if (newSearch) {
          this.updateData();
        } else {
          this.updatePageObjects().subscribe(() => {
            this.blockUI.stop();
          }, error => {
            this.helper.error(error);
            this.blockUI.stop();
          });
        }
        break;
    }
  }

  updateDataReadOnly() {
    this.updateObjectList().subscribe(() => {
      this.sections = this.getGroupedObjectsByWorkTypes();
      if (this.loading) { this.loading = false; }
      this.blockUI.stop();
    }, error => {
      if (this.loading) { this.loading = false; }
      this.helper.error(error);
      this.blockUI.stop();
    });
  }

  search() {
    this.searchMode = 'solr';
    return this.changePage({page: 1}, true);
  }

  clearFilter() {
    this.filterCommon.objectTypes = [];
    this.filterCommon.workTypes = [];
    this.pageObjects = [];
    this.searchMode = 'types';
  }

  editTab() {
    this.edit.emit();
  }

  updateObjectList(): Observable<any> {
    return Observable.create(observer => {
      const makeEmptyResult = () => {
        this.objects = this.works = [];
        observer.next();
        observer.complete();
      };
      const makeResult = result => {
        this.objects = result && result.docs ? result.docs : [];
        this.updateWorkList().subscribe(() => observer.next(), error => observer.error(error), () => observer.complete());
      };
      if (this.readOnly || this.showOnlyLinked) {
        const ids = this.links.length ? this.links
          .map(i => find([i.link.id1, i.link.id2], r => r !== this.document.documentId)) : [];
        if (ids.length) {
          const docIdsQuery: string = 'documentId:' + ids.join(' OR documentId:');
          const queryParams: any = {
            common: this.filterCommon.search,
            page: 0, pageSize: ids.length, types: [this.solrMediator.types.obj],
            query: this.filterQueryString // 'documentId:' + ids.join(' OR documentId:')
          };
          this.addWorkFilter(queryParams);
          queryParams.query = queryParams.query
            ? '(' + queryParams.query + ') AND (' + docIdsQuery + ')'
            : '(' + docIdsQuery + ')';
          this.solrMediator.query(queryParams).pipe(
            mergeMap(res => {
              if (this.filterCommon.search && !res.numFound) {
                return this.solrMediator.query({
                  common: '', page: 0, pageSize: ids.length, types: [this.solrMediator.types.obj],
                  query: docIdsQuery,
                  filterQuery: [
                    this.joinQueryTmpl + this.commonQuery.commonToQuery(this.filterCommon.search, [
                      {code: 'typeNameWork'}
                    ], true)
                  ]
                });
              } else {
                return of(res);
              }
            })
          ).subscribe(res => {
            this.objects = res && res.docs ? res.docs : [];
            if (this.readOnly) {
              queryParams.common = '';
              queryParams.types = [this.solrMediator.types.work];
              queryParams.query = docIdsQuery;
              queryParams.filterQuery = [];
              this.solrMediator.query(queryParams).pipe(
                mergeMap(workRes => {
                  this.works = workRes && workRes.docs ? workRes.docs : [];
                  return this.assignWorksData();
                })
              ).subscribe(() => {
                observer.next();
                observer.complete();
              }, error => {
                observer.error(error);
                observer.complete();
              });
            } else {
              this.updateWorkList().subscribe(() => observer.next(), error => observer.error(error), () => observer.complete());
            }
          }, error => {
            observer.error(error);
            observer.complete();
          });
        } else {
          this.objects = [];
          this.works = [];
          observer.next();
          observer.complete();
        }
      } else {
        const filter: any = this.getObjectListQueryParams(1);
        if (!filter.pageSize) {
          makeEmptyResult();
        } else {
          this.solrMediator.query(filter).subscribe(res => {
            if (filter.pageSize === 1) {
              if (res && res.numFound) {
                filter.pageSize = res.numFound;
                this.getSolrParts(filter).subscribe(deepRes => {
                  makeResult(deepRes);
                }, error => {
                  observer.error(error);
                  observer.complete();
                });
              } else {
                makeEmptyResult();
              }
            } else {
              makeResult(res);
            }
          }, error => {
            observer.error(error);
            observer.complete();
          });
        }
      }
    });
  }

  updateWorkList(): Observable<any> {
    return Observable.create(observer => {
      const ids: string[] = this.objects.map(i => i.documentId);
      if (ids.length) {
        this.getSolrPartsByIds([this.solrMediator.types.work], ids, 'objectIdWork').subscribe(res => {
          this.works = res && res.docs ? res.docs : [];
          console.log('Objects:', this.objects);
          console.log('Works:', this.works);
          if (this.works.length) {
            this.objects.forEach(o => {
              o.works = this.works.filter(w => w.objectIdWork === o.documentId);
            });
            this.objects = this.objects.filter(i => i.works.length);
            this.pagination.totalItems = this.objects.length;
          }
          observer.next();
          /*this.getWorksData().subscribe(works => {
            if (works) {
              works.forEach(w => {
                this.works.filter(i => i.documentId === w.documentId).forEach(i => assignIn(i, w));
              });
              console.log('Objects:', this.objects);
              console.log('Works:', this.works);
            }
            observer.next();
          }, error => observer.error(error), () => observer.complete());*/
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        this.objects = [];
        this.works = [];
        this.pagination.totalItems = this.objects.length;
        observer.next();
        observer.complete();
      }
    });
  }

  assignWorksData(): Observable<any> {
    return Observable.create(observer => {
      this.getWorksData().subscribe(works => {
        if (works) {
          works.forEach(w => {
            this.works.filter(i => i.documentId === w.documentId).forEach(i => assignIn(i, w));
          });
        }
        observer.next();
      }, error => observer.error(error), () => observer.complete());
    });
  }

  getWorksData(): Observable<any> {
    return Observable.create(observer => {
      const ids: string[] = this.works.map(i => i.documentId);
      this.works.forEach(i => {
        if (ids.indexOf(i.documentId) < 0) { ids.push(i.documentId); }
      });
      this.workService.list(ids).subscribe(
        res => observer.next(res),
        error => observer.error(error),
        () => observer.complete());
    });
  }

  getObjectListQueryParams(pageSize: number = 9999): any {
    // if pageSize === 0 - empty results
    // if pageSize === 1 - all results on the one page
    // else - pageSize = pagination.itemsPerPage
    const result: any = {
      common: this.filterCommon.search,
      page: 0,
      types: [this.solrMediator.types.obj],
      pageSize: pageSize,
      query: '',
      filterQuery: []
    };
    if (this.showOnlyLinked) {
      const ids = this.links.length ? this.links.map(i => find([i.link.id1, i.link.id2], r => r !== this.document.documentId)) : [];
      if (ids.length) {
        result.pageSize = ids.length;
        result.query = 'documentId:' + ids.join(' OR documentId:');
      } else {
        result.pageSize = 0;
      }
    } else {
      if (!result.pageSize) { result.pageSize = 1; }
      result.query = this.filterQueryString;
      this.addWorkFilter(result);
      // result.query += (result.query ? ' AND ' : '') + '(dominantObject:false)';
      // result.query += (result.query ? ' AND ' : '') + '(commercialObject:false)';
      // result.query += (result.query ? ' AND ' : '') + '(archiveObject:false)';
    }
    return result;
  }

  addWorkFilter(queryParams: any): void {
    if (!queryParams.filterQuery) { queryParams.filterQuery = []; }
    if (this.filterWork.organizationCodeWork && this.filterWork.organizationCodeWork.length) {
      queryParams.filterQuery.push(this.joinQueryTmpl
        + 'organizationCodeWork:(' + this.filterWork.organizationCodeWork.join(' OR ') + ')');
    }
    if (this.filterWork.typeCodeWork && this.filterWork.typeCodeWork.length) {
      queryParams.filterQuery.push(this.joinQueryTmpl
        + 'typeCodeWork:(' + this.filterWork.typeCodeWork.join(' OR ') + ')');
    }
    if (!queryParams.filterQuery.length) { queryParams.filterQuery.push(this.joinQueryTmpl + '*:*'); }
  }

  getLinksByStatus(status: string | string[], docTypes = ['OBJECT', 'WORK']): any[] {
    const statuses: string[] = isArray(status) ? status : [status];
    return this.links.filter(i => {
      return some(statuses, s => s === i.link.status);
    });
  }

  getLink(id: string) {
    return find(this.links, (i: any) => i.link.id1 === id || i.link.id2 === id);
  }

  hasLink(id: string, includeRejected: boolean = false) {
    return some(this.links, (i: any) => {
      return some([i.link.id1, i.link.id2], x => x === id)
        && (includeRejected ? true : i.link.status !== 'reject');
    });
  }

  someLinks(ids: string[] = [], includeRejected: boolean = false): boolean {
    return some(ids, id => this.hasLink(id, includeRejected));
  }

  isRejected(id: string) {
    return some(this.links, (i: any) => (i.link.id1 === id || i.link.id2 === id) && i.link.status === 'reject');
  }

  getDocType(doc: any): string {
    let r = 'OBJECT';
    if (doc.sys_type) {
      r = doc.sys_type === this.solrMediator.types.work ? 'WORK' : 'OBJECT';
    } else if (doc.objectId) {
      r = 'WORK';
    }
    return r;
  }

  add(doc: any) {
    const startLinking = () => doc.linking = true;
    const finishLinking = () => doc.linking = false;
    if (this.isRejected(doc.documentId)) { // if link exists but it has status "reject"
      startLinking();
      this.linkService.changeStatus(this.getLink(doc.documentId).link, 'approve').subscribe(() => {
        if (this.getDocType(doc) === 'WORK') {
          if (!this.hasLink(doc.objectId)) {
            const objectDoc = find(this.pageObjects, i => i.documentId === doc.objectIdWork);
            if (objectDoc) { this.add(objectDoc); }
          }
        }
        finishLinking();
      });
    } else if (!this.hasLink(doc.documentId)) {
      startLinking();
      this.createLink(doc).subscribe(res => {
        this.links.push(res);
        if (this.getDocType(doc) === 'WORK') {
          if (!this.hasLink(doc.objectId)) {
            const objectDoc = find(this.pageObjects, i => i.documentId === doc.objectIdWork);
            if (objectDoc) { this.add(objectDoc); }
          }
          finishLinking();
        }
      });
    }
  }

  createLink(doc: any): Observable<any> {
    return this.linkService.create({
      link: {
        id1: this.document.documentId,
        id2: doc.documentId,
        system1: 'mr',
        system2: 'mr',
        subsystem1: 'program',
        subsystem2: 'program',
        type1: 'PLAN',
        type2: this.getDocType(doc),
        status: 'review'
      }
    });
  }

  remove(doc: any) {
    doc.linking = true;
    const link = find(this.links, i => i.link.id1 === doc.documentId || i.link.id2 === doc.documentId);
    const linkId = link ? link.link.documentId : null;
    const rejecting: boolean = link && link.link.status === 'approve';
    if (linkId) {
      const action$ = rejecting ? this.linkService.changeStatus(link.link, 'reject') : this.linkService.deleteLink([linkId]);
      action$.subscribe(() => {
        if (!rejecting) { this.links.splice(this.links.map(i => i.link.documentId).indexOf(linkId), 1); }
        if (this.getDocType(doc) === 'WORK') {
          const hasWorks = this.someLinks(this.works.filter(w => w.objectIdWork === doc.objectIdWork).map(w => w.documentId));
          if (!hasWorks && this.hasLink(doc.objectIdWork)) {
            const obj = find(this.pageObjects, o => o.documentId === doc.objectIdWork);
            if (obj) { this.remove(obj); }
          }
        }
        doc.linking = false;
      });
    }
  }

  getSolrParts(requestData: any): Observable<any> {
    const maxPage = 5000;
    if (requestData.pageSize > maxPage) {
      return Observable.create(observer => {
        const requests$ = [], stepRequestData: any = copy(requestData),
          stopLength: number = Math.ceil(requestData.pageSize / maxPage);
        let result: any = null;
        const runStep = () => {
          if (requests$.length) {
            const step: any = requests$.splice(0, 1)[0];
            this.solrMediator.query(step).subscribe(res => {
              if (!result) {
                result = res;
              } else if (res.docs) { result.docs.push(...res.docs); }
              if (!result.docs) { result.docs = []; }
              runStep();
            }, error => {
              console.log(error);
              runStep();
            });
          } else {
            observer.next(result || {numFound: 0});
            observer.complete();
          }
        };
        stepRequestData.pageSize = maxPage;
        while (requests$.length < stopLength) {
          requests$.push(assignIn(copy(stepRequestData), {page: requests$.length}));
        }
        if (requests$.length) {
          // runStep();
          forkJoin(requests$.map(rq => this.solrMediator.query(rq))).subscribe(res => {
            let r: any = {};
            if (res && res.length) {
              res.forEach((item, index) => {
                if (!index) {
                  r = item;
                  if (!r.docs) { r.docs = []; }
                } else if (item.docs) {
                  r.docs.push(...item.docs);
                }
              });
              observer.next(r);
              observer.complete();
            } else {
              observer.next({numFound: 0});
              observer.complete();
            }
          });
        } else {
          observer.next({numFound: 0});
          observer.complete();
        }
      });
    } else {
      return this.solrMediator.query(requestData);
    }
  }

  getSolrPartsByIds(types: string[], ids: string[], propName: string = 'id'): Observable<any> {
    // console.log('getSolrPartsByIds:', ids.length);
    const maxIdsPage = 1000;
    if (ids.length) {
      const queryParams: any = {
        page: 0, pageSize: 1, types: types, query: ''
      };
      return Observable.create(observer => {
        if (ids.length > maxIdsPage) {
          const requestData$ = [];
          const idPack = copy(ids);
          while (idPack.length) {
            requestData$.push(assignIn(copy(queryParams), {
              query: propName + ':' + idPack.splice(0, idPack.length > maxIdsPage ? maxIdsPage : idPack.length)
                .join(' OR ' + propName + ':')
            }));
          }
          console.log(requestData$);
          forkJoin(requestData$.map(rq => this.solrMediator.query(rq))).subscribe(resOne => {
            if (resOne && resOne.length) {
              resOne.forEach((item, index) => {
                if (item && item.numFound) { requestData$[index].pageSize = item.numFound; }
              });
              forkJoin(requestData$.filter(rq => rq.pageSize).map(rq => this.getSolrParts(rq))).subscribe(res => {
                let r: any = {};
                if (res && res.length) {
                  res.forEach((w, wi) => {
                    if (!wi) {
                      r = w;
                      if (!r.docs) { r.docs = []; }
                    } else if (w.docs) {
                      r.docs.push(...w.docs);
                    }
                  });
                }
                observer.next(r);
                observer.complete();
              }, error => {
                observer.error(error);
                observer.complete();
              });
            } else {
              observer.next({numFound: 0});
              observer.complete();
            }
          });
        } else {
          queryParams.query = propName + ':' + ids.join(' OR ' + propName + ':');
          this.solrMediator.query(queryParams).subscribe(resOne => {
            if (resOne && resOne.numFound) {
              queryParams.pageSize = resOne.numFound;
              this.getSolrParts(queryParams).subscribe(res => {
                observer.next(res);
                observer.complete();
              }, error => {
                observer.error(error);
                observer.complete();
              });
            } else {
              observer.next({numFound: 0});
              observer.complete();
            }
          });
        }
      });
    }
  }

  getGroupedObjectsByWorkTypes() {
    const r: any[] = [];
    const objects = this.showOnlyLinked ? this.objects.filter(o => this.hasLink(o.documentId)) : this.objects;
    this.works.forEach((w: any) => {
      if (!w.typeCodeWork) {
        w.typeCodeWork = 'unknown';
        w.typeNameWork = 'Не определен вид работы';
      }
      const object: any = copy(find(objects, o => o.documentId === w.objectIdWork));
      if (object) {
        if (!object.kindCodeObject) {
          object.kindCodeObject = 'unknown';
          object.kindNameObject = 'Не определен вид объекта';
        }
        let container: any = find(r, i => i.workType.code === w.typeCodeWork && i.objectKind.code === object.kindCodeObject);
        if (container) {
          let localObject: any = find(container.objects, i => i.documentId === object.documentId);
          if (!localObject) {
            localObject = object;
            container.objects.push(localObject);
          }
          if (!localObject.works) { localObject.works = []; }
          localObject.works.push(w);
        } else {
          if (!object.works) { object.works = []; }
          object.works.push(w);
          container = {
            workType: { code: w.typeCodeWork, name: w.typeNameWork },
            objectKind: { code: object.kindCodeObject, name: object.kindNameObject},
            objects: [object],
            expanded: false
          };
          r.push(container);
        }
      }
    });
    r.sort(this.sectionSort);
    return r;
  }

  sectionSort(a, b) {
    let result = 0;
    const isUnknownObjectKind = item => item.objectKind.code === 'unknown';
    const isUnknownWorkType = item => item.workType.code === 'unknown';

    if (isUnknownObjectKind(a) && !isUnknownObjectKind(b)) { return  1; }
    if (isUnknownObjectKind(b) && !isUnknownObjectKind(a)) { return -1; }
    if (isUnknownObjectKind(a) && isUnknownWorkType(a)) { return  1; }
    if (isUnknownObjectKind(b) && isUnknownWorkType(b)) { return -1; }

    if (a.objectKind.name > b.objectKind.name) {
      result = 1;
    } else if (a.objectKind.name < b.objectKind.name) {
      result = -1;
    } else {
      if (a.workType.name > b.workType.name) {
        result = 1;
      } else if (a.workType.name < b.workType.name) {
        result = -1;
      } else {
        result = 0;
      }
    }
    return result;
  }

  isDisabledWork(doc: any): boolean {
    return false;
  }

  expandAll(expanded) {
    this.isShowContent = false;
    this.sections.forEach(s => {
      s.expanded = expanded;
    });
    setTimeout(() => {
      this.isShowContent = true;
    }, 0);
  }

  save() {
    this.blockUI.start();
    const linkIds: any = {
      toApprove: this.getLinksByStatus('review').map(i => i.link.documentId),
      toRemove: this.getLinksByStatus('reject').map(i => i.link.documentId)
    };
    if (linkIds.toApprove.length || linkIds.toRemove.length) {
      const request = this.helper.getField(this.document, 'statusId.code') === 'project'
        ? this.modifyLinksAfterSave()
        : this.planService.updateMasterPlanMarks(linkIds.toApprove, linkIds.toRemove);
      request.subscribe(() => {
        this.blockUI.stop();
        this.close.emit();
      }, error => {
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.blockUI.stop();
      this.close.emit();
    }
  }

  modifyLinksAfterSave(): Observable<any> { // only for status 'project'
    const links: any = {
      toApprove: this.getLinksByStatus('review'),
      toRemove: this.getLinksByStatus('reject')
    };
    const requests$ = [];
    if (links.toApprove.length) {
      requests$.push(...links.toApprove.map(i => this.linkService.changeStatus(i.link, 'approve')));
    }
    if (links.toRemove) { requests$.push(this.linkService.deleteLink(links.toRemove.map(i => i.link.documentId))); }
    return requests$.length ? forkJoin(requests$) : from(of('ok'));
  }

  cancel() {
    this.blockUI.start();
    const docTypes: string[] = ['OBJECT', 'WORK'];
    const actions: any[] = [];
    let ids: string[] = [];
    const getList = status => this.getLinksByStatus(status, docTypes) || [];

    ids = getList('review').map(i => i.link.documentId);
    if (ids.length) { actions.push(this.linkService.deleteLink(ids)); }
    const linksToRepair: any[] = getList('reject').map(i => i.link);
    if (linksToRepair.length) {
      linksToRepair.forEach(i => actions.push(this.linkService.changeStatus(i, 'approve')));
    }

    if (actions.length) {
      forkJoin(actions).subscribe(() => {
        this.blockUI.stop();
        this.close.emit();
      }, error => {
        this.blockUI.stop();
        this.helper.error(error);
      });
    } else {
      this.blockUI.stop();
      this.close.emit();
    }
  }

  changeOnlyLinked() {
    this.showOnlyLinked = !this.showOnlyLinked;
    this.changePage({page: 1}, true);
    // this.sections = this.getGroupedObjectsByWorkTypes();
  }

  getWorkCount(section): number {
    return [].concat.apply([], section.objects.map(o => o.works || [])).length;
  }

  get isEmptyFilter(): boolean {
    let r = false;
    forIn(this.filter, value => {
      if (!r && !!value) { r = isEmpty(value); }
    });
    return r;
  }

  get filterDefault(): any {
    const r: any = {};
    if (!this.readOnly) { r.prefectsCodeObject = [this.document.prefect.code]; }
    return r;
  }

  get filterQueryString(): string {
    let r = '';
    const _and = () => r ? ' AND ' : '';
    const _stars = str => '(*' + str.split(/\s+/).join('* AND *') + '*)';
    const _str = str => str && /\s+/.test(str) ? _stars(str) : str;

    forIn(this.filter, (value: any, key: string) => {
      switch (key) {
        case 'prefectsCodeObject':
        case 'districtsCodeObject':
        case 'objectFormNameObject':
        case 'kindCodeObject':
        case 'organizationCodeObject':
        case 'specialCodeObject':
          if (this.filter[key].length) {
            r += _and() + '(' + key + ':(' + value.join(') OR ' + key + ':(') + '))';
          }
          break;
        // string fields:
        case 'addressObject':
        case 'nameObject':
          r += _and() + '(' + key + ':' + _stars(value) + ')';
          break;
        // boolean fields:
        case 'problemObject':
        case 'attractionObject':
        case 'masterplanObject':
        case 'publicObject':
        case 'mosPublicObject':
        case 'oknObject':
          if (value) { r += _and() + '(' + key + ':' + value + ')'; }
          break;
        default:
          if (!isEmpty(value) || value === true) {
            r += _and() + '(' + key + ':' + _str(value) + ')';
          }
          break;
      }
    });

    return r;
  }

  get linkedObjects(): number {
    return this.links.filter(lnk => lnk.link.type2 === 'OBJECT' && lnk.link.status !== 'reject').length;
  }

  get linkedWorks(): number {
    return this.links.filter(lnk => lnk.link.type2 === 'WORK' && lnk.link.status !== 'reject').length;
  }
}
