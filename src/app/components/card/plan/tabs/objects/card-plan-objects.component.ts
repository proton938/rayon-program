import {Component} from "@angular/core";
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../../../services/plan-resource.service";
import {StateService, Transition} from "@uirouter/core";

@Component({
  selector: 'card-plan-objects',
  templateUrl: './card-plan-objects.component.html'
})
export class CardPlanObjectsComponent {
  document: RayonPlan;
  modeParam: string;

  constructor(private planService: PlanResourceService,
              private transition: Transition,
              private state: StateService) {}

  ngOnInit() {
    this.document = this.planService.document;
    this.modeParam = this.transition.params()['mode'];
  }

  goToEditMode() {
    this.state.go(this.state.current, {mode: 'edit'});
  }

  goToViewMode() {
    this.state.go(this.state.current, {mode: 'view'});
  }
}
