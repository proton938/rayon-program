import {Component, EventEmitter, Input, Output} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {isArray, isBoolean, isString, some, forIn} from 'lodash';
import {Observable} from "rxjs/Rx";
import {from} from "rxjs/index";

@Component({
  selector: 'plan-objects-filter',
  templateUrl: './plan-objects-filter.component.html'
})
export class PlanObjectsFilterComponent {
  loading: boolean = true;
  cleaning: boolean = false;
  waiting: boolean = false;
  available: any = {
    districts: [],
    kinds: []
  };

  @Input() filter: any = {};
  @Input() filterWork: any = {};
  @Input() list: any = {};
  @Input() expanded: boolean;
  @Input() showSpecific: boolean = false;
  @Output() filterChange: any;
  @Output() filterChanged = new EventEmitter();
  @Output() filterCleared = new EventEmitter();

  constructor(private nsi: NsiResourceService) {}

  ngOnInit() {
    (<any>window).filter = this.filter;
    this.getList([
      'Prefect',
      'District',
      'mr_program_ObjectForms',
      'mr_program_ObjectIndustries',
      'mr_program_ObjectCategories',
      'mr_program_ResponsibleOrganizations',
      'mr_program_SpecialObject',
      'mr_program_WorkTypes'
    ]).subscribe(() => {
      this.updateList();
      this.loading = false;
    });
  }

  onFilterChange() {
    this.filterChanged.emit();
  }

  clearFilters() {
    this.cleaning = true;
    this.clearFilterContainer(this.filter);
    this.clearFilterContainer(this.filterWork);
    this.filterCleared.emit();
    this.updateList();
    setTimeout(() => {this.cleaning = false}, 0);
  }

  clearFilterContainer(container: any): void {
    forIn(container, (value, key) => {
      if (isArray(value)) {
        container[key] = [];
      } else if (isBoolean(value)) {
        container[key] = false;
      } else if (isString(value)) {
        container[key] = '';
      }
    });
  }

  getList(dictNames: string[] = []): Observable<any> {
    return Observable.create(observer => {
      if (dictNames.length) {
        from(this.nsi.getDictsFromCache(dictNames)).subscribe(res => {
          if (res) {
            dictNames.forEach(name => {
              if (!this.list[name] && res[name]) this.list[name] = res[name];
            });
          }
          observer.next();
          observer.complete();
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next();
        observer.complete();
      }
    });
  }

  updateList() {
    // update values if it's necessary
    this.updateDistricts();
    this.updateKinds();
  }

  updateDistricts() {
    this.available.districts = this.districts;
  }

  updateKinds() {
    this.available.kinds = this.kinds;
  }

  changeSpecial(target: string) {
    switch(target) {
      case 'specialNameObject':
        if (this.filter[target] && this.filter.specialNameObjectExcept) {
          this.waiting = true;
          this.filter.specialNameObjectExcept = false;
          setTimeout(() => {this.waiting = false}, 0);
        }
        break;
      case 'specialNameObjectExcept':
        if (this.filter[target] && this.filter.specialNameObject) {
          this.waiting = true;
          this.filter.specialNameObject = false;
          setTimeout(() => {this.waiting = false}, 0);
        }
        break;
    }
  }

  get districts(): any[] {
    let result: any[] = [];
    if (this.filter.prefectsCodeObject && this.filter.prefectsCodeObject.length) {
      result = this.list.District
        .filter(d => this.filter.prefectsCodeObject.filter(p => p === d.perfectId[0].code).length);
      if (this.filter.districtsCodeObject && this.filter.districtsCodeObject.length) {
        this.filter.districtsCodeObject = this.filter.districtsCodeObject
          .filter(i => result.filter(f => f.code === i).length);
      }
    } else {
      result = this.list.District;
    }
    return result;
  }

  get kinds(): any[] {
    let dic: any[] = this.list.mr_program_ObjectCategories;
    let result: any[] = dic ? dic.filter(d => {
      return this.filter.industryCodeObject && this.filter.industryCodeObject.length
        ? this.filter.industryCodeObject.filter(p => some(d.industry, ind => ind.code === p)).length
        : true;
    }) : [];

    if (this.filter.kindCodeObject && this.filter.kindCodeObject.length) {
      this.filter.kindCodeObject = this.filter.kindCodeObject
        .filter(i => result.filter(f => f.code === i).length);
    }
    return result;
  }
}
