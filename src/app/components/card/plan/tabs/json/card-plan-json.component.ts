import {Component} from "@angular/core";
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../../../services/plan-resource.service";

@Component({
  selector: 'card-plan-json',
  templateUrl: './card-plan-json.component.html'
})
export class CardPlanJsonComponent {
  document: RayonPlan;

  constructor(public planService: PlanResourceService) {}

  ngOnInit() {
    this.document = this.planService.document;
  }
}
