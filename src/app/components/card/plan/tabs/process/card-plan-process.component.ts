import {Component} from "@angular/core";
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../../../services/plan-resource.service";

@Component({
  selector: 'card-plan-process',
  templateUrl: './card-plan-process.component.html'
})
export class CardPlanProcessComponent {
  document: RayonPlan;
  modeParam: string;
  isLoading: boolean = true;
  config: any;
  constructor(private planService: PlanResourceService) {}

  ngOnInit() {
    this.document = this.planService.document;
    this.config = {
      sysName: 'mrplan',
      linkVarValue: this.document.documentId,
      defaultVars: [{
        name: 'EntityIdVar',
        value: this.document.documentId
      }]
    };
    this.isLoading = false;
  }
}
