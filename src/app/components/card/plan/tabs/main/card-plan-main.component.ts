import {Component} from "@angular/core";
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {StateService, Transition} from "@uirouter/core";
import {PlanResourceService} from "../../../../../services/plan-resource.service";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {SessionStorage} from "@reinform-cdp/security";
import {some} from 'lodash';
import {RayonHelperService} from "../../../../../services/rayon-helper.service";

@Component({
  selector: 'card-plan-main',
  templateUrl: './card-plan-main.component.html'
})
export class CardPlanMainComponent {
  document: RayonPlan;
  modeParam: string;
  isLoading: boolean = true;
  files: any[] = [];

  constructor(private transition: Transition,
              private planService: PlanResourceService,
              private fileResourceService: FileResourceService,
              private session: SessionStorage,
              private helper: RayonHelperService,
              public state: StateService) {}

  ngOnInit() {
    this.document = this.planService.document;
    this.modeParam = this.transition.params()['mode'];
    this.getFiles();
    this.isLoading = false;
  }

  getFiles() {
    this.planService.getAllFiles(this.document.folderId).subscribe((res: any[]) => {
      this.files = res ? res.filter(f => some(this.document.files, i => i === f.idFile)) : [];
    });
  }

  isShowEditBtn(): boolean {
    let r: boolean = false;
    if (this.document && this.document.statusId && this.document.statusId.code) {
      switch(this.document.statusId.code) {
        case 'project':
          r = this.session.hasPermission('MR_PLAN_CARDEDIT_BUTTON');
          break;
        default:
          r = this.session.hasPermission('MR_PLAN_EDITARCACT_BUTTON');
          break;
      }
    }
    return r;
  }

  approveStop() {
    this.helper.success('Готово!');
    this.state.go(this.state.current, {}, {reload: true});
  }
}
