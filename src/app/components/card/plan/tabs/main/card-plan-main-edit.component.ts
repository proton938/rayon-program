import {Component, Input} from "@angular/core";
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonHelperService} from "../../../../../services/rayon-helper.service";
import {StateService} from "@uirouter/core";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {copy} from 'angular';
import {SessionStorage} from "@reinform-cdp/security";
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../../../services/plan-resource.service";
import {BsModalService} from "ngx-bootstrap";
import {NewPlanModalComponent} from "../../../../new-plan/new-plan-modal.component";

@Component({
  selector: 'card-plan-main-edit',
  templateUrl: './card-plan-main-edit.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class CardPlanMainEditComponent {
  document: RayonPlan;
  old: RayonPlan;
  validate: boolean;
  isShowButtons: boolean;

  @Input() files: any;

  @BlockUI('card-main-edit') blockUI: NgBlockUI;

  constructor(private planService: PlanResourceService,
              private location: Location,
              private helper: RayonHelperService,
              private stateService: StateService,
              private modalService: BsModalService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.document = this.planService.document;
    this.old = copy(this.document);
  }

  showButtons() {
    this.isShowButtons = true;
  }

  isValid(): boolean {
    let r: boolean = true;
    if (r) r = !!this.document.planName;
    return r;
  }

  prepareData() {
    if (this.document.planApproveDate) this.document.planApproveDate.setHours(0, 0, 0, 0);
    if (this.files) this.document.files = this.files.map(f => f.idFile);
  }

  save() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      this.prepareData();
      let diff = this.planService.diff(this.old, this.document);
      if (diff.length) {
        this.planService.update(this.document.documentId, diff).subscribe((response: RayonPlan) => {
          this.stateService.go(this.stateService.current, {mode: 'view'});
          this.blockUI.stop();
        });
      } else {
        this.blockUI.stop();
        this.helper.warning('Нет изменений для сохранения!');
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  cancel() {
    this.location.back();
  }

  isShowBtn(type?: string): boolean {
    let r: boolean = false;
    switch(type) {
      default:
        r = this.session.hasPermission('MR_PLAN_CARDEDIT_BUTTON');
        break;
    }
    return r;
  }

  testModal() {
    let modalRef = this.modalService.show(NewPlanModalComponent, {
      class: 'modal-lg',
      initialState: {
        prevId: 'a3d559f4-e302-48cf-9341-5cfb39a52e81',
        saveCallback: () => {
          this.helper.success('Успешно!');
        }
      }
    });
  }
}
