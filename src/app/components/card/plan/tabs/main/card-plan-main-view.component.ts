import {Component, Input} from "@angular/core";
import {Observable, from} from "rxjs";
import {SessionStorage} from "@reinform-cdp/security";
import {ScriptService} from "../../../../../services/script.service";
import {RayonProgram} from "../../../../../models/program/RayonProgram";
import {RayonHelperService} from "../../../../../services/rayon-helper.service";
import {RayonGeoService} from "../../../../../services/rayon-geo.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {find} from 'lodash';
import {RayonPlan} from "../../../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../../../services/plan-resource.service";

@Component({
  selector: 'card-plan-main-view',
  templateUrl: './card-plan-main-view.component.html',
  providers: [ScriptService]
})
export class CardPlanMainViewComponent {
  document: RayonPlan;
  dictionaries: any = {};

  @Input() files: any;

  constructor(public planService: PlanResourceService,
              private session: SessionStorage,
              private helper: RayonHelperService,
              private script: ScriptService,
              private nsi: NsiResourceService) {}

  ngOnInit() {
    this.document = this.planService.document;

    let dicts$ = this.nsi.getDictsFromCache([
      'DynamicRegistries',
      'Prefect',
      'mr_program_planStatus'
    ]);

    from(dicts$).subscribe(res => {
      this.dictionaries = res;
    });
  }
}
