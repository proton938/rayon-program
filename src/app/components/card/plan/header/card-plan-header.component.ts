import {Component} from "@angular/core";
import {PlanResourceService} from "../../../../services/plan-resource.service";
import {RayonPlan} from "../../../../models/plan/RayonPlan";
import {Location, LocationStrategy, PathLocationStrategy} from "@angular/common";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {AlertService} from "@reinform-cdp/widgets";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'card-plan-header',
  templateUrl: './card-plan-header.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class CardPlanHeaderComponent {
  document: RayonPlan;
  isShow: any = {};

  constructor(private planService: PlanResourceService,
              private location: Location,
              private helper: RayonHelperService,
              private alertService: AlertService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.document = this.planService.document;
    this.isShow.del = this.session.hasPermission('MR_PLAN_DELETE_BUTTON');
  }

  remove() {
    this.alertService.confirm({
      okButtonText: 'Удалить',
      message: 'Удалить мастер-план?',
      type: 'danger',
      size: 'md'
    }).then(response => {
      this.planService.delete(this.document.documentId).subscribe(response => {
        this.helper.success('Мастер-план удалён');
        // this.location.back();
        this.goToShowcase();
      });
    });
  }

  goToShowcase() {
    let crumbs = this.helper.getBreadcrumbs();
    (<any>window).location.href = crumbs[crumbs.length-2].url;
  }
}
