import {Component} from "@angular/core";
import {RayonPlan} from "../../../models/plan/RayonPlan";
import {PlanResourceService} from "../../../services/plan-resource.service";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {StateService, Transition} from "@uirouter/core";
import {SessionStorage} from "@reinform-cdp/security";
import {forkJoin} from "rxjs/index";
import {find} from 'lodash';

@Component({
  selector: 'card-plan',
  styleUrls: ['../card.component.scss'],
  templateUrl: './card-plan.component.html'
})
export class CardPlanComponent {
  document: RayonPlan;
  isLoading: boolean = true;
  isError: boolean;
  documentId: string;
  dictionaries: any = {};
  modeParam: string;
  tabs: any[] = [];
  commonCardState: string = 'app.plan.card';

  constructor(private planService: PlanResourceService,
              private helper: RayonHelperService,
              private nsi: NsiResourceService,
              private transition: Transition,
              private stateService: StateService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    this.modeParam = this.transition.params()['mode'];

    const document$ = this.planService.init(this.documentId);
    const dicts$ = this.nsi.getDictsFromCache(['DynamicRegistries', 'Prefect', 'mr_program_planStatus']);

    this.tabs = this.getTabs();

    forkJoin([document$, dicts$])
      .subscribe((response: any) => {
        this.document = response[0];
        this.updateBreadcrumbs();
        this.initSuccess();
      }, error => {
        this.isError = true;
        this.helper.error(error);
      });
  }

  initSuccess() {
    this.tabs = this.tabs.filter(i => i.show);
    this.isLoading = false;
  }

  getTabs(): any[] {
    let tabs = [];

    tabs.push({
      code: 'main',
      name: 'Общая информация',
      state: this.commonCardState + '.main',
      show: true
    }, {
      code: 'objects',
      name: 'Объекты и работы',
      state: this.commonCardState + '.objects',
      show: this.session.hasPermission('MR_PLAN_CARD_OBJECTS')
    }, {
      code: 'versions',
      name: 'Версии',
      state: this.commonCardState + '.versions',
      show: this.session.hasPermission('MR_PLAN_CARD_VERSIONS')
    }, {
      code: 'log',
      name: 'История изменений',
      state: this.commonCardState + '.log',
      show: this.session.hasPermission('MR_PLAN_CARD_LOG')
    }, {
      code: 'process',
      name: 'Процесс',
      state: this.commonCardState + '.process',
      show: this.session.hasPermission('MR_PLAN_CARD_PROCESS')
    }, {
      code: 'json',
      name: 'JSON',
      state: this.commonCardState + '.json',
      show: this.session.hasPermission('MR_PLAN_CARD_JSON')
    });

    return tabs;
  }

  updateBreadcrumbs() {
    this.helper.setBreadcrumbs(this.helper.cardBreadcrumbs({
      name: 'Планы по развитию районов по программе "Мой район"',
      code: 'MR_PROGRAM_PLAN',
      documentCode: 'MR_PROGRAM_PLAN'
    }, this.document && this.document.planName ? this.document.planName : ''));
  }

  getTab(code: string): any {
    return find(this.tabs, i => i.code === code);
  }

  tabChange(tabState: string): void {
    let tabParams: any = { id: this.documentId };
    if (tabState !== this.stateService.current.name) tabParams.mode = 'view';
    this.stateService.go(tabState, tabParams);
  }

  isActiveTab(tabState: string): boolean {
    return tabState === this.stateService.current.name;
  }
}
