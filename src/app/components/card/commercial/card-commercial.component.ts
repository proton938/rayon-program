import {Component, OnInit, Inject} from '@angular/core';
import {StateService, Transition} from '@uirouter/core';
import {ActivityProcessHistoryManager, IInfoAboutProcessInit} from '@reinform-cdp/bpm-components';
import {Observable, forkJoin, from} from 'rxjs';
import {IRootScopeService} from 'angular';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {ExFileType} from '@reinform-cdp/widgets';
import {SessionStorage} from '@reinform-cdp/security';
import {CdpSolrResourceService} from "@reinform-cdp/search-resource";
import {first, find} from "lodash";
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";

@Component({
  selector: 'program-card-commercial',
  styleUrls: ['../card.component.scss'],
  templateUrl: './card-commercial.component.html'
})
export class ProgramCardCommercialComponent {
  document: any;
  isLoading: boolean = true;
  isError: boolean;
  documentId: string;
  dictionaries: any = {};
  modeParam: string;
  tabs: any[] = [];
  isObjectAdmin: boolean;
  currentAddress: string = '';
  commonCardState: string = 'app.objectCommercial.card';

  constructor(private programService: RayonProgramResourceService,
              private helper: RayonHelperService,
              private transition: Transition,
              private stateService: StateService,
              private activityProcessHistoryManager: ActivityProcessHistoryManager,
              private cdpSolr: CdpSolrResourceService,
              private session: SessionStorage,
              private nsi: NsiResourceService) {}

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    this.modeParam = this.transition.params()['mode'];

    const document$ = this.programService.init(this.documentId);
    const dicts$ = this.nsi.getDictsFromCache(['DynamicRegistries']);

    this.isObjectAdmin = !!~this.session.groups().indexOf('MR_OBJECT_ADMIN');

    this.tabs = this.getTabs();

    if (!this.tabs.filter(i => i.state === this.stateService.current.name).length) {
      //This tab on current view-mode not exists
      this.stateService.go(this.commonCardState + '.main');
    }

    forkJoin([document$, dicts$])
      .subscribe((response: any) => {
        this.document = response[0];
        let breadcrumbs = find(response[1]['DynamicRegistries'], item => item.code === 'MR_PROGRAM_COMMERCIAL');
        this.updateBreadcrumbs(breadcrumbs);
        this.initSuccess();
      }, error => {
        this.isError = true;
        this.helper.error(error);
      });
  }

  initSuccess() {
    this.tabs = this.tabs.filter(i => i.show);
    this.isLoading = false;
  }

  updateBreadcrumbs(breadcrumbs) {
    let showcaseBuilderLink = breadcrumbs.showcaseBuilderLink && breadcrumbs.showcaseBuilderLink[0];
    this.helper.setBreadcrumbs(this.helper.cardBreadcrumbs({
      name: breadcrumbs.name || 'Коммерческие объекты',
      code: showcaseBuilderLink ? showcaseBuilderLink.showcaseCode : 'COMMERCIAL'
    }, this.document && this.document.object && this.document.object.name ? this.document.object.name : ''));
  }

  getTabs(): any[] {
    let tabs = [];

    tabs.push({
      code: 'main',
      name: 'Общая информация',
      state: this.commonCardState + '.main',
      view: true,
      edit: true,
      show: true
    });

    tabs.push({
      code: 'objects',
      name: 'Объекты',
      state: this.commonCardState + '.objects',
      view: true,
      edit: true,
      show: true
    });

    tabs.push({
      code: 'process',
      name: 'Процессы',
      state: this.commonCardState + '.process',
      view: true,
      edit: true,
      show: true
    });

    tabs.push({
      code: 'json',
      name: 'JSON',
      state: this.commonCardState + '.json',
      view: true,
      edit: true,
      show: this.session.hasPermission('objectCardTab')
    });

    tabs.push({
      code: 'log',
      name: 'История изменений',
      state: this.commonCardState + '.log',
      view: true,
      edit: true,
      show: this.session.hasPermission('objectCardTab')
    });

    return tabs;
  }

  getTab(code: string): any {
    return find(this.tabs, i => i.code === code);
  }

  tabChange(tabState: string): void {
    let tabParams: any = { id: this.documentId };
    if (tabState !== this.stateService.current.name) tabParams.mode = 'view';
    this.stateService.go(tabState, tabParams);
  }

  isActiveTab(tabState: string): boolean {
    return tabState === this.stateService.current.name;
  }
}
