import {Component, Input, OnInit} from '@angular/core';
import {Observable, forkJoin, from} from 'rxjs';
import {StateService, Transition} from '@uirouter/core';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {first, toString} from 'lodash';
import {AlertService} from "@reinform-cdp/widgets";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";
import {RayonWorkTypeService} from "../../../../services/rayon-work-type.service";
import {RayonWork} from "../../../../models/work/RayonWork";
import {ActivityResourceService} from "@reinform-cdp/bpm-components";
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";

@Component({
  selector: 'program-card-commercial-header',
  templateUrl: './program-card-commercial-header.component.html'
})
export class ProgramCardCommercialHeaderComponent implements OnInit {
  documentId: string = 'debug';
  h3: string = '';
  dictionaries: any = {};
  workDocs: any[] = [];
  works: any[] = [];
  status: any;
  modeParam: string;
  workStatusColors: any = {};

  isShow: any = {};
  isMosRu: boolean;
  correcting: boolean = false;

  @Input() document: RayonProgram;
  @Input() address?: string;

  constructor(private nsi: NsiResourceService,
              private transition: Transition,
              private alertService: AlertService,
              private programService: RayonProgramResourceService,
              private stateService: StateService,
              private solrMediator: SolrMediatorService,
              private workTypeService: RayonWorkTypeService,
              private helper: RayonHelperService,
              private activityService: ActivityResourceService,
              private session: SessionStorage) {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    this.isShow.removeObjectBtn = this.session.hasPermission('objectCardDel');
    this.isShow.archiveObjectBtn = this.session.hasPermission('objectCardArchive');
  }

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    this.modeParam = this.transition.params()['mode'];

    let dictionaries$ = from(this.nsi.getDictsFromCache(['mr_program_WorkStatuses']));
    let works$ = this.solrMediator.query({page: 0, pageSize: 50, types: [this.solrMediator.types.work], query: 'objectIdWork:' + this.documentId});

    forkJoin([dictionaries$, works$]).subscribe((response: any) => {
      this.dictionaries = response[0];
      this.dictionaries['mr_program_WorkStatuses'].forEach(i => { this.workStatusColors[i.code] = i.color; });
      this.workDocs = response[1] && response[1].docs ? response[1].docs : [];
      if (this.workDocs.length) {
        this.workTypeService.list(this.workDocs.map(i => i.documentId)).subscribe(res => {
          this.works = res.sort((a: RayonWork, b: RayonWork) => +b.documentDate - +a.documentDate);
        });
      }
      this.h3 = this.getH3();
    })
  }

  getH3(): string {
    let resultParts: string[] = [];
    if (this.document && this.document.address) {
      resultParts.push(this.document.address.prefect.map(i => i.name).join(', '));
      resultParts.push(this.document.address.district.map(i => i.name).join(', '));
      resultParts.push(this.document.address.address);
    }
    return resultParts.filter(i => i).join(', ');
  }

  delete() {
    this.alertService.confirm({
      message: 'Вы действительно хотите удалить данный документ?',
      okButtonText: 'Ок',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.programService.remove([this.documentId]).subscribe(response => {
        this.helper.success('Документ успешно удален');
        this.stateService.go('app.objectCommercial.list');
      }, error => {
        this.helper.success('Ошибка при удалении документа');
        console.log(error);
      });
    }).catch(error => {
      console.log(error);
    });
  }

  getCurrentStateName(): string {
    return this.stateService.current.name;
  }

  startCorrectProcess() {
    let processId = 'mrobject_ObjectAdjustment';
    this.correcting = true;
    this.getOpenedTasks().subscribe(tasks => {
      if (tasks.length) {
        this.helper.goToTask('app.execution.adjustObjectInformation', tasks[0].id);
        /*this.stateService.go('app.execution.adjustObjectInformation', {
          system: 'mrobject',
          taskId: tasks[0].id,
          systemCode: 'MR'
        });*/
      } else {
        from(this.activityService.getProcessDefinitions({
          keyLike: processId + '%',
          latest: true,
          size: 1
        })).subscribe((def: any) => {
          let defId = def.data[0].id;
          from(this.activityService.initProcess({
            processDefinitionId: defId,
            variables: [
              {name: 'EntityIdVar', value: this.documentId},
              {name: 'ExecutorVar', value: this.session.login()}
            ]
          })).subscribe(processId => {
            this.helper.success('Процесс успешно запущен!');
            from(this.activityService.getTasks({executionId: toString(processId)})).subscribe((res: any) => {
              if (res && res.data && res.data[0]) {
                let taskId = res.data[0].id;
                this.helper.goToTask('app.execution.adjustObjectInformation', taskId);
                /*this.stateService.go('app.execution.adjustObjectInformation', {
                  system: 'mrobject',
                  taskId: taskId,
                  systemCode: 'MR'
                });*/
              }
              this.correcting = false;
            }, error => {
              this.helper.error(error);
              this.correcting = false;
            });
          }, error => {
            this.helper.error(error);
            this.correcting = false;
          });
        }, error => {
          this.helper.error(error);
          this.correcting = false;
        });
      }
    }, error => {
      this.helper.error(error);
      this.correcting = false;
    });
  }

  getOpenedTasks(): Observable<any> {
    return Observable.create(observer => {
      from(this.activityService.getTasksHistory({
        processDefinitionKeyLike: 'mrobject_ObjectAdjustment%',
        processVariables: [{
          name: 'EntityIdVar',
          value: this.documentId,
          operation: 'equals',
          type: 'string'
        }, {
          name: 'ExecutorVar',
          value: this.session.login(),
          operation: 'equals',
          type: 'string'
        }]
      })).subscribe(res => {
        let tasks = [];
        if (res && res.data && res.data.length) {
          tasks = res.data.filter(i => !i.endTime);
        }
        observer.next(tasks);
      }, error => this.helper.error(error), () => {
        observer.complete();
      });
    });
  }
}
