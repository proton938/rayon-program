import {Component, Input} from "@angular/core";
import {RayonProgramResourceService} from "../../../../../services/rayon-program-resource.service";
import {RayonProgram} from "../../../../../models/program/RayonProgram";
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import * as jsonpatch from 'fast-json-patch';
import {RayonProgramWrapper} from "../../../../../models/program/RayonProgramWrapper";
import {RayonHelperService} from "../../../../../services/rayon-helper.service";
import {StateService} from "@uirouter/core";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {copy} from 'angular';
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-card-commercial-main-edit',
  templateUrl: './card-commercial-main-edit.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class ProgramCardCommercialMainEditComponent {
  document: RayonProgram;
  old: RayonProgram;
  validate: boolean;
  isShowButtons: boolean;

  @Input() files: any;

  @BlockUI('card-main-edit') blockUI: NgBlockUI;

  constructor(private programService: RayonProgramResourceService,
              private location: Location,
              private helper: RayonHelperService,
              private stateService: StateService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.document = this.programService.document;
    this.old = copy(this.document);
  }

  showButtons() {
    this.isShowButtons = true;
  }

  isValid(): boolean {
    let r: boolean = true;
    if (!this.session.hasPermission('mr_objectCardMos')) {
      if (r) r = !!this.document.address.prefect.length;
      if (r) r = !!this.document.address.district.length;
      if (r && !this.document.address.addressId) r = !!this.document.address.address;
      if (r) r = !!this.document.object.industry;
      if (r) r = !!this.document.object.kind;
      if (r) r = !!this.document.object.name;
      if (r) r = !!this.document.rubricator.length;
    }

    return r;
  }

  save() {
    this.validate = true;
    if (this.isValid()) {
      this.blockUI.start();
      let model = new RayonProgram();
      model.build(JSON.parse(JSON.stringify(this.document)));
      let doc = model.min();
      let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc});
      if (diff.length) {
        this.programService.patch(this.document.documentId, diff).subscribe((response: RayonProgramWrapper) => {
          this.stateService.go(this.stateService.current, {mode: 'view'});
          this.blockUI.stop();
        });
      } else {
        this.blockUI.stop();
        this.helper.warning('Нет изменений для сохранения!');
      }
    } else {
      this.helper.error('Не заполнены обязательные поля!');
    }
  }

  cancel() {
    this.location.back();
  }
}
