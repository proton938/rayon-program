import {Component, OnInit, Input} from '@angular/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import * as JSONEditor from 'jsoneditor';
import {copy} from 'angular';
import {ToastrService} from 'ngx-toastr';
import * as jsonpatch from 'fast-json-patch';
import {StateService} from '@uirouter/core';
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {RayonProgram} from "../../../models/program/RayonProgram";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {RayonSchemaService} from "../../../services/rayon-schema.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'program-object-json-editor',
  templateUrl: './json-editor.component.html',
  styleUrls: ['./json-editor.component.scss']
})
export class RayonProgramJsonEditorComponent implements OnInit {
  @Input() document: RayonProgram;
  @Input() docType: string = '';
  @Input() rootName: string = '';
  @Input() serviceUrl: string = '';
  @Input() serviceGet: string = '';
  @Input() servicePatch: string = '';
  container: any;
  options: any;
  editor: any;
  @BlockUI('card-editor') blockUI: NgBlockUI;

  doc: any;
  docEdited: any;
  docOld: any;
  loading: boolean = true;

  constructor(private http: HttpClient,
              private toastr: ToastrService,
              private schemaService: RayonSchemaService,
              public stateService: StateService) {
  }

  ngOnInit() {
    this.http.get(this.getUrl).subscribe(res => {
      if (res && res[this.rootName]) {
        this.schemaService.getSchema(this.docType).subscribe(schemaRes => {
          this.doc = this.schemaService.getModel(schemaRes, res);
          this.loading = false;
          this.init();
        });
      }
    });
  }

  init() {
    this.docOld = copy(this.doc);
    this.docEdited = copy(this.doc);
    this.container = document.getElementById('jsoneditor');
    this.options = {};
    this.editor = new JSONEditor(this.container, this.options);
    this.editor.set(this.docEdited[this.rootName]);
  }

  save() {
    this.blockUI.start();
    this.docEdited[this.rootName] = this.editor.get();
    const diff = jsonpatch.compare(this.schemaService.min(this.docOld), this.schemaService.min(this.docEdited));
    if (diff.length > 0) {
      this.http.patch(this.patchUrl, diff, {
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).subscribe(response => {
        this.blockUI.stop();
        this.reloadState();
        this.toastr.success('Документ успешно сохранен!');
      }, error => {
        this.toastr.error('Ошибка при сохранении документа!');
        this.blockUI.stop();
      });
    } else {
      this.toastr.warning('В документе изменений нет!');
      this.blockUI.stop();
    }
  }

  reloadState() {
    this.stateService.go(this.stateService.current, {
      id: this.document.documentId
    }, {reload: true});
  }

  get getUrl(): string {
    let url: string = this.serviceGet || this.serviceUrl;
    return url + '/' + this.document.documentId;
  }

  get patchUrl(): string {
    let url: string = this.servicePatch || this.serviceUrl;
    return url + '/' + this.document.documentId;
  }
}
