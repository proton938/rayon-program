import {Component} from "@angular/core";
import {RayonProgramResourceService} from "../../../../../services/rayon-program-resource.service";
import {RayonProgram} from "../../../../../models/program/RayonProgram";
import {from} from "rxjs/index";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {ExFileType} from "@reinform-cdp/widgets";
import {StateService, Transition} from '@uirouter/core';
import {SessionStorage} from "@reinform-cdp/security";
import { some } from 'lodash';

@Component({
  selector: 'program-card-dominant-main',
  templateUrl: './card-dominant-main.component.html'
})
export class ProgramCardDominantMainComponent {
  document: RayonProgram;
  modeParam: string;
  isLoading: boolean = true;
  files: any = {
    photobefore: [],
    photoafter: [],
    addmaterial: [],
    image: [],
    photomosru: []
  };

  constructor(private transition: Transition,
              private programService: RayonProgramResourceService,
              private fileResourceService: FileResourceService,
              private session: SessionStorage,
              public state: StateService) {}

  ngOnInit() {

    this.document = this.programService.document;
    this.modeParam = this.transition.params()['mode'];
    this.getFiles();
    this.isLoading = false;
  }

  getFiles() {
    if (this.document && this.document.folderId) {
      from(this.fileResourceService.getFolderContent(this.document.folderId, false)).subscribe(response => {
        if (response) {
          response.forEach(file => {
            if (file && file.fileType && this.files[file.fileType]) {
              this.files[file.fileType].push(ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
                false, file.dateCreated, file.fileType, file.mimeType));
            }
          });
        }
      });
    }
  }
}
