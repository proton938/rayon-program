import {Component} from "@angular/core";
import {RayonProgram} from "../../../../../models/program/RayonProgram";
import {StateService, Transition} from "@uirouter/core";
import {RayonProgramResourceService} from "../../../../../services/rayon-program-resource.service";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-card-dominant-objects',
  templateUrl: './card-dominant-objects.component.html'
})
export class ProgramCardDominantObjectsComponent {
  document: RayonProgram;
  modeParam: string;
  isLoading: boolean = true;
  isShow: any = {
    editBtn: true
  };

  constructor(private programService: RayonProgramResourceService,
              private transition: Transition,
              private session: SessionStorage,
              public state: StateService) {}

  ngOnInit() {
    this.isShow.editBtn = this.session.hasPermission('dominantCardEdit');

    this.document = this.programService.document;
    this.modeParam = this.transition.params()['mode'];
    this.isLoading = false;
  }
}
