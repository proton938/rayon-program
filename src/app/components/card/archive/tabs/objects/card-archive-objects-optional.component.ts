import {Component, Input} from "@angular/core";
import {RayonProgram} from "../../../../../models/program/RayonProgram";
import {RayonProgramResourceService} from "../../../../../services/rayon-program-resource.service";
import {RayonLinkResourceService} from "../../../../../services/rayon-link-resource.service";
import {RayonHelperService} from "../../../../../services/rayon-helper.service";
import {forkJoin, from, Observable} from "rxjs";
import {find, some, first} from "lodash";
import {SolrMediatorService} from "../../../../../services/solr-mediator.service";

@Component({
  selector: 'program-card-archive-objects-optional',
  templateUrl: './card-archive-objects-optional.component.html'
})
export class ProgramCardArchiveObjectsOptionalComponent {
  document: RayonProgram;
  documentId: string;
  isLoading: boolean = true;
  objects: any[] = [];
  candidates: any[] = [];
  links: any[] = [];
  objectParams: any = {};
  saving: boolean = false;
  showCreateBtn: boolean = false;

  @Input() readOnly: boolean;
  @Input() type: string = '';

  constructor(private programService: RayonProgramResourceService,
              private solrMediator: SolrMediatorService,
              private linkService: RayonLinkResourceService,
              private helper: RayonHelperService) {}

  ngOnInit() {
    this.document = this.programService.document;
    this.documentId = this.document.documentId;
    this.objectParams = this.getObjectParams();

    // Если не уникальный объект (обычный)
    if (!this.type) this.showCreateBtn = true;

    let objects$ = this.getObjects();
    let candidates$ = this.getCandidates();
    forkJoin([objects$, candidates$]).subscribe(res => {
      if (res[0] && res[0].length) this.objects = res[0];
      if (res[1] && res[1].length) this.candidates = res[1];
      this.isLoading = false;
    }, error => {
      this.helper.error(error)
    });
  }

  getObjects(): Observable<any[]> {
    return Observable.create(observer => {
      this.linkService.find(this.documentId).subscribe(res => {
        this.links = res || [];
        let ids = this.links.length ? this.links.map(i => find([i.link.id1, i.link.id2], r => r !== this.documentId)) : [];
        if (ids.length) {
          let query: string = 'documentId:' + ids.join(' OR documentId:');
          this.solrMediator.query({ page: 0, pageSize: 999, types: [this.solrMediator.types.obj], query: query}).subscribe(res => {
            observer.next(res ? (res.docs || []) : []);
          }, error => {
            observer.error(error);
          }, () => {
            observer.complete();
          });
        } else {
          observer.next([]);
          observer.complete();
        }
      })
    });
  }

  getCandidates(): Observable<any[]> {
    return Observable.create(observer => {
      let queryParts = [];
      if (!this.readOnly) {
        if (this.document.address && this.document.address.prefect && this.document.address.prefect.length) {
          queryParts.push('(prefectsCodeObject:' + this.document.address.prefect.map(i => i.code).join(' OR prefectsCodeObject:') + ')');
        }
        if (this.document.address && this.document.address.district && this.document.address.district.length) {
          queryParts.push('(districtsCodeObject:' + this.document.address.district.map(i => i.code).join(' OR districtsCodeObject:') + ')');
        }
        queryParts.push('itemTypeCodeObject:object');
        queryParts.push('NOT dominantObject:true');
        if (this.type !== 'dominant') queryParts.push('NOT commercialObject:true');
        this.solrMediator.query({page: 0, pageSize: 999, types: [this.solrMediator.types.obj], query: queryParts.join(' AND ')}).subscribe(res => {
          observer.next(res ? (res.docs || []) : []);
        }, error => {
          observer.error(error);
        }, () => {
          observer.complete();
        });
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  add(row: any) {
    row.linking = true;
    this.createLink(row.documentId).subscribe(res => {
      this.links.push(res);
      row.linking = false;
      this.objects.push(row);
    });
  }

  remove(row: any) {
    row.linking = true;
    let linkId = find(this.links, i => i.link.id2 === row.documentId).link.documentId;
    if (linkId) {
      this.deleteLink(linkId).subscribe(res => {
        this.links.splice(this.links.map(i => i.documentId).indexOf(linkId), 1);
        this.objects.splice(this.objects.map(i => i.documentId).indexOf(row.documentId), 1);
        row.linking = false;
      });
    }
  }

  hasLink(id: string) {
    return some(this.links, i => i.link.id2 === id);
  }

  createLink(id: string): Observable<any> {
    return Observable.create(observer => {
      this.linkService.create({
        link: {
          id1: this.document.documentId,
          id2: id,
          system1: 'mr',
          system2: 'mr',
          subsystem1: 'program',
          subsystem2: 'program',
          type1: 'OBJECT',
          type2: 'OBJECT',
          status: 'approve'
        }
      }).subscribe(response => {
        observer.next(response);
      }, error => {
        this.helper.error(error);
        observer.error(error);
      }, () => {
        observer.complete();
      });
    });
  }

  deleteLink(linkId: string): Observable<any> {
    return Observable.create(observer => {
      this.linkService.delete(linkId).subscribe(response => {
        observer.next(response);
      }, error => {
        this.helper.error(error);
        observer.error(error);
      }, () => {
        observer.complete();
      });
    });
  }

  getObjectParams(): any {
    let r: any = {};
    if (this.document.address.prefect && this.document.address.prefect.length) {
      r.prefect = (<any>first(this.document.address.prefect)).code;
    }
    if (this.document.address.district && this.document.address.district.length) {
      r.district = (<any>first(this.document.address.district)).code;
    }
    r.itemType = 'object';
    return r;
  }

  saveData() {
    this.saving = true;
    setTimeout(() => {
      this.helper.success('Данные успешно сохранены!');
      this.saving = false;
    }, 1000);
  }
}
