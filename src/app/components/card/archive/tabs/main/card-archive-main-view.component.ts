import {Component, Input} from "@angular/core";
import {RayonProgramResourceService} from "../../../../../services/rayon-program-resource.service";
import {RayonProgram} from "../../../../../models/program/RayonProgram";
import {ScriptService} from "../../../../../services/script.service";
import {SessionStorage} from "@reinform-cdp/security";
import {RayonHelperService} from "../../../../../services/rayon-helper.service";
import {RayonGeoService} from "../../../../../services/rayon-geo.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {from} from "rxjs/index";
import {find} from 'lodash';

@Component({
  selector: 'program-card-archive-main-view',
  templateUrl: './card-archive-main-view.component.html',
  providers: [ScriptService]
})
export class ProgramCardArchiveMainViewComponent {
  document: RayonProgram;
  address: string;
  panoParams: any = null;
  dictionaries: any = {};
  geometry: any;

  isMosRu: boolean;

  @Input() files: any;

  constructor(public programService: RayonProgramResourceService,
              private helper: RayonHelperService,
              private session: SessionStorage,
              private script: ScriptService,
              private geo: RayonGeoService,
              private nsi: NsiResourceService) {}

  ngOnInit() {
    this.document = this.programService.document;

    this.isMosRu = this.session.hasPermission('mr_objectCardMos');

    this.script.load('panoapi').then(data => {
      if (this.panoParams) setTimeout(() => { this.panoReady(this.panoParams) }, 1000);
    });

    this.helper.getFullAddress(this.document.address.addressId).subscribe(response => {
      this.programService.address = response;
    });

    from(this.nsi.getDictsFromCache([
      'SubSystems',
      'mr_program_ObjectIndustries',
      'mr_program_ObjectCategories'
    ])).subscribe(res => {
      this.dictionaries = res;
      this.geo.find(this.document.documentId, 'MR_PROGRAM_OBJ', 'MR_PROGRAM').subscribe(geometryRes => {
        // this.mapLayer = this.makeLayerName();
        this.geometry = geometryRes;
      });
    });
  }

  /*makeLayerName(): string {
    let r: string = '';
    if (!!this.document.aipId) {
      if (this.document.object && this.document.object.industry) {
        let industryInfo = find(this.dictionaries['mr_program_ObjectIndustries'], i => i.code === this.document.object.industry.code);
        let layers = industryInfo ? (industryInfo.layer || []) : [];
        if (layers.length) {
          if (layers.length === 1) r = layers[0];
          else if (this.document.object && this.document.object.kind) {
            r = find(layers, i => (new RegExp(this.document.object.kind.code)).test(i));
          }
        }
      } else console.log('Not found object industry inform');
    }
    if (!r && this.document.okn) {
      r = 'geo_obj_point_okn';
    }
    if (!r && this.document.object && this.document.object.kind) {
      let kindInfo = find(this.dictionaries['mr_program_ObjectCategories'], i => i.code === this.document.object.kind.code);
      if (kindInfo) r = kindInfo.layer || '';
    }
    return r;
  }*/

  panoReady(params) {
    let imap = (<any>window).IMAP;
    if (imap && imap.Panorama) {
      let ipano = new imap.Panorama('panorama', 'guest', 'guest', params);
    } else setTimeout(() => { this.panoReady(params) }, 1000);
  }
}



