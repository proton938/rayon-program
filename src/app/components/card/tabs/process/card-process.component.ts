import {Component} from "@angular/core";
import {StateService, Transition} from '@uirouter/core';
import {ActivityProcessHistoryManager, IInfoAboutProcessInit, CompareType} from "@reinform-cdp/bpm-components";
import {forkJoin, from} from "rxjs/index";
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";

@Component({
  selector: 'program-card-process',
  templateUrl: './card-process.component.html'
})
export class ProgramCardProcessComponent {
  document: RayonProgram;
  init: IInfoAboutProcessInit;
  modeParam: string;
  isLoading: boolean = true;
  config: any;
  constructor(private programService: RayonProgramResourceService,
              private transition: Transition,
              private activityProcessHistoryManager: ActivityProcessHistoryManager) {}

  ngOnInit() {
    this.document = this.programService.document;
    this.modeParam = this.transition.params()['mode'];
    this.config = {
      sysName: 'mrobject',
      linkVarValue: this.document.documentId,
      defaultVars: [{
        name: 'EntityIdVar',
        value: this.document.documentId
      }]
    };
    this.isLoading = false;
    /*

    const processes$ = from(<any>this.activityProcessHistoryManager
      .init('mrobject', [{
        name: 'Процессы работы с обектами',
        key: 'mrobject',
        compareType: CompareType.startsWith
      }], this.document.documentId, 'EntityIdVar'));

    forkJoin([processes$]).subscribe((response: any) => {
      this.init = response[0];
      this.isLoading = false;
    });*/

  }
}
