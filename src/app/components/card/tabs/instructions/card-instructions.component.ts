import {Component} from "@angular/core";
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {from} from "rxjs/index";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {RayonLinkResourceService} from "../../../../services/rayon-link-resource.service";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";

@Component({
  selector: 'program-card-instructions',
  templateUrl: './card-instructions.component.html'
})
export class ProgramCardInstructionsComponent {
  document: RayonProgram;
  rows: any[] = [];
  isLoading: boolean = true;
  links: any[] = [];

  constructor(private helper: RayonHelperService,
              private rayonService: RayonProgramResourceService,
              private linkService: RayonLinkResourceService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    this.document = this.rayonService.document;
    this.getInstructions();
  }

  getInstructions() {
    this.linkService.find(this.document.documentId).subscribe(linkRes => {
      this.links.length = 0;
      if (linkRes) this.links.splice(0, 0, ...linkRes);
      let ids = this.links.length ? this.links.filter(i => i.link.id2 === this.document.documentId && i.link.status === 'approve').map(i => i.link.id1) : [];
      if (ids.length) {
        let query = 'documentId:' + ids.join(' OR documentId:');
        this.solrMediator.query({page: 0, pageSize: 999, types: [this.solrMediator.types.instruction], query: query}).subscribe(res => {
          if(res && res.docs) {
            this.rows = res.docs;
          }
          this.isLoading = false;
        });
      } else {
        this.isLoading = false;
      }
    });
  }
}
