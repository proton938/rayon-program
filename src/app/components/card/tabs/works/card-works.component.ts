import {Component} from "@angular/core";
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {StateService, Transition} from '@uirouter/core';

@Component({
  selector: 'program-card-works',
  templateUrl: './card-works.component.html'
})
export class ProgramCardWorksComponent {
  document: RayonProgram;
  modeParam: string;
  isLoading: boolean = true;

  constructor(private transition: Transition,
              private programService: RayonProgramResourceService) {}

  ngOnInit() {
    this.document = this.programService.document;
    this.modeParam = this.transition.params()['mode'];
    this.isLoading = false;
  }
}
