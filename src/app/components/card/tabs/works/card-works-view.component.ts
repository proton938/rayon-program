import {Component, Input, OnInit} from '@angular/core';
import {RayonProgramResourceService} from '../../../../services/rayon-program-resource.service';
import {RayonProgram} from '../../../../models/program/RayonProgram';
import {from} from 'rxjs';
import {RayonWorkTypeService} from '../../../../services/rayon-work-type.service';
import {RayonHelperService} from '../../../../services/rayon-helper.service';
import {RayonWork} from '../../../../models/work/RayonWork';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {SessionStorage} from '@reinform-cdp/security';
import {find, first, findIndex, some} from 'lodash';
import {StateService, Transition} from '@uirouter/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import {FormWorkTypeEditModalComponent} from '../../../forms/work-type/form-work-type-edit-modal.component';
import {AlertService, ExFileType} from '@reinform-cdp/widgets';
import {RayonProgramBtnService} from '../../../../services/rayon-program-btn.service';
import {RayonWorkProgress} from '../../../../models/work/RayonWorkProgress';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'program-card-works-view',
  templateUrl: './card-works-view.component.html',
  styleUrls: ['./card-works-view.component.scss']
})
export class ProgramCardWorksViewComponent implements OnInit {
  isLoading = true;
  document: RayonProgram;
  works: RayonWork[] = [];
  workStatusColors: any = {};
  address: string;
  expanded: any = {
    work: [],
    workFinance: [],
    info: [],
    progress: []
  };
  responsibleOrganizations: any[] = [];
  organizationNames: any = {};
  addressList: any = {};
  isShow: any = {
    add: false,
    edit: false,
    del: false,
    work: {
      edit: {}
    }
  };
  workId: string = null;
  files: any = {};
  dictionaries: any = {};

  isMosRu: boolean;

  @Input() showButtons: boolean;

  constructor(private nsi: NsiResourceService,
              private programService: RayonProgramResourceService,
              private workTypeService: RayonWorkTypeService,
              private helper: RayonHelperService,
              private solrMediator: SolrMediatorService,
              private session: SessionStorage,
              private stateService: StateService,
              private modalService: BsModalService,
              private alertService: AlertService,
              private transition: Transition,
              private btnService: RayonProgramBtnService) {}

  ngOnInit() {
    this.document = this.programService.document;
    this.address = this.programService.address || '';
    this.workId = this.transition.params()['workId'];
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    this.isShow.del = this.session.hasPermission('mr_program_work_delete');
    this.isShow.add = this.session.hasPermission('mr_program_work_add');
    this.isShow.edit = this.session.hasPermission('mr_program_work_edit');

    from(this.nsi.getDictsFromCache([
      'mr_program_WorkStatuses',
      'mr_program_ResponsibleOrganizations',
      'mr_program_WorkCancelReasons'
    ])).subscribe(response => {
      this.dictionaries = response;
      response['mr_program_WorkStatuses'].forEach(i => { this.workStatusColors[i.code] = i.color; });
      this.responsibleOrganizations = response['mr_program_ResponsibleOrganizations'];
      this.responsibleOrganizations.forEach((i: any) => this.organizationNames[i.code] = i.shortName);
    });

    this.solrMediator.query({
      page: 0, pageSize: 999, types: [this.solrMediator.types.work],
      query: 'objectIdWork:' + this.document.documentId
    }).subscribe(response => {
      if (response && response.docs) {
        const ids = response.docs.map(i => i.documentId);
        this.workTypeService.list(ids).subscribe(res => {
          this.works = res.sort((a: RayonWork, b: RayonWork) => +b.documentDate - +a.documentDate);
          if (this.works.length === 1) {
            this.expanded.work[0] = true;
          } else if (this.workId) {
            const workIndex: number = findIndex(this.works, w => w.documentId === this.workId);
            if (workIndex >= 0) { this.expanded.work[workIndex] = true; }
          }
          this.works.forEach((wItem, wIndex) => {
            if (wItem.finance.length === 1) { (<any>wItem.finance[0]).expanded = true; }
            this.btnService.isShowEditWorkBtn(this.document, wItem).subscribe((show: boolean) => {
              // (<any>wItem).isShowEditBtn = show;
              this.isShow.work.edit[wItem.documentId] = show;
            });
          });
          this.getWorkAddressList();
          this.getFiles();
          this.isLoading = false;
        }, error => this.helper.error(error));
      } else { this.isLoading = false; }
    });

    // this.btnService.isShowAddWorkBtn(this.document).subscribe((show: boolean) => this.isShow.add = show);
  }

  getWorkAddressList() {
    const addressIds = this.works.filter(i => i.addressId).map(i => i.addressId);
    if (addressIds.length) {
      this.solrMediator.query({
        page: 0, pageSize: 999, types: [this.solrMediator.types.address],
        query: 'documentId:' + addressIds.join(' OR documentId:')
      }).subscribe(response => {
        if (response && response.docs) { (<any[]>response.docs).forEach((i: any) => this.addressList[i.documentId] = i.fullAddress); }
      });
    }
  }

  isShowField(fieldName: string, work: RayonWork, index: number = 0) {
    let r = false;
    const works: any[] = this.getSortedProgress(work);
    const context: any = works[index];
    const contextStageCode = this.helper.getField(context, 'stage.code');
    const old: any = works[index - 1] || null;
    const oldStageCode = this.helper.getField(old, 'stage.code');
    const hasCode = (codes: string[]) => some(codes, i => i === contextStageCode);
    switch (fieldName) {
      case 'bargainNumberEaist':
        r = hasCode(['torgi_psd', 'torgi_smr']);
        break;
      case 'registryNumberEaist':
        r = hasCode(['contract_psd', 'contract_smr']);
        break;
      case 'itemNumber':
        r = hasCode(['torgi_psd', 'torgi_smr', 'contract_psd', 'contract_smr']);
        break;
      case 'executionFactStart':
        r = hasCode(['psd', 'smr']);
        break;
      case 'executionFactFinish':
      case 'captionWorkDocs':
      case 'workDocs':
        r = hasCode(['psd_done', 'done']);
        break;
      case 'status':
        r = work.type && some(['building', 'reconstruction'], i => i === work.type.code);
        break;
      case 'cancelReason':
      case 'comment':
      case 'captionWorkCancelDocs':
      case 'workCancelDocs':
        r = contextStageCode === 'canceled';
        break;
      case 'progressReason':
        if (oldStageCode !== contextStageCode) {
          r = (oldStageCode && this.getDictValue('mr_program_WorkStatuses', contextStageCode).sortValue
            < this.getDictValue('mr_program_WorkStatuses', oldStageCode).sortValue) || true;
        }
        break;
      case 'approveMunicipalDep':
        r = this.helper.getField(work, 'type.code') === 'accomplishment'
          && this.helper.getField(this.document, 'object.kind.code') === 'YARD';
        break;
    }
    return r;
  }

  getDictValue(dictName: string, code: string, codePropName: string = 'code'): any {
    return find(this.dictionaries[dictName], i => i[codePropName] === code);
  }

  isShowEditBtn(work: RayonWork): boolean {
    if (this.session.hasPermission('objectCardAdmin')) { return true; }
    if (!work.organization) { return false; }
    const dictOrg = first(this.responsibleOrganizations.filter(i => i.code === work.organization.code));
    return !!this.session.groups().find(i => i === dictOrg.group);
  }

  editWork(workId: string) {
    const modalRef = this.modalService.show(FormWorkTypeEditModalComponent, {
      class: 'modal-lg',
      initialState: {
        documentId: workId,
        objectId: this.document.documentId,
        saveCallback: () => {
          this.stateService.go(this.stateService.current, {mode: 'view'}, {reload: true});
        }
      }
    });
  }

  removeWork(workId: string) {
    this.alertService.confirm({
      message: 'Вы действительно хотите удалить данную работу?',
      okButtonText: 'Ок',
      type: 'warning',
      size: 'md'
    }).then(response => {
      this.workTypeService.delete([workId]).subscribe(response => {
        this.works = this.works.filter(i => i.documentId !== workId);
        this.helper.success('Работа успешно удалена');
      }, error => this.helper.error(error));
    }).catch(error => {
      console.log(error);
    });
  }

  toogleExpand(i) {
    this.expanded.info[i] = !this.expanded.info[i];
  }

  getSortedProgress(work: RayonWork): RayonWorkProgress[] {
    return work.progress.slice().reverse();
  }

  getFiles() {
    this.works.forEach(w => {
      this.files[w.documentId] = {
        loading: true,
        files: []
      };
      if (w.folderId) {
        this.helper.getFiles(w.folderId).subscribe(res => {
          this.files[w.documentId].files = res.map(file => {
            return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
              false, file.dateCreated, file.fileType, file.mimeType);
          });
          this.files[w.documentId].loading = false;
        });
      } else {
        this.files[w.documentId].loading = false;
      }
    });
  }

  filesByIds(workId: string, ids: string[]): any[] {
    return this.files[workId].files.filter(f => some(ids, id => id === f.idFile));
  }

  getSpecificUrl(code: string, id: string): string {
    let r = '';
    const isogdCodes = { gpzunumber: 'GPZU', rvnumber: 'RVOE', rsnumber: 'RS' };
    if (isogdCodes[code]) {
      r = '/isogd/#/app/document/card/' + id + '?reestr=' + isogdCodes[code];
    }
    return r;
  }
}
