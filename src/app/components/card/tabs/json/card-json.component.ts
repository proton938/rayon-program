import {Component} from "@angular/core";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {RayonProgram} from "../../../../models/program/RayonProgram";

@Component({
  selector: 'program-card-json',
  templateUrl: './card-json.component.html'
})
export class ProgramCardJsonComponent {
  document: RayonProgram;

  constructor(public programService: RayonProgramResourceService) {}

  ngOnInit() {
    this.document = this.programService.document;
  }
}
