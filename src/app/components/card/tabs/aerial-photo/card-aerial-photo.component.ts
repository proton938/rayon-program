import {Component} from "@angular/core";
import {Transition} from "@uirouter/core";

@Component({
  selector: 'program-card-aerial-photo',
  templateUrl: './card-aerial-photo.component.html'
})
export class ProgramCardAerialPhotoComponent {
  isLoading: boolean = true;
  modeParam: string;

  constructor(private transition: Transition) {}

  ngOnInit() {
    this.modeParam = this.transition.params()['mode'];
    this.isLoading = false;
  }
}
