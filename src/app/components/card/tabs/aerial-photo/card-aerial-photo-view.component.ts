import {Component, Input} from "@angular/core";
import {formatDate} from '@angular/common';
import {Observable} from "rxjs/Rx";
import {RayonLinkResourceService} from "../../../../services/rayon-link-resource.service";
import {StateService, Transition} from "@uirouter/core";
import {find, compact} from 'lodash';
import {DroneService} from "../../../../services/drone.service";
import {forkJoin} from "rxjs/index";
import {SessionStorage} from "@reinform-cdp/security";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";
import {RayonHelperService} from "../../../../services/rayon-helper.service";

@Component({
  selector: 'program-card-aerial-photo-view',
  templateUrl: './card-aerial-photo-view.component.html'
})
export class ProgramCardAerialPhotoViewComponent {
  isLoading: boolean = true;
  objectId: string = '';
  links: any[] = [];
  docs: any[] = [];
  videos: any[] = [];

  constructor(public state: StateService,
              private transition: Transition,
              private linkService: RayonLinkResourceService,
              private droneService: DroneService,
              private session: SessionStorage,
              private helper: RayonHelperService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    // this.isLoading = false;
    this.objectId = this.transition.params()['id'];
    this.linkService.find(this.objectId).subscribe(res => {
      if (res && res.length) this.links = res.filter(i => i.link.status === 'approve');
      let requestIds: string[] = [];
      this.links.forEach(i => {
        let id: string = find([i.link.id1, i.link.id2], d => d !== this.objectId);
        let type: string = [i.link.type1, i.link.type2][+!(i.link.id2 === this.objectId)];
        if (type === this.solrMediator.types.droneRequest) requestIds.push(id);
      });
      if (requestIds.length) {
        forkJoin(requestIds.map(rid => this.getRequest(rid))).subscribe(res => {
          this.docs = compact(res);
          this.makeVideoList();
          this.isLoading = false;
        });
      } else {
        this.isLoading = false;
      }
    });
  }

  getVideoUrl(fileName: string, flyByDate: any, isSource: boolean, contract: any): string {
    const videoRootPath = '/app/drone/videoFiles/';
    const videoType = isSource ? 'Source_video' : 'Converted_video';
    return [
      window.location.origin + videoRootPath + videoType,
      'Private',
      `Contract_${contract.number}_${formatDate(new Date(contract.date), 'dd.MM.yyyy', 'en')}`,
      'DS',
      formatDate(new Date(flyByDate), 'dd.MM.yyyy', 'en'),
      fileName.trim()
    ].join('/');
  }

  getRequest(id: string): Observable<any> {
    return Observable.create(obs => {
      this.droneService.getRequest(id).subscribe(res => {
          if (res && res.DroneRequest) {
            const flyBy$ = this.getFlyBy(res.DroneRequest.flybyId);
            const flyObject$ = this.getFlyObject(res.DroneRequest.objectId);
            const contract$ = this.getContract(res.DroneRequest.contractId);
            forkJoin([flyBy$, flyObject$, contract$]).subscribe(s => {
              ['FlyBy', 'flyObject', 'DroneStateContract'].forEach((p, i) => res[p] = s[i] ? s[i][p] : {});
              obs.next(res);
              obs.complete();
            }, error => {
              obs.next(null);
              obs.complete();
            });
          } else {
            obs.next(null);
            obs.complete();
          }
        }, error => {
        obs.next(null);
        obs.complete();
      })
    });
  }

  getFlyBy(id: string): Observable<any> {
    return Observable.create(obs => {
      this.droneService.getFlyBy(id)
        .subscribe(res => obs.next(res), error => obs.next(null), () => obs.complete());
    });
  }

  getFlyObject(id: string): Observable<any> {
    return Observable.create(obs => {
      this.droneService.getFlyObject(id)
        .subscribe(res => obs.next(res), error => obs.next(null), () => obs.complete());
    });
  }

  getContract(id: string): Observable<any> {
    return Observable.create(obs => {
      this.droneService.getFlyContract(id)
        .subscribe(res => obs.next(res), error => obs.next(null), () => obs.complete());
    });
  }

  isShowEditBtn(): boolean {
    return this.session.hasPermission('mr_objectAeroLink');
  }

  makeVideoList() {
    this.videos = [];
    this.docs.forEach((doc: any) => {
      if (doc.FlyBy && doc.FlyBy.dataMaterials) {
        doc.FlyBy.dataMaterials.forEach(fileName => {
          this.videos.push({
            videoName: fileName,
            objectName: this.helper.getField(doc, 'flyObject.name'),
            date: this.helper.getField(doc, 'FlyBy.dataDate'),
            time: this.helper.getField(doc, 'FlyBy.dataTime'),
            contract: this.helper.getField(doc, 'DroneStateContract')
          });
        });
      }
    });
  }
}
