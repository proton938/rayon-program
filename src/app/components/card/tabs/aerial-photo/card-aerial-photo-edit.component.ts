import {Component} from "@angular/core";
import {RayonLinkResourceService} from "../../../../services/rayon-link-resource.service";
import {Observable} from "rxjs/Rx";
import {Transition, fromJson, toJson, StateService} from "@uirouter/core";
import {find, some, uniq, isArray, findIndex} from 'lodash';
import {copy} from 'angular';
import {SolrMediatorService} from "../../../../services/solr-mediator.service";
import {CommonQueryService} from "../../../../services/common-query.service";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {forkJoin} from "rxjs/index";
import {DroneService} from "../../../../services/drone.service";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-card-aerial-photo-edit',
  templateUrl: './card-aerial-photo-edit.component.html',
  styleUrls: ['./card-aerial-photo-edit.component.scss']
})
export class ProgramCardAerialPhotoEditComponent {
  isLoading: boolean = true;
  showOnlyLinked: boolean = false;
  isPermissionDenied: boolean = false;
  errorMessage: string = '';
  links: any[] = [];
  linking: any = {};
  objectId: string;
  docs: any[] = [];
  commonSubject: string = '';
  flyBy: any = {};
  pagination: any = {
    currentPage: 1,
    itemsPerPage: 20,
    totalItems: 0,
    disable: false,
    itemsPerPageList: [20, 50, 100, null]
  };

  @BlockUI('docs-ui-block') blockUI: NgBlockUI;

  constructor(public state: StateService,
              private session: SessionStorage,
              private transition: Transition,
              private linkService: RayonLinkResourceService,
              private solrMediator: SolrMediatorService,
              private commonQuery: CommonQueryService,
              private helper: RayonHelperService,
              private droneService: DroneService) {}

  ngOnInit() {
    this.objectId = this.transition.params()['id'];
    this.isPermissionDenied = !this.session.hasPermission('mr_objectAeroLink');
    if (this.isPermissionDenied) {
      this.isLoading = false;
    } else {
      forkJoin([this.updateLinks(), this.updateList()]).subscribe(res => {
        this.upodateFlyByData();
        this.isLoading = false;
      }, (error: any) => {
        this.helper.error(error);
        this.errorMessage = error && error.userMessages && error.userMessages.length ? error.userMessages.join('. ') : '';
        this.isLoading = false;
      });
    }
  }

  changePage($event) {
    this.pagination.currentPage = $event.page;
    this.blockUI.start();
    this.updateList().subscribe(res => this.blockUI.stop());
  }

  updateLinks(): Observable<any> {
    return Observable.create(obs => {
      this.linkService.find(this.objectId).subscribe( res => {
        if (res && res.length) this.links = res;
        obs.next(res);
        obs.complete();
      }, error => {
        this.helper.error(error);
        obs.next([]);
        obs.complete();
      });
    });
  }

  updateList(): Observable<any> {
    return Observable.create(observer => {
      let makeEmptyResult = () => {
        this.pagination.totalItems = 0;
        this.docs = [];
        observer.next();
        observer.complete();
      };
      let makeResult = result => {
        this.pagination.totalItems = result && result.numFound ? result.numFound : 0;
        this.docs = result && result.docs ? result.docs : [];
        this.upodateFlyByData();
        observer.next();
        observer.complete();
      };
      let filter: any = this.getObjectListQueryParams();
      if (!filter.pageSize) {
        makeEmptyResult();
      } else {
        this.solrMediator.query(filter).subscribe(res => {
          if (filter.pageSize === 1) {
            this.pagination.totalItems = res && res.numFound ? res.numFound : 0;
            if (this.pagination.totalItems) {
              filter.pageSize = this.pagination.totalItems;
              this.solrMediator.query(filter).subscribe(deepRes => {
                makeResult(deepRes);
              }, error => {
                observer.error(error);
                observer.complete();
              });
            } else {
              makeEmptyResult();
            }
          } else {
            makeResult(res);
          }
        }, error => {
          observer.error(error);
          observer.complete();
        });
      }
    });
  }

  getObjectListQueryParams(): any {
    // if pageSize === 0 - empty results
    // if pageSize === 1 - all results on the one page
    // else - pageSize = pagination.itemsPerPage
    let result: any = {
      common: this.commonSubject,
      page: this.pagination.currentPage - 1 || 0,
      types: [this.solrMediator.types.droneRequest],
      pageSize: this.pagination.itemsPerPage || 0
    };
    if (this.showOnlyLinked) {
      let ids = this.links.length ? this.links
        .filter(i => i.link.status !== 'reject')
        .map(i => find([i.link.id1, i.link.id2], r => r !== this.objectId)) : [];
      if (ids.length) {
        result.pageSize = ids.length;
        result.query = 'documentId:' + ids.join(' OR documentId:');
      } else {
        result.pageSize = 0;
      }
    } else {
      if (!result.pageSize) result.pageSize = 1;
    }
    return result;
  }

  onSelectItemsPerPage() {
    this.changePage({page: 1});
  }

  upodateFlyByData() {
    let ids: string[] = this.docs.map(i => i.flyby_id).filter(id => !this.flyBy[id]);
    if (ids.length) {
      this.droneService.getFlyByList(ids).subscribe(res => {
        if (res && res.length) {
          res.forEach(fly => {
            if (fly.FlyBy) this.flyBy[fly.FlyBy.id] = fly.FlyBy;
          });
        }
      });
    }
  }

  add(id: string) {
    this.linking[id] = true;
    let link = this.getLink(id);
    if (!link) {
      this.createLink(id).subscribe(link => {
        this.links.push(link);
        this.linking[id] = false;
      }, error => {
        this.helper.error(error);
        this.linking[id] = false;
      });
    } else {
      if (link.link.status === 'reject') {
        this.linkService.changeStatus(link.link, 'approve').subscribe(() => {
          this.linking[id] = false;
        });
      }
    }
  }

  remove(id: string) {
    let link = this.getLink(id);
    if (link) {
      this.linking[id] = true;
      if (link.link.status === 'approve') {
        this.linkService.changeStatus(link.link, 'reject').subscribe(() => {
          this.linking[id] = false;
        });
      } else {
        let linkId: string = link.link.documentId;
        this.deleteLink(linkId).subscribe(() => {
          this.links.splice(findIndex(this.links, i => i.link.documentId === linkId), 1);
          this.linking[id] = false;
        });
      }
    }
  }

  getLink(id: string) {
    return find(this.links, (i: any) => i.link.id1 === id || i.link.id2 === id);
  }

  getLinksByStatus(status: string | string[], type?: string | string[]): any[] {
    let statuses: string[] = isArray(status) ? status : [status];
    let types: string[] = type ? (isArray(type) ? type : [type]) : [];
    let result: any[] = this.links.filter(i => some(statuses, s => s === i.link.status));
    if (types.length) result = result.filter(i => some(types, t => t === i.link.type2));
    return result;
  }

  hasLink(id: string, includeRejected: boolean = false) {
    return some(this.links, (i: any) => {
      return some([i.link.id1, i.link.id2], x => x === id)
        && (includeRejected ? true : i.link.status !== 'reject');
    });
  }

  isRejected(id: string) {
    return some(this.links, (i : any) => (i.link.id1 === id || i.link.id2 === id) && i.link.status === 'reject');
  }

  createLink(id: string): Observable<any> {
    return Observable.create(observer => {
      this.linkService.create({
        link: {
          id1: this.objectId,
          id2: id,
          system1: 'mr',
          system2: 'drone',
          subsystem1: 'program',
          subsystem2: 'drone',
          type1: 'OBJECT',
          type2: this.solrMediator.types.droneRequest,
          status: 'review'
        }
      }).subscribe(response => {
        observer.next(response);
      }, error => {
        this.helper.error(error);
        observer.error(error);
      }, () => {
        observer.complete();
      });
    });
  }

  deleteLink(linkId: string): Observable<any> {
    return Observable.create(observer => {
      this.linkService.delete(linkId).subscribe(response => {
        observer.next(response);
      }, error => {
        this.helper.error(error);
        observer.error(error);
      }, () => {
        observer.complete();
      });
    });
  }

  save() {
    this.blockUI.start();

    let links: any[] = this.getLinksByStatus('review');
    let linkRequests$ = links.map(i => this.linkService.changeStatus(i.link, 'approve'));

    let linksToDel: any[] = this.getLinksByStatus('reject');
    if (linksToDel.length) {
      linkRequests$.push(this.linkService.deleteLink(linksToDel.map(i => i.link.documentId)));
    }

    if (linkRequests$.length) {
      forkJoin(linkRequests$).subscribe(res => {
        this.links = this.links.filter(i => !some(linksToDel, d => d.link.documentId === i.link.documentId));
        this.blockUI.stop();
        this.close();
      }, error => {
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.blockUI.stop();
      this.close();
    }
  }

  close() {
    this.state.go(this.state.current, {mode: 'view'});
  }

  cancel() {
    this.blockUI.start();
    let actions: any[] = [], ids: string[] = [];
    let getList = status => this.getLinksByStatus(status) || [];

    ids = getList('review').map(i => i.link.documentId);
    if (ids.length) actions.push(this.linkService.deleteLink(ids));
    let linksToRepair: any[] = getList('reject').map(i => i.link);
    if (linksToRepair.length) {
      linksToRepair.forEach(i => actions.push(this.linkService.changeStatus(i, 'approve')));
    }

    if (actions.length) {
      forkJoin(actions).subscribe(() => {
        this.blockUI.stop();
        this.close();
      }, error => {
        this.blockUI.stop();
        this.helper.error(error);
      });
    } else {
      this.blockUI.stop();
      this.close();
    }
  }

  changeOnlyLinked() {
    this.showOnlyLinked = !this.showOnlyLinked;
    this.changePage({page: 1});
  }
}
