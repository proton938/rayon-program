import {Component} from "@angular/core";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {RayonLinkResourceService} from "../../../../services/rayon-link-resource.service";
import {forkJoin, from} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {find} from 'lodash';
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";
import {CommonQueryService} from "../../../../services/common-query.service";

@Component({
  selector: 'program-card-decisions',
  templateUrl: './card-decisions.component.html'
})
export class ProgramCardDecisionsComponent {
  document: RayonProgram;
  rows: any[] = [];
  isLoading: boolean = true;
  links: any[] = [];
  dicts: any = {};
  filterCommon: any = {
    subject: '',
    fields: [
      {code: 'prefectsName'},
      {code: 'districtsName'},
      {code: 'agendaDate', type: 'date'},
      {code: 'protocolNumber'},
      {code: 'protocolItem'},
      {code: 'typeName'},
      {code: 'content'},
      {code: 'executionDate', type: 'date'},
      {code: 'statusName'},
      {code: 'executorName'}
    ]
  };

  @BlockUI('block-ui-list') blockUI: NgBlockUI;

  constructor(private helper: RayonHelperService,
              private nsi: NsiResourceService,
              private rayonService: RayonProgramResourceService,
              private linkService: RayonLinkResourceService,
              private solrMediator: SolrMediatorService,
              private commonQueryService: CommonQueryService) {}

  ngOnInit() {
    this.document = this.rayonService.document;
    forkJoin([this.getDicts(), this.getLinks()]).subscribe(res => {
        if (res[0]) {
          this.dicts.organizations = this.dicts['mr_program_ResponsiblePersons'];
        }
        if (res[1]) {
          this.links = res[1];
        }
        this.search();
    });
  }

  search() {
    this.blockUI.start();
    this.searchDecisions(this.filterCommon.subject).subscribe(res => {
      this.rows = res;
      this.blockUI.stop();
    }, error => {
      this.helper.error(error);
      this.blockUI.stop();
    });
  }

  searchDecisions(commonSearch: string = ''): Observable<any[]> {
    return Observable.create(observer => {
      let ids = this.links.length ? this.links.filter(i => i.link.id2 === this.document.documentId && i.link.status === 'approve').map(i => i.link.id1) : [];
      if (ids.length) {
        let query = 'documentId:' + ids.join(' OR documentId:');
        if (commonSearch) {
          let commonQuery: string = this.commonQueryService.commonToQuery(commonSearch, this.filterCommon.fields, true);
          if (commonQuery) query += ' AND (' + commonQuery + ')';
        }
        this.solrMediator.query({page: 0, pageSize: 999, types: ['MR_MEETING_DECISION'], query: query}).subscribe(res => {
          observer.next(res && res.docs ? res.docs : []);
          observer.complete();
          this.isLoading = false;
        }, error => {
          observer.error(error);
          observer.complete();
          this.isLoading = false;
        });
      } else {
        observer.next([]);
        observer.complete();
        this.isLoading = false;
      }
    });
  }

  getLinks(): Observable<any[]> {
    return Observable.create(observer => {
      this.linkService.find(this.document.documentId).subscribe(linkRes => {
        observer.next(linkRes || []);
      }, error => observer.error(error), () => observer.complete());
    });
  }

  getExecutor(code: string, row: any, index: number): string {
    let r: string = '';
    let org: any = find(this.dicts.organizations, org => org.code === code);
    if (org) {
      if (org.fullName) r = org.fullName;
      if (org.position) r = org.position + ' (' + r + ')';
    }
    return r;
  }

  getDicts(): Observable<any[]> {
    return Observable.create(observer => {
      from(this.nsi.getDictsFromCache(['mr_program_ResponsiblePersons'])).subscribe(res => {
        observer.next(res);
        observer.complete()
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }
}
