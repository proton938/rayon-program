import {Component} from "@angular/core";
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {first} from "lodash";
import {from} from "rxjs/index";

@Component({
  selector: 'program-card-consideration',
  templateUrl: './card-consideration.component.html',
  styleUrls: ['./card-consideration.component.scss']
})
export class ProgramCardConsiderationComponent {
  document: RayonProgram;
  isLoading: boolean = true;
  settings: any = {};

  items: any[] = [];

  constructor(private helper: RayonHelperService,
              private rayonService: RayonProgramResourceService,
              private nsiService: NsiResourceService) {}

  ngOnInit() {
    this.document = this.rayonService.document;

    from(this.nsiService.getDictsFromCache(['Settings'])).subscribe(response => {
      this.settings = response['Settings'];
      this.makeItems();
    });
  }

  makeItems() {
    switch(this.document.documentId) {
      case '03ae67b6-4d3f-4f29-afba-b3cd68807cca':
        this.items.push({
          date: '2018-11-22',
          name: 'Совещание у Мэра Москвы',
          href: this.getSetting('ref_meeting'),  // 'card-headquarters_subpoena.html',
          description: 'О выполнении поручений Мэра Москвы С.С.Собянина в районах Таганский, Западное Дегунино',
          questions: [
            {
              name: 'Обеспечить строительство нового детского сада на 200 мест по адресу: 2-я Дубровская ул., вл. 1',
              href: this.getSetting('ref_question_sad'), // 'card-headquarters_2.html',
              speaker: 'Хуснуллин М.Ш.',
              resolutions: [
                {
                  name: 'Принять к сведению информацию о строительстве за счет средств АИП  ДОУ по адресу: Дубровский пр., д. 7 (район Южнопортовый) в связи с невозможностью строительства ДОУ на 200 мест по адресу: 2-я Дубровская,  вл. 1.',
                  icon: 'ticket',
                  status: 'В работе',
                  statusColor: 'primary',
                  statusDate: '2021-12-31',
                  author: 'М.Ш. Хуснуллин'
                }
              ]
            }
          ]
        });
        break;

      case 'e29b253a-3e83-46ff-bb89-8e4bf63b0d6e':
        this.items.push({
          date: '2018-11-22',
          name: 'Совещание у Мэра Москвы',
          href: this.getSetting('ref_meeting'),  // 'card-headquarters_subpoena.html',
          description: 'О выполнении поручений Мэра Москвы С.С.Собянина в районах Таганский, Западное Дегунино',
          questions: [
            {
              name: 'Обеспечить проведение капитального ремонта Московского театра русской драмы под руководством М.Щепенко, ул. Земляной Вал,  д. 64/17',
              href: this.getSetting('ref_question_teatr'), // 'card-headquarters_1.html',
              speaker: 'Сергунина Н.А.',
              resolutions: [
                {
                  name: 'Обеспечить завершение капитального ремонта Московского театра русской драмы под руководством М.Щепенко, ул. Земляной Вал,  д. 64/17',
                  icon: 'exclamation',
                  status: '',
                  statusColor: '',
                  statusDate: '',
                  author: 'Сергунина Н.А.'
                }
              ]
            }
          ]
        });
        break;
    }
    console.log('items:', this.items);
  }

  getSetting(code: string = ''): string {
    let r: string = '';
    this.settings.filter(i => i.code === code).forEach(i => r = i.value);
    return r;
  }
}
