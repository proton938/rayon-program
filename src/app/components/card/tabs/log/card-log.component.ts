import {Component} from "@angular/core";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {RayonProgram} from "../../../../models/program/RayonProgram";

@Component({
  selector: 'program-card-log',
  templateUrl: './card-log.component.html'
})
export class ProgramCardLogComponent {
  document: RayonProgram;

  constructor(private programService: RayonProgramResourceService) {}

  ngOnInit() {
    this.document = this.programService.document;
  }
}
