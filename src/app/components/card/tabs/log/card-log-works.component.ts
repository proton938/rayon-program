import {Component} from "@angular/core";
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {from} from "rxjs/index";
import {RayonWorkTypeService} from "../../../../services/rayon-work-type.service";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {RayonWork} from "../../../../models/work/RayonWork";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";

@Component({
  selector: 'program-card-log-works',
  templateUrl: './card-log-works.component.html'
})
export class ProgramCardLogWorksComponent {
  isLoading: boolean = true;
  document: RayonProgram;
  works: RayonWork[] = [];
  expanded: boolean[] = [];
  workStatusColors: any = {};

  constructor(private programService: RayonProgramResourceService,
              private nsi: NsiResourceService,
              private solrMediator: SolrMediatorService,
              private workTypeService: RayonWorkTypeService,
              private helper: RayonHelperService) {}

  ngOnInit() {
    this.document = this.programService.document;

    from(this.nsi.getDictsFromCache(['mr_program_WorkStatuses'])).subscribe(response => {
      response['mr_program_WorkStatuses'].forEach(i => { this.workStatusColors[i.code] = i.color; });
    });

    const workRequest$ = this.solrMediator.query({
      page: 0,
      pageSize: 999,
      types: [this.solrMediator.types.work],
      query: 'objectIdWork:' + this.document.documentId
    });
    from(workRequest$).subscribe(response => {
      if (response && response.docs) {
        let ids = response.docs.map(i => i.documentId);
        this.workTypeService.list(ids).subscribe(res => {
          this.works = res.sort((a: RayonWork, b: RayonWork) => +b.documentDate - +a.documentDate);
          this.expanded = this.works.map(i => false);
          this.isLoading = false;
        });
      } else this.isLoading = false;
    });
  }
}
