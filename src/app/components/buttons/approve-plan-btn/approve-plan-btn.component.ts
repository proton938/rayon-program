import {Component, EventEmitter, Input, Output} from "@angular/core";
import {RayonPlan} from "../../../models/plan/RayonPlan";
import {AlertService} from "@reinform-cdp/widgets";
import {SessionStorage} from "@reinform-cdp/security";
import {PlanResourceService} from "../../../services/plan-resource.service";
import {find, findIndex} from 'lodash';
import {copy} from 'angular';
import {forkJoin} from "rxjs/index";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'approve-plan-btn',
  templateUrl: './approve-plan-btn.component.html'
})
export class ApprovePlanBtnComponent {
  @Input() document: RayonPlan;
  @Input() disabled: boolean;
  @Input() className: string = '';
  @Input() mode: string = 'view';
  @Output() onStart = new EventEmitter();
  @Output() onStop = new EventEmitter();
  @Output() onError = new EventEmitter();

  loading: boolean = true;
  isShow: boolean = false;
  inProgress: boolean = false;

  constructor(private alertService: AlertService,
              private session: SessionStorage,
              private planService: PlanResourceService) {}

  ngOnInit() {
    this.isShow = this.session.hasPermission('MR_PLAN_APPROVE_BUTTON');
    if (this.isShow) this.isShow = this.document.statusId.code === 'project';
  }

  approve() {
    this.alertService.confirm({
      message: 'Вы действительно хотите изменить статус текущего мастер-плана на «Действующий»?',
      okButtonText: 'Ок',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.startApprove();
    }).catch(error => {
      console.log(error);
    });
  }

  startApprove() {
    this.startEmitter();
    this.planService.approve(this.document.documentId).subscribe(() => {
      this.stopEmitter();
    }, error => {
      this.stopEmitter();
      this.onError.emit(error);
    });
  }

  startEmitter() {
    this.inProgress = true;
    this.onStart.emit();
  }

  stopEmitter() {
    this.inProgress = false;
    this.onStop.emit();
  }
}
