import {Component, Input, OnInit} from '@angular/core';
import {RayonProgram} from '../../../models/program/RayonProgram';
import {Observable, from} from 'rxjs';
import {SessionStorage} from '@reinform-cdp/security';
import {toString} from 'lodash';
import {StateService} from '@uirouter/core';
import {RayonHelperService} from '../../../services/rayon-helper.service';
import {RayonProgramBtnService} from '../../../services/rayon-program-btn.service';
import {ActivityResourceService} from '@reinform-cdp/bpm-components';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'object-adjustment-btn',
  templateUrl: './object-adjustment-btn.component.html'
})
export class ObjectAdjustmentBtnComponent implements OnInit {
  @Input() document: RayonProgram;
  @Input() disabled = false;
  @Input() className = '';

  loading = true;
  correcting = false;
  isShow = false;

  constructor(public state: StateService,
              private session: SessionStorage,
              private helper: RayonHelperService,
              private btnService: RayonProgramBtnService,
              private activityService: ActivityResourceService) {}

  ngOnInit() {
    // #ISMR-3033
    this.isShow = this.session.hasPermission('mr_objectAdjustment');
    this.loading = false;
    /*this.btnService.isShowEditObjectBtn(this.document).subscribe((isShow: boolean) => {
      this.isShow = isShow;
      this.loading = false;
    }, error => {
      this.helper.error(error);
      this.loading = false;
    });*/
  }

  startCorrectProcess() {
    const processId = 'mrobject_ObjectAdjustment';
    this.correcting = true;
    this.getOpenedTasks().subscribe(tasks => {
      if (tasks.length) {
        this.helper.goToTask('app.execution.adjustObjectInformation', tasks[0].id);
        /*this.state.go('app.execution.adjustObjectInformation', {
          system: 'mrobject',
          taskId: tasks[0].id,
          systemCode: 'MR'
        });*/
      } else {
        from(this.activityService.getProcessDefinitions({
          keyLike: processId + '%',
          latest: true,
          size: 1
        })).subscribe((def: any) => {
          const defId = def.data[0].id;
          from(this.activityService.initProcess({
            processDefinitionId: defId,
            variables: [
              {name: 'EntityIdVar', value: this.document.documentId},
              {name: 'ExecutorVar', value: this.session.login()}
            ]
          })).subscribe(processId => {
            this.helper.success('Процесс успешно запущен!');
            from(this.activityService.getTasks({executionId: toString(processId)})).subscribe((res: any) => {
              if (res && res.data && res.data[0]) {
                const taskId = res.data[0].id;
                this.helper.goToTask('app.execution.adjustObjectInformation', taskId);
                /*this.state.go('app.execution.adjustObjectInformation', {
                  system: 'mrobject',
                  taskId: taskId,
                  systemCode: 'MR'
                });*/
              }
              this.correcting = false;
            }, error => {
              this.helper.error(error);
              this.correcting = false;
            });
          }, error => {
            this.helper.error(error);
            this.correcting = false;
          });
        }, error => {
          this.helper.error(error);
          this.correcting = false;
        });
      }
    }, error => {
      this.helper.error(error);
      this.correcting = false;
    });
  }

  getOpenedTasks(): Observable<any> {
    return Observable.create(observer => {
      from(this.activityService.getTasksHistory({
        processDefinitionKeyLike: 'mrobject_ObjectAdjustment%',
        processVariables: [{
          name: 'EntityIdVar',
          value: this.document.documentId,
          operation: 'equals',
          type: 'string'
        }, {
          name: 'ExecutorVar',
          value: this.session.login(),
          operation: 'equals',
          type: 'string'
        }]
      })).subscribe(res => {
        let tasks = [];
        if (res && res.data && res.data.length) {
          tasks = res.data.filter(i => !i.endTime);
        }
        observer.next(tasks);
      }, error => this.helper.error(error), () => {
        observer.complete();
      });
    });
  }
}
