import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SessionStorage} from '@reinform-cdp/security';
import {AlertService} from '@reinform-cdp/widgets';
import {ChangeRequestService} from '../../../services/change-request.service';
import {ChangeRequest} from '../../../models/change-request/ChangeRequest';
import {Observable, from, of} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {find, first, toString, some} from 'lodash';
import {RayonProgramResourceService} from '../../../services/rayon-program-resource.service';
import {RayonWorkTypeService} from '../../../services/rayon-work-type.service';
import {ChangeRequestDocument} from '../../../models/change-request/ChangeRequestDocument';
import {RayonHelperService} from '../../../services/rayon-helper.service';
import {ChangeRequestCreator} from '../../../models/change-request/ChangeRequestCreator';
import {ActivityResourceService, ITaskVariable} from '@reinform-cdp/bpm-components';
import { BsModalService } from 'ngx-bootstrap/modal';
import {ChangeRequestDataModalComponent} from './change-request-data-modal.component';
import {RayonProgram} from '../../../models/program/RayonProgram';
import {ChangeRequestObject} from '../../../models/change-request/ChangeRequestObject';
import {ChangeRequestWork} from '../../../models/change-request/ChangeRequestWork';
import {toJson, fromJson} from '@uirouter/core';
import {RayonWorkWrapper} from "../../../models/work/RayonWorkWrapper";

@Component({
  selector: 'change-request-btn',
  templateUrl: './change-request-btn.component.html'
})
export class ChangeRequestBtnComponent implements OnInit {
  processing = false;
  isReady = false;
  dicts: any = {};
  vars: any = {};
  obj: RayonProgram;

  @Input() id = '';
  @Input() text = '';
  @Input() title = '';
  @Input() className = 'btn';
  @Input() classIcon = 'fa fa-list m-r-sm';
  @Input() type = ''; // 'object' | 'work'
  @Input() show = true;

  @Output() onComplete = new EventEmitter();
  @Output() onStart = new EventEmitter();
  @Output() onStop = new EventEmitter();

  constructor(private alertService: AlertService,
              private nsi: NsiResourceService,
              private programService: RayonProgramResourceService,
              private workService: RayonWorkTypeService,
              private helper: RayonHelperService,
              private session: SessionStorage,
              private changeRequest: ChangeRequestService,
              private activityResourceService: ActivityResourceService,
              private modalService: BsModalService) {}

  ngOnInit() {
    from(this.nsi.getDictsFromCache([
      'mr_program_requestStatus',
      'mr_program_ResponsibleOrganizations',
      'mr_program_requestAgreement'
    ])).subscribe(res => {
      this.dicts = res;
      this.dicts.requestAgreement = this.dicts['mr_program_requestAgreement'].map(i => this.helper.convertToNsi(i));
      this.isReady = true;
    });
    if (this.type === 'object' || this.type === 'work') {
      this.show = this.session.hasPermission('MR_CHANGEREQUEST_BUTTON');
    }
  }

  onClick() {
    if (!this.isReady || this.processing) { return false; }
    this.alertService.confirm({
      message: 'Вы действительно хотите запустить процесс по изменению данных?',
      okButtonText: 'Ок',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.start();
    }).catch(error => {
      console.log(error);
    });
  }

  start() {
    this.showIndicator();
    this.createRequest().subscribe(doc => {
      this.helper.success(this.lang.requestCreated);
      this.startProcess(doc.documentId).subscribe((processId: string) => {
        this.helper.success('Процесс успешно запущен!');
        from(this.activityResourceService.getTasks({
          processInstanceId: processId
        })).subscribe(tasksRes => {
          if (tasksRes && tasksRes.data) {
            const targetTaskInfo = first(tasksRes.data.sort((a: any, b: any) => b.createTime - a.createTime));
            this.hideIndicator();
            this.helper.goToTask('app.execution.' + this.lang.task, targetTaskInfo.id);
          }
        }, error => {
          this.helper.error(error);
          this.hideIndicator();
        });
      }, error => {
        this.helper.error(error);
        this.hideIndicator();
      });
    }, error => {
      if (error) { this.helper.error(error); }
      this.hideIndicator();
    });
  }

  createRequest(): Observable<ChangeRequest> {
    const request = new ChangeRequest();
    request.documentDate = new Date();
    request.documentDate.setHours(0, 0, 0);
    request.status = find(this.dicts['mr_program_requestStatus'], i => i.code === 'notsend') || null;
    if (request.status) { request.status = this.helper.convertToNsi(request.status); }
    request.creator = new ChangeRequestCreator();
    request.creator.build({
      fio: this.session.fullName(),
      organization: {
        ldapOrgCode: this.session.departmentCode(),
        ldapOrgName: this.session.departmentFullName()
      },
      login: this.session.login()
    });
    request.modifiedTab = this.lang.docType;

    return Observable.create(obs => {
      const createRequest = () => {
        request.object = new ChangeRequestObject();
        request.object.build({
          prefect: this.helper.getField(this.obj, 'address.prefect') || [],
          district: this.helper.getField(this.obj, 'address.district') || [],
          name: this.helper.getField(this.obj, 'object.name') || '',
          industry: this.helper.getField(this.obj, 'object.industry') || null,
          kind: this.helper.getField(this.obj, 'object.kind') || null,
          organization: this.helper.getField(this.obj, 'responsible.organization') || null
        });
        this.changeRequest.create(request.min()).subscribe(res => obs.next(res), error => obs.error(error), () => obs.complete());
      };
      this.getDocument(this.id).subscribe(doc => {
        if (!request.document) { request.document = new ChangeRequestDocument(); }
        debugger;
        request.document.jsonOld = request.document.jsonNew = toJson(doc.min());

        if (this.type === 'work') { // #ISMR-2788
          this.programService.get(doc.work.objectId).subscribe(res => {
            this.obj = this.programService.getInstance(res);
            this.prepareWork(doc).subscribe(res => {
              if (this.helper.getField(doc.work, 'type.name')) {
                request.work = new ChangeRequestWork();
                request.work.build({type: doc.work.type});
              }
              request.agreement = find(this.dicts.requestAgreement, i => {
                return i.code === (this.vars.neededApproval ? 'agreement' : 'noagreement');
              });
              createRequest();
            }, error => {
              obs.error(error);
              obs.complete();
            });
          }, error => {
            obs.error(error);
            obs.complete();
          });
        } else {
          this.obj = this.programService.getInstance(doc);
          request.agreement = find(this.dicts.requestAgreement, i => i.code === 'agreement');
          createRequest();
        }
      }, error => {
        obs.error(error);
        obs.complete();
      });
    });
  }

  prepareWork(doc: any): Observable<any> {
    return Observable.create(obs => {
      // step 1
      const orgCode: string = this.helper.getField(doc, 'work.organization.code');
      const org = orgCode ? find(this.dicts['mr_program_ResponsibleOrganizations'], i => i.code === orgCode) : null;
      const iCan: boolean = org ? some(this.session.groups(), g => g === org.group) : false;
      // step 2
      if (iCan) {
        const workStatus: string = this.helper.getField(doc, 'work.status.code');
        const workApproveMunDep: string = this.helper.getField(doc, 'work.approveMunicipalDep.code');
        const workApproveAg: string = this.helper.getField(doc, 'work.approveAg.code');
        if (((workStatus === 'canceled' || workStatus === 'done') && workApproveMunDep !== 'neprov' && workApproveAg !== 'neprov')
          /*|| (!workStatus && !workStatus && !workApproveAg)*/) {
          // go to step 4
          this.vars.neededApproval = true;
          obs.next(doc);
          obs.complete();

        } else {
          // step 3
          let value: any = null;
          const modalRef = this.modalService.show(ChangeRequestDataModalComponent, {
            class: 'modal-md',
            initialState: {
              model: doc.work,
              type: this.type,
              obj: this.obj,
              saveCallback: (result: any) => {
                if (result && result.value !== null) { this.vars.neededApproval = value = result.value; }
              }
            }
          });
          const subscription = this.modalService.onHide.subscribe(() => {
            if (value !== null) { obs.next(doc); } else { obs.error(''); }
            obs.complete();
            subscription.unsubscribe();
          });
        }
      } else {
        // go to step 4
        this.vars.neededApproval = true;
        obs.next(doc);
        obs.complete();
      }
    });
  }

  startProcess(id: string): Observable<string> {
    const processKey: string = this.lang.processKey;
    return Observable.create(observer => {
      this.activityResourceService.getProcessDefinitions({
        key: processKey,
        latest: true
      }).then(response => {
        const processId: string = first(this.helper.getField(response, 'data.id') || [null]);
        if (processId) {
          return this.activityResourceService.initProcess({
            processDefinitionId: processId,
            variables: this.getProcessVariables(id)
          });
        } else {
          throw new Error('Процесс с идентификатором ' + processKey + ' не найден');
        }
      }).then((processId: number) => {
        observer.next(toString(processId));
        observer.complete();
      }).catch(error => {
        this.helper.error('Ошибка при запуске процесса!');
        this.helper.error(error && error.message ? error.message : error);
        observer.error(error);
        observer.complete();
      });
    });
  }

  getDocument(id): Observable<any> {
    switch (this.type) {
      case 'object':
        return this.programService.get(id);
      case 'work':
        return this.workService.get(id).pipe(map(res => {
          const r = new RayonWorkWrapper();
          r.build(res);
          return r;
        }));
    }
  }

  getProcessVariables(id): ITaskVariable[] {
    const r: ITaskVariable[] = [
      {name: 'EntityIdVar', value: id},
      {name: 'ResponsibleVar', value: this.session.login()}
    ];
    if (this.type === 'work') {
      r.push({name: 'ApprovalNeededVar', value: this.vars.neededApproval});
    }
    return r;
  }

  get lang(): any {
    switch (this.type) {
      case 'object':
        return {
          docType: 'OBJ',
          requestCreated: 'Заявка на изменение объекта создана',
          processKey: 'mrchangerequest_prepAppObjChangesAmipmAppr',
          task: 'mrchangerequestprepAppObjWorkChanges'
        };
      case 'work':
        return {
          docType: 'WORK',
          requestCreated: 'Заявка на изменение работы на объекте создана',
          processKey: 'mrchangerequest_prepAppWorksChangesAmipm',
          task: 'mrchangerequestprepAppObjectWorkChanges'
        };
    }
  }

  get iconClassName(): string {
    let r: string = this.classIcon;
    if (this.processing) {
      const parts: string[] = r.split(/\s+/).filter(i => !/fa\-.+/.test(i));
      parts.push('fa-spinner fa-spin');
      r = parts.join(' ');
    }
    return r;
  }

  showIndicator() {
    this.processing = true;
    this.onStart.emit();
  }

  hideIndicator() {
    this.processing = false;
    this.onStop.emit();
  }
}
