import {Component, Input, OnInit} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {RayonHelperService} from '../../../services/rayon-helper.service';
import {some, isBoolean} from 'lodash';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'change-request-data-modal',
  templateUrl: './change-request-data-modal.component.html',
  styleUrls: ['./change-request-data-modal.component.scss']
})
export class ChangeRequestDataModalComponent implements OnInit {

  selected: any = {
    value: null
  };
  dicts: any = {};

  @Input() model: any;
  @Input() type = 'work';
  @Input() obj: any;
  @Input() saveCallback: (result: any) => void;

  constructor(private bsModalRef: BsModalRef,
              public helper: RayonHelperService) {}

  ngOnInit() {
    this.dicts.amipm = [
      {
        code: false,
        name: 'Данные, изменение которых не требует согласования с АМиПМ',
        fields: this.getFieldList(false)
      },
      {
        code: true,
        name: 'Данные, для изменения которых необходимо согласование с АМиПМ',
        fields: this.getFieldList(true)
      },
    ];
    const filledAmipm = this.dicts.amipm.filter(i => i.fields.length);
    if (filledAmipm.length === 1) {
      this.selected.value = filledAmipm[0].code;
    }
  }

  getFieldList(code: any): any[] {
    switch (this.type) {
      case 'work':
        return this.getWorkFieldList(code);
      default:
        return [];
    }
  }

  getWorkFieldList(code: any): any[] {
    const r: any[] = [];
    const status =              this.helper.getField(this.model, 'status.code');
    const approveAg =           this.helper.getField(this.model, 'approveAg.code');
    const approveMunicipalDep = this.helper.getField(this.model, 'approveMunicipalDep.code');
    const name =                this.helper.getField(this.model, 'name');
    const organization =        this.helper.getField(this.model, 'organization.code');
    const executionStart =      this.helper.getField(this.model, 'execution.start');
    const executionFinish =     this.helper.getField(this.model, 'execution.finish');
    const typeCode =            this.helper.getField(this.model, 'type.code');
    const kindCode =            this.helper.getField(this.obj, 'object.kind.code');

    if (code) {
      if (name) { r.push({code: 'name', name: 'Состав работ'}); }
      if (organization) { r.push({code: 'organization', name: 'Ответственный исполнитель'}); }
      if (executionStart) { r.push({code: 'execution.start', name: 'Плановый срок начала работ'}); }
      if (executionFinish) { r.push({code: 'execution.finish', name: 'Плановый срок окончания работ'}); }
      if (isBoolean(this.model.budget)
        || this.model.stateProgram
        || this.model.stateCustomer
        || this.model.general
        || this.model.financed
        || (this.model.byYear && this.model.byYear.length)
        || (this.model.finance && this.model.finance.length)
        || this.model.additional) { r.push({code: 'execution.finish', name: 'Сведения о финансировании'}); }
      if (status && some(['canceled', 'done'], i => i === status)) { r.push({code: 'status', name: 'Текущая стадия работ'}); }
      r.push({code: 'risk', name: 'Риски публикации'});
      if (approveMunicipalDep && approveMunicipalDep !== 'neprov'
        && typeCode === 'accomplishment' && kindCode === 'YARD') {
        r.push({code: 'approveMunicipalDep', name: 'Согласование с муниципальными депутатами'});
      }
      if (approveAg && approveAg !== 'neprov') { r.push({code: 'approveAg', name: 'Согласование на АГ'}); }
      if (this.model.comment) { r.push({code: 'comment', name: 'Дополнительные сведения'}); }
    } else {
      if (!status || (status && !some(['canceled', 'done'], i => i === status))) {
        r.push({code: 'status', name: 'Текущая стадия работ'});
      }
      if (!approveAg || approveAg === 'neprov') {
        r.push({code: 'approveAg', name: 'Согласование на АГ'});
      }
      if ((!approveMunicipalDep || approveMunicipalDep === 'neprov') && typeCode === 'accomplishment' && kindCode === 'YARD') {
        r.push({code: 'approveMunicipalDep', name: 'Согласование с мунципальными депутатами'});
      }
    }
    return r;
  }

  save() {
    if (this.isOK()) {
      if (this.saveCallback) { this.saveCallback(this.selected); }
      this.cancel();
    }
  }

  cancel() {
    this.bsModalRef.hide();
  }

  isOK(): boolean {
    return this.selected.value !== null;
  }
}
