import {Component, Input, OnInit} from '@angular/core';
import {RayonProgram} from '../../../models/program/RayonProgram';
import {SessionStorage} from '@reinform-cdp/security';
import {StateService} from '@uirouter/core';
import {RayonHelperService} from '../../../services/rayon-helper.service';
import {RayonProgramBtnService} from '../../../services/rayon-program-btn.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'object-edit-btn',
  templateUrl: './object-edit-btn.component.html'
})
export class ObjectEditBtnComponent implements OnInit {
  @Input() document: RayonProgram;
  @Input() disabled: boolean;
  @Input() className = '';
  @Input() mode = 'view';

  loading = true;
  isShow = false;

  constructor(public state: StateService,
              private session: SessionStorage,
              private helper: RayonHelperService,
              private btnService: RayonProgramBtnService) {}

  ngOnInit() {
    // #ISMR-3033
    this.isShow = this.session.hasPermission('mr_objectEdit');
    this.loading = false;
    /*this.btnService.isShowEditObjectBtn(this.document).subscribe((isShow: boolean) => {
      this.isShow = isShow;
      this.loading = false;
    }, error => {
      this.helper.error(error);
      this.loading = false;
    });*/
  }
}
