import {Component, Input} from "@angular/core";
import * as jsonpatch from 'fast-json-patch';
import {RayonProgram} from "../../../models/program/RayonProgram";
import {forkJoin, from} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {RayonWork} from "../../../models/work/RayonWork";
import {RayonWorkTypeService} from "../../../services/rayon-work-type.service";
import {AlertService} from "@reinform-cdp/widgets";
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SolrMediatorService} from "../../../services/solr-mediator.service";

@Component({
  selector: 'object-to-archive-btn',
  templateUrl: './object-to-archive-btn.component.html'
})
export class ObjectToArchiveBtnComponent {
  @Input() documentId?: string;
  @Input() document?: RayonProgram;
  @Input() className?: string = '';

  archiving: boolean = false;
  workDocs: any[] = [];
  works: any[] = [];

  constructor(private solrMediator: SolrMediatorService,
              private workTypeService: RayonWorkTypeService,
              private alertService: AlertService,
              private helper: RayonHelperService,
              private programService: RayonProgramResourceService) {}

  ngOnInit() {
    if (!this.document) this.document = this.programService.document;
    if (!this.documentId) this.documentId = this.document.documentId;
  }

  getWorks(): Observable<any> {
    return Observable.create(observer => {
      this.solrMediator.query({page: 0, pageSize: 50, types: [this.solrMediator.types.work], query: 'objectIdWork:' + this.documentId}).subscribe(res => {
        this.workDocs = res&& res.docs ? res.docs : [];
        if (this.workDocs.length) {
          this.workTypeService.list(this.workDocs.map(i => i.documentId)).subscribe(res => {
            this.works = res.sort((a: RayonWork, b: RayonWork) => +b.documentDate - +a.documentDate);
            observer.next(res);
            observer.complete();
          }, error => {
            observer.error(error);
            observer.complete();
          });
        } else {
          observer.next([]);
          observer.complete()
        }
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  toArchive() {
    this.alertService.confirm({
      message: 'Вы действительно ходите переместить документ в архив?',
      okButtonText: 'Ок',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.sendObjectToArchive();
    }).catch(error => {
      console.log(error);
    });
  }


  toRestore() {
    this.alertService.confirm({
      message: 'Вы действительно ходите восстановить документ?',
      okButtonText: 'Ок',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.sendObjectToRestore();
    }).catch(error => {
      console.log(error);
    });
  }

  sendObjectToRestore() {
    this.archiving = false;
    let old = new RayonProgram();
    old.build(JSON.parse(JSON.stringify(this.document)));

    let doc = new RayonProgram();
    doc.build(JSON.parse(JSON.stringify(this.document)));
    doc.archive = false;

    let diff: any[] = jsonpatch.compare({obj: old.min()}, {obj: doc.min()});
    this.programService.patch(this.document.documentId, diff).subscribe( () => {
      this.helper.success('Документ восстановлен!');
      this.document.archive = false;
    }, error => {
      this.helper.error(error);
      this.archiving = true;
    });
  }

  sendObjectToArchive() {
    this.archiving = true;
    let old = new RayonProgram();
    old.build(JSON.parse(JSON.stringify(this.document)));

    let doc = new RayonProgram();
    doc.build(JSON.parse(JSON.stringify(this.document)));
    doc.archive = true;
    doc['public'] = doc.mos['public'] = false;

    let diff: any[] = jsonpatch.compare({obj: old.min()}, {obj: doc.min()});
    this.programService.patch(this.document.documentId, diff).subscribe(() => {
      this.getWorks().subscribe((works: RayonWork[]) => {
        let workList$ = [];
        works.forEach(w => {
          let workOld = new RayonWork();
          workOld.build(JSON.parse(JSON.stringify(w)));
          let work = new RayonWork();
          work.build(JSON.parse(JSON.stringify(w)));
          work['public'] = work.mos['public'] = false;
          let workDiff: any[] = jsonpatch.compare({work: workOld.min()}, {work: work.min()});
          workList$.push(this.workTypeService.patch(w.documentId, workDiff));
        });
        if (workList$.length) {
          forkJoin(workList$).subscribe(res => {
            this.sendChangeNotify();
          });
        } else {
          this.sendChangeNotify();
        }
      }, error => {
        this.helper.error(error);
        this.archiving = false;
      });
    }, error => {
      this.helper.error(error);
      this.archiving = false;
    });
  }

  sendChangeNotify() {
    // this.archiving = true;
    this.programService.changeNotify(this.documentId, 'OBJ').subscribe(res => {
      this.helper.success('Перемещение в архив: успешно');
      window.location.href = '/main/';
      this.archiving = false;
    }, error => {
      this.helper.error(error);
      this.archiving = false;
    });
  }
}
