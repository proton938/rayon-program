import {Component, Input} from "@angular/core";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {RayonWorkTypeService} from "../../../../services/rayon-work-type.service";
import {RayonGeoService} from "../../../../services/rayon-geo.service";
import {from} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {find, findIndex, compact} from 'lodash';
import {RayonWork} from "../../../../models/work/RayonWork";
import {FormWorkTypeEditModalComponent} from "../../../forms/work-type/form-work-type-edit-modal.component";
import {BsModalService} from "ngx-bootstrap";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";

@Component({
  selector: 'publication-request-documents',
  templateUrl: './publication-request-documents.component.html'
})
export class PublicationRequestDocumentsComponent {
  @Input() documents: any[] = [];
  @Input() selection: boolean;
  @Input() mosRu?: boolean = false;
  @Input() readOnly?: boolean = true;

  isLoading: boolean = true;
  dicts: any = {};
  docs: any[] = [];
  docWorks: any = {};
  gisCollection: any = {};
  docsInfo: any = {};
  isDocsLoading: boolean = false;
  isWorksLoading: boolean = false;
  docsPagination: any = {
    totalItems: 0,
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(private helper: RayonHelperService,
              private nsi: NsiResourceService,
              private workTypeService: RayonWorkTypeService,
              private geo: RayonGeoService,
              private modalService: BsModalService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    this.docsPagination.totalItems = this.documents.length;
    from(this.nsi.getDictsFromCache(['mr_program_requestPublish'])).subscribe(res => {
      this.dicts = res;
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess() {
    this.isLoading = false;
    this.changeDocsPage({page: 1});
  }

  changeDocsPage($event) {
    this.docsPagination.currentPage = $event.page;
    this.docs = this.documents.filter((item, index) => {
      return index >= ($event.page - 1) * this.docsPagination.itemsPerPage
        && index < ($event.page - 1) * this.docsPagination.itemsPerPage + this.docsPagination.itemsPerPage;
    });
    /*this.docs.forEach(r => {
      this.getGeometry(r.id, 'MR_PROGRAM_' + (this.selection || r.type === 'OBJ' ? 'OBJECT' : r.type));
    });*/
    this.getDocsInfo();
  }

  getDocsInfo() {
    let objectIds: string[] = this.docs.filter(i => i.type === 'OBJ' && !this.docsInfo[i.id]).map(i => i.id);
    let workIds: string [] = this.docs.filter(i => i.type === 'WORK').map(i => i.id);

    this.getWorksByIds(workIds).subscribe(ids => {
      if (ids.length) objectIds = objectIds.concat(ids);
      objectIds = compact(objectIds);

      if (objectIds.length) {
        this.isDocsLoading = true;
        this.solrMediator.getByIds([{type: this.solrMediator.types.obj, ids: objectIds}]).subscribe(res => {
          if (res && res[0] && res[0].docs) {
            res[0].docs.forEach((d: any) => {
              this.docsInfo[d.documentId] = d;
            });
            this.getWorks();
            this.isDocsLoading = false;
          }
        })
      }
    });
  }

  getWorks() {
    let queryParts: string[] = [];

    this.docs.forEach(i => {
      switch(i.type) {
        case 'WORK':
          queryParts.push('documentId:' + i.id); // Чтобы показать только его, если вдруг их много привязано к объекту
          break;
        case 'OBJ':
          queryParts.push('objectIdWork:' + i.id);
          break;
      }
    });

    if (queryParts.length) {
      let query = {page: 0, pageSize: 999, types: [this.solrMediator.types.work], query: queryParts.join(' OR ')};
      this.isWorksLoading = true;
      this.solrMediator.query(query).subscribe(res => {
        this.fillDocWorks(res).subscribe(() => {
          this.isWorksLoading = false;
        }, error => {
          this.helper.error(error);
          this.isWorksLoading = false;
        });
      }, error => {
        this.helper.error(error);
        this.isWorksLoading = false;
      });
    }
  }

  getDocOrLink(id: string) {
    let doc: any;
    this.docs.forEach(i => {
      if (!doc && i.id === id) doc = i;
      if (!doc && i.documentLink && i.documentLink.length) {
        i.documentLink.forEach(t => {
          if (!doc && t.idLink === id) doc = t;
        });
      }
    });
    return doc;
  }

  isChecked(id: string, mos: boolean = this.mosRu): boolean {
    let doc = this.getDocOrLink(id);
    let result: boolean = false;
    let propName = 'checked';
    if (doc) {
      if (mos) propName += 'Mos';
      if (doc.idLink) propName += 'Link';
      result = doc[propName];
    }
    return result;
  }

  changeChecked(id: string, mos: boolean = this.mosRu) {
    let doc = this.getDocOrLink(id);
    let propName = 'checked';
    if (doc) {
      if (mos) propName += 'Mos';
      if (doc.idLink) propName += 'Link';
      if (propName) doc[propName] = !doc[propName];
    }
  }

  isGeo(id: string): boolean {
    let doc = this.getDocOrLink(id);
    let result: boolean = false;
    let propName: string = 'geo';
    if (doc) {
      if (doc.idLink) propName += 'Link';
      result = doc[propName];
    }
    return result;
  }

  getWorksByIds(ids: string[]): Observable<string[]> {
    return Observable.create(observer => {
      if (ids.length) {
        this.solrMediator.getByIds([{type: this.solrMediator.types.work, ids: ids}]).subscribe(res => {
          if (res && res[0] && res[0].docs) {
            res[0].docs.forEach((i: any) => {
              find(this.docs, d => d.id === i.documentId).objectId = i.objectIdWork;
            });
            observer.next(res[0].docs.map((i: any) => i.objectIdWork));
          } else observer.next([]);
          observer.complete();
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  fillDocWorks(solrResponse: any): Observable<void> {
    return Observable.create(observer => {
      if (solrResponse && solrResponse.docs && solrResponse.docs.length) {
        solrResponse.docs.forEach((doc: any) => {
          if (!this.docWorks[doc.objectIdWork]) this.docWorks[doc.objectIdWork] = [];
          let workDocIndex: any = findIndex(this.docWorks[doc.objectIdWork], (i: any) => i.documentId === doc.documentId);
          if (!~workDocIndex) {
            this.docWorks[doc.objectIdWork].push(doc);
          } else {
            this.docWorks[doc.objectIdWork][workDocIndex] = doc;
          }
        });
        let ids = solrResponse.docs.map((i: any) => i.documentId);
        this.workTypeService.list(ids).subscribe((works: RayonWork[]) => {
          works.forEach(w => {
            let docWork: any = find(this.docWorks[w.objectId], (i: any) => i.documentId === w.documentId);
            if (docWork) docWork.data = w;
          });
          observer.next();
          observer.complete();
        },error => {
          this.helper.error(error);
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next();
        observer.complete();
      }
    });
  }

  updateWorkInfo(workId: string, objectId: string) {
    this.solrMediator.getByIds([{type: this.solrMediator.types.work, ids: [workId]}]).subscribe(res => {
      if (res && res[0]) {
        this.fillDocWorks(res[0]).subscribe(() => {}, error => {
          this.helper.error(error);
        });
      }
    })
  }

  editWork(workId: string, objectId: string) {
    let modalRef = this.modalService.show(FormWorkTypeEditModalComponent, {
      class: 'modal-lg',
      initialState: {
        documentId: workId,
        objectId: objectId,
        saveCallback: () => {
          this.updateWorkInfo(workId, objectId);
        }
      }
    });
  }

  getWorkExecution(execution: any): string {
    let years: string[] = [];
    if (execution && (execution.start || execution.finish)) {
      if (execution.start && execution.start !== execution.finish) years.push(execution.start);
      if (execution.finish) years.push(execution.finish);
    }
    return years.length ? '(' + years.join('-') + ')' : '';
  }

  /*getGeometry(docId: string, docType: string, subSystemType: string = 'MR_PROGRAM') {
    this.geo.find(docId, docType, subSystemType).subscribe(res => {
      if (res) this.gisCollection[docId] = res;
    });
  }*/
}
