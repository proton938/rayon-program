import {Component} from "@angular/core";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {RayonPublication} from "../../../../models/publication/RayonPublication";
import {RayonPublicationService} from "../../../../services/rayon-publication.service";
import {forkJoin, from} from "rxjs/index";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {RayonWorkTypeService} from "../../../../services/rayon-work-type.service";
import {RayonWork} from "../../../../models/work/RayonWork";
import {find, compact} from 'lodash';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {RayonGeoService} from "../../../../services/rayon-geo.service";
import {Observable} from "rxjs/Rx";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";

@Component({
  selector: 'program-publication-request-main',
  templateUrl: './publication-request-main.component.html'
})
export class PublicationRequestMainComponent {
  document: RayonPublication;
  isLoading: boolean = true;
  dicts: any = {};
  docs: any[] = [];
  docWorks: any = {};
  gisCollection: any = {};
  docsInfo: any = {};
  isDocsLoading: boolean = false;
  isWorksLoading: boolean = false;
  docsPagination: any = {
    totalItems: 0,
    itemsPerPage: 10,
    currentPage: 1
  };

  @BlockUI('docs-ui-block') blockUI: NgBlockUI;

  constructor(private publicationService: RayonPublicationService,
              private helper: RayonHelperService,
              private nsi: NsiResourceService,
              private workTypeService: RayonWorkTypeService,
              private geo: RayonGeoService,
              private solrMediator: SolrMediatorService) {}

  ngOnInit() {
    this.document = this.publicationService.document;
    this.docsPagination.totalItems = this.document.document.length;
    from(this.nsi.getDictsFromCache(['mr_program_requestPublish'])).subscribe(res => {
      this.dicts = res;
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess() {
    this.isLoading = false;
    this.changeDocsPage({page: 1});
  }

  changeDocsPage($event) {
    this.docsPagination.currentPage = $event.page;
    this.docs = this.document.document.filter((item, index) => {
      return index >= ($event.page - 1) * this.docsPagination.itemsPerPage
        && index < ($event.page - 1) * this.docsPagination.itemsPerPage + this.docsPagination.itemsPerPage;
    });
    this.docs.forEach(r => {
      this.getGeometry(r.id, 'MR_PROGRAM_' + r.type);
    });
    this.getDocsInfo();
  }

  getDocsInfo() {
    let objectIds: string[] = this.docs.filter(i => i.type === 'OBJ' && !this.docsInfo[i.id]).map(i => i.id);
    let workIds: string [] = this.docs.filter(i => i.type === 'WORK').map(i => i.id);

    this.getWorksByIds(workIds).subscribe(ids => {
      if (ids.length) objectIds = objectIds.concat(ids);
      objectIds = compact(objectIds);

      if (objectIds.length) {
        this.isDocsLoading = true;
        this.solrMediator.getByIds([{type: this.solrMediator.types.obj, ids: objectIds}]).subscribe(res => {
          if (res && res[0] && res[0].docs) {
            res[0].docs.forEach(d => {
              this.docsInfo[d.documentId] = d;
            });
            this.getWorks();
            this.isDocsLoading = false;
          }
        })
      }
    });
  }

  getWorks() {
    let queryParts: string[] = [];

    this.docs.forEach(i => {
      switch(i.type) {
        case 'WORK':
          queryParts.push('documentId:' + i.id); // Чтобы показать только его, если вдруг их много привязано к объекту
          break;
        case 'OBJ':
          queryParts.push('objectIdWork:' + i.id);
          break;
      }
    });

    if (queryParts.length) {
      let query = {page: 0, pageSize: 999, types: [this.solrMediator.types.work], query: queryParts.join(' OR ')};
      this.isWorksLoading = true;
      this.solrMediator.query(query).subscribe(res => {
        if (res && res.docs && res.docs.length) {
          res.docs.forEach((doc: any) => {
            if (!this.docWorks[doc.objectIdWork]) this.docWorks[doc.objectIdWork] = [];
            this.docWorks[doc.objectIdWork].push(doc);
          });
          let ids = res.docs.map(i => i.documentId);
          this.workTypeService.list(ids).subscribe((works: RayonWork[]) => {
            works.forEach(w => {
              this.getGeometry(w.documentId, this.solrMediator.types.work);
              let docWork: any = find(this.docWorks[w.objectId], i => i.documentId === w.documentId);
              if (docWork) docWork.data = w;
            });
          },error => this.helper.error(error), () => this.isWorksLoading = false);
        }
      }, error => {
        this.helper.error(error);
        this.isWorksLoading = false;
      });
    }
  }

  getWorksByIds(ids: string[]): Observable<string[]> {
    return Observable.create(observer => {
      if (ids.length) {
        this.solrMediator.getByIds([{type: this.solrMediator.types.work, ids: ids}]).subscribe(res => {
          if (res && res[0] && res[0].docs) {
            res[0].docs.forEach((i: any) => {
              find(this.docs, d => d.id === i.documentId).objectId = i.objectIdWork;
            });
            observer.next(res[0].docs.map((i: any) => i.objectIdWork));
          } else observer.next([]);
          observer.complete();
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next([]);
        observer.complete();
      }
    });

  }

  getGeometry(docId: string, docType: string, subSystemType: string = 'MR_PROGRAM') {
    this.geo.find(docId, docType, subSystemType).subscribe(res => {
      if (res) this.gisCollection[docId] = res;
    });
  }
}
