import {Component} from "@angular/core";
import {StateService} from '@uirouter/core';
import {ActivityProcessHistoryManager, IInfoAboutProcessInit, CompareType} from "@reinform-cdp/bpm-components";
import {forkJoin, from} from "rxjs/index";
import {RayonPublication} from "../../../../models/publication/RayonPublication";
import {RayonPublicationService} from "../../../../services/rayon-publication.service";

@Component({
  selector: 'program-publication-request-process',
  templateUrl: './publication-request-process.component.html'
})
export class ProgramPublicationRequestProcessComponent {
  document: RayonPublication;
  init: IInfoAboutProcessInit;
  modeParam: string;
  isLoading: boolean = true;
  config: any;
  constructor(private publicationService: RayonPublicationService) {}

  ngOnInit() {
    this.document = this.publicationService.document;
    this.config = {
      sysName: 'mrpub',
      linkVarValue: this.document.documentId,
      defaultVars: [{
        name: 'EntityIdVar',
        value: this.document.documentId
      }]
    };
    this.isLoading = false;

  }
}
