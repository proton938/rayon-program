import {Component} from "@angular/core";
import {RayonPublication} from "../../../../models/publication/RayonPublication";
import {RayonPublicationService} from "../../../../services/rayon-publication.service";

@Component({
  selector: 'program-publication-request-log',
  templateUrl: './publication-request-log.component.html'
})
export class ProgramPublicationRequestLogComponent {
  document: RayonPublication;

  constructor(private publicationService: RayonPublicationService) {}

  ngOnInit() {
    this.document = this.publicationService.document;
  }
}
