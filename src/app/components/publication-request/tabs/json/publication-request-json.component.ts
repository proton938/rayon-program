import {Component} from "@angular/core";
import {RayonPublicationService} from "../../../../services/rayon-publication.service";
import {RayonPublication} from "../../../../models/publication/RayonPublication";

@Component({
  selector: 'program-publication-request-json',
  templateUrl: './publication-request-json.component.html'
})
export class ProgramPublicationRequestJsonComponent {
  document: RayonPublication;

  constructor(private publicationService: RayonPublicationService) {}

  ngOnInit() {
    this.document = this.publicationService.document;
  }
}
