import {Component} from "@angular/core";
import {RayonPublicationService} from "../../services/rayon-publication.service";
import {StateService, Transition} from "@uirouter/core";
import {RayonPublication} from "../../models/publication/RayonPublication";
import {RayonHelperService} from "../../services/rayon-helper.service";
import {forkJoin} from "rxjs/index";
import {find} from 'lodash';
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-publication-request',
  templateUrl: './publication-request.component.html'
})
export class PublicationRequestComponent {
  document: RayonPublication;
  documentId: string;
  isLoading: boolean = true;
  isError: boolean;
  dictionaries: any = {};
  activeTab: string;
  tabs: any[] = [];

  constructor(private publicationService: RayonPublicationService,
              private transition: Transition,
              private helper: RayonHelperService,
              public state: StateService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    this.activeTab = this.transition.params()['tab'];
    this.updateBreadcrumbs();
    const document$ = this.publicationService.init(this.documentId);

    this.tabs = [
      { code: 'main',
        name: 'Общая информация',
        view: true,
        edit: true,
        show: true
      },
      { code: 'executors',
        name: 'Исполнители',
        view: true,
        edit: true,
        show: false // TODO вернуть после того как будет реализован бизнес-процесс (формы)
      },
      { code: 'process',
        name: 'Процесс',
        view: true,
        edit: true,
        show: true
      },
      { code: 'json',
        name: 'JSON',
        view: true,
        edit: true,
        show: this.session.hasPermission('objectCardTab')
      },
      { code: 'log',
        name: 'История изменений',
        view: true,
        edit: true,
        show: this.session.hasPermission('objectCardTab')
      }
    ];

    if (!this.tabs.filter(i => i.code === this.state.current.params.tab).length) {
      this.state.go(this.state.current, {id: this.documentId, tab: 'main'});
    }

    forkJoin([document$])
      .subscribe((response: any) => {
        this.document = response[0];
        this.initSuccess();
      }, error => {
        this.isError = true;
        this.helper.error(error);
      });
    /*this.document = this.publicationService.document = new RayonPublication();
    this.initSuccess();*/
  }

  initSuccess() {
    this.tabs = this.tabs.filter(i => i.show);
    this.isLoading = false;
  }

  updateBreadcrumbs() {
    this.helper.setBreadcrumbs([
      /*{
        title: 'Объекты и мероприятия',
        url: '/rayon/' + this.state.href('app.object.list', {})
      },*/
      {
        title: 'Карточка',
        url: null
      }
    ]);
  }

  getTab(code: string): any {
    return find(this.tabs, i => i.code === code);
  }

  tabChange(tabCode: string): void {
    let tabParams: any = { id: this.documentId, tab: tabCode };
    // if (tabState !== this.state.current.name) tabParams.mode = 'view';
    this.state.go(this.state.current, tabParams);
  }

  isActiveTab(tabCode: string): boolean {
    return tabCode === this.activeTab;
  }
}
