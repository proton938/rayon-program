import {Component, OnInit, Input} from '@angular/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import * as JSONEditor from 'jsoneditor';
import {copy} from 'angular';
import {ToastrService} from 'ngx-toastr';
import * as jsonpatch from 'fast-json-patch';
import {StateService} from '@uirouter/core';
import {RayonPublication} from "../../../models/publication/RayonPublication";
import {RayonPublicationService} from "../../../services/rayon-publication.service";
import {IRayonPublication} from "../../../models/publication/abstracts/IRayonPublication";

@Component({
  selector: 'program-publication-request-json-editor',
  templateUrl: './json-editor.component.html',
  styleUrls: ['./json-editor.component.scss']
})
export class RayonPublicationRequestJsonEditorComponent implements OnInit {
  @Input() document: RayonPublication;
  documentEdited: IRayonPublication;
  container: any;
  options: any;
  editor: any;
  @BlockUI('card-editor') blockUI: NgBlockUI;

  constructor(private publicationService: RayonPublicationService,
              private toastrService: ToastrService,
              public stateService: StateService) {
  }

  ngOnInit() {
    this.documentEdited = this.document.convert(); // copy(this.document);
    this.container = document.getElementById('jsoneditor');
    this.options = {};
    this.editor = new JSONEditor(this.container, this.options);
    this.editor.set(this.documentEdited);
  }

  save() {
    this.blockUI.start();
    this.documentEdited = this.editor.get();
    let objOld = new RayonPublication();
    objOld.build(this.document.convert());
    let objNew = new RayonPublication();
    objNew.build(this.documentEdited);
    const diff = this.publicationService.diff(objOld, objNew);
    if (diff.length > 0) {
      this.publicationService.patch(this.document.documentId, <any>JSON.stringify(diff)).subscribe(response => {
        this.blockUI.stop();
        this.reloadState();
        this.toastrService.success('Документ успешно сохранен!');
      }, error => {
        this.toastrService.error('Ошибка при сохранении документа!');
        this.blockUI.stop();
      });
    } else {
      this.toastrService.warning('В документе изменений нет!');
      this.blockUI.stop();
    }
  }

  reloadState() {
    const stateName = 'app.publication.request';
    this.stateService.go(stateName, {
      id: this.document.documentId
    }, {reload: stateName});
  }
}
