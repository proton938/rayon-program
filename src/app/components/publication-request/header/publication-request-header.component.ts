import {Component} from "@angular/core";
import {RayonPublication} from "../../../models/publication/RayonPublication";
import {Transition} from "@uirouter/core";
import {RayonPublicationService} from "../../../services/rayon-publication.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {from} from "rxjs/index";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'program-publication-request-header',
  templateUrl: './publication-request-header.component.html'
})
export class PublicationRequestHeaderComponent {
  document: RayonPublication;
  dicts: any = {};
  documentId: string;

  constructor(private transition: Transition,
              private nsi: NsiResourceService,
              private helper: RayonHelperService,
              private publicationService: RayonPublicationService) {}

  ngOnInit() {
    this.documentId = this.transition.params()['id'];
    this.document = this.publicationService.document;
    from(this.nsi.getDictsFromCache(['mr_program_requestPublish'])).subscribe(res => {
      this.dicts = res;
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess() {

  }
}
