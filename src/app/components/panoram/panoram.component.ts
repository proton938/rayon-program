import {Component} from "@angular/core";
import {Transition} from "@uirouter/core";
import {ScriptService} from "../../services/script.service";

@Component({
  selector: 'panoram',
  templateUrl: './panoram.component.html',
  styleUrls: ['./panoram.component.scss'],
  providers: [ScriptService]
})
export class PanoramComponent {
  params: any = {};

  constructor(private script: ScriptService, private transition: Transition) {}

  ngOnInit() {
    this.params = this.transition.params();
    this.script.load('panoapi').then(data => {
      let panoParams: any = {};
      if (this.params.initpoint) panoParams.initpoint = this.params.initpoint;
      if (this.params.heading) panoParams.heading = this.params.heading;
      setTimeout(() => { this.panoReady(panoParams) }, 1000);
    });
  }

  panoReady(params) {
    let imap = (<any>window).IMAP;
    if (imap && imap.Panorama) {
      let ipano = new imap.Panorama('panoram', 'guest', 'guest', params);
    } else setTimeout(() => this.panoReady(params), 1000);
  }
}
