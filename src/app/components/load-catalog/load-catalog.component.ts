import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {EhdService} from '../../services/ehd.service';
import {Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'load-catalog',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './load-catalog.component.html',
  styleUrls: ['./load-catalog.component.scss']
})
export class LoadCatalogComponent implements OnInit, OnDestroy {
  @BlockUI('load-catalog') blockUI: NgBlockUI;

  catalogCode: string;
  odopmIds: string;

  subscription: Subscription = new Subscription();

  constructor(private location: Location,
              private ehdService: EhdService,
              private toastrService: ToastrService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  loadCatalog() {
    this.blockUI.start();
    let ids = this.odopmIds.split(",").map(id => id.trim());
    let sub = this.ehdService.loadCatalogEntries(this.catalogCode, ids).subscribe(() => {
      this.toastrService.success("Данные из каталогов ЕХД обновлены");
      this.blockUI.stop();
    }, (error) => {
      let message = error.message ? error.message : error;
      this.toastrService.error(`Данные из каталогов ЕХД не обновлены: ${message}`);
      this.blockUI.stop();
    });
    this.subscription.add(sub);
  }

  cancel() {
    this.location.back();
  }

}
