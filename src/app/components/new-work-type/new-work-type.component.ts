import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {LoadingStatus} from '@reinform-cdp/widgets';
import * as jsonpatch from 'fast-json-patch';
import {copy} from 'angular';
import {RayonWork} from '../../models/work/RayonWork';
import {fromJson, StateService, toJson, Transition} from '@uirouter/core';
import {RayonHelperService} from '../../services/rayon-helper.service';
import {ToastrService} from 'ngx-toastr';
import {RayonWorkTypeService} from '../../services/rayon-work-type.service';
import {RayonWorkWrapper} from '../../models/work/RayonWorkWrapper';
import {SessionStorage} from '@reinform-cdp/security';
import {FormWorkTypeComponent} from '../forms/work-type/form-work-type.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'program-new-work-type',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './new-work-type.component.html'
})
export class NewWorkTypeComponent implements OnInit {
  objectId: string;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  isLoading = true;
  document: RayonWork;
  documentId: string;
  old: RayonWork;
  validate: boolean;
  itemCode: string;
  @BlockUI('new-work-type') blockUI: NgBlockUI;
  @ViewChild(FormWorkTypeComponent)   workForm:   FormWorkTypeComponent;

  constructor(private location: Location,
              private helper: RayonHelperService,
              private transition: Transition,
              private toastr: ToastrService,
              private workTypeService: RayonWorkTypeService,
              private session: SessionStorage) {}

  ngOnInit() {
    let pageTitle = 'Работа на объекте';
    this.documentId = this.transition.params()['id'];
    this.objectId = this.transition.params()['objectId'];
    this.itemCode = this.transition.params()['itemCode'];
    if (!this.documentId) {
      pageTitle = 'Новая работа на объекте';
      this.document = new RayonWork();
      this.document.documentDate = (() => { const d = new Date(); d.setHours(0, 0, 0); return d; })();
      this.isLoading = false;
      this.loadingStatus = LoadingStatus.SUCCESS;
    } else {
      pageTitle = 'Редактирование работы на объекте';
      this.workTypeService.init(this.documentId).subscribe(response => {
        this.document = response;
        this.old = copy(response);
        this.isLoading = false;
        this.loadingStatus = LoadingStatus.SUCCESS;
      });
    }

    this.helper.setBreadcrumbs([{ title: pageTitle, url: null }]);
  }

  isValid(): boolean {
    let r = true;
    this.validate = true;
    if (this.workForm.isValid) { r = this.workForm.isValid(); }
    return r;
  }

  prepareData() {
    if (this.workForm.prepareData) { this.workForm.prepareData(); }
  }

  save(action?: string) {
    if (this.isValid()) {
      this.blockUI.start();
      this.prepareData();
      const model = new RayonWork();
      model.build(fromJson(toJson(this.document)));
      const doc = model.min();
      if (!this.documentId) {
        this.workTypeService.create(doc)
          .subscribe((response: RayonWorkWrapper) => {
          if (response && response.work && response.work.documentId) {
            setTimeout(() => {
              this.helper.success('Вид работ успешно создан!!');
              this.saveResult(response.work, action);
              this.blockUI.stop();
            }, 1000);
          }
        }, error => this.helper.error(error));
      } else {
        const diff: any[] = jsonpatch.compare({work: this.old.min()}, {work: doc});
        if (diff.length) {
          this.workTypeService.patch(this.documentId, diff).subscribe((response: RayonWorkWrapper) => {
            this.saveResult(response.work, action);
            this.blockUI.stop();
          });
        } else {
          // Нечего сохранять
          this.saveResult(this.document, action);
          this.blockUI.stop();
        }
      }
    } else {
      this.toastr.error('Не заполнены обязательные поля!');
    }
  }

  saveResult(doc: any, action?: string): void {
    switch (action) {
      case 'reload':
        this.old = new RayonWork();
        this.old.build(doc);
        this.document = new RayonWork();
        this.document.build(doc);
        this.documentId = this.document.documentId; // чтобы не создавалось 2 одинаковых документа
        break;

      default:
        this.cancel();
        break;
    }
  }

  cancel() {
    this.location.back();
  }
}
