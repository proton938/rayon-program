import {Component} from '@angular/core';
import {SessionStorage} from '@reinform-cdp/security';
import {find, isEmpty} from 'lodash';
import {RayonProgramResourceService} from '../../../services/rayon-program-resource.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {RayonHelperService} from '../../../services/rayon-helper.service';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Component({
  selector: 'form-heading-ukk',
  templateUrl: './heading-ukk.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class FormHeadingUkkComponent {
  loading: boolean = true;
  isShow: any = {
    form: false
  };
  selected: any = {
    ids: ''
  };

  @BlockUI('form-block-ui') blockUI: NgBlockUI;

  constructor(private session: SessionStorage,
              private helper: RayonHelperService,
              private location: Location,
              private service: RayonProgramResourceService) {}

  ngOnInit() {
    this.isShow.form = this.session.hasPermission('mr_objectUKK');
    this.updateBreadcrumbs();
    this.loading = false;
  }

  updateBreadcrumbs() {
    this.helper.setBreadcrumbs([
      {
        title: 'Проставить рубрики УКК',
        url: null
      }
    ]);
  }

  ok() {
    this.blockUI.start();
    let data: any = this.requestData;
    if (!isEmpty(data)) {
      this.service.updateObjRubricators(data).subscribe(res => {
        this.helper.success('Рубрики УКК на объектах проставлены');
        this.blockUI.stop();
        this.cancel();
      }, error => {
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.helper.error('Нет данных для проставления рубрик');
      this.blockUI.stop();
    }
  }

  cancel() {
    this.location.back();
  }

  get requestData(): any {
    let idsRow: string = this.selected.ids.trim();
    return idsRow ? idsRow.split(/[^a-z0-9\-]+/) : [];
  }
}
