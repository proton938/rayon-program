import {Component, Input} from "@angular/core";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {LoadingStatus} from "@reinform-cdp/widgets";
import {fromJson, StateService, toJson, Transition} from "@uirouter/core";
import {copy} from 'angular';
import {ToastrService} from "ngx-toastr";
import {forkJoin, from} from "rxjs";
import {find, some, toString, concat} from 'lodash';
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {RayonPublication} from "../../../../models/publication/RayonPublication";
import {Observable} from "rxjs/Rx";
import {SessionStorage} from "@reinform-cdp/security";
import {NSIDocumentType, TepDocumentType, UserDocumentType} from "@reinform-cdp/core";
import {RayonPublicationDocument} from "../../../../models/publication/RayonPublicationDocument";
import {RayonPublicationService} from "../../../../services/rayon-publication.service";
import {ActivityResourceService} from "@reinform-cdp/bpm-components";
import {RayonPublicationDocumentLink} from "../../../../models/publication/RayonPublicationDocumentLink";
import {RayonGeoService} from "../../../../services/rayon-geo.service";
import {map} from "rxjs/internal/operators";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";


@Component({
  selector: 'form-data-publication',
  templateUrl: './form-data-publication.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class FormDataPublicationComponent {
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  isLoading: boolean = true;
  dictionaries: any = {};
  model: any = {};
  rows: any[] = [];
  searchFinished: boolean = false;
  districtsAvailables: any[] = [];
  numFound: number = 0;
  maxPageSize: number = 5000;
  @BlockUI('process-create-form') blockUI: NgBlockUI;

  constructor(public helper: RayonHelperService,
              private location: Location,
              private nsi: NsiResourceService,
              private programService: RayonProgramResourceService,
              private publicationService: RayonPublicationService,
              private activityResourceService: ActivityResourceService,
              private geo: RayonGeoService,
              private session: SessionStorage,
              private solrMediator: SolrMediatorService) {
    helper.setBreadcrumbs([{ title: 'Запуск процесса публикации данных', url: null }]);
  }

  ngOnInit() {
    const dictionaries$ = this.nsi.getDictsFromCache([
      'Prefect', 'District',
      'mr_program_ObjectIndustries',
      'mr_program_ObjectCategories',
      'mr_program_ResponsibleOrganizations',
      'mr_program_dataSource'
    ]);
    forkJoin([dictionaries$]).subscribe((response: any) => {
      this.dictionaries = response[0];
      this.updateObjectCategories();
      this.initSuccess();
    });
  }

  initSuccess(): void {
    this.loadingStatus = LoadingStatus.SUCCESS;
    this.isLoading = false;
    this.updateDistrictList();
  }

  updateObjectCategories() {
    let list = this.dictionaries['mr_program_ObjectCategories'];
    this.dictionaries.objectCategories = this.model.industry ? list.filter(i => {
      return i.industry ? some(this.model.industry, d => some(i.industry, r => r.code === d.code)) : false;
    }) : list;
  }

  onChangePrefect() {
    this.updateDistrictList();
  }

  updateDistrictList() {
    this.districtsAvailables = [];
    if (this.model.prefect && this.model.prefect.length) {
      this.districtsAvailables = this.dictionaries.District
        .filter(d => this.model.prefect.filter(p => p.code === d.perfectId[0].code).length);
      if (this.model.district && this.model.district.length) {
        this.model.district = this.model.district
          .filter(i => this.districtsAvailables.filter(f => f.code === i.code).length);
      }
    } else {
      this.districtsAvailables = this.dictionaries.District;
    }
  }

  start() {
    let docRows: any = {};
    this.rows.forEach(i => docRows[i.documentId] = 'OBJ');

    this.blockUI.start();
    this.programService.changeNotifyDocs(docRows).subscribe(() => {
      this.blockUI.stop();
      this.createPublication().subscribe(res => {
        this.helper.success('Заявка на публикацию создана');
        this.startProcess(res.documentId).subscribe(() => {
          this.cancel();
        }, error => {
          this.helper.error(error);
          this.blockUI.stop();
        });
      }, error => {
        this.helper.error(error);
        this.blockUI.stop();
      });
    }, error => {
      this.helper.error(error);
      this.blockUI.stop();
    });
  }

  search(all: boolean = false, page: number = 0) {
    this.blockUI.start();
    let addMultiValue = (fieldName: string, solrFieldName: string, propName: string = 'name') => {
      if (this.model[fieldName] && this.model[fieldName].length) {
        query += (query ? ' AND (' : '(') + solrFieldName + ':' + this.model[fieldName].map(i => {
          let str = i[propName].replace(/["']/g, '');
          if (str.match(/\s+/)) str = '(*' + str.replace(/\s+/g, '* AND *') + '*)';
          return str;
        }).join(' OR ' + solrFieldName + ':') + ')';
      }
    };
    let searchParams: any = {page: page, pageSize: all ? this.maxPageSize : 1, types: [this.solrMediator.types.obj]};
    let query: string = '';

    addMultiValue('prefect', 'prefectsNameObject');
    addMultiValue('district', 'districtsNameObject');
    addMultiValue('industry', 'industryNameObject');
    addMultiValue('category', 'kindNameObject');
    addMultiValue('organization', 'organizationNameObject');
    addMultiValue('source', 'dataSourceObject', 'nameShort');
    query += (query ? ' AND ' : '') + 'commercialObject:' + toString(!!this.model.commercialObject);
    query += (query ? ' AND ' : '') + 'dominantObject:' + toString(!!this.model.dominantObject);

    if (query) searchParams.query = query;

    this.solrMediator.query(searchParams).subscribe(res => {
      if (page === 0) this.rows = [];
      let docs = res && res.docs ? res.docs : [];
      if (docs.length) this.rows = concat(this.rows, docs);
      if (!all) {
        this.numFound = res.numFound || 0;
        if (this.numFound) {
          this.search(true);
        } else {
          this.blockUI.stop();
          this.searchFinished = true;
        }
      } else {
        let overage = this.numFound % this.maxPageSize;
        let pages = (this.numFound - overage) / this.maxPageSize + +!!overage;
        if (page == pages - 1) {
          this.blockUI.stop();
          this.searchFinished = true;
        } else {
          this.search(true, page + 1);
        }
      }
    }, error => {
      this.rows = [];
      this.numFound = 0;
      this.helper.error(error);
      this.blockUI.stop();
      this.searchFinished = true;
    });
  }

  createPublication(): Observable<RayonPublication> {
    let addMultiParam = (modelCode: string, modelName: string, valueCode: string = 'name') => {
      if (this.model[modelCode] && this.model[modelCode].length) {
        this.model[modelCode].forEach(m => {
          let param = new TepDocumentType();
          param.build({name: modelName, value: m[valueCode]});
          pub.requestContent.parameter.push(param);
        });
      }
    };
    let addBooleanParam = (modelCode: string, modelName: string) => {
      if (this.model[modelCode] === true) {
        let param = new TepDocumentType();
        param.build({name: modelName, value: 'Да'});
        pub.requestContent.parameter.push(param);
      }
    };

    let pub = new RayonPublication();
    pub.selection = true;
    pub.requestDate = new Date();
    pub.status = new NSIDocumentType();
    pub.status.build({code: 'null', name: 'null'});
    pub.requestContent.requestAuthor = new UserDocumentType();
    pub.requestContent.requestAuthor.login = this.session.login();
    pub.requestContent.requestAuthor.fio = this.session.fullName();
    pub.requestContent.parameter = [];

    addMultiParam('prefect', 'Округ');
    addMultiParam('district', 'Район');
    addMultiParam('industry', 'Отрасль');
    addMultiParam('category', 'Вид объекта / мероприятия');
    addBooleanParam('commercialObject', 'Коммерческий объект');
    addBooleanParam('dominantObject', 'Уникальный объект');
    addMultiParam('organization', 'Ответственный орган исполнительной власти');
    addMultiParam('source', 'Источник данных об объекте программы');

    pub.document = [];
    this.rows.forEach(i => {
      let doc = new RayonPublicationDocument();
      doc.id = i.documentId;
      doc.type = 'OBJ',
      doc.name = i.nameObject || '';
      doc.address = i.addressObject || '';
      pub.document.push(doc);
    });

    return Observable.create(observer => {
      this.helper.getWorks([], pub.document.map(i => i.id)).subscribe(works => {
        let geoRequestIds: string[] = [];
        pub.document.forEach(doc => {
          geoRequestIds.push(doc.id);
          works.filter(i => i.objectIdWork === doc.id).forEach(i => {
            let link = new RayonPublicationDocumentLink();
            link.build({
              idLink: i.documentId,
              typeLink: 'WORK',
              geoLink: null,
              checkedLink: false,
              checkedMosLink: false
            });
            geoRequestIds.push(i.documentId);
            doc.documentLink.push(link);
          });
        });

        if (geoRequestIds.length) {
          this.geo.exists(geoRequestIds).subscribe((geoRes: any) => {
            let isGeoExist = (id): boolean => {
              let geoInfo: any = find(geoRes.geoExists, i => i.docId === id);
              return !!geoInfo && !!geoInfo.exist;
            };
            if (geoRes && geoRes.geoExists && geoRes.geoExists.length) {
              pub.document.forEach(doc => {
                doc.geo = isGeoExist(doc.id);
                doc.documentLink.forEach(link => {
                  link.geoLink = isGeoExist(link.idLink);
                });
              });
              this.publicationService.create(pub.min()).subscribe(newPub => {
                observer.next(newPub);
                observer.complete();
              }, error => {
                observer.error(error);
                observer.complete();
              });
            } else {
              observer.next(this.publicationService.create(pub.min()));
              observer.complete();
            }
          }, error => {
            observer.error(error);
            observer.complete();
          });
        }
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  getGeometry(docId: string, docType: string, source: any, sourceProp: string): Observable<any> {
    let subSystemType: string = 'MR_PROGRAM';
    return this.geo.find(docId, docType, subSystemType).pipe(map(r => {
      return {
        source: source,
        sourceProp: sourceProp,
        value: !!r
      };
    }));
  }

  startProcess(id: string): Observable<void> {
    return Observable.create(observer => {
      this.activityResourceService.getProcessDefinitions({
        key: 'mrpub_PubDataOnMosRu',
        latest: true
      }).then(response => {
        let vars = [];
        vars.push({name: 'EntityIdVar', value: id});
        return this.activityResourceService.initProcess({
          processDefinitionId: response.data[0].id,
          variables: vars
        }).then(res => {
          this.helper.success('Процесс успешно запущен!');
          observer.next();
          observer.complete();
        }).catch(error => {
          this.helper.error('Ошибка при запуске процесса!');
          observer.error(error);
          observer.complete();
        });
      });
    });
  }

  cancel() {
    this.location.back();
  }
}
