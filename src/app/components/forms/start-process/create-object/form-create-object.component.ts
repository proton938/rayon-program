import {Component} from "@angular/core";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {RayonProgramResourceService} from "../../../../services/rayon-program-resource.service";
import {SessionStorage} from "@reinform-cdp/security";
import {RayonProgram} from "../../../../models/program/RayonProgram";
import {RayonProgramWrapper} from "../../../../models/program/RayonProgramWrapper";
import {ActivityResourceService} from "@reinform-cdp/bpm-components";
import {Observable} from "rxjs/Rx";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {from} from "rxjs/index";
import {first, toString} from 'lodash';
import {StateService} from "@uirouter/core";

@Component({
  selector: 'form-create-object',
  templateUrl: './form-create-object.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class FormCreateObjectComponent {
  isLoading: boolean = true;

  @BlockUI('process-create-form') blockUI: NgBlockUI;

  constructor(private programService: RayonProgramResourceService,
              private session: SessionStorage,
              private state: StateService,
              private helper: RayonHelperService,
              private location: Location,
              private activityResourceService: ActivityResourceService) {
    helper.setBreadcrumbs([{ title: 'Запуск процесса по созданию объекта', url: null }]);
  }

  makeVars(id: string): any[] {
    return [
      {name: 'EntityIdVar', value: id},
      {name: 'ResponsibleVar', value: this.session.login()}
    ];
  }

  start() {
    this.blockUI.start();
    let document = new RayonProgram();
    document.documentDate = (() => { let d = new Date(); d.setHours(0, 0, 0); return d;})();
    this.programService.create(document.min(), true).subscribe((res: RayonProgramWrapper) => {
      if (res && res.obj && res.obj.documentId) {
        this.startProcess(res.obj.documentId).subscribe(processId => {
          this.helper.success('Процесс успешно запущен!');
          from(this.activityResourceService.getTasks({
            processInstanceId: processId
          })).subscribe(tasksRes => {
            if (tasksRes && tasksRes.data) {
              let targetTaskInfo = first(tasksRes.data.sort((a: any, b: any) => b.createTime - a.createTime));
              this.blockUI.stop();
              this.helper.goToTask('app.execution.mrobjectFillFormForCreatingObject', targetTaskInfo.id);
            }
          }, error => {
            this.blockUI.stop();
            this.helper.error(error);
          });
          // this.cancel();
        }, error => {
          this.helper.error(error);
          this.blockUI.stop();
        });
      }
    });
  }

  startProcess(id: string): Observable<string> {
    return Observable.create(observer => {
      this.activityResourceService.getProcessDefinitions({
        key: 'mrobject_CreateObject',
        latest: true
      }).then(response => {
        let vars = [];
        vars.push({name: 'EntityIdVar', value: id});
        return this.activityResourceService.initProcess({
          processDefinitionId: response.data[0].id,
          variables: this.makeVars(id)
        }).then((processId: number) => {
          observer.next(toString(processId));
          observer.complete();
        }).catch(error => {
          this.helper.error('Ошибка при запуске процесса!');
          observer.error(error);
          observer.complete();
        });
      });
    });
  }

  cancel() {
    this.location.back();
  }
}
