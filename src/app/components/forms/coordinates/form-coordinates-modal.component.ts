import {Component, Input} from "@angular/core";
import {LoadingStatus} from "@reinform-cdp/widgets";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {Observable, Observer, forkJoin, from} from "rxjs";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {Geo} from "../../../models/geodata/Geo";
import {RayonGeoService} from "../../../services/rayon-geo.service";
import {some, toString, toNumber, find, first, assignIn} from 'lodash';
import {fromJson, toJson} from "@uirouter/core";
import {copy} from 'angular';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'form-coordinates-modal',
  templateUrl: './form-coordinates-modal.component.html'
})
export class FormCoordinatesModalComponent {
  @Input() documentId: string;
  @Input() geoId: string;
  @Input() docTypeCode: string = 'MR_PROGRAM_OBJ';
  @Input() saveCallback: () => void;

  @BlockUI('modal-block-ui') blockUI: NgBlockUI;

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  dicts: any = {};
  isGeoExists: boolean = false;
  geo: Geo;
  old: Geo = new Geo();
  selected: any = {
    coordType: null,
    vectorData: null,
    vectorFiles: []
  };
  private subSystemCode: string = '';


  constructor(private nsi: NsiResourceService,
              private bsModalRef: BsModalRef,
              public helper: RayonHelperService,
              private geoService: RayonGeoService) {}

  ngOnInit() {
    this.selected.coordType = 'mr_xy';
    const dicts$ = from(this.nsi.getDictsFromCache([
      'CoordinatesType', 'vectorFileExtensions', 'DocumentTypes', 'SubSystems']));
    const geo$ = this.getGeoModel(this.geoId);
    forkJoin([dicts$, geo$]).subscribe(res => {
      if (res[0]) {
        this.dicts = res[0];
        this.dicts.coordTypes = this.dicts['CoordinatesType'].filter(i => i.subsystem && some(i.subsystem, s => s.code === 'MR_PROGRAM'));
      }
      if (res[1]) {
        this.geo = res[1];
        this.old = copy(this.geo);
      }
      this.initSuccess();
    }, error => {
      this.helper.error(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  initSuccess() {
    let docType = find(this.dicts['DocumentTypes'], i => i.code === this.docTypeCode);
    let subSystem: any = first(docType.subsystem);
    if (subSystem) this.subSystemCode = subSystem.code;

    this.loadingStatus = LoadingStatus.SUCCESS;
  }

  isLoaded(): boolean {
    return this.loadingStatus === LoadingStatus.SUCCESS;
  }

  degPipeX(value: string = '', config): any {
    let min = -90, max = 90;
    if (this.geo.srid === 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }

  degPipeY(value: string = '', config): any {
    let min = -180, max = 180;
    if (this.geo.srid === 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }

  getGeoModel(id?: string): Observable<Geo> {
    return Observable.create(observer => {
      if (id) {
        this.geoService.get(id).subscribe(res => {
          let item = new Geo();
          if (res) {
            item.build(res);
            this.isGeoExists = true;
          }
          observer.next(item);
        }, error => observer.error(error), () => observer.complete());
      } else {
        let item = new Geo();
        item.srid = 4326;
        item.docId = this.documentId;
        observer.next(item);
        observer.complete();
      }
    });
  }

  isValid(): boolean {
    let r: boolean = true;
    switch(this.selected.coordType) {
      case 'mr_xy':
        this.geo.wkt = this.geo.geoJson = this.geo.esriJson = '';
        break;

      case 'mr_wkt':
        this.geo.x = this.geo.y = this.geo.geoJson = this.geo.esriJson = '';
        break;

      case 'mr_geojson':
        this.geo.x = this.geo.y = this.geo.wkt = this.geo.esriJson = '';
        break;

      case 'mr_esri_json':
        this.geo.x = this.geo.y = this.geo.wkt = this.geo.geoJson = '';
        break;

      case 'mr_vector_data':
        this.geo.x = this.geo.y = this.geo.wkt = this.geo.geoJson = this.geo.esriJson = '';
        if (!this.selected.vectorFiles.length) {
          r = false;
          this.helper.error('Необходимо загрузить архив с векторными данными!');
        }
        break;
    }
    return r;
  }

  save() {
    if (this.isValid()) {
      this.blockUI.start();
      if (!this.geo.docType) this.geo.docType = this.docTypeCode;
      if (!this.geo.subsystemType) this.geo.subsystemType = this.subSystemCode;

      if (this.selected.coordType === 'mr_vector_data') {
        let data: any = assignIn({}, this.geo, {extension: this.selected.vectorData});
        this.geoService.createVector(this.selected.vectorFiles[0], data).subscribe(res => {
          if (this.saveCallback) this.saveCallback();
          this.blockUI.stop();
          this.cancel();
        }, error => {
          this.blockUI.stop();
          this.helper.error(error);
        });
      } else {
        let model = new Geo();
        model.build(fromJson(toJson(this.geo)));

        this.geoService.create(model.min()).subscribe(res => {
          if (this.saveCallback) this.saveCallback();
          this.blockUI.stop();
          this.cancel();
        }, error => {
          this.blockUI.stop();
          this.helper.error(error);
        });
      }
    }
  }

  cancel() {
    this.bsModalRef.hide();
  }
}
