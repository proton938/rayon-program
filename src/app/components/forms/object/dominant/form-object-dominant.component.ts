import {Component, EventEmitter, Input, Output} from "@angular/core";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {RayonProgramInstruction} from "../../../../models/program/RayonProgramInstruction";
import {first, find, compact, some, isString} from 'lodash';
import {forkJoin, from, of, Observable, Subject, Subscription} from "rxjs";
import {debounceTime, delay, flatMap, map} from "rxjs/internal/operators";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {Transition} from "@uirouter/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {NSIDocumentType} from "@reinform-cdp/core";
import {RayonProgramSpecific} from "../../../../models/program/RayonProgramSpecific";
import {SessionStorage} from "@reinform-cdp/security";
import {FormCoordinatesModalComponent} from "../../coordinates/form-coordinates-modal.component";
import { BsModalService } from 'ngx-bootstrap/modal';
import {ObjectMatrixService} from "../../../../services/object-matrix.service";
import {SolrMediatorService} from "../../../../services/solr-mediator.service";

@Component({
  selector: 'program-form-object-dominant',
  templateUrl: './form-object-dominant.component.html'
})
export class ProgramFormObjectDominantComponent {
  @Input() model: any;
  @Input() validate: boolean;
  @Input() files: any;

  @Output() onLoad = new EventEmitter<void>();

  documentId: string;
  dictionaries: any = {};
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  districtsAvailables: any[] = [];
  selected: any = {
    address: ''
  };
  counter: any = {
    changeObjectsParams: 0
  };
  isAddressLoading: boolean = false;
  private currentAddressFilter: string;
  public addressKeyUp = new Subject<string>();
  private addressSubscription: Subscription;

  attributesAvailable: any[] = [];
  attrList: any[] = [];

  isMosRu: boolean;
  isObjectCardPublic: boolean;

  constructor(private nsi: NsiResourceService,
              private transition: Transition,
              private helper: RayonHelperService,
              private modalService: BsModalService,
              private solrMediator: SolrMediatorService,
              private matrix: ObjectMatrixService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    this.isObjectCardPublic = this.session.hasPermission('mr_objectCardPublic');

    const dictionaries$ = this.nsi.getDictsFromCache([
      'District',
      'DocumentTypes',
      'mr_program_BasesInclusion',
      'mr_program_ConstructionPeriods',
      'mr_program_ConstructionPrograms',
      'mr_program_ItemTypes',
      'mr_program_ObjectCategories',
      'mr_program_ObjectForms',
      'mr_program_ObjectKinds',
      'mr_program_ObjectTypes',
      'mr_program_OwnershipForms',
      'mr_program_ResponsibleOrganizations',
      'mr_program_StatePrograms',
      'mr_program_Teps',
      'mr_program_WorkStatuses',
      'mr_program_WorkTypes',
      'mr_program_ObjectIndustries',
      'mr_program_SpecificObject',
      'Prefect',
      'SubSystems',
      'mr_program_RubricatorUKK']);
    const objectMatrix$ = this.matrix.init();
    forkJoin([dictionaries$, objectMatrix$]).subscribe((response: any) => {
      this.dictionaries = response[0];
      this.dictionaries.rubricator = this.dicToLine(this.dictionaries['mr_program_RubricatorUKK']);
      this.dictionaries.prefects = this.matrix.filter(this.dictionaries['Prefect'], 'prefect');
      this.dictionaries.industries = this.matrix.filter(this.dictionaries['mr_program_ObjectIndustries'], 'industry');
      if (this.model.address.addressId) this.getAvailableAddressList();
      if (!this.model.dominant.dominantIs) this.model.dominant.dominantIs = true;
      if (!this.model.itemType) {
        this.model.itemType = new NSIDocumentType();
        this.model.itemType.build(find(this.dictionaries['mr_program_ItemTypes'], i => i.code === 'object'));
      }
      this.initSuccess();
    }, error => this.helper.error(error));

    this.addressSubscription = this.addressKeyUp
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(100),
        flatMap(search => of(search).pipe(delay(100)))
      ).subscribe(value => {
        if (value !== this.currentAddressFilter) this.getAvailableAddressList(value);
        this.currentAddressFilter = value;
      });
  }

  getFieldByTag(tagName: string): any {
    let result: any = null;
    switch(tagName) {
      default:
        result = this.model[tagName];
        break;
    }
    return result;
  }

  updateAttributesAvailable() {
    this.attributesAvailable = this.dictionaries['mr_program_SpecificObject'].filter(i => {
      let result: boolean = false;
      if (this.model.object && this.model.object.industry
        && some(i.useInIndustry, s => this.model.object.industry.code === s)) {
        result = true;
      }
      if (!result && i.useInTag) {
        result = some(i.useInTag, s => this.getFieldByTag(s) === true)
      }
      return result;
    });
  }

  updateAttrList() {
    if (this.model.specific) {
      this.attrList = this.model.specific.map(i => {
        let dic = find(this.dictionaries['mr_program_SpecificObject'], d => d.code === i.specificCode);
        return {
          value: i.specificValue,
          dictionary: dic || null,
          possibleValues: dic && dic.possibleValues ? dic.possibleValues.map(d => { return {code: d, name: d} }) : []
        };
      });
    }
  }

  ngOnDestroy(): void {
    this.addressSubscription.unsubscribe();
  }

  initSuccess(): void {
    if (!this.documentId) {
      let stateParams: any = this.transition.params();
      if (stateParams.prefect) {
        this.model.address.prefect = this.dictionaries.prefects.filter(i => i.code === stateParams.prefect);
      }
      if (stateParams.district) {
        this.model.address.district = this.dictionaries.District.filter(i => i.code === stateParams.district);
      }
      if (stateParams.industry) {
        this.model.object.industry = first(this.dictionaries.industries.filter(i => i.code === stateParams.industry));
        // this.updateCategoriList();
      }
    }

    this.updateAttributesAvailable();
    this.updateAttrList();
    this.updateDistrictList();

    this.loadingStatus = LoadingStatus.SUCCESS;
    this.onLoad.emit();
  }

  onChancePrefect() {
    this.updateDistrictList();
  }

  updateDistrictList() {
    this.districtsAvailables = [];
    if (this.model.address.prefect && this.model.address.prefect.length) {
      this.districtsAvailables = this.dictionaries.District
        .filter(d => this.model.address.prefect.filter(p => p.code === d.perfectId[0].code).length);
      if (this.model.address.district && this.model.address.district.length) {
        this.model.address.district = this.model.address.district
          .filter(i => this.districtsAvailables.filter(f => f.code === i.code).length);
      }
    } else {
      this.districtsAvailables = this.dictionaries.District;
    }
  }

  onChangeAddress() {
    if (this.model.address.addressId) {
      this.model.address.address = find(this.dictionaries.addressList, i => i.code === this.model.address.addressId).name;
    } else this.model.address.address = '';
  }

  getAvailableAddressList(searchString?: string) {
      let filter: string = '';
      let result: any[] = [];
      if ((searchString && searchString.length >= 3) || (!searchString && this.model.address.addressId)) {
        this.isAddressLoading = true;
        if (this.model.address.prefect && this.model.address.prefect.length) {
          filter += (filter ? ' AND ' : '') + '(prefectCodeAddress:' + this.model.address.prefect.map(i => i.code).join(' OR prefectCodeAddress:') + ')'
        }
        if (this.model.address.district && this.model.address.district.length) {
          filter += (filter ? ' AND ' : '') + '(districtCodeAddress:' + this.model.address.district.map(i => i.code).join(' OR districtCodeAddress:') + ')'
        }
        if (!searchString && this.model.address.addressId) {
          filter += (filter ? ' AND ' : '') + '(documentId:' + this.model.address.addressId + ')';
        } else {
          filter += (filter ? ' AND ' : '') + '(fullAddress:*' + searchString.replace(/\s+/g, '* AND fullAddress:*') + '*)';
        }
        filter += (filter ? ' AND NOT ' : '') + '(isDeletedAddress:true)';
        this.solrMediator.query({page: 0, pageSize: 100, types: [this.solrMediator.types.address], query: filter}).subscribe(response => {
          if (response && response.docs) {
            this.dictionaries['addressList'] = result = response.docs.map((d: any) => {
              return {
                code: d.documentId,
                name: d.fullAddress
              };
            });
            this.isAddressLoading = false;
          } else {
            this.dictionaries['addressList'] = result = [];
            this.isAddressLoading = false;
          }
        });
      }
  }

  addressSearchFn(term: string, item: any) { // Чтобы не фильтровал уже отфильтрованные данные с сервера
    return !!item;
  }

  isLoaded(): boolean {
    return this.loadingStatus === LoadingStatus.SUCCESS;
  }

  getTypeof(str: any) { // remove this method
    return typeof str;
  }

  addAttribute() {
    let attr = new RayonProgramSpecific();
    this.model.specific.push(attr);
    this.updateAttrList();
  }

  onChangeAttributeName(attr, i) {
    if (attr.dictionary) {
      attr.possibleValues = attr.dictionary.possibleValues && attr.dictionary.possibleValues.length
        ? attr.dictionary.possibleValues.map(d => { return {code: d, name: d} }) : [];
      this.model.specific[i].specificCode = attr.dictionary.code;
      this.model.specific[i].specificName = attr.dictionary.name;
    } else {
      this.attrList.splice(i, 1);
      this.model.specific.splice(i, 1);
    }
  }
  onChangeAttributeValue(attr, i) {
    this.model.specific[i].specificValue = attr.value;
  }

  dicToLine(dic: any[], parentName: string = '', level: number = 0, source: any[] = []): any[] {
    dic.forEach(i => {
      i.level = level;
      i.fullName = (parentName ? parentName + ': ' : '') +  i.name;
      source.push(i);
      if (i.children) this.dicToLine(i.children, i.fullName, level + 1, source);
    });
    return source;
  }

  inputCoordinates() {
    let modalRef = this.modalService.show(FormCoordinatesModalComponent, {
      class: 'modal-lg',
      initialState: {
        documentId: this.model.documentId,
        saveCallback: () => {
          this.helper.success('Успешно сохранено!');
        }
      }
    });
  }
}
