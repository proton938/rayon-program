import {Component, EventEmitter, Input, Output} from "@angular/core";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {forIn, remove, some, find, first, toNumber, toString, assign} from 'lodash';
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";
import {BsModalService} from "ngx-bootstrap";
import {FormRenameFileModalComponent} from "../../rename-file/form-rename-file-modal.component";

@Component({
  selector: 'program-form-object-dominant-materials',
  templateUrl: 'form-object-dominant-materials.component.html'
})
export class ProgramFormObjectDominantMaterialsComponent {
  @Input() model: any;
  @Input() validate: boolean;
  @Input() dictionaries: any = {};
  @Input() files: any;

  theses: any[] = [];
  videos: any[] = [];
  isMosRu: boolean;
  fileChanges: any = [];

  constructor(private fileResourceService: FileResourceService,
              private session: SessionStorage,
              public helper: RayonHelperService,
              private modalService: BsModalService) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');

    if (this.model.materials && this.model.materials.video) this.model.materials.video.forEach(i => this.addVideo(i));
    if (!this.videos.length) this.addVideo();
    this.onChangeVideo();

    if (this.model.info3D && this.model.info3D.theses) this.model.info3D.theses.forEach(i => this.addTheses(i));
    if (!this.theses.filter(i => !i).length) this.addTheses();
    this.onChangeTheses();

    if (!this.model.materials.location3D.EPSG) this.model.materials.location3D.EPSG = '4326';
  }

  addVideo(item: string = '') {
    this.videos.push({video: item});
    this.syncVideo();
  }

  onChangeVideo() {
    this.videos = this.videos.filter(i => i.video !== '');
    if (!!this.videos[this.videos.length-1] || !this.videos.length) this.addVideo();
    this.syncVideo();
  }

  syncVideo() {
    this.model.materials.video = this.videos.map(i => i.video);
  }

  addTheses(item: string = '') {
    this.theses.push({theses: item});
    this.syncTheses();
  }

  onChangeTheses() {
    this.theses = this.theses.filter(i => i.theses !== '');
    if (!!this.theses[this.theses.length-1] || !this.theses.length) this.addTheses();
    this.syncTheses();
  }

  syncTheses() {
    this.model.info3D.theses = this.theses.map(i => i.theses);
  }

  onRemoveFile(file) {
    this.fileResourceService.deleteFile(file.idFile);
  }

  updateFileChanges(): void {
    let result: any[] = [];
    forIn(this.files, (files, fileType) => files.forEach(f => result.push(f)));
    this.fileChanges = result;
  }

  filesOnChange(type: string) {
    if (this.model.folderId) {
      this.files[type].forEach(f => {
        if (!some(this.model.files, i => i.id === f.idFile)) {
          this.model.files.push(this.helper.getShortFileInfo(f));
        }
      });
      let toRemove = this.model.files.filter(i => i.type === type && !some(this.files[type], f => f.idFile === i.id)).map(i => i.id);
      remove(this.model.files, (i: any) => some(toRemove, r => r === i.id));
    } else {
      if (this.files[type].length >= this.fileChanges.filter(i => i.typeFile === type)) {
        let _file = this.files[type][this.files[type].length - 1];
        if (!_file.typeFile) _file.typeFile = type;
        let num: number = this.helper.getNumberOfSimilarFile(_file.name, this.fileChanges);
        if (num > 0) {
          let modalRef = this.modalService.show(FormRenameFileModalComponent, {
            class: 'modal-md rename-file-modal',
            initialState: {
              file: {
                oldName: _file.name,
                numberOfFile: num
              },
              saveCallback: (result: string) => {
                _file['nameFile'] = result + '.' + _file.name.substring(_file.name.lastIndexOf('.') + 1);
              }
            }
          });
        }
      }
      this.updateFileChanges();
    }
  }

  degPipeX(value: string = '', config): any {
    let min = -90, max = 90;
    if (this.model.materials.location3D.EPSG == 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }

  degPipeY(value: string = '', config): any {
    let min = -180, max = 180;
    if (this.model.materials.location3D.EPSG == 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }
}
