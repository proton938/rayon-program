import {Component, EventEmitter, Input, Output} from "@angular/core";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {RayonProgramInstruction} from "../../../models/program/RayonProgramInstruction";
import {first, find, compact, some, isString, isArray, every, isNull, isUndefined} from 'lodash';
import {forkJoin, from, of, Observable, Subject, Subscription} from "rxjs";
import {debounceTime, delay, flatMap, map} from "rxjs/internal/operators";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {Transition} from "@uirouter/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {NSIDocumentType} from "@reinform-cdp/core";
import {RayonBtiService} from "../../../services/rayon-bti.service";
import {RayonProgramSpecific} from "../../../models/program/RayonProgramSpecific";
import {SessionStorage} from "@reinform-cdp/security";
import {ObjectMatrixService} from "../../../services/object-matrix.service";
import {SolrMediatorService} from "../../../services/solr-mediator.service";

@Component({
  selector: 'program-form-object',
  templateUrl: './form-object.component.html',
  styleUrls: ['form-object.component.scss']
})
export class ProgramFormObjectComponent {
  @Input() model: any;
  @Input() validate: boolean;
  @Input() files: any;

  @Output() onLoad = new EventEmitter<void>();
  @Output() uploadAlert = new EventEmitter<void>();

  documentId: string;
  dictionaries: any = {};
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  districtsAvailables: any[] = [];
  selected: any = {
    itemType: null,
    objectForm: null,
    address: ''
  };
  counter: any = {
    changeObjectsParams: 0
  };
  isAddressLoading: boolean = false;
  private currentAddressFilter: string;
  public addressKeyUp = new Subject<string>();
  private addressSubscription: Subscription;

  attributesAvailable: any[] = [];
  attrList: any[] = [];

  isMosRu: boolean;

  constructor(private nsi: NsiResourceService,
              private transition: Transition,
              private helper: RayonHelperService,
              private solrMediator: SolrMediatorService,
              private session: SessionStorage,
              private matrix: ObjectMatrixService,
              private bti: RayonBtiService) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');

    if (this.model) {
      this.documentId = this.model.documentId;
      if (this.model.itemType) this.selected.itemType = this.model.itemType.code;
      else this.counter.changeObjectsParams++;
      if (this.model.objectForm) this.selected.objectForm = this.model.objectForm.code;
    }

    const dictionaries$ = this.nsi.getDictsFromCache([
      'District',
      'DocumentTypes',
      'mr_program_BasesInclusion',
      'mr_program_ConstructionPeriods',
      'mr_program_ConstructionPrograms',
      'mr_program_ItemTypes',
      'mr_program_ObjectCategories',
      'mr_program_ObjectForms',
      'mr_program_ObjectKinds',
      'mr_program_ObjectTypes',
      'mr_program_OwnershipForms',
      'mr_program_ResponsibleOrganizations',
      'mr_program_StatePrograms',
      'mr_program_Teps',
      'mr_program_WorkStatuses',
      'mr_program_WorkTypes',
      'mr_program_ObjectIndustries',
      'mr_program_SpecificObject',
      'Prefect',
      'SubSystems',
      'mr_program_RubricatorUKK',
      'mr_program_SpecialObject']);
    const objectMatrix$ = this.matrix.init();

    forkJoin([dictionaries$, objectMatrix$]).subscribe((response: any) => {
      this.dictionaries = response[0];
      this.dictionaries.rubricator = this.dicToLine(this.dictionaries['mr_program_RubricatorUKK']);
      this.dictionaries.organizations = this.matrix.filter(this.dictionaries['mr_program_ResponsibleOrganizations'], 'organization', {add: !this.documentId});
      this.dictionaries.prefects = this.matrix.filter(this.dictionaries['Prefect'], 'prefect');
      this.dictionaries.industries = this.matrix.filter(this.dictionaries['mr_program_ObjectIndustries'], 'industry', {add: !this.documentId});
      this.dictionaries.kinds = this.matrix.filter(this.dictionaries['mr_program_ObjectCategories'].filter(i => i.object), 'kind', {add: !this.documentId});
      if (this.model.address.addressId) this.getAvailableAddressList();
      this.initSuccess();
    }, error => this.helper.error(error));

    this.addressSubscription = this.addressKeyUp
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(100),
        flatMap(search => of(search).pipe(delay(100)))
      ).subscribe(value => {
        if (value !== this.currentAddressFilter) this.getAvailableAddressList(value);
        this.currentAddressFilter = value;
      });
  }

  getFieldByTag(tagName: string): any {
    let result: any = null;
    switch(tagName) {
      default:
        result = this.model[tagName];
        break;
    }
    return result;
  }

  updateAttributesAvailable() {
    this.attributesAvailable = this.dictionaries['mr_program_SpecificObject'].filter(i => {
      let result: boolean = false;
      let kindCode: string = this.helper.getField(this.model, 'object.kind.code');
      let industryCode: string = this.helper.getField(this.model, 'object.industry.code');
      if (kindCode && i.useInObjectTypes && i.useInObjectTypes.length) {
        result = some(i.useInObjectTypes, s => kindCode === s);
      } else if (industryCode && i.useInIndustry && i.useInIndustry.length) {
        result = some(i.useInIndustry, s => industryCode === s);
      } else if (i.useInTag && i.useInTag.length) {
        result = some(i.useInTag, s => this.getFieldByTag(s) === true)
      }
      return result;
    });
  }

  updateAttrList() {
    if (this.model.specific) {
      this.attrList = this.model.specific.map(i => {
        let dic = find(this.dictionaries['mr_program_SpecificObject'], d => d.code === i.specificCode);
        return {
          value: i.specificValue,
          dictionary: dic || null,
          possibleValues: dic && dic.possibleValues ? dic.possibleValues.map(d => { return {code: d, name: d} }) : []
        };
      });
    }
  }

  ngOnDestroy(): void {
    this.addressSubscription.unsubscribe();
  }

  initSuccess(): void {
    if (!this.documentId) {
      let stateParams: any = this.transition.params();
      if (stateParams.prefect) {
        this.model.address.prefect = this.dictionaries.prefects/*Prefect*/.filter(i => i.code === stateParams.prefect);
      }
      if (stateParams.district) {
        this.model.address.district = this.dictionaries.District.filter(i => i.code === stateParams.district);
      }
      if (stateParams.itemType) {
        this.model.itemType = find(this.dictionaries.mr_program_ItemTypes, i => i.code === stateParams.itemType);
        this.selected.itemType = this.model.itemType.code;
      }
      if (stateParams.industry) {
        this.model.object.industry = first(this.dictionaries.industries/*mr_program_ObjectIndustries*/.filter(i => i.code === stateParams.industry));
        this.updateCategoriList();
      }
    }
    if (!this.model.itemType) {
      this.model.itemType = find(this.dictionaries.mr_program_ItemTypes, i => i.code === 'object');
      this.selected.itemType = this.model.itemType.code;
    }

    this.setSingleValues();
    this.updateDistrictList();
    this.updateCategoriList();
    this.updateAttributesAvailable();
    this.updateAttrList();

    this.loadingStatus = LoadingStatus.SUCCESS;
    this.onLoad.emit();
  }

  setSingleValues() { // #ISMR-2103 Форма создания объекта. Реализация автоматического заполнения списков в зависимости от содержимого
    let fieldList: any[] = [
      {
        field: 'prefect',
        list: this.dictionaries.prefects,
        multiple: true,
        source: this.model.address
      },
      {
        field: 'organization',
        list: this.dictionaries.organizations,
        source: this.model.responsible
      },
      {
        field: 'industry',
        list: this.dictionaries.industries,
        source: this.model.object
      }
  ];
    fieldList.forEach(f => {
      if (f.list && f.list.length === 1 && (f.multiple ? !f.source[f.field].length : !f.source[f.field])) {
        f.source[f.field] = f.multiple ? f.list : f.list[0];
      }
    });
  }

  uploadFileAlert() {
    this.uploadAlert.emit();
  }

  onChancePrefect() {
    this.updateDistrictList();
  }

  updateDistrictList() {
    this.districtsAvailables = [];
    if (this.model.address.prefect && this.model.address.prefect.length) {
      this.districtsAvailables = this.dictionaries.District
        .filter(d => this.model.address.prefect.filter(p => p.code === d.perfectId[0].code).length);
      if (this.model.address.district && this.model.address.district.length) {
        this.model.address.district = this.model.address.district
          .filter(i => this.districtsAvailables.filter(f => f.code === i.code).length);
      }
    } else {
      this.districtsAvailables = this.dictionaries.District;
      this.model.address.district = [];
    }
    this.updateInstructions();
  }

  updateInstructions() {
    let filter: string = '';
    if (this.model.address.prefect.length) {
      filter += (filter ? ' AND ' : '') + '(' +this.model.address.prefect.map((i: any) => 'prefectsNameInstruction:' + i.name).join(' OR ') + ')';
    }
    if (this.model.address.district.length) {
      filter += (filter ? ' AND ' : '') + '(' + this.model.address.district.map((i: any) => 'districtsNameInstruction:' + i.name).join(' OR ') + ')';
    }
    if (this.model.object.industry && this.model.object.industry.name) {
      filter += (filter ? ' AND ' : '') + '(industryNameInstruction :' + this.model.object.industry.name + ')';
    }
    if (filter) {
      this.solrMediator.query({ query: filter, page: 0, pageSize: 9999, types: [this.solrMediator.types.instruction] }).subscribe(response => {
        if (response && response.docs) {
          this.dictionaries.instructions = response.docs.map((i: any) => {
            let req = '';
            if (i.documentDateInstruction) req += 'от ' + i.documentDateInstruction.split('-').reverse().join('.');
            if (i.documentNumberInstruction) req += ' №' + i.documentNumberInstruction;

            return (new RayonProgramInstruction()).build({
              instructionId: i.documentId,
              instructionReq: req
            })
          });
        }
      });
    } else this.dictionaries.instructions = [];
  }

  onChangeItemType(item) {
    if (!item) return null;
    this.model.itemType = new NSIDocumentType();
    this.model.itemType.build(item);
    this.updateCategoriList();
    if (this.counter.changeObjectsParams) this.setObjectParams();
  }

  onChangeObjectForm(item) {
    if (!item) return null;
    this.model.objectForm = new NSIDocumentType();
    this.model.objectForm.build(item);

    if (this.model.objectForm.code === 'linear') {
      this.model.address.addressId = null;
      if (this.counter.changeObjectsParams) this.setObjectParams();
      this.model.object.typeObject = '';
      this.model.parameterObject.capacity = '';
    }
  }

  onChangeAddress() {
    if (this.model.address.addressId) {
      this.model.address.address = find(this.dictionaries.addressList, i => i.code === this.model.address.addressId).name;
    } else this.model.address.address = '';

    this.getBuildingInfo().subscribe(buildingInfo => {
      this.setObjectParams(buildingInfo);
    });
  }

  clearAddressList() {
    this.dictionaries.addressList = [];
  }

  getAvailableAddressList(searchString?: string) {
      let filter: string = '';
      let result: any[] = [];
      if ((searchString && searchString.length >= 3) || (!searchString && this.model.address.addressId)) {
        this.isAddressLoading = true;
        if (this.model.address.prefect && this.model.address.prefect.length) {
          filter += (filter ? ' AND ' : '') + '(prefectCodeAddress:' + this.model.address.prefect.map(i => i.code).join(' OR prefectCodeAddress:') + ')'
        }
        if (this.model.address.district && this.model.address.district.length) {
          filter += (filter ? ' AND ' : '') + '(districtCodeAddress:' + this.model.address.district.map(i => i.code).join(' OR districtCodeAddress:') + ')'
        }
        if (!searchString && this.model.address.addressId) {
          filter += (filter ? ' AND ' : '') + '(documentId:' + this.model.address.addressId + ')';
        } else {
          filter += (filter ? ' AND ' : '') + '(fullAddress:*' + searchString.replace(/\s+/g, '* AND fullAddress:*') + '*)';
        }
        filter += (filter ? ' AND NOT ' : '') + '(isDeletedAddress:true)';
        this.solrMediator.query({page: 0, pageSize: 100, types: [this.solrMediator.types.address], query: filter}).subscribe(response => {
          if (response && response.docs) {
            this.dictionaries['addressList'] = result = response.docs.map((d: any) => {
              return {
                code: d.documentId,
                name: d.fullAddress
              };
            });
            this.isAddressLoading = false;
          } else {
            this.dictionaries['addressList'] = result = [];
            this.isAddressLoading = false;
          }
        });
      }
  }

  addressSearchFn(term: string, item: any) { // Чтобы не фильтровал уже отфильтрованные данные с сервера
    return !!item;
  }

  updateCategoriList() {
    this.dictionaries.objectCategories = this.dictionaries.kinds/*mr_program_ObjectCategories*/.filter(this.filterObjectCategories.bind(this));
    if (this.model.object.kind && !some(this.dictionaries.objectCategories.map(i => i.code), i => i === this.model.object.kind.code)) {
      this.model.object.kind = null;
    }
  }

  filterObjectCategories(item) {
    return (item.industry && this.model.object.industry
      ? some(item.industry, i => i.code === this.model.object.industry.code)
      : false
    ) && (this.selected.itemType ? item.object === (this.selected.itemType === 'object') : true);
  }

  isLoaded(): boolean {
    return this.loadingStatus === LoadingStatus.SUCCESS;
  }

  getTypeof(str: any) { // remove this method
    return typeof str;
  }

  getBuildingInfo(): Observable<any> {
    return Observable.create(observer => {
      if (this.model.itemType && this.model.itemType.code === 'object' && this.model.objectForm.code === 'building'
        && this.model.address.addressId) {
        this.solrMediator.query({page: 0, pageSize: 1, types: [this.solrMediator.types.address],
          query: 'documentId:' + this.model.address.addressId}).subscribe((addRes: any) => {
          if (addRes && addRes.docs && addRes.docs.length) {
            let unoms: string[] = addRes.docs.map(i => i.unomAddress);
            this.bti.findBuildings(unoms).subscribe((res: any) => {
              if (res && res.length === 1) {
                let buildingInfo: any = res[0].building || null;
                observer.next(buildingInfo);
              } else {
                observer.next(null);
              }
              observer.complete();
            });
          } else {
            observer.next(null);
            observer.complete();
          }
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next(null);
        observer.complete();
      }
    });
  }

  setObjectParams(params: any = null) {
    let check = p => params && !isNull(params[p]) && !isUndefined(params[p]);
    this.model.object.areaObject = check('areaObject') ? params.areaObject : '';
    this.model.parameterObject.constructionYear = check('constructionYear') ? params.constructionYear : '';
    this.model.parameterObject.use = check('use') ? params.use : '';
    this.model.parameterObject.floor = check('floor') ? params.floor : '';
    this.model.parameterObject.wallMaterial = check('wallMaterial') ? params.wallMaterial : '';
    this.model.parameterObject.wearRate = check('wearRate') ? params.wearRate : '';
    this.counter.changeObjectsParams++;
  }

  addAttribute() {
    let attr = new RayonProgramSpecific();
    this.model.specific.push(attr);
    this.updateAttrList();
  }

  onChangeAttributeName(attr, i) {
    if (attr.dictionary) {
      attr.possibleValues = attr.dictionary.possibleValues && attr.dictionary.possibleValues.length
        ? attr.dictionary.possibleValues.map(d => { return {code: d, name: d} }) : [];
      this.model.specific[i].specificCode = attr.dictionary.code;
      this.model.specific[i].specificName = attr.dictionary.name;
    } else {
      this.attrList.splice(i, 1);
      this.model.specific.splice(i, 1);
    }
  }
  onChangeAttributeValue(attr, i) {
    this.model.specific[i].specificValue = attr.value;
  }

  dicToLine(dic: any[], parentName: string = '', level: number = 0, source: any[] = []): any[] {
    dic.forEach(i => {
      i.level = level;
      i.fullName = (parentName ? parentName + ': ' : '') +  i.name;
      source.push(i);
      if (i.children) this.dicToLine(i.children, i.fullName, level + 1, source);
    });
    return source;
  }
}
