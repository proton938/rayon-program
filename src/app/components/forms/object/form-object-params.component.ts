import {Component, Input} from "@angular/core";
import {AlertService, LoadingStatus} from "@reinform-cdp/widgets";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-form-object-params',
  templateUrl: './form-object-params.component.html',
  styleUrls: ['form-object-params.component.scss']
})
export class ProgramFormObjectParamsComponent {
  @Input() model: any;
  @Input() validate: boolean;
  @Input() dictionaries: any = {};
  isMosRu: boolean;

  cadastralNumbers: any[] = [];

  constructor(public helper: RayonHelperService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
  }
}
