import {Component, Input} from "@angular/core";
import {AlertService, LoadingStatus} from "@reinform-cdp/widgets";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-form-object-params-zu',
  templateUrl: './form-object-params-zu.component.html'
})
export class ProgramFormObjectParamsZuComponent {
  @Input() model: any;
  @Input() validate: boolean;
  @Input() dictionaries: any = {};

  isMosRu: boolean;
  cadastralNumbers: any[] = [];

  constructor(public helper: RayonHelperService,
              private session: SessionStorage) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    if (this.model.parameterZu && this.model.parameterZu.cadastralNumber) this.model.parameterZu.cadastralNumber.forEach(i => this.addCadastral(i));
    if (!this.cadastralNumbers.length) this.addCadastral();
    this.onChangeCadastralNumber();
  }

  addCadastral(item: string = '') {
    this.cadastralNumbers.push({cad: item});
    this.syncCadastralNumbers();
  }

  removeCadastral(index) {
    this.cadastralNumbers.splice(index, 1);
    this.syncCadastralNumbers();
  }

  onChangeCadastralNumber() {
    this.cadastralNumbers = this.cadastralNumbers.filter(i => i.cad !== '');
    if (!!this.cadastralNumbers[this.cadastralNumbers.length-1] || !this.cadastralNumbers.length) this.addCadastral();
    this.syncCadastralNumbers();
  }

  syncCadastralNumbers() {
    this.model.parameterZu.cadastralNumber = this.cadastralNumbers.map(i => i.cad);
  }

  isMandatoryObjectArea(): boolean {
    return !!this.model.parameterZu && !!this.model.parameterZu.cadastralNumber && !!this.model.parameterZu.cadastralNumber.join('').trim().length;
  }
}
