import {Component, EventEmitter, Input, Output} from "@angular/core";
import {FileResourceService} from "@reinform-cdp/file-resource";
import { BsModalService } from 'ngx-bootstrap/modal';
import {FormCoordinatesModalComponent} from "../../coordinates/form-coordinates-modal.component";
import {remove, some, find, first, toNumber, toString, forIn} from 'lodash';
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";
import {RayonProgramFileInfo} from "../../../../models/program/RayonProgramFileInfo";

@Component({
  selector: 'program-form-object-archive-materials',
  templateUrl: 'form-object-archive-materials.component.html'
})
export class ProgramFormObjectArchiveMaterialsComponent {
  @Input() model: any;
  @Input() validate: boolean;
  @Input() dictionaries: any = {};
  @Input() files: any;
  @Output() uploadAlert = new EventEmitter<void>();

  theses: any[] = [];
  videos: any[] = [];
  isMosRu: boolean;

  constructor(private fileResourceService: FileResourceService,
              private modalService: BsModalService,
              private session: SessionStorage,
              public helper: RayonHelperService) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');

    if (this.model.materials && this.model.materials.video) this.model.materials.video.forEach(i => this.addVideo(i));
    if (!this.videos.length) this.addVideo();
    this.onChangeVideo();

    if (this.model.info3D && this.model.info3D.theses) this.model.info3D.theses.forEach(i => this.addTheses(i));
    if (!this.theses.filter(i => !i).length) this.addTheses();
    this.onChangeTheses();

    if (!this.model.materials.location3D.EPSG) this.model.materials.location3D.EPSG = '4326';
  }

  uploadFileAlert() {
    this.uploadAlert.emit();
  }

  addVideo(item: string = '') {
    this.videos.push({video: item});
    this.syncVideo();
  }

  onChangeVideo() {
    this.videos = this.videos.filter(i => i.video !== '');
    if (!!this.videos[this.videos.length-1] || !this.videos.length) this.addVideo();
    this.syncVideo();
  }

  syncVideo() {
    this.model.materials.video = this.videos.map(i => i.video);
  }

  addTheses(item: string = '') {
    this.theses.push({theses: item});
    this.syncTheses();
  }

  onChangeTheses() {
    this.theses = this.theses.filter(i => i.theses !== '');
    if (!!this.theses[this.theses.length-1] || !this.theses.length) this.addTheses();
    this.syncTheses();
  }

  syncTheses() {
    this.model.info3D.theses = this.theses.map(i => i.theses);
  }

  onRemoveFile(file) {
    this.fileResourceService.deleteFile(file.idFile);
  }

  filesOnChange(type: string) {
    let fileList: RayonProgramFileInfo[] = [];
    forIn(this.files, (files, fileType) => {
      files.forEach(f => fileList.push(this.helper.getShortFileInfo(f)));
    });
    this.model.files = fileList;
    /*this.files[type].forEach(f => {
      if (!some(this.model.files, i => i.id === f.idFile)) {
        this.model.files.push(this.helper.getShortFileInfo(f));
      }
    });
    let toRemove = this.model.files.filter(i => i.type === type && !some(this.files[type], f => f.idFile === i.id)).map(i => i.id);
    remove(this.model.files, (i: any) => some(toRemove, r => r === i.id));*/
  }

  inputCoordinates() {
    let modalRef = this.modalService.show(FormCoordinatesModalComponent, {
      class: 'modal-lg',
      initialState: {
        documentId: this.model.documentId,
        saveCallback: () => {
          this.helper.success('Успешно сохранено!');
        }
      }
    });
  }

  degPipeX(value: string = '', config): any {
    let min = -90, max = 90;
    if (this.model.materials.location3D.EPSG == 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }

  degPipeY(value: string = '', config): any {
    let min = -180, max = 180;
    if (this.model.materials.location3D.EPSG == 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }
}
