import {Component, EventEmitter, Input, Output} from "@angular/core";
import {SessionStorage} from "@reinform-cdp/security";
import {RayonHelperService} from "../../../../services/rayon-helper.service";
import {toNumber, toString} from 'lodash';
import {FileResourceService} from "@reinform-cdp/file-resource";

@Component({
  selector: 'program-form-object-archive-features',
  templateUrl: './form-object-archive-features.component.html'
})
export class ProgramFormObjectArchiveFeaturesComponent {
  @Input() model: any;
  @Input() validate: boolean;
  @Input() dictionaries: any = {};
  @Input() files: any;
  @Output() updateAttributesCallback = new EventEmitter();
  @Output() uploadAlert = new EventEmitter();

  isMosRu: boolean;
  isObjectCardPublic: boolean;

  constructor(private session: SessionStorage,
              public helper: RayonHelperService,
              private fileResourceService: FileResourceService) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    this.isObjectCardPublic = this.session.hasPermission('mr_objectCardPublic');

    if (this.model.dominant.dominantIs && !this.model.dominant.locationDominant.EPSG) this.model.dominant.locationDominant.EPSG = '4326';
  }

  updateAttrs() {
    this.updateAttributesCallback.emit();
  }

  degPipeX(value: string = '', config): any {
    let min = -90, max = 90;
    if (this.model.dominant.dominantIs && this.model.dominant.locationDominant.EPSG == 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }

  degPipeY(value: string = '', config): any {
    let min = -180, max = 180;
    if (this.model.dominant.dominantIs && this.model.dominant.locationDominant.EPSG == 4326) {
      if (toNumber(value) < min) return toString(min);
      if (toNumber(value) > max) return toString(max);
    }
    return value;
  }

  onRemoveFile(file) {
    this.fileResourceService.deleteFile(file.idFile);
  }

  uploadFileAlert() {
    this.uploadAlert.emit();
  }
}
