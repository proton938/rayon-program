import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {Observable, Subject, Subscription, forkJoin, from, of} from 'rxjs';
import {copy} from 'angular';
import {RayonHelperService} from '../../../services/rayon-helper.service';
import {LoadingStatus, ExFileType} from '@reinform-cdp/widgets';
import {RayonWork} from '../../../models/work/RayonWork';
import {debounceTime, delay, map, flatMap} from 'rxjs/internal/operators';
import {RayonProgramResourceService} from '../../../services/rayon-program-resource.service';
import {RayonProgram} from '../../../models/program/RayonProgram';
import {some, forIn, find, concat, last} from 'lodash';
import {RayonWorkSpecific} from '../../../models/work/RayonWorkSpecific';
import {SessionStorage} from '@reinform-cdp/security';
import {WorkMatrixService} from '../../../services/work-matrix.service';
import {RayonWorkProgress} from '../../../models/work/RayonWorkProgress';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {RayonWorkProgressDocument} from '../../../models/work/RayonWorkProgressDocument';
import {compare} from 'fast-json-patch';
import {RayonWorkUser} from '../../../models/work/RayonWorkUser';
import {SolrMediatorService} from '../../../services/solr-mediator.service';
import {RayonWorkTypeService} from "../../../services/rayon-work-type.service";

@Component({
  selector: 'program-form-work-type',
  templateUrl: './form-work-type.component.html'
})
export class FormWorkTypeComponent implements OnInit, OnDestroy {
  @Input() model: RayonWork;
  @Input() validate: boolean;
  @Input() itemCode: string; // object / event
  @Input() objectId: string;

  @Output() onLoad = new EventEmitter<void>();

  oldModel: RayonWork;

  isAddressLoading = false;
  public addressKeyUp = new Subject<string>();
  private addressSubscription: Subscription;
  private addressFilterValue = '';

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  dictionaries: any = {};
  objectData: RayonProgram;
  years: number[] = [];
  startYears = { execution: [], psd: [], smr: [] };
  finishYears = { execution: [], psd: [], smr: [] };

  attributesAvailable: any[] = [];
  attrList: any[] = [];

  isMosRu: boolean;
  progress: RayonWorkProgress = new RayonWorkProgress();
  progressFiles: any[] = [];
  files: any[] = [];

  constructor(public helper: RayonHelperService,
              private nsi: NsiResourceService,
              private programService: RayonProgramResourceService,
              private session: SessionStorage,
              private solrMediator: SolrMediatorService,
              private fileResourceService: FileResourceService,
              private workMatrix: WorkMatrixService,
              private workService: RayonWorkTypeService) {}

  ngOnInit() {
    this.oldModel = copy(this.model);
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');

    if (!this.model.objectId) {
      this.model.objectId = this.objectId;
    } else if (!this.objectId && this.model.objectId) {
      this.objectId = this.model.objectId;
    }

    this.years = this.makeYearList();
    const object$ = this.programService.init(this.objectId);
    const dictionaries$ = this.nsi.getDictsFromCache([
      'mr_program_ResponsibleOrganizations', // Ответственные организации
      'mr_program_WorkTypes', // Виды работ
      'mr_program_StatePrograms', // Государственные программы
      'mr_program_StateCustomers', // Государственные заказчики
      'mr_instruction_SupportTypes', // Источники финансирования
      'mr_program_WorkStatuses', // Этапы реализации мероприятий по объекту
      'mr_program_SpecificWork', // Специфические атрибуты видов работ
      'mr_program_approveAgStatus', // Статусы согласования работ на АГ
      'mr_program_approveMunicipalStatus', // Статусы согласования работ с муниципальными депутатами,
      'mr_program_WorkDocTypes', // Типы документов, подтверждающих выполнение работ
      'mr_program_approveDocStatus', // Статусы документов, подтверждающих стадию работ
      'mr_program_WorkDocTypesCanceled', // Типы документов, подтверждающих отмену работ,
      'mr_program_WorkCancelReasons' // Причины отмены работ
    ]);
    const matrix$ = this.workMatrix.init();
    const files$ = this.getFiles();
    forkJoin([dictionaries$, object$, matrix$, files$]).subscribe((response: any) => {
      forIn(response[0], (item, key) => {
        this.dictionaries[key] = item;
      });
      this.dictionaries = response[0];
      this.dictionaries.years = this.years.map(i => {
        return {code: i, name: i};
      });
      this.objectData = response[1];
      if (this.objectData.itemType && !this.itemCode) { this.itemCode = this.objectData.itemType.code; }
      this.files = response[3] || [];
      this.conversionDictionaries();
      this.initSuccess();
    }, error => this.helper.error(error));

    this.addressSubscription = this.addressKeyUp
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(100),
        flatMap(search => of(search).pipe(delay(100)))
      ).subscribe(value => {
        if (value !== this.addressFilterValue) { this.getAvailableAddressList(value); }
        this.addressFilterValue = value;
      });
  }

  ngOnDestroy(): void {
    this.addressSubscription.unsubscribe();
  }

  initSuccess() {
    if (this.model.addressId) { this.getAvailableAddressList(); }
    this.updateStartYears();
    this.updateFinishYears();
    this.updateAttributesAvailable();
    this.updateAttrList();
    this.setSingleValues();
    this.updateProgressFieldset();
    this.initFields();

    this.loadingStatus = LoadingStatus.SUCCESS;
    this.onLoad.emit();
  }

  initFields() {
    if (!this.model.approveAg) {
      this.model.approveAg = this.getDictValue('approveAgStatus', 'neprov');
    }
    if (!this.model.approveMunicipalDep) {
      this.model.approveMunicipalDep = this.getDictValue('approveMunicipalStatus', 'neprov');
    }

    if (this.model.status && this.model.status.code && !this.getDictValue('workStatuses', this.model.status.code)) {
      this.model.status = null; // #ISMR-2722
    }
  }

  isNewProgress(): boolean {
    return this.model.status && (!this.oldModel.status || this.model.status.code !== this.oldModel.status.code);
  }

  updateProgressFieldset() {
    if (this.model.progress && this.model.progress.length && !this.isNewProgress()) {
      this.progress = last(this.model.progress);
    } else {
      this.progress = new RayonWorkProgress();
      this.progress.stage = this.model.status || null;
    }
    if (!this.progress.documentsExecution) {
      this.progress.documentsExecution = [];
      // this.addProgressExDoc();
    } else {
      this.progressFiles = this.progress.documentsExecution.map(i => {
        return i.files.map(id => find(this.files, f => f.idFile === id));
      });
    }
  }

  addProgressExDoc() {
    this.progress.documentsExecution.push(new RayonWorkProgressDocument());
    this.progressFiles.push([]);
  }

  removeProgressExDoc(index) {
    this.progress.documentsExecution.splice(index, 1);
    this.progressFiles.splice(index, 1);
  }

  setSingleValues() { // #ISMR-2077 Автоматическое заполнение списков в зависимости от содержимого
    const fieldList: any[] = [
      {field: 'type', list: this.dictionaries.workTypes},
      {field: 'organization', list: this.dictionaries.responsibleOrganizations}
    ];
    fieldList.forEach(f => {
      if (f.list && f.list.length === 1 && !this.model[f.field]) { this.model[f.field] = f.list[0]; }
    });
  }

  updateStartYears(initiator: string = 'start') {
    this.updateStartYearList('execution', initiator);
    this.updateStartYearList('psd', initiator);
    this.updateStartYearList('smr', initiator);
  }

  updateStartYearList(listName: string, initiator: string) { // target = 'start' | 'finish'
    if (this.model[listName].finish) {
      this.startYears[listName] = this.dictionaries.years.filter(i => i.code <= this.model[listName].finish);
      if (this.model[listName].start > this.model[listName].finish) {
        this.model[listName][initiator === 'start' ? 'finish' : 'start'] = null;
      }
    } else {
      this.startYears[listName] = this.dictionaries.years;
    }

    switch (listName) {
      case 'smr':
      case 'psd':
        if (this.model.execution && this.model.execution.start) {
          this.startYears[listName] = this.startYears[listName].filter(i => i.code >= this.model.execution.start);
          if (this.model[listName].start < this.model.execution.start) {
            this.model[listName].start = null;
          }
        }
        if (this.model.execution && this.model.execution.finish) {
          this.startYears[listName] = this.startYears[listName].filter(i => i.code <= this.model.execution.finish);
          if (this.model[listName].start > this.model.execution.finish) {
            this.model[listName].start = null;
          }
        }
        break;
    }
  }

  updateFinishYears(initiator: string = 'start') {
      this.updateFinishYearList('execution', initiator);
      this.updateFinishYearList('psd', initiator);
      this.updateFinishYearList('smr', initiator);
  }

  updateFinishYearList(listName: string, initiator: string) {
    if (this.model[listName].start) {
      this.finishYears[listName] = this.dictionaries.years.filter(i => i.code >= this.model[listName].start);
      if (this.model[listName].finish < this.model[listName].start) {
        this.model[listName][initiator === 'start' ? 'finish' : 'start'] = null;
      }
    } else {
      this.finishYears[listName] = this.dictionaries.years;
    }

    if (listName === 'smr' && this.model.execution && this.model.execution.finish) {
      this.finishYears[listName] = this.finishYears[listName].filter(i => i.code <= this.model.execution.finish);
      if (this.model[listName].finish > this.model.execution.finish) {
        this.model[listName].finish = null;
      }
    }

    switch (listName) {
      case 'smr':
      case 'psd':
        if (this.model.execution && this.model.execution.start) {
          this.finishYears[listName] = this.finishYears[listName].filter(i => i.code >= this.model.execution.start);
          if (this.model[listName].finish < this.model.execution.start) { this.model[listName].finish = null; }
        }
        if (this.model.execution && this.model.execution.finish) {
          this.finishYears[listName] = this.finishYears[listName].filter(i => i.code <= this.model.execution.finish);
          if (this.model[listName].finish > this.model.execution.finish) { this.model[listName].finish = null; }
        }
        break;
    }
  }

  conversionDictionaries(): void {
    const getMatrixItem = (type: string): any => {
      const rows: any[] = this.workMatrix.getUserMatrixRows().filter(i => {
        let r = true;
        if (r && this.objectData.address.prefect.length) {
          r = this.workMatrix.checkByList(this.objectData.address.prefect.map(p => p.code), 'prefect', {source: [i]});
        }
        if (r && this.objectData.object.industry) {
          r = this.workMatrix.checkByList(this.objectData.object.industry.code, 'industry', {source: [i]});
        }
        if (r && i) { r = !<any>i.okn == !<any>this.objectData.okn; }
        // if (type === 'type') console.log('ROLE:', i.role, '   R:', r);
        return r;
      }) || [];
      return !some(rows, i => !i[type])
        ? concat.apply([], rows.map((i: any) => i[type] || []))
        : []; // If there is row without items (no limit) - return empty array!
    };
    const matrixDicts: any = {
      type: getMatrixItem('type') || [],
      organization: getMatrixItem('organization') || []
    };
    const matrixFilter = (i: any, type: string): boolean => {
      if (!matrixDicts[type].length) { return true; }
      return some(matrixDicts[type], d => d.code === i.code);
    };

    this.dictionaries.responsibleOrganizations = this.dictionaries['mr_program_ResponsibleOrganizations']
      .filter(i => matrixFilter(i, 'organization'));
    this.dictionaries.workTypes = this.dictionaries['mr_program_WorkTypes'].filter(i => {
      return (i.industry && i.industry.length && this.objectData.object.industry
        ? some(i.industry, ind => ind.code === this.objectData.object.industry.code)
        : true
      ) && matrixFilter(i, 'type');
    });
    this.dictionaries.statePrograms = this.dictionaries['mr_program_StatePrograms'];
    this.dictionaries.stateCustomers = this.dictionaries['mr_program_StateCustomers'];
    this.dictionaries.supportTypes = this.dictionaries['mr_instruction_SupportTypes'].filter(i => some(i.item, d => d === this.itemCode));
    this.dictionaries.workStatuses = this.dictionaries['mr_program_WorkStatuses'].sort((a, b) => a.sortValue - b.sortValue);
    this.dictionaries.approveAgStatus = this.dictionaries['mr_program_approveAgStatus'];
    this.dictionaries.approveMunicipalStatus = this.dictionaries['mr_program_approveMunicipalStatus'];
    this.dictionaries.workDocTypes = this.dictionaries['mr_program_WorkDocTypes'];
    this.dictionaries.workDocTypesCanceled = this.dictionaries['mr_program_WorkDocTypesCanceled'];
    this.dictionaries.workDocStatuses = this.dictionaries['mr_program_approveDocStatus'];
    this.dictionaries.budgetList = [ {code: true, name: 'Предусмотрено'}, {code: false, name: 'Не предусмотрено'} ];
    this.dictionaries.cancelReasons = this.dictionaries['mr_program_WorkCancelReasons'];
    // this.updateAvailableStatuses();
  }

  addressSearchFn(term: string, item: any) { // Чтобы не фильтровал уже отфильтрованные данные с сервера
    return !!item;
  }

  getAvailableAddressList(searchString?: string) {
    let filter = '';
    if ((searchString && searchString.length >= 3) || (!searchString && this.model.addressId)) {
      this.isAddressLoading = true;
      if (!searchString && this.model.addressId) {
        filter += (filter ? ' AND ' : '') + '(documentId:' + this.model.addressId + ')';
      } else {
        filter += (filter ? ' AND ' : '') + '(fullAddress:*' + searchString.replace(/\s+/g, '* AND fullAddress:*') + '*)';
      }
      filter += (filter ? ' AND NOT ' : '') + '(isDeletedAddress:true)';
        this.solrMediator.query({page: 0, pageSize: 100, types: [this.solrMediator.types.address], query: filter}).subscribe(response => {
        if (response && response.docs) {
          this.dictionaries.addressList = response.docs.map((d: any) => {
            return {
              code: d.documentId,
              name: d.fullAddress
            };
          });
          this.isAddressLoading = false;
        } else {
          this.dictionaries.addressList = [];
          this.isAddressLoading = false;
        }
      });
    }
  }

  makeYearList() {
    const year = (new Date()).getFullYear();
    const min = year - 10;
    const max = year + 10;
    let startYear = min;
    const r: any[] = [startYear];
    for (let i = 0; startYear < max; i++) {
      r.push(++startYear);
    }
    return r;
  }

  isLoaded(): boolean {
    return this.loadingStatus === LoadingStatus.SUCCESS;
  }

  updateAttributesAvailable() {
    this.attributesAvailable = this.dictionaries['mr_program_SpecificWork'].filter(i => {
      let result = false;
      if (this.model.type && some(i.useInType, s => this.model.type.code === s)) {
        result = true;
      }
      return result;
    });
  }

  updateAttrList() {
    if (this.model.specific) {
      this.attrList = this.model.specific.map(i => {
        const dic = find(this.dictionaries['mr_program_SpecificWork'], d => d.code === i.specificCode);
        return {
          value: i.specificValue,
          dictionary: dic || null,
          possibleValues: dic && dic.possibleValues ? dic.possibleValues.map(d => ({code: d, name: d})) : []
        };
      });
    }
  }

  addAttribute() {
    const attr = new RayonWorkSpecific();
    this.model.specific.push(attr);
    this.updateAttrList();
  }

  onChangeAttributeName(attr, i) {
    attr.possibleValues = attr.dictionary.possibleValues && attr.dictionary.possibleValues.length
      ? attr.dictionary.possibleValues.map(d => ({code: d, name: d})) : [];
    this.model.specific[i].specificCode = attr.dictionary.code;
    this.model.specific[i].specificName = attr.dictionary.name;
  }
  onChangeAttributeValue(attr, i) {
    this.model.specific[i].specificValue = attr.value;
  }

  onChangeAddress() {
    if (this.model.addressId) {
      this.model.address = find(this.dictionaries.addressList, i => i.code === this.model.addressId).name;
    } else { this.model.address = ''; }
  }

  onChangeStatus() {
    if (this.model.status) {
      this.model.mos.status = this.getMosStatus(this.model.status.code);
    }
    this.updateProgressFieldset();
  }

  getFiles(): Observable<any[]> {
    return this.model.folderId
      ? this.helper.getFiles(this.model.folderId).pipe(map(files => files.map(file => {
        return ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
          false, file.dateCreated, file.fileType, file.mimeType);
      })))
      : from(of([]));
  }

  onRemoveFile(file) {
    if (file && file.idFile) {
      this.fileResourceService.deleteFile(file.idFile);
    }
  }

  filesOnChange() {
    this.progress.documentsExecution.forEach((doc, index) => {
      doc.files = this.progressFiles[index].map(i => i.idFile);
    });
  }

  isShowField(fieldName: string) {
    let r = false;
    const hasCode = (codes: string[], context = this.model) => {
      return some(codes, i => i === this.helper.getField(context, 'status.code'));
    };
    const type = this.helper.getField(this.model, 'type.code');
    const status = this.helper.getField(this.model, 'status.code');
    const oldStatus = this.helper.getField(this.oldModel, 'status.code');
    switch (fieldName) {
      case 'bargainNumberEaist':
        r = hasCode(['torgi_psd', 'torgi_smr']);
        break;
      case 'registryNumberEaist':
        r = hasCode(['contract_psd', 'contract_smr']);
        break;
      case 'itemNumber':
        r = hasCode(['torgi_psd', 'torgi_smr', 'contract_psd', 'contract_smr']);
        break;
      case 'executionFactStart':
        r = hasCode(['psd', 'smr']);
        break;
      case 'executionFactFinish':
      case 'captionWorkDocs':
      case 'workDocs':
        r = hasCode(['psd_done', 'done']);
        break;
      case 'status':
        r = type && some(['building', 'reconstruction'], i => i === type);
        break;
      case 'cancelReason':
      case 'comment':
      case 'captionWorkCancelDocs':
      case 'workCancelDocs':
        r = status === 'canceled';
        break;
      case 'progressReason':
        const needShow = oldStatus && !hasCode(['psd_done', 'done', 'canceled'], this.oldModel);
        if (needShow && oldStatus !== status) {
          const sortVal = status ? this.getDictSortValue('workStatuses', status) : 0;
          const oldSortVal = oldStatus ? this.getDictSortValue('workStatuses', oldStatus) : 0;
          r = sortVal < oldSortVal;
        }
        break;
      case 'approveMunicipalDep':
        r = this.helper.getField(this.model, 'type.code') === 'accomplishment'
          && this.helper.getField(this.objectData, 'object.kind.code') === 'YARD';
        break;
    }
    return r;
  }

  getWorkDocTypes(): any[] {
    let r: any[] = [];
    if (this.model && this.model.status) {
      switch (this.model.status.code) {
        case 'done':
        case 'psd_done':
          r = this.dictionaries.workDocTypes;
          break;
        case 'canceled':
          r = this.dictionaries.workDocTypesCanceled;
          break;
      }
    }
    return r;
  }

  /*getWorkStatuses(): any[] {
    let r: any[] = this.dictionaries.workStatuses;
    if (this.oldModel && this.oldModel.status) {
      switch (this.oldModel.status.code) {
        case 'psd_done':
        case 'done':
        case 'canceled':
          r = r.slice(findIndex(r, i => i.code === this.oldModel.status.code));
          break;
      }
    }
    return r;
  }*/

  /*updateAvailableStatuses() {
    this.dictionaries.workStatusesAvailable = this.getWorkStatuses();
  }*/

  getDictValue(dictName: string, code: string, codePropName: string = 'code'): any {
    return find(this.dictionaries[dictName], i => i[codePropName] === code);
  }

  getDictSortValue(dictName: string, code: string, codePropName: string = 'code') {
    let r = 0;
    const dictValue: any = this.getDictValue(dictName, code, codePropName);
    if (dictValue && dictValue.sortValue) { r = +dictValue.sortValue; }
    return r;
  }

  getMosStatus(code: string): string {
    let r = '';
    switch (code) {
      case 'prior':
        r = 'Предварительная проработка вопроса';
        break;
      case 'psd':
      case 'torgi_psd':
      case 'contract_psd':
      case 'psd_done':
        r = 'Ведется проектирование';
        break;
      case 'smr':
      case 'torgi_smr':
      case 'contract_smr':
        r = 'Ведутся работы';
        break;
      case 'done':
        r = 'Работы завершены';
        break;
      case 'canceled':
        r = 'Дополнительная проработка вопроса';
        break;
    }
    return r;
  }

  prepareData(): void {
    if (this.isNewProgress() || (this.model.status && !this.model.progress.length)) {
      this.model.progress.push(this.progress);
    }
    const currentDate: Date = new Date();
    currentDate.setHours(0, 0, 0);
    if (compare(this.model.progress, this.oldModel.progress).length) {
      this.progress.executor = new RayonWorkUser();
      this.progress.executor.login = this.session.login() || '';
      this.progress.executor.fio = this.session.fullName() || '';
      this.progress.executor.date = currentDate;
      this.progress.executor.organization = this.session.departmentFullName() || '';
    }
    // if (this.model.status && this.model.status.code === 'canceled') {
    //   this.model['public'] = this.model.mos['public'] = false;
    // }
  }

  afterSave(id: string = this.model.documentId): Observable<any> {
    const requests = [];
    if (id) {
      if (this.model.status && this.model.status.code === 'canceled') {
        requests.push(this.programService.changeNotifyDocs({[id]: 'WORK'}));
      }
      requests.push(this.workService.updateIsogdSpecific(this.model));
    }
    if (requests.length) {
      return forkJoin(requests);
    }
    return from(of('ok'));
  }

  isValid(): boolean {
    let r = true;
    if (!this.session.hasPermission('mr_objectCardMos')) {
      if (r) { r = !!this.model.organization; }
      if (r) { r = !!this.model.status; }
      if (r) { r = !!this.model.type; }
    }
    if (this.progress) {
      if (r && this.isShowField('executionFactFinish')) { r = !!this.progress.executionFactFinish; }
      if (r && this.isShowField('cancelReason')) { r = !!this.progress.cancelReason; }
    }
    if (r) { r = !!this.model.approveAg; }
    if (this.isShowField('approveMunicipalDep')) {
      if (r) { r = !!this.model.approveMunicipalDep; }
    }
    return r;
  }
}
