import {Component, Input} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {RayonWorkFinanceByYear} from "../../../models/work/RayonWorkFinanceByYear";
import {RayonWorkFinance} from "../../../models/work/RayonWorkFinance";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-form-work-type-finance',
  templateUrl: './form-work-type-finance.component.html'
})
export class FormWorkTypeFinanceComponent {
  @Input() model: RayonWorkFinance[];
  @Input() validate: boolean;
  @Input() dictionaries: any = {};
  @Input() years: number[] = [];
  isMosRu: boolean;

  constructor(public helper: RayonHelperService, private session: SessionStorage) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    if (!this.model.length) this.addFinance();
  }

  addFinance() {
    let r: RayonWorkFinance = new RayonWorkFinance();
    this.addFinanceByYear(r.byYear);
    this.model.push(r);
  }
  removeFinance(index: number) {
    this.model.splice(index, 1);
  }

  addFinanceByYear(source: any) {
    let r: RayonWorkFinanceByYear = new RayonWorkFinanceByYear();
    source.push(r);
  }
  removeFinanceByYear(source: any[], index: number) {
    source.splice(index, 1);
  }
}
