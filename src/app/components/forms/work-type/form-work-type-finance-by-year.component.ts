import {Component, Input} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {RayonWorkFinanceByYear} from "../../../models/work/RayonWorkFinanceByYear";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'program-form-work-type-finance-by-year',
  templateUrl: './form-work-type-finance-by-year.component.html'
})
export class FormWorkTypeFinanceByYearComponent {
  @Input() model: RayonWorkFinanceByYear[];
  @Input() validate: boolean;
  @Input() dictionaries: any = {};
  @Input() years: number[] = [];
  isMosRu: boolean;

  constructor(public helper: RayonHelperService, private session: SessionStorage) {}

  ngOnInit() {
    this.isMosRu = this.session.hasPermission('mr_objectCardMos');
    if (!this.model.length) this.addFinanceByYear();
  }

  addFinanceByYear() {
    let r: RayonWorkFinanceByYear = new RayonWorkFinanceByYear();
    this.model.push(r);
  }
  removeFinanceByYear(index: number) {
    this.model.splice(index, 1);
  }
}
