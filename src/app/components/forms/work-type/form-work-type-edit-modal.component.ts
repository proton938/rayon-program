import {Component, Input, ViewChild} from "@angular/core";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import * as jsonpatch from 'fast-json-patch';
import {RayonWork} from "../../../models/work/RayonWork";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {fromJson, StateService, toJson, Transition} from "@uirouter/core";
import {copy} from 'angular';
import {ToastrService} from "ngx-toastr";
import {RayonWorkTypeService} from "../../../services/rayon-work-type.service";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {RayonWorkWrapper} from "../../../models/work/RayonWorkWrapper";
import {SessionStorage} from "@reinform-cdp/security";
import {FormWorkTypeComponent} from "./form-work-type.component";
import {Observable} from "rxjs/Rx";
import {from, of} from "rxjs/index";

@Component({
  selector: 'form-work-type-edit',
  templateUrl: './form-work-type-edit-modal.component.html',
  styleUrls: ['form-work-type-edit-modal.component.scss']
})
export class FormWorkTypeEditModalComponent {
  @Input() documentId;
  @Input() objectId;
  @Input() saveCallback: () => void;
  document: RayonWork;
  old: RayonWork;
  validate: boolean;
  itemCode: string;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  isLoading: boolean = true;
  isShowButtons: boolean = false;
  @BlockUI('edit-work-type') blockUI: NgBlockUI;
  @ViewChild(FormWorkTypeComponent)  workForm: FormWorkTypeComponent;

  constructor(private helper: RayonHelperService,
              private toastr: ToastrService,
              private workTypeService: RayonWorkTypeService,
              private session: SessionStorage,
              private bsModalRef: BsModalRef) {}

  ngOnInit() {
    let pageTitle = 'Вид работ';
    if (!this.documentId) {
      pageTitle = 'Новый вид работ';
      this.document = new RayonWork();
      this.isLoading = false;
      this.loadingStatus = LoadingStatus.SUCCESS;
    } else {
      pageTitle = 'Редактирование вида работ';
      this.workTypeService.init(this.documentId).subscribe(response => {
        this.document = response;
        this.old = copy(response);
        this.isLoading = false;
        this.loadingStatus = LoadingStatus.SUCCESS;
      });
    }
  }

  isValid(): boolean {
    let r: boolean = true;
    if (this.workForm.isValid) r = this.workForm.isValid();
    return r;
  }

  prepareData() {
    if (this.workForm.prepareData) this.workForm.prepareData();
  }

  afterSave(): Observable<any> {
    if (this.workForm.afterSave) return this.workForm.afterSave(this.document.documentId);
    return from(of());
  }

  save(action?: string) {
    if (this.isValid()) {
      this.blockUI.start();
      this.prepareData();
      let model = new RayonWork();
      model.build(fromJson(toJson(this.document)));
      let doc = model.min();
      if (!this.documentId) {
        this.workTypeService.create(doc)
          .subscribe((response: RayonWorkWrapper) => {
            if (response && response.work && response.work.documentId) {
              this.helper.success('Вид работ успешно создан!!');
              this.saveResult(response.work, action);
              this.blockUI.stop();
            }
          }, error => this.helper.error(error));
      } else {
        let diff: any[] = jsonpatch.compare({work: this.old.min()}, {work: doc});
        if (diff.length) {
          this.workTypeService.patch(this.documentId, diff).subscribe((response: RayonWorkWrapper) => {
            this.saveResult(response.work, action);
            this.blockUI.stop();
          });
        } else {
          // Нечего сохранять
          this.saveResult(this.document, action);
          this.blockUI.stop();
        }
      }
    } else {
      this.toastr.error('Не заполнены обязательные поля!');
    }
  }

  saveResult(doc: any, action?: string): void {
    switch(action) {
      case 'reload':
        this.old = new RayonWork();
        this.old.build(doc);
        this.document = new RayonWork();
        this.document.build(doc);
        this.documentId = this.document.documentId; // чтобы не создавалось 2 одинаковых документа
        break;

      default:
        this.afterSave().subscribe(() => {
          if (this.saveCallback) this.saveCallback();
          this.cancel();
        });
        break;
    }
  }

  cancel() {
    this.bsModalRef.hide();
  }

  showButtons() {
    this.isShowButtons = true;
  }
}
