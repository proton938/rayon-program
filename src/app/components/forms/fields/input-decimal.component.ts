import {Component, Input} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'input-decimal',
  templateUrl: './input-decimal.component.html'
})
export class InputDecimalComponent {
  @Input() model: string;
  // @Input() name: string;

  constructor(public helper: RayonHelperService) {}

  maskNumber(t: string) {
    return this.helper.maskDecimal(t);
  }
}
