import {Component, Input} from "@angular/core";
import {RayonProgramFinance} from "../../../models/program/RayonProgramFinance";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'finance-form',
  templateUrl: './finance-form.component.html'
})
export class FinanceFormComponent {
  @Input() model: RayonProgramFinance;
  @Input() dictionaries: any;

  constructor(private helper: RayonHelperService) {}

  maskDecimal(text): any[] {
    return this.helper.maskDecimal(text);
  }
}
