import {Component} from '@angular/core';
import {SessionStorage} from '@reinform-cdp/security';
import {find, isEmpty} from 'lodash';
import {RayonProgramResourceService} from "../../../services/rayon-program-resource.service";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {RayonHelperService} from '../../../services/rayon-helper.service';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Component({
  selector: 'form-public-mos',
  templateUrl: './public-mos.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class PublicMosComponent {
  loading: boolean = true;
  isShow: any = {
    form: false
  };
  selected: any = {
    ids: ''
  };

  @BlockUI('form-block-ui') blockUI: NgBlockUI;

  constructor(private session: SessionStorage,
              private helper: RayonHelperService,
              private location: Location,
              private service: RayonProgramResourceService) {}

  ngOnInit() {
    this.isShow.form = !!find(this.session.groups(), i => i === 'MR_OBJECT_PUBLIC_MOS');
    this.updateBreadcrumbs();
    this.loading = false;
  }

  updateBreadcrumbs() {
    this.helper.setBreadcrumbs([
      {
        title: 'Публикация на Mos.ru',
        url: null
      }
    ]);
  }

  ok() {
    this.blockUI.start();
    let data: any = this.requestData;
    if (!isEmpty(data)) {
      this.service.changeNotifyDocs(data).subscribe(res => {
        this.helper.success('Данные направлены в Mos.ru');
        this.blockUI.stop();
        this.cancel();
      }, error => {
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.helper.error('Нет данных для отправки');
      this.blockUI.stop();
    }
  }

  cancel() {
    this.location.back();
  }

  get requestData(): any {
    let r: any = {};
    let idsRow: string = this.selected.ids.trim();
    let ids = idsRow ? idsRow.split(/[^a-z0-9\-]+/) : [];
    if (ids.length) {
      ids.forEach(i => r[i] = 'OBJ');
    }
    return r;
  }
}
