import {Component, Input} from "@angular/core";
import { BsModalRef } from 'ngx-bootstrap/modal';
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'form-rename-file-modal',
  templateUrl: './form-rename-file-modal.component.html'
})
export class FormRenameFileModalComponent {
  @Input() file: any;
  @Input() saveCallback: (result: string) => void;

  newName: string = '';

  @BlockUI('modal-block-ui') blockUI: NgBlockUI;


  constructor(private bsModalRef: BsModalRef,
              public helper: RayonHelperService) {}

  ngOnInit() {
    this.newName = this.file.oldName.substring(0, this.file.oldName.lastIndexOf('.')) + '(' + this.file.numberOfFile + ')';
  }

  rename() {
    if (this.saveCallback) this.saveCallback(this.newName);
    this.cancel();
  }

  cancel() {
    this.bsModalRef.hide();
  }
}
