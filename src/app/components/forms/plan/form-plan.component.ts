import {Component, EventEmitter, Input, Output} from "@angular/core";
import {RayonPlan} from "../../../models/plan/RayonPlan";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {RayonHelperService} from "../../../services/rayon-helper.service";
import {SessionStorage} from "@reinform-cdp/security";
import {forkJoin} from "rxjs/index";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {RayonNsiColor} from "../../../models/common/RayonNsiColor";
import {find} from 'lodash';
import {NSIDocumentType} from "@reinform-cdp/core";

@Component({
  selector: 'program-form-plan',
  templateUrl: './form-plan.component.html'
})
export class FormPlanComponent {
  @Input() model: RayonPlan;
  @Input() validate: boolean;
  @Input() files: any;
  @Input() isCopy: boolean = false;

  @Output() onLoad = new EventEmitter<void>();

  documentId: string;
  dictionaries: any = {};
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  selected: any = {};

  constructor(private nsi: NsiResourceService,
              private helper: RayonHelperService,
              private session: SessionStorage,
              private fileResourceService: FileResourceService) {}

  ngOnInit() {

    if (this.model) {
      this.documentId = this.model.documentId;
    }

    const dictionaries$ = this.nsi.getDictsFromCache([
      'Prefect',
      'mr_program_planStatus'
    ]);

    forkJoin([dictionaries$]).subscribe(res => {
      this.dictionaries = res[0];
      this.dictionaries.prefects = this.dictionaries.Prefect.map(i => {
        let r = new NSIDocumentType();
        r.build(i);
        return r;
      });
      this.dictionaries.completeYears = ((startDate, steps) => {
        let r = [startDate - steps];
        while(r.length <= steps * 2) {
          r.push(r[r.length-1] + 1);
        }
        return r;
      })(new Date().getFullYear(), 10).map(i => {
        return {code: i};
      });
      this.dictionaries.statuses = this.dictionaries['mr_program_planStatus'].map(i => {
        let r = new RayonNsiColor();
        r.build(i);
        return r;
      });
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess(): void {
    if (!this.documentId) {
      this.model.statusId = find(this.dictionaries.statuses, i => i.code === 'project');
    }
    this.loadingStatus = LoadingStatus.SUCCESS;
    this.onLoad.emit();
  }

  isLoaded(): boolean {
    return this.loadingStatus === LoadingStatus.SUCCESS;
  }

  isDisabledPrefect(): boolean {
    let r: boolean = !!this.documentId || this.isCopy;
    if (!r) r = this.model.statusId && (this.model.statusId.code === 'archive' || this.model.statusId.code === 'active');
    return r;
  }

  onRemoveFile(file) {
    if (file && file.idFile) {
      this.fileResourceService.deleteFile(file.idFile);
    }
  }
}
