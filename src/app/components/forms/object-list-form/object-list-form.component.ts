import {Component, OnInit, Input, TemplateRef} from '@angular/core';
import {AlertService, LoadingStatus} from '@reinform-cdp/widgets';
import {ToastrService} from 'ngx-toastr';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {RayonProgramResourceService} from '../../../services/rayon-program-resource.service';
import {ShowcaseService} from '../../showcase/showcase.service';
import {RayonNsiService} from '../../../services/rayon-nsi.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {FormObjectComponent} from "../../form-object/form-object.component";
import {StateService} from "@uirouter/core";
import {IRayonRequest} from "../../../models/request/abstracts/IRayonRequest";
import {first} from 'lodash';
import {SolrMediatorService} from "../../../services/solr-mediator.service";

@Component({
  selector: 'object-list-form',
  templateUrl: './object-list-form.component.html',
  styleUrls: ['./object-list-form.component.scss']
})
export class ObjectListFormComponent implements OnInit {
  loading: LoadingStatus = LoadingStatus.LOADING;
  @BlockUI('object-list-form') blockUI: NgBlockUI;
  @Input() request: IRayonRequest;
  @Input() readOnly: boolean;
  // @Input() requestId: string;
  dicts: any;
  docs: any[];
  constructor(private programService: RayonProgramResourceService,
              private showcaseService: ShowcaseService,
              private solrMediator: SolrMediatorService,
              private rayonNsiService: RayonNsiService,
              private toastrService: ToastrService,
              private stateService: StateService,
              private alertService: AlertService,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.rayonNsiService.getDicts().subscribe(response => {
      this.dicts = response;
      this.solrMediator.query({
        page: 0,
        pageSize: 1000,
        types: [this.solrMediator.types.obj],
        query: `(districtsCodeObject:${this.request.district.code}) AND (industryCodeObject:${this.request.industry.code})`,
        sort: 'requestIdObject asc'
      }).subscribe(response => {
        response.subscribe(result => {
          this.docs = result.docs;
          console.log(this.docs);
        });
      });
      this.loading = LoadingStatus.SUCCESS;
    }, error => {
      console.log(error);
      this.loading = LoadingStatus.ERROR;
    });
    /*this.modalService.onHide.subscribe(r => {
      console.log(r);
      this.stateService.reload();
    });*/
  }

  add() {
    let modalRef = this.modalService.show(FormObjectComponent, {
      class: 'modal-lg',
      initialState: {
        requestId: this.request.documentId,
        district: this.request.district,
        industry: this.request.industry,
        organization: this.request.organization,
        saveCallback: () => {
          this.stateService.reload(this.stateService.current);
        }
      }
    });
  }

  edit(i) {
    let modalRef = this.modalService.show(FormObjectComponent, {
      class: 'modal-lg',
      initialState: {
        documentId: this.docs[i].documentId,
        requestId: this.request.documentId,
        district: this.request.district,
        industry: this.request.industry,
        organization: this.request.organization,
        saveCallback: () => {
          this.stateService.reload(this.stateService.current);
        }
      }
    });
  }

  remove(i) {
    this.alertService.confirm({
      okButtonText: 'Удалить',
      message: 'Удалить выбранный объект',
      type: 'danger',
      size: 'md'
    }).then(response => {
      this.programService.delete(this.docs[i].documentId).subscribe(response => {
        this.docs.splice(i, 1);
      });
    });
  }
}
