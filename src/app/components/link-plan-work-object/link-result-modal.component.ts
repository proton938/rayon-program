import {Component} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {ResultMsg} from './link-plan-work-object.component';

@Component({
  selector: 'link-result-modal',
  templateUrl: './link-result-modal.component.html',
  styleUrls: ['./link-result-modal.component.scss']
})
export class LinkResultModalComponent {

  public resultMsg: ResultMsg;

  constructor(private bsModalRef: BsModalRef) {
  }

  ok() {
    this.bsModalRef.hide();
  }

}
