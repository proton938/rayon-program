import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {interval, Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {PlanResourceService} from '../../services/plan-resource.service';
import {ProcessInfo, ProcessResourceService} from '../../services/process-resource.service';
import {filter, first, mergeMap} from 'rxjs/operators';
import {BsModalService} from 'ngx-bootstrap';
import {LinkResultModalComponent} from './link-result-modal.component';
import {Observable} from 'rxjs/Rx';
import {RayonHelperService} from '../../services/rayon-helper.service';

@Component({
  selector: 'link-plan-work-object',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './link-plan-work-object.component.html',
  styleUrls: ['./link-plan-work-object.component.scss']
})
export class LinkPlanWorkObjectComponent implements OnInit, OnDestroy {
  @BlockUI('link-plan-work-object') blockUI: NgBlockUI;

  planNumber: string;
  workIds: string;
  isLinked: boolean = true;

  subscription: Subscription = new Subscription();

  constructor(private location: Location,
              private helper: RayonHelperService,
              private planResourceService: PlanResourceService,
              private processResourceService: ProcessResourceService,
              private modalService: BsModalService,
              private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.updateBreadcrumbs();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  linkPlan() {
    if (!/^([0-9a-zA-Z\-]*\n?)*$/g.test(this.workIds)) {
      this.toastrService.error("Идентификаторы работ должны быть перечислены с новой строки, запятая, пробел и другие символы не могут быть использованы в качестве разделителя.");
      return;
    }
    this.blockUI.start();
    let ids = this.workIds.split("\n").map(id => id.trim());
    let sub = this.planResourceService.linkPlanWithWorkObjectByIds(this.isLinked, this.planNumber, ids).pipe(
      mergeMap(processId => {
        return interval(5000).pipe(
          mergeMap(() => {
            return this.processResourceService.getInfo(processId);
          })
        );
      }),
      filter(info => info.status !== 'RUNNING'),
      first(),
      mergeMap((info) => {
        this.blockUI.stop();
        return this.showLinkResultModal(info, ids);
      })
    ).subscribe(() => {
      this.location.back();
    }, (error) => {
      console.log(error);
      this.blockUI.stop();
    });
    this.subscription.add(sub);
  }

  cancel() {
    this.location.back();
  }

  showLinkResultModal(info: ProcessInfo, workIds: string[]): Observable<any> {
    let ref = this.modalService.show(LinkResultModalComponent, {
      class: 'modal-lg',
      initialState: {
        resultMsg: new ResultMsg(info.resultMsg, workIds),
        status: info.status
      }
    });
    return new Observable(subscriber => {
      let subscr = this.modalService.onHide.subscribe(() => {
        subscriber.next();
        subscr.unsubscribe();
      }, () => {
        subscr.unsubscribe();
      })
    });
  }

  updateBreadcrumbs() {
    this.helper.setBreadcrumbs([
      {
        title: 'Привязать / отвязать объекты и работы к мастер-плану',
        url: null
      }
    ]);
  }

}

export class ResultMsg {
  start?: string;
  end?: string;
  total?: ResultMsgTotal = new ResultMsgTotal();

  constructor(resultMsg: string, workIds: string[]) {
    let items = resultMsg.split("\n\n").map(item => item.trim());
    this.process(items, workIds);
  }

  process(items: string[], workIds: string[]) {
    items.filter(i => i.startsWith("Start: ")).forEach(i =>
      this.start = i.substring("Start: ".length)
    );
    items.filter(i => i.startsWith("End: ")).forEach(i =>
      this.end = i.substring("End: ".length)
    );
    items.filter(i => i.startsWith("Total: ")).forEach(i => {
      let totalItems = i.substring("Total: ".length).split("\n");
      this.total.process(totalItems, workIds);
    });
  }
}

export class ResultMsgTotal {
  num?: string;
  error?: string;
  work: ResultMsgTotalWork[] = [];

  process(items: string[], workIds: string[]) {
    this.num = items[0];
    let messages = items.slice(1);
    messages.filter(i => i.startsWith("Error: ")).forEach(i =>
      this.error = i.substring("Error: ".length)
    );
    messages.forEach(i => {
      workIds.filter(id => new RegExp(`.*${id}.*`).test(i)).forEach(id => {
        this.work.push(new ResultMsgTotalWork(id, i));
      })
    });
  }
}

export class ResultMsgTotalWork {
  id?: string;
  message?: string;

  constructor(id: string, message: string) {
    this.id = id;
    this.message = message;
  }
}
