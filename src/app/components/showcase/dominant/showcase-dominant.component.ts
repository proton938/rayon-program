import {Component} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'program-showcase-dominant',
  templateUrl: './showcase-dominant.component.html'
})
export class ProgramShowcaseDominantComponent {
  config: any = {
    system: 'MR',
    subsystem: 'MR_PROGRAM',
    document: 'OBJECT',
    showcase: 'DOMINANT',
    filters: [
      {code: 'dominantObject', value: true},
      {code: 'archiveObject', value: false},
      {code: 'commercialObject', value: false}
    ]
  };
  constructor(public helper: RayonHelperService) {
    helper.setBreadcrumbs([
      {title: 'Уникальные объекты', url: null}
    ]);
  }
}
