import {Component, Inject, OnInit} from "@angular/core";
import {copy, fromJson, toJson} from "angular";
import {StateService, Transition} from "@uirouter/core";
import {SolarResourceService} from '@reinform-cdp/search-resource';
import {ShowcaseService} from "./showcase.service";
import {ISortingBuilder} from "../../models/showcase/sorting/ISortingBuilder";
import {RayonProgramSortingBuilder} from "../../models/showcase/sorting/builders/RayonProgramSortingBuilder";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {from, forkJoin} from 'rxjs/index';
import {RayonProgramFilter} from "../../models/showcase/filters/RayonProgramFilter";
import {SessionStorage} from "@reinform-cdp/security";
import {map, filter, isEmpty, forIn, some, compact, find, toString} from 'lodash';
import {RayonHelperService} from "../../services/rayon-helper.service";

@Component({
  selector: 'program-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.scss']
})
export class RayonProgramShowcaseComponent implements OnInit {
  isLoading: boolean = true;
  commonSearch: string = '';
  filter: RayonProgramFilter = new RayonProgramFilter();
  prevFilter: RayonProgramFilter = new RayonProgramFilter();
  list: any = {};
  docs: any[] = [];
  dataIsUpdating = true;
  sortings: any[] = [];
  block = false;
  advancedSearchExpand: boolean = false;
  showCreateBtn: boolean = false;
  pagination: any = {
    currentPage: 1,
    itemsPerPage: 20,
    totalItems: 0,
    disable: false
  };
  columnStorageName: string = 'mr_program_columns_all_objects_showcase';
  filterStorageName: string = 'mr_program_filters_all_objects_showcase';
  pageParams: any = {};
  ru: any = {
    showcaseCaption: 'Объекты и мероприятия'
  };
  cardStateName: string = 'app.object.card.main';

  constructor(public stateService: StateService,
              private transition: Transition,
              private helper: RayonHelperService,
              private showcaseService: ShowcaseService,
              private nsi: NsiResourceService,
              private session: SessionStorage) {
    this.filterStorageName = 'mr_program_filters_' + stateService.current.name;
    this.pageParams = transition.params();
    this.initSortings();
    this.initFilters();
    this.initLocals();
  }

  ngOnInit() {
    let dictionaries$ = from(this.nsi.getDictsFromCache([
      'Prefect',
      'District',
      'mr_program_ObjectCategories',
      'mr_program_ResponsibleOrganizations',
      'mr_program_WorkStatuses',
      'mr_program_ObjectForms',
      'mr_program_ItemTypes',
      'mr_program_ObjectTypes',
      'mr_program_OwnershipForms',
      'mr_program_ConstructionPrograms',
      'mr_program_ConstructionPeriods',
      'mr_program_ObjectIndustries',
      'mr_program_ObjectKinds']));
    let users$ = this.nsi.ldapUsers();

    this.updateBreadcrumbs();
    this.showCreateBtn = this.session.hasPermission('objectAdd');

    forkJoin([dictionaries$, users$]).subscribe(response => {
      this.list = response[0];
      this.list.users = response[1].filter(i => !!i.displayName);
      this.updateAvailableLists();
      this.makeObjectTypeList();
      this.updateData(true);
      this.isLoading = false;
    }, error => {
      console.log(error);
      this.isLoading = false;
    });
  }

  updateAvailableLists() {
    this.updateDistrictList();
    this.updateKindList();
  }

  updateDistrictList() {
    this.list.availableDistricts = [];
    if (this.filter.prefectsNameObject && this.filter.prefectsNameObject.length) {
      this.list.availableDistricts = this.list.District
        .filter(d => this.filter.prefectsNameObject.filter(p => p === d.perfectId[0].name).length);
      if (this.filter.districtsNameObject && this.filter.districtsNameObject.length) {
        this.filter.districtsNameObject = this.filter.districtsNameObject
          .filter(i => (<any[]>this.list.availableDistricts).filter(f => f.name === i).length);
      }
    } else {
      this.list.availableDistricts = this.list.District;
    }
  }

  updateKindList() {
    let dic: any[] = this.list.mr_program_ObjectCategories;
    this.list.availableKinds = dic
      .filter(d => {
        return this.filter.industryNameObject && this.filter.industryNameObject.length
          ? this.filter.industryNameObject.filter(p => some(d.industry, ind => ind.name === p)).length
          : true;
      })
      .filter(d => {
        let selectedItemTypeCodes: string[] = this.filter.itemTypeNameObject
          .map(p => find(this.list.mr_program_ItemTypes, i => i.name === p).code);
        return this.filter.itemTypeNameObject && this.filter.itemTypeNameObject.length
          ? (selectedItemTypeCodes.length === 1 ?  d.object === (selectedItemTypeCodes.join('') === 'object') : true)
          : true;
      });

    if (this.filter.kindNameObject && this.filter.kindNameObject.length) {
      this.filter.kindNameObject = this.filter.kindNameObject
        .filter(i => (<any[]>this.list.availableKinds).filter(f => f.name === i).length);
    }
  }

  initSortings() {
    const sortingBuilder: ISortingBuilder = new RayonProgramSortingBuilder();
    this.sortings = sortingBuilder.build();
    if (this.columnStorageName) {
      let ls = JSON.parse(localStorage.getItem(this.columnStorageName) || '[]');
      if (ls.length) {
        this.sortings.forEach(i => {
          i.show = !!~ls.indexOf(i.code);
        });
      }
    }
  }

  updateLocalSortings() {
    if (this.columnStorageName) {
      localStorage.setItem(this.columnStorageName, JSON.stringify(map(filter(this.sortings, i => i.show), i => i.code)));
    }
  }

  makeObjectTypeList() {
    this.list.objectTypes = [];
    this.list['mr_program_ObjectTypes'].forEach(industry => {
      let currentIndustry = {code: industry.code, name: industry.name};
      if (industry.children && industry.children.length) {
        industry.children.forEach(i => {
          this.list.objectTypes.push({
            code: i.code,
            name: i.name,
            parent: currentIndustry,
            level: 1
          });
        });
      }
    });
  }

  onCommonSearchChange(value) {
    this.commonSearch = value;
    this.pagination.currentPage = 1;
    this.updateData(false);
  }

  filterChanged() {
    this.updateAvailableLists();
    if ((this.filter.toString() !== this.prevFilter.toString())) {
      this.prevFilter = copy(this.filter);
      this.pagination.currentPage = 1;
      this.updateData(false);
    }
  }

  filterCleared() {
    this.filter.clear();
    this.updateAvailableLists();
    if ((this.filter.toString() !== this.prevFilter.toString()) || this.commonSearch.trim() !== '') {
      this.commonSearch = '';
      this.pagination.currentPage = 1;
      this.prevFilter = copy(this.filter);
      this.updateData(false);
    }
  }

  initFilters() {
    if (this.filterStorageName) {
      let ls = fromJson(localStorage.getItem(this.filterStorageName) || '{}');
      if (!isEmpty(ls)) {
        forIn(ls, (item, key) => {
          this.filter[key] = item;
        });
        this.prevFilter = copy(this.filter);
      }
    }
  }

  updateFilterStorage() {
    if (this.filterStorageName) {
      if (!this.filter.isEmpty()) {
        localStorage.setItem(this.filterStorageName, toJson(this.filter));
      } else {
        localStorage.removeItem(this.filterStorageName);
      }
    }
  }

  buildFilter(): string {
    let result = '';

    /*if (this.commonSearch) {
      result += result ? ' AND ' : '';
      result += this.filter.toCommon(this.commonSearch);
    }*/

    this.filter.dominantObject = toString(this.pageParams.type === 'dominant');
    this.filter.commercialObject = toString(this.pageParams.type === 'commercial');

    //if (!this.filter.isEmpty()) {
      const _filter = this.filter.toString();
      result += result ? ' AND ' : '';
      result += _filter;
    //}

    this.updateFilterStorage();

    return result;
  }

  create() {
    let stateName: string = 'app.object.new';
    switch(this.pageParams.type) {
      case 'dominant':
        stateName = 'app.objectDominant.new';
        break;
      default:
        stateName = 'app.object.new';
        break;
    }
    this.stateService.go(stateName);
  }

  changePage($event) {
    this.pagination.currentPage = $event.page;
    this.updateData(false);
  }

  updateData(loading: boolean) {
    if (loading) this.dataIsUpdating = true;
    else this.block = true;

    const filter = this.buildFilter().trim() || undefined;
    this.showcaseService.query({
      common: this.commonSearch,
      query: filter,
      page: this.pagination.currentPage - 1,
      pageSize: 20,
      sort: compact(['sampleObject desc', this.sortingsToString()]).join(','),
      types: ['OBJECT']
    }).subscribe(response => {
      response.subscribe(result => {
        this.docs = result.docs;
        this.pagination.totalItems = result.numFound;
        this.pagination.itemsPerPage = result.pageSize;
        this.pagination.currentPage = result.start / result.pageSize + 1;
        if (loading) this.dataIsUpdating = false;
        else this.block = false;
      });
    });
  }

  sortingsToString(): string {
    let result = '';

    this.sortings.filter(i => i.value).forEach(s => {
      if (s.value !== 'none') {
        result = `${s.code} ${s.value}`;
      }
    });

    return result;
  }

  sotringChanged(s) {
    if (s && s.value) {
      this.sortings.filter(i => i.value).forEach(_s => {
        if (s !== _s) _s.value = 'none';
        else _s.value = (_s.value === 'none') ? 'desc' : ((_s.value === 'desc') ? 'asc' : 'desc');
      });
      this.updateData(false);
    }

  }

  updateBreadcrumbs() {
    this.helper.setBreadcrumbs([
      { title: this.ru.showcaseCaption, url: null }
    ]);
  }

  getSortings(): any {
    return this.sortings.filter(i => i.show);
  }

  initLocals() {
    if (this.pageParams.type === 'dominant') {
      this.ru.showcaseCaption = 'Уникальные объекты';
      this.cardStateName = 'app.objectDominant.card.main'
    }
  }
}
