import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'rayon-program-common-search-showcase',
  templateUrl: './showcase-common-search.component.html'
})
export class RayonProgramCommonSearchShowcaseComponent {
  @Input() value: string;
  @Output() valueChange = new EventEmitter();
  @Output() valueChanged = new EventEmitter();

  searchChanged() {
    this.valueChanged.emit(this.value);
  }
}
