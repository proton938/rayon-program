import {Component, Input, Output, EventEmitter} from '@angular/core';
import {RayonProgramFilter} from "../../models/showcase/filters/RayonProgramFilter";
import {NSIDocumentType} from "@reinform-cdp/core";

@Component({
  selector: 'rayon-program-showcase-filter',
  templateUrl: './showcase-filter.component.html'
})
export class RayonProgramShowcaseFilterComponent {
  incList: any[] = []; // Включение в программу
  budgetList: any[] = []; // Финансирование в бюджете
  overhaulList: any[] = []; // Капитальный ремонт
  cleaning: boolean = false;

  @Input() filter: RayonProgramFilter;
  @Input() list: any;
  @Input() expanded: boolean;
  @Output() filterChange: RayonProgramFilter;
  @Output() filterChanged = new EventEmitter();
  @Output() filterCleared = new EventEmitter();

  ngOnInit() {
    this.updateLists();
  }

  onFilterChange() {
    this.filterChanged.emit();
  }

  clearFilters() {
    this.cleaning = true;
    this.filterCleared.emit();
    this.updateLists();
    setTimeout(() => {this.cleaning = false}, 0);
  }

  updateLists() {
    this.incList = [
      {name: 'Включен', code: 'true'},
      {name: 'Не включен', code: 'false'}
    ];
    this.budgetList = [
      {name: 'Предусмотрено', code: 'true'},
      {name: 'Не предусмотрено', code: 'false'}
    ];
    this.overhaulList = [
      {name: 'Есть', code: 'true'},
      {name: 'Будет', code: 'false'}
    ];
    if (this.list.mr_program_ConstructionPeriods) {
      this.list.mr_program_ConstructionPeriods = JSON.parse(JSON.stringify(this.list.mr_program_ConstructionPeriods));
    }
  }

  groupByObjectType(item) {
    return item.parent.name;
  }
}
