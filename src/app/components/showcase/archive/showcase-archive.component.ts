import {Component} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'program-showcase-archive',
  templateUrl: './showcase-archive.component.html'
})
export class ProgramShowcaseArchiveComponent {
  config: any = {
    system: 'MR',
    subsystem: 'MR_PROGRAM',
    document: 'OBJECT',
    showcase: 'ARCHIVE',
    filters: [
      {code: 'archiveObject', value: true}
    ]
  };
  constructor(public helper: RayonHelperService) {
    helper.setBreadcrumbs([
      {title: 'Архивные объекты и мероприятия', url: null}
    ]);
  }
}
