import {Component} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'program-showcase-objects',
  templateUrl: './showcase-objects.component.html'
})
export class ProgramShowcaseObjectsComponent {
  config: any = {
    system: 'MR',
    subsystem: 'MR_PROGRAM',
    document: 'OBJECT',
    showcase: 'OBJECT',
    filters: [
      {code: 'archiveObject', value: false},
      {code: 'itemTypeCodeObject', value: 'object'},
      {code: 'commercialObject', value: false},
      {code: 'dominantObject', value: false}
    ]
  };
  constructor(public helper: RayonHelperService) {
    helper.setBreadcrumbs([
      {title: 'Объекты', url: null}
    ]);
  }
}
