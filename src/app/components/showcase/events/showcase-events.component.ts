import {Component} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'program-showcase-events',
  templateUrl: './showcase-events.component.html'
})
export class ProgramShowcaseEventsComponent {
  config: any = {
    system: 'MR',
    subsystem: 'MR_PROGRAM',
    document: 'OBJECT',
    showcase: 'EVENT',
    filters: [
      {code: 'archiveObject', value: false},
      {code: 'itemTypeCodeObject', value: 'event'}
    ]
  };
  constructor(public helper: RayonHelperService) {
    helper.setBreadcrumbs([
      {title: 'Мероприятия', url: null}
    ]);
  }
}
