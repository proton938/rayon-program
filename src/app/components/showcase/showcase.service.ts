import {Injectable} from '@angular/core';
import {SolarResourceService} from "@reinform-cdp/search-resource";
import {IQueryRequestParams} from "../../models/showcase/IQueryRequestParams";
import {Observable} from "rxjs";
import {forkJoin, from, of} from 'rxjs/index';
import {map} from "rxjs/internal/operators";
import {QueryResponseDataAsync} from "../../models/showcase/QueryResponseDataAsync";
import {QueryResponseData} from "../../models/showcase/QueryResponseData";
import {SolrDocumentBuilder} from "../../models/showcase/builders/SolrDocumentBuilder";

@Injectable()
export class ShowcaseService {
  constructor(private solarResourceService: SolarResourceService,
              private solrDocumentBuilder: SolrDocumentBuilder) {}

  query(params: IQueryRequestParams): Observable<any> {
    return from(this.solarResourceService.query(params)).pipe(
      map(_response => {
        const response = new QueryResponseDataAsync(_response, this.solrDocumentBuilder);
        if (response.docs && response.docs.length > 0) {
          return forkJoin(response.docs).pipe(map((result: any) => {
            return new QueryResponseData(response.start, response.pageSize, response.numFound, result);
          }));
        } else {
          return of([]).pipe(map((result: any) => {
            return new QueryResponseData(response.start, response.pageSize, response.numFound, result);
          }));
        }
      })
    );
  }
}
