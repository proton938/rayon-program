import {Component} from "@angular/core";
import {RayonHelperService} from "../../../services/rayon-helper.service";

@Component({
  selector: 'program-showcase-commercial',
  templateUrl: './showcase-commercial.component.html'
})
export class ProgramShowcaseCommercialComponent {
  config: any = {
    system: 'MR',
    subsystem: 'MR_PROGRAM',
    document: 'OBJECT',
    showcase: 'COMMERCIAL',
    filters: [
      {code: 'commercialObject', value: true},
      {code: 'archiveObject', value: false}
    ]
  };
  constructor(public helper: RayonHelperService) {
    helper.setBreadcrumbs([
      {title: 'Коммерческие объекты', url: null}
    ]);
  }
}
