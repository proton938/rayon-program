import {Component, OnInit, Input, Inject} from '@angular/core';
import {StateService, Transition} from '@uirouter/core';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {AlertService, ExFileType, LoadingStatus} from "@reinform-cdp/widgets";
import {IRootScopeService, copy} from "angular";
import {RayonProgramResourceService} from "../../services/rayon-program-resource.service";
import {RayonProgram} from "../../models/program/RayonProgram";
import * as jsonpatch from 'fast-json-patch';
import {forkJoin, from} from 'rxjs/index';
import {RayonHelperService} from "../../services/rayon-helper.service";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {first, some, filter, find} from 'lodash';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {NSIDocumentType} from "@reinform-cdp/core";

@Component({
  selector: 'form-object',
  templateUrl: './form-object.component.html',
  styleUrls: ['./form-object.component.scss']
})
export class FormObjectComponent implements OnInit {
  @Input() documentId: string;
  @Input() requestId: string;
  @Input() district: NSIDocumentType;
  @Input() industry: NSIDocumentType;
  @Input() organization: NSIDocumentType;
  @Input() saveCallback: () => void;

  document: RayonProgram;
  old: RayonProgram;
  validate: boolean = false;
  saving: boolean = false;
  dictionaries: any = {};
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  districtsAvailables: any[] = [];
  files = {
    statusquo: [],
    proposedproject: []
  };
  executors: any[] = [];
  executorsAvailables: any[] = [];
  selectedInstruction: any = null;
  objectTypes: any[] = [];
  selectedObjectType: any = null;
  selectedPeriod: string = null;

  constructor(private stateService: StateService,
              private nsi: NsiResourceService,
              private programService: RayonProgramResourceService,
              private helper: RayonHelperService,
              private fileResourceService: FileResourceService,
              private alertService: AlertService,
              @Inject('$rootScope') private rootScope: IRootScopeService,
              private bsModalRef: BsModalRef) {

  }

  ngOnInit() {
    // this.updateBreadcrumbs();

    const dictionaries$ = this.nsi.getDictsFromCache([
      'District',
      'mr_program_BasesInclusion',
      'mr_program_ConstructionPeriods',
      'mr_program_ConstructionPrograms',
      'mr_program_ObjectCategories',
      'mr_program_ObjectKinds',
      'mr_program_ObjectTypes',
      'mr_program_OwnershipForms',
      'mr_program_ResponsibleOrganizations',
      'mr_program_StatePrograms',
      'mr_program_Teps',
      'mr_program_WorkStatuses',
      'mr_program_WorkTypes',
      'Prefect']);
    const document$ = this.programService.init(this.documentId);

    forkJoin([dictionaries$, document$]).subscribe((response: any) => {
      this.dictionaries = response[0];
      this.document = response[1];
      this.old = this.helper.convertDocToProgram(response[1]);
      this.initSuccess();
    }, error => this.helper.error(error));
  }

  initSuccess(): void {
    this.dictionaries.yearsPsd = this.dictionaries.yearsSmr = this.getYearList();
    // this.updatePsdYears();
    // this.updateSmrYears();
    this.dictionaries.budget = [
      {code: true, name: 'Предусмотрено'},
      {code: false, name: 'Не предусмотрено'},
      {code: null, name: 'Отсутствует'}
    ];
    this.dictionaries.overhaul = [
      {code: true, name: 'Есть'},
      {code: false, name: 'Будет'},
      {code: null, name: 'Отсутствует'}
    ];
    this.getInstructions();
    if (this.district) {
      this.addDistrictFromInput();
    }
    if (this.industry) this.document.object.industry = this.helper.convertToNsi(<any>this.industry);
    this.updateDistrictList();
    this.getFiles();
    this.getExecutorList();
    this.makeObjectTypeList();

    if (this.document.object.kind) {
      this.selectedObjectType = first(this.objectTypes.filter(i => i.code === this.document.object.kind.code));
    }

    if (this.document.object.period) {
      this.selectedPeriod = this.document.object.period.code;
    }

    this.loadingStatus = LoadingStatus.SUCCESS;
  }

  uploadFileAlert() {
    this.alertService.confirm({
      message: 'Чтобы загрузить файл(ы) необходимо сначала сохранить форму!',
      okButtonText: 'Сохранить',
      type: 'warning',
      size: 'lg'
    }).then(response => {
      this.save('reload');
    }).catch(error => {
      console.log(error);
    });
  }

  groupByObjectType(item) {
    return item.parent.name;
  }

  makeObjectTypeList() {
    this.objectTypes = [];
    if (this.industry) {
      this.objectTypes = filter(this.dictionaries['mr_program_ObjectCategories'], p => {
        return find(p.industry, i => {
          return i.code === this.industry.code;
        });
      });
    }
  }

  onChangeCategory() {
    this.document.object.kind = this.selectedObjectType ? this.helper.convertToNsi(this.selectedObjectType) : null;
  }

  onChancePrefect() {
    this.updateDistrictList();
  }

  onChangeRespOrg() {
    this.updateExecutorList();
  }

  onChangeInclusion() {
    let inclusion = this.document.object.programm.inclusion;
    // if (!inclusion) this.document.object.programm.basis = null;
  }

  onChangePeriod(period) {
    this.document.object.period = period ? this.helper.convertToNsi(period) : null;
  }

  addDistrictFromInput() {
    if (!this.document.address.district.filter(d => d.code === this.district.code).length) {
      this.document.address.district.push(this.helper.convertToNsi(this.district));
      let prefect = (<any>first(this.dictionaries.District.filter(d => d.code === this.district.code))).perfectId[0];
      if (!this.document.address.prefect.filter(p => p.code === prefect.code).length) {
        this.document.address.prefect.push(this.helper.convertToNsi(prefect));
      }
    }
  }

  updateDistrictList() {
    this.districtsAvailables = [];
    if (this.document.address.prefect && this.document.address.prefect.length) {
      this.districtsAvailables = this.dictionaries.District
        .filter(d => this.document.address.prefect.filter(p => p.code === d.perfectId[0].code).length);
      if (this.document.address.district && this.document.address.district.length) {
        this.document.address.district = this.document.address.district
          .filter(i => this.districtsAvailables.filter(f => f.code === i.code).length);
      }
    } else {
      this.districtsAvailables = this.dictionaries.District;
    }
  }

  getInstructions() {
    // TODO Заполнение списка документов типа INSTRUCTION из SOLR для поля "Связанное поручение"
    // this.dictionaries.instructions
  }

  getFiles() {
    if (this.document && this.document.folderId) {
      from(this.fileResourceService.getFolderContent(this.document.folderId, false)).subscribe(response => {
        if (response) {
          response.forEach(file => {
            if (file && file.fileType && this.files[file.fileType]) {
              this.files[file.fileType].push(ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
                false, file.dateCreated, file.fileType, file.mimeType));
            }
          });
        }
      });
    }
  }

  getExecutorList() {
    this.helper.findLdapUsers({
      department: this.dictionaries.mr_program_ResponsibleOrganizations.map(i => i.code)
    }).subscribe(response => {
      this.executors = response;
      this.updateExecutorList();
    });
  }
  updateExecutorList() {
    this.executorsAvailables = (this.document.responsible.organization
      ? this.executors.filter(i => i.departmentCode === this.document.responsible.organization.code)
      : this.executors).map(ex => {
      return {
        login: ex.accountName,
        fio: ex.displayName,
        post: ex.post
      };
    });
  }

  onRemoveFile(file) {
    this.fileResourceService.deleteFile(file.idFile);
  }

  // updatePsdYears() {
  //   if (this.document.object.year.psd.start) {
  //     this.dictionaries.yearsPsd2 = this.dictionaries.yearsPsd.filter(i => i >= this.document.object.year.psd.start);
  //     if (this.document.object.year.psd.finish < this.document.object.year.psd.start) this.document.object.year.psd.finish = null;
  //   } else {
  //     this.dictionaries.yearsPsd2 = this.dictionaries.yearsPsd;
  //   }
  // }
  //
  // updateSmrYears() {
  //   if (this.document.object.year.smr.start) {
  //     this.dictionaries.yearsSmr2 = this.dictionaries.yearsSmr.filter(i => i >= this.document.object.year.smr.start);
  //     if (this.document.object.year.smr.finish < this.document.object.year.smr.start) this.document.object.year.smr.finish = null;
  //   } else {
  //     this.dictionaries.yearsSmr2 = this.dictionaries.yearsSmr;
  //   }
  // }
  //
  // filesOnChange(type: string) {
  //
  // }

  isValid(): boolean {
    let r : boolean = true;
    // if (r) r = !!this.document.address.prefect.length;
    // if (r) r = !!this.document.address.district.length;
    if (r) r = !!this.document.object.kind;
    // if (r) r = !!this.document.object.type;
    if (r) r = !!this.document.object.name;
    // if (r) r = typeof this.document.object.programm.inclusion === 'boolean';
    // if (r && this.document.object.programm.inclusion) r = !!this.document.object.programm.basis;
    return r;
  }

  save(action?: string) {
    this.validate = true;
    if (this.isValid()) {
      this.document.requestId = this.requestId;
      this.document.addOrganization = this.helper.convertToNsi(<any>this.organization);
      const doc = this.helper.convertDocToProgram(this.document);
      if (!this.documentId) {
        this.programService.create(<any>doc)
          .subscribe((response: any) => {
            if (response && response.obj && response.obj.documentId) {
              this.helper.success('Предложение успешно создано!');
              this.saveResult(response.obj, action);
              // this.stateService.go('app.object.card', {id: response.obj.documentId});
            }
          }, error => this.helper.error(error));
      } else {
        let diff: any[] = jsonpatch.compare({obj: this.old.min()}, {obj: doc.min()});
        if (diff.length) {
          this.programService.patch(this.documentId, diff).subscribe((response: any) => {
            this.saveResult(response.obj, action);
          });
        } else {
          // Нечего сохранять
          this.saveResult(this.document, action);
        }
      }
    } else {
      this.helper.error('Не заполнены обязательные поля');
    }
  }

  saveResult(doc: any, action?: string): void {
    switch(action) {
      case 'reload':
        this.old = this.helper.convertDocToProgram(this.document);
        this.document = this.helper.convertDocToProgram(doc);
        this.documentId = this.document.documentId; // чтобы не создавалось 2 одинаковых документа
        break;

      default:
        // this.stateService.go('app.object.card', {id: doc.documentId});
        if (this.saveCallback) this.saveCallback();
        this.bsModalRef.hide();
        break;
    }
  }

  cancel() {
    this.bsModalRef.hide();
  }

  updateBreadcrumbs() {
    if (!this.rootScope['breadcrumbs']) this.rootScope['breadcrumbs'] = [];
    this.rootScope['breadcrumbs'].length = 0;
    this.rootScope['breadcrumbs'].push({
      title: 'Объекты и мероприятия',
      url: '/rayon/' + this.stateService.href('app.object.list', {})
    });
    this.rootScope['breadcrumbs'].push({
      title: this.documentId ? 'Редактирование объекта или мероприятия' : 'Создание объекта или мероприятия',
      url: null
    });
  }

  getYearList(): number[] {
    let year = new Date().getFullYear(),
      result: number[] = [year];
    while (result.length <= 10) {
      result.push(++year);
    }
    return result;
  }

  maskDecimal(text): any[] {
    return this.helper.maskDecimal(text);
  }

  isLoading(): boolean {
    return this.loadingStatus === LoadingStatus.LOADING;
  }
}
