import {AppComponent} from './app.component';
import {NewProgramComponent} from './components/new-program/new-program.component';
import {ProgramCardComponent} from './components/card/card.component';
import {RayonProgramShowcaseComponent} from './components/showcase/showcase.component';
import {NewRequestComponent} from './components/new-request/new-request.component';
import {TaskPrepareObjectList} from './components/tasks/prepare-object-list/prepare-object-list.component';
import {TaskAgreeObjectList} from "./components/tasks/agree-object-list/agree-object-list.component";
import {ProgramCardMainComponent} from "./components/card/tabs/main/card-main.component";
import {ProgramCardLogComponent} from "./components/card/tabs/log/card-log.component";
import {ProgramCardProcessComponent} from "./components/card/tabs/process/card-process.component";
import {ProgramCardJsonComponent} from "./components/card/tabs/json/card-json.component";
import {ProgramCardWorksComponent} from "./components/card/tabs/works/card-works.component";
import {ProgramCardInstructionsComponent} from "./components/card/tabs/instructions/card-instructions.component";
import {NewWorkTypeComponent} from "./components/new-work-type/new-work-type.component";
import {ProgramCardConsiderationComponent} from "./components/card/tabs/consideration/card-consideration.component";
import {PanoramComponent} from "./components/panoram/panoram.component";
import {ProgramCardObjectsComponent} from "./components/card/tabs/objects/card-objects.component";
import {TaskAdjustObjectInformation} from './components/tasks/adjust-object-information/adjust-object-information.component';
import {FormDataPublicationComponent} from "./components/forms/start-process/data-publication/form-data-publication.component";
import {NewProgramDominantComponent} from "./components/new-program/new-program-dominant.component";
import {ProgramCardDominantComponent} from "./components/card/dominant/card-dominant.component";
import {ProgramCardDominantMainComponent} from "./components/card/dominant/tabs/main/card-dominant-main.component";
import {ProgramCardDominantObjectsComponent} from "./components/card/dominant/tabs/objects/card-dominant-objects.component";
import {ProgramShowcaseDominantComponent} from "./components/showcase/dominant/showcase-dominant.component";
import {NewProgramCommercialComponent} from "./components/new-program/new-program-commercial.component";
import {ProgramCardCommercialComponent} from "./components/card/commercial/card-commercial.component";
import {ProgramCardCommercialMainComponent} from "./components/card/commercial/tabs/main/card-commercial-main.component";
import {ProgramCardCommercialObjectsComponent} from "./components/card/commercial/tabs/objects/card-commercial-objects.component";
import {ProgramShowcaseCommercialComponent} from "./components/showcase/commercial/showcase-commercial.component";
import {ProgramShowcaseEventsComponent} from "./components/showcase/events/showcase-events.component";
import {ProgramShowcaseArchiveComponent} from "./components/showcase/archive/showcase-archive.component";
import {PublicationRequestComponent} from "./components/publication-request/publication-request.component";
import {ProgramShowcaseObjectsComponent} from "./components/showcase/objects/showcase-objects.component";
import {TaskModerateData} from "./components/tasks/moderate-data/moderate-data.component";
import {TaskModerateDataMosRu} from "./components/tasks/moderate-data-mos-ru/moderate-data-mos-ru.component";
import {ProgramCardArchiveComponent} from "./components/card/archive/card-archive.component";
import {ProgramCardArchiveMainComponent} from "./components/card/archive/tabs/main/card-archive-main.component";
import {ProgramCardArchiveObjectsComponent} from "./components/card/archive/tabs/objects/card-archive-objects.component";
import {NewProgramArchiveComponent} from "./components/new-program/new-program-archive.component";
import {FormCreateObjectComponent} from "./components/forms/start-process/create-object/form-create-object.component";
import {TaskFillFormForCreatingObjectComponent} from "./components/tasks/fill-form-for-creating-object/fill-form-for-creating-object.component";
import {FormCreateObjectDominantComponent} from "./components/forms/start-process/create-object-dominant/form-create-object-dominant.component";
import {TaskFillFormForCreatingUniqueObjectComponent} from "./components/tasks/fill-form-for-creating-unique-object/fill-form-for-creating-unique-object.component";
import {TaskApproveData} from "./components/tasks/approve-data/approve-data.component";
import {ProgramCardDecisionsComponent} from "./components/card/tabs/decisions/card-decisions.component";
import {ProgramCardLogWorksComponent} from "./components/card/tabs/log/card-log-works.component";
import {NewPlanComponent} from "./components/new-plan/new-plan.component";
import {CardPlanComponent} from "./components/card/plan/card-plan.component";
import {CardPlanMainComponent} from "./components/card/plan/tabs/main/card-plan-main.component";
import {CardPlanLogComponent} from "./components/card/plan/tabs/log/card-plan-log.component";
import {CardPlanProcessComponent} from "./components/card/plan/tabs/process/card-plan-process.component";
import {CardPlanJsonComponent} from "./components/card/plan/tabs/json/card-plan-json.component";
import {CardPlanVersionsComponent} from "./components/card/plan/tabs/versions/card-plan-versions.component";
import {CardPlanObjectsComponent} from "./components/card/plan/tabs/objects/card-plan-objects.component";
import {PublicMosComponent} from "./components/forms/public-mos/public-mos.component";
import {FormHeadingUkkComponent} from "./components/forms/heading-ukk/heading-ukk.component";
import {ProgramCardAerialPhotoComponent} from "./components/card/tabs/aerial-photo/card-aerial-photo.component";
import {TaskPrepAppObjWorkChangesComponent} from "./components/tasks/change-request/prep-app-obj-work-changes/prep-app-obj-work-changes.component";
import {TaskPrepAppObjectWorkChangesComponent} from "./components/tasks/change-request/prep-app-object-work-changes/prep-app-object-work-changes.component";
import {TaskApprAppObjWorkChangesComponent} from "./components/tasks/change-request/appr-app-obj-work-changes/appr-app-obj-work-changes.component";
import {TaskReviewRequestResultsComponent} from "./components/tasks/change-request/review-request-results/review-request-results.component";
import {CardChangeRequestComponent} from "./components/card/change-request/card.component";
import {CardChangeRequestMainComponent} from "./components/card/change-request/tabs/main/main.component";
import {CardChangeRequestJsonComponent} from "./components/card/change-request/tabs/json/json.component";
import {CardChangeRequestLogComponent} from "./components/card/change-request/tabs/log/log.component";
import {CardChangeRequestProcessComponent} from "./components/card/change-request/tabs/process/process.component";
import {TaskApprAppObjectWorkChangesComponent} from "./components/tasks/change-request/appr-app-object-work-changes/appr-app-object-work-changes.component";
import {TaskReviewRequestWorkResultsComponent} from "./components/tasks/change-request/review-request-work-results/review-request-work-results.component";
import {LoadCatalogComponent} from './components/load-catalog/load-catalog.component';
import {LinkPlanWorkObjectComponent} from './components/link-plan-work-object/link-plan-work-object.component';

export const states = [
  {
    name: 'app.object',
    abstract: true,
    url: '/object',
    component: AppComponent
  },
  {
    name: 'app.request',
    abstract: true,
    url: '/request',
    component: AppComponent
  },
  {
    name: 'panoram',
    url: '/panoram?initpoint?heading',
    component: PanoramComponent,
    params: {
      initpoint: null,
      heading: null
    }
  },
  {
    name: 'app.catalog',
    abstract: true,
    url: '/catalog',
    component: AppComponent
  },
  {
    name: 'app.catalog.load',
    url: '/load',
    component: LoadCatalogComponent
  },
  {
    name: 'app.request.new',
    url: '/new',
    component: NewRequestComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.object.new',
    url: '/new?prefect?district?industry?itemType',
    component: NewProgramComponent,
    params: {
      prefect: null,
      district: null,
      industry: null,
      itemType: null
    }
  },
  {
    name: 'app.object.edit',
    url: '/edit/:id',
    component: NewProgramComponent
  },
  {
    name: 'app.object.card',
    abstract: true,
    url: '/card/:id/:mode',
    component: ProgramCardComponent,
    params: {
      mode: 'view'
    }
  },
  {
    name: 'app.object.card.main',
    url: '/main',
    component: ProgramCardMainComponent
  },
  {
    name: 'app.object.card.works',
    url: '/works?workId',
    component: ProgramCardWorksComponent,
    params: {
      workId: null
    }
  },
  {
    name: 'app.object.card.aerial-photo',
    url: '/aerial-photo',
    component: ProgramCardAerialPhotoComponent
  },
  {
    name: 'app.object.card.log',
    url: '/log',
    component: ProgramCardLogComponent
  },
  {
    name: 'app.object.card.log-works',
    url: '/log-works',
    component: ProgramCardLogWorksComponent
  },
  {
    name: 'app.object.card.process',
    url: '/process',
    component: ProgramCardProcessComponent
  },
  {
    name: 'app.object.card.json',
    url: '/json',
    component: ProgramCardJsonComponent
  },
  {
    name: 'app.object.card.instructions',
    url: '/instructions',
    component: ProgramCardInstructionsComponent
  },
  {
    name: 'app.object.card.decisions',
    url: '/decisions',
    component: ProgramCardDecisionsComponent
  },
  {
    name: 'app.object.card.consideration',
    url: '/consideration',
    component: ProgramCardConsiderationComponent
  },
  {
    name: 'app.object.card.objects',
    url: '/objects',
    component: ProgramCardObjectsComponent
  },
  {
    name: 'app.object.showcase', // old
    url: '/showcase',
    component: RayonProgramShowcaseComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.object.list',
    url: '/list',
    component: ProgramShowcaseObjectsComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.event',
    abstract: true,
    url: '/event',
    component: AppComponent
  },
  {
    name: 'app.event.list',
    url: '/list',
    component: ProgramShowcaseEventsComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.plan',
    abstract: true,
    url: '/plan',
    component: AppComponent
  },
  {
    name: 'app.plan.new',
    url: '/new',
    component: NewPlanComponent
  },
  {
    name: 'app.plan.card',
    abstract: true,
    url: '/card/:id/:mode',
    component: CardPlanComponent,
    params: {
      mode: 'view'
    }
  },
  {
    name: 'app.plan.card.main',
    url: '/main',
    component: CardPlanMainComponent
  },
  {
    name: 'app.plan.card.log',
    url: '/log',
    component: CardPlanLogComponent
  },
  {
    name: 'app.plan.card.process',
    url: '/process',
    component: CardPlanProcessComponent
  },
  {
    name: 'app.plan.card.json',
    url: '/json',
    component: CardPlanJsonComponent
  },
  {
    name: 'app.plan.card.versions',
    url: '/versions',
    component: CardPlanVersionsComponent
  },
  {
    name: 'app.plan.card.objects',
    url: '/objects',
    component: CardPlanObjectsComponent
  },
  {
    name: 'app.plan.link-work-object',
    url: '/link-work-object',
    component: LinkPlanWorkObjectComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.change-request',
    abstract: true,
    url: '/change-request',
    component: AppComponent
  },
  {
    name: 'app.change-request.card',
    abstract: true,
    url: '/card/:id',
    component: CardChangeRequestComponent
  },
  {
    name: 'app.change-request.card.main',
    url: '/main',
    component: CardChangeRequestMainComponent
  },
  {
    name: 'app.change-request.card.json',
    url: '/json',
    component: CardChangeRequestJsonComponent
  },
  {
    name: 'app.change-request.card.log',
    url: '/log',
    component: CardChangeRequestLogComponent
  },
  {
    name: 'app.change-request.card.process',
    url: '/process',
    component: CardChangeRequestProcessComponent
  },
  {
    name: 'app.archive',
    abstract: true,
    url: '/archive',
    component: AppComponent
  },
  {
    name: 'app.archive.list',
    url: '/list',
    component: ProgramShowcaseArchiveComponent
  },
  {
    name: 'app.archive.new',
    url: '/new?prefect?district?industry?itemType',
    component: NewProgramArchiveComponent,
    params: {
      prefect: null,
      district: null,
      industry: null,
      itemType: null
    }
  },
  {
    name: 'app.archive.card',
    abstract: true,
    url: '/card/:id/:mode',
    component: ProgramCardArchiveComponent,
    params: {
      mode: 'view'
    }
  },
  {
    name: 'app.archive.card.main',
    url: '/main',
    component: ProgramCardArchiveMainComponent
  },
  {
    name: 'app.archive.card.works',
    url: '/works',
    component: ProgramCardWorksComponent
  },
  {
    name: 'app.archive.card.log',
    url: '/log',
    component: ProgramCardLogComponent
  },
  {
    name: 'app.archive.card.log-works',
    url: '/log-works',
    component: ProgramCardLogWorksComponent
  },
  {
    name: 'app.archive.card.process',
    url: '/process',
    component: ProgramCardProcessComponent
  },
  {
    name: 'app.archive.card.json',
    url: '/json',
    component: ProgramCardJsonComponent
  },
  {
    name: 'app.archive.card.instructions',
    url: '/instructions',
    component: ProgramCardInstructionsComponent
  },
  {
    name: 'app.archive.card.consideration',
    url: '/consideration',
    component: ProgramCardConsiderationComponent
  },
  {
    name: 'app.archive.card.objects',
    url: '/objects',
    component: ProgramCardArchiveObjectsComponent
  },
  {
    name: 'app.objectDominant',
    abstract: true,
    url: '/dominant',
    component: AppComponent
  },
  {
    name: 'app.objectDominant.new',
    url: '/new?prefect?district?industry?itemType',
    component: NewProgramDominantComponent,
    params: {
      prefect: null,
      district: null,
      industry: null,
      itemType: null
    }
  },
  {
    name: 'app.objectDominant.list',
    url: '/list',
    component: ProgramShowcaseDominantComponent
  },
  {
    name: 'app.objectDominant.card',
    abstract: true,
    url: '/card/:id/:mode',
    component: ProgramCardDominantComponent,
    params: {
      mode: 'view'
    }
  },
  {
    name: 'app.objectDominant.card.main',
    url: '/main',
    component: ProgramCardDominantMainComponent
  },
  {
    name: 'app.objectDominant.card.objects',
    url: '/objects',
    component: ProgramCardDominantObjectsComponent
  },
  {
    name: 'app.objectDominant.card.log',
    url: '/log',
    component: ProgramCardLogComponent
  },
  {
    name: 'app.objectDominant.card.process',
    url: '/process',
    component: ProgramCardProcessComponent
  },
  {
    name: 'app.objectDominant.card.json',
    url: '/json',
    component: ProgramCardJsonComponent
  },
  {
    name: 'app.objectCommercial',
    abstract: true,
    url: '/commercial',
    component: AppComponent
  },
  {
    name: 'app.objectCommercial.new',
    url: '/new?prefect?district?industry?itemType',
    component: NewProgramCommercialComponent,
    params: {
      prefect: null,
      district: null,
      industry: null,
      itemType: null
    }
  },
  {
    name: 'app.objectCommercial.list',
    url: '/list',
    component: ProgramShowcaseCommercialComponent
  },
  {
    name: 'app.objectCommercial.card',
    abstract: true,
    url: '/card/:id/:mode',
    component: ProgramCardCommercialComponent,
    params: {
      mode: 'view'
    }
  },
  {
    name: 'app.objectCommercial.card.main',
    url: '/main',
    component: ProgramCardCommercialMainComponent
  },
  {
    name: 'app.objectCommercial.card.objects',
    url: '/objects',
    component: ProgramCardCommercialObjectsComponent
  },
  {
    name: 'app.objectCommercial.card.log',
    url: '/log',
    component: ProgramCardLogComponent
  },
  {
    name: 'app.objectCommercial.card.process',
    url: '/process',
    component: ProgramCardProcessComponent
  },
  {
    name: 'app.objectCommercial.card.json',
    url: '/json',
    component: ProgramCardJsonComponent
  },
  {
    name: 'app.object.startObjectCreateProcess',
    url: '/start-object-create-process',
    component: FormCreateObjectComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.object.startDominantCreateProcess',
    url: '/start-dominant-create-process',
    component: FormCreateObjectDominantComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.object.startDataPublishProcess',
    url: '/start-data-publish-process',
    component: FormDataPublicationComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.object.workTypeForm',
    url: '/workTypeForm?id?objectId?itemCode',
    component: NewWorkTypeComponent,
    params: {
      id: null,
      objectId: null,
      itemCode: null
    }
  },
  {
    name: 'app.publication',
    abstract: true,
    url: '/publication',
    component: AppComponent
  },
  {
    name: 'app.publication.request',
    abstract: false,
    url: '/request/:id/:tab',
    component: PublicationRequestComponent,
    params: {
      useBreadcrumbs: true,
      tab: 'main'
    }
  },
  {
    name: 'app.publication.mos-ru',
    abstract: false,
    url: '/mos-ru',
    component: PublicMosComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.rubricators',
    abstract: true,
    url: '/rubricators',
    component: AppComponent
  },
  {
    name: 'app.rubricators.update-ukk',
    abstract: false,
    url: '/update-ukk',
    component: FormHeadingUkkComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.prepareObjectList',
    url: '/mrrequestPrepareObjectList',
    component: TaskPrepareObjectList,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.agreeObjectList',
    url: '/mrrequestAgreeObjectList',
    component: TaskAgreeObjectList,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.adjustObjectInformation',
    url: '/mrobjectAdjustObjectInformation?systemCode',
    component: TaskAdjustObjectInformation,
    params: {
      useBreadcrumbs: true,
      systemCode: 'MR'
    }
  },
  {
    name: 'app.execution.moderateData',
    url: '/mrpubModerateData',
    component: TaskModerateData,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.moderateDataForMosRu',
    url: '/mrpubModerateDataForMosRu',
    component: TaskModerateDataMosRu,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.mrobjectFillFormForCreatingObject',
    url: '/mrobjectFillFormForCreatingObject?systemCode',
    component: TaskFillFormForCreatingObjectComponent,
    params: {
      useBreadcrumbs: true,
      systemCode: 'MR'
    }
  },
  {
    name: 'app.execution.mrobjectFillFormForCreatingUniqueObject',
    url: '/mrobjectFillFormForCreatingUniqueObject?systemCode',
    component: TaskFillFormForCreatingUniqueObjectComponent,
    params: {
      useBreadcrumbs: true,
      systemCode: 'MR'
    }
  },
  {
    name: 'app.execution.approveDataPublication',
    url: '/mrpubApproveDataPublication',
    component: TaskApproveData,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.mrchangerequestprepAppObjWorkChanges',
    url: '/mrchangerequestprepAppObjWorkChanges',
    component: TaskPrepAppObjWorkChangesComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.mrchangerequestapprAppObjWorkChanges',
    url: '/mrchangerequestapprAppObjWorkChanges',
    component: TaskApprAppObjWorkChangesComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.mrchangerequestreviewRequestResults',
    url: '/mrchangerequestreviewRequestResults',
    component: TaskReviewRequestResultsComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.mrchangerequestprepAppObjectWorkChanges',
    url: '/mrchangerequestprepAppObjectWorkChanges',
    component: TaskPrepAppObjectWorkChangesComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.mrchangerequestapprAppObjectWorkChanges',
    url: '/mrchangerequestapprAppObjectWorkChanges',
    component: TaskApprAppObjectWorkChangesComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.mrchangerequestreviewRequestWorkResults',
    url: '/mrchangerequestreviewRequestWorkResults',
    component: TaskReviewRequestWorkResultsComponent,
    params: {
      useBreadcrumbs: true
    }
  }
];
