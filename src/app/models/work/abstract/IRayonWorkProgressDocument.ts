import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';

export interface IRayonWorkProgressDocument extends IDocumentBase {
  type?: INSIDocumentType; // Тип документа
  status?: INSIDocumentType; // Статус документа
  date?: string; // Дата
  'number'?: string; // Номер
  files?: string[]; // Файлы
}
