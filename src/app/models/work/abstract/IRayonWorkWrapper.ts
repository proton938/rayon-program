import {IDocumentBase} from '@reinform-cdp/core';
import {IRayonWork} from './IRayonWork';

export interface IRayonWorkWrapper extends IDocumentBase {
  work: IRayonWork;
}
