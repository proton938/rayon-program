import {IDocumentBase} from '@reinform-cdp/core';

export interface IRayonWorkMos extends IDocumentBase {
  name?: string; // Наименование работы
  status?: string; // Текущая стадия работ
  'public'?: boolean; // К публикации на mos.ru
  risk?: string; // Риски публикации на mos.ru
  comment?: string; // Примечание
  execution?: string; // Срок исполнения работы
}
