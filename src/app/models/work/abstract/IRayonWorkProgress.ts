import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';
import {IRayonWorkProgressDocument} from './IRayonWorkProgressDocument';
import {IRayonWorkUser} from './IRayonWorkUser';

export interface IRayonWorkProgress extends IDocumentBase {
  stage?: INSIDocumentType; // Стадия работ
  bargainNumberEaist?: string; // Реестровый номер торгов ЕАИСТ
  registryNumberEaist?: string; // Реестровый номер контракта ЕАИСТ
  itemNumber?: string; // Номер лота
  documentsExecution?: IRayonWorkProgressDocument[]; // Документы, подтверждающие стадию
  cancelReason?: INSIDocumentType; // Причина отмены работ
  comment?: string; // Комментарий
  executor?: IRayonWorkUser; // Исполнитель
  executionFactStart?: string; // Фактический срок начала работ
  executionFactFinish?: string; // Фактический срок окончания работ
  reason?: string; // Причина изменения текущей стадии
}
