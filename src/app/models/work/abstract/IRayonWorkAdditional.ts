import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';
import {IRayonWorkFinanceByYear} from './IRayonWorkFinanceByYear';

export interface IRayonWorkAdditional extends IDocumentBase {
  required?: boolean; // Требуется дополнительное финансирование
  source?: INSIDocumentType; // Предполагаемый источник финансирования
  additional?: string; // Объем дополнительного финансирования, руб
  byYear?: IRayonWorkFinanceByYear[]; // Объем дополнительного финасирования по годам
}
