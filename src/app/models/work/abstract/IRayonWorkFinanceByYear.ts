import {IDocumentBase} from '@reinform-cdp/core';

export interface IRayonWorkFinanceByYear extends IDocumentBase {
  year?: number; // Год
  total?: string; // Объем финансирования, руб
}
