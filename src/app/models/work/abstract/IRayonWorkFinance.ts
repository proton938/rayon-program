import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';
import {IRayonWorkFinanceByYear} from './IRayonWorkFinanceByYear';

export interface IRayonWorkFinance extends IDocumentBase {
  source?: INSIDocumentType; // Источник финансирования
  general?: string; // Общий объем финансирования, руб
  financed?: string; // Профинансировано, тыс. руб.
  byYear?: IRayonWorkFinanceByYear[]; // Общий объем финансирования по годам
}
