import {IDocumentBase, INSIDocumentType, IRangeDocumentType} from '@reinform-cdp/core';
import {IRayonWorkFinance} from './IRayonWorkFinance';
import {IRayonWorkAdditional} from './IRayonWorkAdditional';
import {IRayonWorkFinanceByYear} from './IRayonWorkFinanceByYear';
import {IRayonWorkSpecific} from './IRayonWorkSpecific';
import {IRayonWorkMos} from './IRayonWorkMos';

export interface IRayonWork extends IDocumentBase {
  documentId?: string; // Идентификатор документа
  folderId?: string; // Идентификатор папки в Alfresco
  documentDate?: Date; // Дата документа
  objectId?: string; // Идентификатор связанного объекта
  organization?: INSIDocumentType; // Ответственный исполнитель
  type?: INSIDocumentType; // Вид работ
  name?: string; // Наименование работы
  address?: string; // Адресный ориентир
  addressId?: string; // Идентификатор адреса БТИ
  coordinate?: string; // Координаты
  execution?: IRangeDocumentType; // Выполнение работ
  psd?: IRangeDocumentType; // Разработка ПСД
  smr?: IRangeDocumentType; // Выполнение СМР
  status?: INSIDocumentType; // Текущая стадия работ
  budget?: boolean; // Финансовое обеспечение
  stateProgram?: INSIDocumentType; // Государственная программа
  stateCustomer?: INSIDocumentType; // Государственный заказчик
  general?: string; // Общий объем финансирования, тыс. руб.
  financed?: string; // Профинансировано, тыс. руб.
  byYear?: IRayonWorkFinanceByYear[]; // Общий объем финансирования по годам
  finance?: IRayonWorkFinance[]; // Общее финансирование
  additional?: IRayonWorkAdditional; // Объем дополнительного финансирования
  'public'?: boolean; // К публикации на открытых информационных ресурсах
  risk?: string; // Риски публикации
  masterplanNumber?: string; // Номер мастер-плана
  masterplanWork?: boolean; // Признак "Работа мастер-плана"
  comment?: string; // Примечание
  specific?: IRayonWorkSpecific[]; // Специфический атрибут документа
  mos?: IRayonWorkMos; // Дополнительные поля для публикации объекта на внешнем контуре
  progress?: any[]; // Ход работ
  approveAg?: INSIDocumentType; // Согласование на АГ
  approveMunicipalDep?: INSIDocumentType; // Согласование с муниципальными депутатами
  correctionReason?: string; // Причина изменения
}
