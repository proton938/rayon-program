import {IDocumentBase} from '@reinform-cdp/core';

export interface IRayonWorkSpecific extends IDocumentBase {
  specificCode?: string; // Код атрибута
  specificName?: string; // Наименование атрибута
  specificValue?: string; // Значение атрибута
  url?: string; // Ссылка
}
