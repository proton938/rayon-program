import {IDocumentBase} from '@reinform-cdp/core';

export interface IRayonWorkUser extends IDocumentBase {
  login?: string; // Имя учетной записи
  fio?: string; // ФИО
  date?: string; // Дата
  organization?: string; // Организация
}
