import {DocumentBase} from '@reinform-cdp/core';
import {IRayonWorkFinanceByYear} from './abstract/IRayonWorkFinanceByYear';
import {map} from 'lodash';

export class RayonWorkFinanceByYear extends DocumentBase<IRayonWorkFinanceByYear, RayonWorkFinanceByYear> {
  year: number = null;
  total = '';

  static buildArray(values: IRayonWorkFinanceByYear[] = []): RayonWorkFinanceByYear[] {
    const result = (values && values.length) ? map(values, u => {
      const value = new RayonWorkFinanceByYear();
      value.build(u);
      return value;
    }) : [];
    return result;
  }

  build(i: IRayonWorkFinanceByYear) {
    super.build(i);
    if (i) {
      if (i.total) { this.total = this._moneyToNormalFormat(i.total); }
    }
  }

  private _moneyToNormalFormat(value: string = '') {
    return value.replace(/[^\d\.]/g, '');
  }
}
