import {DocumentBase} from '@reinform-cdp/core';
import {IRayonWorkUser} from './abstract/IRayonWorkUser';

export class RayonWorkUser extends DocumentBase<IRayonWorkUser, RayonWorkUser> {
  login = '';
  fio = '';
  date: Date = null;
  organization = '';

  build(i?: IRayonWorkUser) {
    super.build(i);
  }
}
