import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonWorkProgressDocument} from './abstract/IRayonWorkProgressDocument';
import {map} from 'lodash';

export class RayonWorkProgressDocument extends DocumentBase<IRayonWorkProgressDocument, RayonWorkProgressDocument> {
  type = null;
  status = null;
  date: Date = null;
  'number' = '';
  files = [];

  build(i?: IRayonWorkProgressDocument) {
    super.build(i);
    if (i) {
      if (i.type) {
        this.type = new NSIDocumentType();
        this.type.build(i.type);
      }
      if (i.status) {
        this.status = new NSIDocumentType();
        this.status.build(i.status);
      }
      this.files = i.files || [];
    }
    return this;
  }

  static buildArray(values: IRayonWorkProgressDocument[]): RayonWorkProgressDocument[] {
    let r: RayonWorkProgressDocument[] = (values || []).map(this.getInstance.bind(this));
    return r;
  }

  static getInstance(i) {
    const value = new RayonWorkProgressDocument();
    return value.build(i);
  }
}
