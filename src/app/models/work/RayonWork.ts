import {DocumentBase, NSIDocumentType, RangeDocumentType} from '@reinform-cdp/core';
import {IRayonWork} from './abstract/IRayonWork';
import {RayonWorkFinance} from './RayonWorkFinance';
import {RayonWorkAdditional} from './RayonWorkAdditional';
import {RayonWorkFinanceByYear} from './RayonWorkFinanceByYear';
import {RayonWorkSpecific} from './RayonWorkSpecific';
import {RayonWorkMos} from './RayonWorkMos';
import {RayonWorkProgress} from './RayonWorkProgress';

export class RayonWork extends DocumentBase<IRayonWork, RayonWork> {
  documentId = '';
  folderId = '';
  documentDate: Date = null;
  objectId = '';
  organization: NSIDocumentType = null;
  type: NSIDocumentType = null;
  name = '';
  address = '';
  addressId: string = null;
  coordinate = '';
  execution: RangeDocumentType = new RangeDocumentType();
  psd: RangeDocumentType = new RangeDocumentType();
  smr: RangeDocumentType = new RangeDocumentType();
  status: NSIDocumentType = null;
  budget: boolean = null;
  stateProgram: NSIDocumentType = null;
  stateCustomer: NSIDocumentType = null;
  general = '';
  financed = '';
  byYear: RayonWorkFinanceByYear[] = [];
  finance: RayonWorkFinance[] = [];
  additional: RayonWorkAdditional = new RayonWorkAdditional();
  'public': boolean = null;
  risk = '';
  masterplanNumber: string = '';
  masterplanWork: boolean = null;
  comment = '';
  specific: RayonWorkSpecific[] = [];
  mos: RayonWorkMos = new RayonWorkMos();
  progress: RayonWorkProgress[] = [];
  approveAg: NSIDocumentType = null;
  approveMunicipalDep: NSIDocumentType = null;
  correctionReason = '';

  build(i?: IRayonWork) {
    super.build(i);
    if (i) {
      if (i.organization) {
        this.organization = new NSIDocumentType();
        this.organization.build(i.organization);
      }
      if (i.type) {
        this.type = new NSIDocumentType();
        this.type.build(i.type);
      }
      if (i.addressId) { this.addressId = '' + i.addressId; }
      if (i.execution) { this.execution.build(i.execution); }
      if (i.psd) { this.psd.build(i.psd); }
      if (i.smr) { this.smr.build(i.smr); }
      if (i.status) {
        this.status = new NSIDocumentType();
        this.status.build(i.status);
      }
      if (i.stateProgram) {
        this.stateProgram = new NSIDocumentType();
        this.stateProgram.build(i.stateProgram);
      }
      if (i.stateCustomer) {
        this.stateCustomer = new NSIDocumentType();
        this.stateCustomer.build(i.stateCustomer);
      }
      this.byYear = RayonWorkFinanceByYear.buildArray(i.byYear);
      this.finance = RayonWorkFinance.buildArray(i.finance);
      if (i.additional) {
        this.additional = new RayonWorkAdditional();
        this.additional.build(i.additional);
      }
      this.specific = RayonWorkSpecific.buildArray(i.specific);
      this.mos.build(i.mos);
      this.progress = RayonWorkProgress.buildArray(i.progress);
      if (i.approveAg) {
        this.approveAg = new NSIDocumentType();
        this.approveAg.build(i.approveAg);
      }
      if (i.approveMunicipalDep) {
        this.approveMunicipalDep = new NSIDocumentType();
        this.approveMunicipalDep.build(i.approveMunicipalDep);
      }
    }
  }
}
