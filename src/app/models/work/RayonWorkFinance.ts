import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonWorkFinance} from './abstract/IRayonWorkFinance';
import {RayonWorkFinanceByYear} from './RayonWorkFinanceByYear';
import {map} from 'lodash';

export class RayonWorkFinance extends DocumentBase<IRayonWorkFinance, RayonWorkFinance> {
  source: NSIDocumentType = null;
  general = '';
  financed = '';
  byYear: RayonWorkFinanceByYear[] = [];

  static buildArray(values: IRayonWorkFinance[] = []): RayonWorkFinance[] {
    const result = (values && values.length) ? map(values, u => {
      const value = new RayonWorkFinance();
      value.build(u);
      return value;
    }) : [];
    return result;
  }

  build(i?: IRayonWorkFinance) {
    super.build(i);
    if (i) {
      if (i.general) this.general = this._moneyToNormalFormat(i.general);
      if (i.source) {
        this.source = new NSIDocumentType();
        this.source.build(i.source);
      }
      this.byYear = RayonWorkFinanceByYear.buildArray(i.byYear);
    }
  }

  private _moneyToNormalFormat(value: string = '') {
    return value.replace(/[^\d\.]/g, '');
  }
}
