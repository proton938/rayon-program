import {DocumentBase} from "@reinform-cdp/core";
import {IRayonWorkMos} from "./abstract/IRayonWorkMos";

export class RayonWorkMos extends DocumentBase<IRayonWorkMos, RayonWorkMos> {
  name: string = '';
  status: string = '';
  'public': boolean = null;
  risk: string = '';
  comment: string = '';
  execution: string = '';
}
