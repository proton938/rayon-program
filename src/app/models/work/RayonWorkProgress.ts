import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonWorkProgress} from './abstract/IRayonWorkProgress';
import {RayonWorkUser} from './RayonWorkUser';
import {map} from 'lodash';
import {RayonWorkProgressDocument} from './RayonWorkProgressDocument';

export class RayonWorkProgress extends DocumentBase<IRayonWorkProgress, RayonWorkProgress> {
  stage: NSIDocumentType = null;
  bargainNumberEaist = '';
  registryNumberEaist = '';
  itemNumber = '';
  documentsExecution: RayonWorkProgressDocument[] = [];
  cancelReason: NSIDocumentType = null;
  comment = '';
  executor: RayonWorkUser = null;
  executionFactStart: Date = null;
  executionFactFinish: Date = null;
  reason = '';

  static buildArray(values: IRayonWorkProgress[]): RayonWorkProgress[] {
    const r: RayonWorkProgress[] = (values || []).map(this.getInstance.bind(this));
    return r;
  }

  static getInstance(i) {
    const value = new RayonWorkProgress();
    return value.build(i);
  }

  build(i?: IRayonWorkProgress) {
    super.build(i);
    if (i) {
      if (i.stage) {
        this.stage = new NSIDocumentType();
        this.stage.build(i.stage);
      }
      if (i.cancelReason) {
        this.cancelReason = new NSIDocumentType();
        this.cancelReason.build(i.cancelReason);
      }
      this.documentsExecution = RayonWorkProgressDocument.buildArray(i.documentsExecution);
      if (i.executor) {
        this.executor = new RayonWorkUser();
        this.executor.build(i.executor);
      }
    }
    return this;
  }
}
