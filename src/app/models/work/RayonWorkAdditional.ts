import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonWorkAdditional} from './abstract/IRayonWorkAdditional';
import {RayonWorkFinanceByYear} from './RayonWorkFinanceByYear';

export class RayonWorkAdditional extends DocumentBase<IRayonWorkAdditional, RayonWorkAdditional> {
  required: boolean = null;
  source: NSIDocumentType = null;
  additional = '';
  byYear: RayonWorkFinanceByYear[] = [];

  build(i?: IRayonWorkAdditional) {
    super.build(i);
    if (i) {
      if (i.source) {
        this.source = new NSIDocumentType();
        this.source.build(i.source);
      }
      if (i.additional) { this.additional = this._moneyToNormalFormat(i.additional); }
      this.byYear = RayonWorkFinanceByYear.buildArray(i.byYear);
    }
  }

  private _moneyToNormalFormat(value: string = '') {
    return value.replace(/[^\d\.]/g, '');
  }
}
