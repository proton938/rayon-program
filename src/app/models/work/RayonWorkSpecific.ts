import {DocumentBase} from '@reinform-cdp/core';
import {IRayonWorkSpecific} from './abstract/IRayonWorkSpecific';
import {map} from 'lodash';

export class RayonWorkSpecific extends DocumentBase<IRayonWorkSpecific, RayonWorkSpecific> {
  specificCode = '';
  specificName = '';
  specificValue: string = null;
  url = '';

  static buildArray(values: IRayonWorkSpecific[]): RayonWorkSpecific[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonWorkSpecific();
      value.build(u);
      return value;
    }) : [];
    return result;
  }

  build(i?: IRayonWorkSpecific) {
    super.build(i);
    if (i) {
      if (i.specificValue) { this.specificValue = i.specificValue; }
    }
  }
}
