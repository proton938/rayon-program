import {DocumentBase} from "@reinform-cdp/core";
import {IRayonWorkWrapper} from "./abstract/IRayonWorkWrapper";
import {RayonWork} from "./RayonWork";

export class RayonWorkWrapper extends DocumentBase<IRayonWorkWrapper, RayonWorkWrapper> {
  work: RayonWork = new RayonWork();

  build(i?: IRayonWorkWrapper) {
    super.build(i);
    if (i) {
      this.work.build(i.work);
    }
  }
}
