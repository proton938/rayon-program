import {IChangeRequestObject} from "./abstracts/IChangeRequestObject";
import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";

export class ChangeRequestObject extends DocumentBase<IChangeRequestObject, ChangeRequestObject> {
  prefect: NSIDocumentType[] = [];
  district: NSIDocumentType[] = [];
  name: string = '';
  industry: NSIDocumentType = null;
  kind: NSIDocumentType = null;
  organization: NSIDocumentType = null;

  build(i?: IChangeRequestObject) {
    super.build(i);
    if (i) {
      this.prefect = NSIDocumentType.buildArray(i.prefect);
      this.district = NSIDocumentType.buildArray(i.district);
      if (i.industry) {
        this.industry = new NSIDocumentType();
        this.industry.build(i.industry);
      }
      if (i.kind) {
        this.kind = new NSIDocumentType();
        this.kind.build(i.kind);
      }
      if (i.organization) {
        this.organization = new NSIDocumentType();
        this.organization.build(i.organization);
      }
    }
  }
}
