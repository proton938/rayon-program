import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";
import {IChangeRequestCoordinator} from "./abstracts/IChangeRequestCoordinator";
import {map} from 'lodash';

export class ChangeRequestCoordinator extends DocumentBase <IChangeRequestCoordinator, ChangeRequestCoordinator> {
  fio: string = '';
  login: string = '';
  comment: string = '';
  date: Date = null;
  reasonForRejecting: NSIDocumentType = null;

  build(i?: IChangeRequestCoordinator) {
    super.build(i);
    if (i) {
      if (i.reasonForRejecting) {
        this.reasonForRejecting = new NSIDocumentType();
        this.reasonForRejecting.build(i.reasonForRejecting);
      }
    }
  }
}
