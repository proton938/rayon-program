import {DocumentBase} from "@reinform-cdp/core";
import {IChangeRequestCreator} from "./abstracts/IChangeRequestCreator";
import {ChangeRequestOrganization} from "./ChangeRequestOrganization";

export class ChangeRequestCreator extends DocumentBase<IChangeRequestCreator, ChangeRequestCreator> {
  fio: string = '';
  organization: ChangeRequestOrganization = null;
  comment: string = '';
  login: string = '';
  contactInformation: string = '';

  build(i?: IChangeRequestCreator) {
    super.build(i);
    if (i) {
      if (i.organization) {
        this.organization = new ChangeRequestOrganization();
        this.organization.build(i.organization);
      }
    }
  }
}
