import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";
import {IChangeRequestWork} from "./abstracts/IChangeRequestWork";

export class ChangeRequestWork extends DocumentBase<IChangeRequestWork, ChangeRequestWork> {
  type: NSIDocumentType = null;

  build(i?: IChangeRequestWork) {
    super.build(i);
    if (i) {
      if (i.type) {
        this.type = new NSIDocumentType();
        this.type.build(i.type);
      }
    }
  }
}
