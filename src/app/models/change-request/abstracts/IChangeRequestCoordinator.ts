import {IDocumentBase, INSIDocumentType} from "@reinform-cdp/core";

export interface IChangeRequestCoordinator extends IDocumentBase {
  fio?: string; // ФИО согласующего
  login?: string; // Логин согласующего
  comment?: string; // Комментарий согласующего
  date?: string; // Дата и время комментария
  reasonForRejecting?: INSIDocumentType; // Причина отклонения
}
