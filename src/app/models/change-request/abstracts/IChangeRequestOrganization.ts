import {IDocumentBase} from "@reinform-cdp/core";

export interface IChangeRequestOrganization extends IDocumentBase {
  ldapOrgCode?: string; // Код организации
  ldapOrgName?: string; // Наименование организации
}
