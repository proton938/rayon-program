import {IDocumentBase} from "@reinform-cdp/core";
import {IChangeRequestOrganization} from "./IChangeRequestOrganization";

export interface IChangeRequestCreator extends IDocumentBase {
  fio?: string; // ФИО автора заявки
  organization?: IChangeRequestOrganization; // Организация автора заявки
  comment?: string; // Комментарий автора заявки
  login?: string; // Логин автора заявки
  contactInformation?: string; // Контактная информация автора заявки
}
