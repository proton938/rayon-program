import {IDocumentBase, INSIDocumentType} from "@reinform-cdp/core";

export interface IChangeRequestWork extends IDocumentBase {
  type?: INSIDocumentType;
}
