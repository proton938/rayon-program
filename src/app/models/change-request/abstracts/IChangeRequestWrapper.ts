import {IDocumentBase} from "@reinform-cdp/core";
import {IChangeRequest} from "./IChangeRequest";

export interface IChangeRequestWrapper extends IDocumentBase {
  changerequest?: IChangeRequest; // Заявка на внесение изменений в объект/работы на объекте
}
