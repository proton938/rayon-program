import {IDocumentBase, INSIDocumentType} from "@reinform-cdp/core";
import {IChangeRequestDocument} from "./IChangeRequestDocument";
import {IChangeRequestCreator} from "./IChangeRequestCreator";
import {IChangeRequestCoordinator} from "./IChangeRequestCoordinator";
import {IChangeRequestObject} from "./IChangeRequestObject";
import {IChangeRequestWork} from "./IChangeRequestWork";

export interface IChangeRequest extends IDocumentBase {
  documentId?: string; // Идентификатор заявки
  documentDate?: string; // Дата создания заявки
  documentNumber?: number; // Номер заявки
  folderId?: string; // Идентификатор папки в Alfresco
  status?: INSIDocumentType; // Статус заявки
  document?: IChangeRequestDocument; // Документ
  creator?: IChangeRequestCreator; // Автор заявки
  coordinator?: IChangeRequestCoordinator; // Согласующий
  modifiedTab?: string; // Изменяемая вкладка
  object?: IChangeRequestObject; // Данные по объекту для витрины
  work?: IChangeRequestWork; // Данные по работе для витрины
  agreement?: INSIDocumentType; // Согласование заявки
  files?: string[]; // Дополнительные материалы
}
