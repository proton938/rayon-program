import {IDocumentBase} from "@reinform-cdp/core";

export interface IChangeRequestDocument extends IDocumentBase {
  jsonOld?: string; // JSON до изменений
  jsonNew?: string; // JSON после изменений
}
