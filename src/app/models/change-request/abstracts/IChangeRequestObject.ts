import {IDocumentBase, INSIDocumentType} from "@reinform-cdp/core";

export interface IChangeRequestObject extends IDocumentBase {
  prefect?: INSIDocumentType[]; // Округ
  district?: INSIDocumentType[]; // Район
  name?: string; // Наименование объекта
  industry?: INSIDocumentType; // Отрасль
  kind?: INSIDocumentType; // Вид объекта
  organization?: INSIDocumentType; // Ответственная организация
}
