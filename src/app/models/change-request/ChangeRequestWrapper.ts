import {DocumentBase} from "@reinform-cdp/core";
import {ChangeRequest} from "./ChangeRequest";
import {IChangeRequestWrapper} from "./abstracts/IChangeRequestWrapper";

export class ChangeRequestWrapper extends DocumentBase<IChangeRequestWrapper, ChangeRequestWrapper> {
  changerequest: ChangeRequest = new ChangeRequest();

  build(i?: IChangeRequestWrapper) {
    super.build(i);
    if (i) {
      this.changerequest.build(i.changerequest);
    }
  }
}
