import {DocumentBase} from "@reinform-cdp/core";
import {IChangeRequestDocument} from "./abstracts/IChangeRequestDocument";

export class ChangeRequestDocument extends DocumentBase<IChangeRequestDocument, ChangeRequestDocument> {
  jsonOld: string = '';
  jsonNew: string = '';

  build(i?: IChangeRequestDocument) {
    super.build(i);
  }
}
