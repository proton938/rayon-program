import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";
import {IChangeRequest} from "./abstracts/IChangeRequest";
import {ChangeRequestDocument} from "./ChangeRequestDocument";
import {ChangeRequestCreator} from "./ChangeRequestCreator";
import {ChangeRequestCoordinator} from "./ChangeRequestCoordinator";
import {ChangeRequestObject} from "./ChangeRequestObject";
import {ChangeRequestWork} from "./ChangeRequestWork";

export class ChangeRequest extends DocumentBase<IChangeRequest, ChangeRequest> {
  documentId: string = '';
  documentDate: Date = null;
  documentNumber: number = null;
  folderId: string = '';
  status: NSIDocumentType = null;
  document: ChangeRequestDocument = null;
  creator: ChangeRequestCreator = null;
  coordinator: ChangeRequestCoordinator = null;
  modifiedTab: string = '';
  object: ChangeRequestObject = null;
  work: ChangeRequestWork = null;
  agreement: NSIDocumentType = null;
  files: string[] = [];

  build(i?: IChangeRequest) {
    super.build(i);
    if (i) {
      if (i.status) {
        this.status = new NSIDocumentType();
        this.status.build(i.status);
      }
      if (i.document) {
        this.document = new ChangeRequestDocument();
        this.document.build(i.document);
      }
      if (i.creator) {
        this.creator = new ChangeRequestCreator();
        this.creator.build(i.creator);
      }
      if (i.coordinator) {
        this.coordinator = new ChangeRequestCoordinator();
        this.coordinator.build(i.coordinator);
      }
      if (i.object) {
        this.object = new ChangeRequestObject();
        this.object.build(i.object);
      }
      if (i.work) {
        this.work = new ChangeRequestWork();
        this.work.build(i.work);
      }
      if (i.agreement) {
        this.agreement = new NSIDocumentType();
        this.agreement.build(i.agreement);
      }
      this.files = i.files || [];
    }
  }
}
