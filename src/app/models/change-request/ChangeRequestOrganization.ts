import {DocumentBase} from "@reinform-cdp/core";
import {IChangeRequestOrganization} from "./abstracts/IChangeRequestOrganization";

export class ChangeRequestOrganization extends DocumentBase<IChangeRequestOrganization, ChangeRequestOrganization> {
  ldapOrgCode: string = '';
  ldapOrgName: string = '';

  build(i?: IChangeRequestOrganization) {
    super.build(i);
  }
}
