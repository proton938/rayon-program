export interface IQueryRequestParams {
  query?: string;
  sort?: string;
  page?: number;
  pageSize?: number;
  types?: string[];
  common?: string;
}
