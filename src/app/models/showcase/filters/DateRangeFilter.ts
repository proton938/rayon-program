import * as moment_ from 'moment';
const moment = moment_;

export class DateRangeFilter {
  from: Date;
  to: Date;

  constructor() {
    this.clear();
  }

  clear() {
    this.from = undefined;
    this.to = undefined;
  }

  isEmpty(): boolean {
    return ((this.from === undefined) && (this.to === undefined));
  }

  toString(): string {
    const formatedMinDate = (this.from) ? `${moment(this.from).format('YYYY-MM-DD')}T00:00:00Z` : '*';
    const formatedMaxDate = (this.to) ? `${moment(this.to).format('YYYY-MM-DD')}T00:00:00Z` : '*';
    return `[${formatedMinDate} TO ${formatedMaxDate}]`;
  }
}
