import {DateRangeFilter} from './DateRangeFilter';
import * as _ from 'lodash';
import * as angular from 'angular';

export class RayonProgramFilter {
  prefectsNameObject = [];
  districtsNameObject = [];
  categoryNameObject = [];
  nameObject = '';
  organizationNameObject = [];
  addressObject = '';
  budgetObject = '';
  executorLoginObject = [];
  executorFioObject = '';
  statusNameObject = [];
  inclusionObject = false;
  psdObjectStart = '';
  psdObjectFinish = '';
  smrObjectStart = '';
  smrObjectFinish = '';
  kindNameObject = [];
  problemObject = false;
  attractionObject = false;
  publicObject = false;
  oknObject = false;
  ownershipNameObject = [];
  constructionProgramNameObject = [];
  periodNameObject = '';
  overhaulObject = '';
  typeObjectNameObject = [];
  itemTypeNameObject = [];
  objectFormNameObject = [];
  industryNameObject = [];
  dominantObject = '';
  commercialObject = '';


  clear() {
    this.prefectsNameObject = [];
    this.districtsNameObject = [];
    this.categoryNameObject = [];
    this.nameObject = '';
    this.organizationNameObject = [];
    this.addressObject = '';
    this.budgetObject = '';
    this.executorLoginObject = [];
    this.executorFioObject = '';
    this.statusNameObject = [];
    this.inclusionObject = false;
    this.psdObjectStart = '';
    this.psdObjectFinish = '';
    this.smrObjectStart = '';
    this.smrObjectFinish = '';
    this.kindNameObject = [];
    this.problemObject = false;
    this.attractionObject = false;
    this.publicObject = false;
    this.oknObject = false;
    this.ownershipNameObject = [];
    this.constructionProgramNameObject = [];
    this.periodNameObject = '';
    this.overhaulObject = '';
    this.typeObjectNameObject = [];
    this.itemTypeNameObject = [];
    this.objectFormNameObject = [];
    this.industryNameObject = [];
    // this.dominantObject = '';
    // this.commercialObject = '';
  }

  isEmpty(): boolean {
    return !this.prefectsNameObject.length &&
      !this.districtsNameObject.length &&
      !this.categoryNameObject.length &&
      this.nameObject === '' &&
      !this.organizationNameObject.length &&
      this.addressObject === '' &&
      this.budgetObject === '' &&
      !this.executorLoginObject.length &&
      this.executorFioObject === '' &&
      !this.statusNameObject.length &&
      this.inclusionObject == false &&
      this.psdObjectStart === '' &&
      this.psdObjectFinish === '' &&
      this.smrObjectStart === '' &&
      this.smrObjectFinish === '' &&
      !this.kindNameObject.length &&
      this.problemObject == false &&
      this.attractionObject == false &&
      this.publicObject == false &&
      this.oknObject == false &&
      !this.ownershipNameObject.length &&
      !this.constructionProgramNameObject.length &&
      this.periodNameObject === '' &&
      this.overhaulObject === '' &&
      !this.typeObjectNameObject.length &&
      !this.itemTypeNameObject.length &&
      !this.objectFormNameObject.length &&
      !this.industryNameObject.length/* &&
      this.dominantObject === '' &&
      this.commercialObject  === ''*/;
  }

  toCommon(text: string): string {
    let result: string = '';
    let fields: string[] = ['nameObject', 'addressObject'];
    result = '(' + fields.map(i => this._onceValue(i, text, false)).join(' OR ') + ')';
    return result;
  }

  toString(): string {
    let result = '';
    if (this.prefectsNameObject && this.prefectsNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('prefectsNameObject', this.prefectsNameObject);
    }
    if (this.districtsNameObject && this.districtsNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('districtsNameObject', this.districtsNameObject);
    }
    if (this.categoryNameObject && this.categoryNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('categoryNameObject', this.categoryNameObject);
    }
    if (this.nameObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('nameObject', this.nameObject, false);
    }
    if (this.organizationNameObject && this.organizationNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('organizationNameObject', this.organizationNameObject, true);
    }
    if (this.addressObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('addressObject', this.addressObject, false);
    }
    if (this.budgetObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('budgetObject', this.budgetObject);
    }
    if (this.executorLoginObject && this.executorLoginObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('executorLoginObject', this.executorLoginObject);
    }
    if (this.executorFioObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('executorFioObject', this.executorFioObject);
    }
    if (this.statusNameObject && this.statusNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('statusNameObject', this.statusNameObject);
    }
    if (this.inclusionObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('inclusionObject', 'true');
    }
    if (this.psdObjectStart) {
      result += result ? ' AND ' : '';
      result += `(psdObjectStart:[${this.psdObjectStart} TO *])`;
    }
    if (this.psdObjectFinish) {
      result += result ? ' AND ' : '';
      result += `(psdObjectFinish:[* TO ${this.psdObjectFinish}])`;
    }
    if (this.smrObjectStart) {
      result += result ? ' AND ' : '';
      result += `(smrObjectStart:[${this.smrObjectStart} TO *])`;
    }
    if (this.smrObjectFinish) {
      result += result ? ' AND ' : '';
      result += `(smrObjectFinish:[* TO ${this.smrObjectFinish}])`;
    }
    if (this.kindNameObject && this.kindNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('kindNameObject', this.kindNameObject);
    }
    if (this.problemObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('problemObject', 'true');
    }
    if (this.attractionObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('attractionObject', 'true');
    }
    if (this.publicObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('publicObject', 'true');
    }
    if (this.oknObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('oknObject', 'true');
    }
    if (this.ownershipNameObject && this.ownershipNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('ownershipNameObject', this.ownershipNameObject);
    }
    if (this.constructionProgramNameObject && this.constructionProgramNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('constructionProgramNameObject', this.constructionProgramNameObject);
    }
    if (this.periodNameObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('periodNameObject', this.periodNameObject);
    }
    if (this.overhaulObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('overhaulObject', this.overhaulObject);
    }
    if (this.typeObjectNameObject && this.typeObjectNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('typeObjectNameObject', this.typeObjectNameObject);
    }
    if (this.itemTypeNameObject && this.itemTypeNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('itemTypeNameObject', this.itemTypeNameObject);
    }
    if (this.objectFormNameObject && this.objectFormNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('objectFormNameObject', this.objectFormNameObject);
    }
    if (this.industryNameObject && this.industryNameObject.length) {
      result += result ? ' AND ' : '';
      result += this._multipleValue('industryNameObject', this.industryNameObject);
    }
    if (this.dominantObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('dominantObject', this.dominantObject);
    }
    if (this.commercialObject) {
      result += result ? ' AND ' : '';
      result += this._onceValue('commercialObject', this.commercialObject);
    }
    return result;
  }

  private _onceValue(propName: string, value: string, hardOneValue: boolean = true): string {
    let result: string = '';
    value = value.trim().replace(/["']/g, '');
    if (value.match(/\s+/)) value = '(*' + value.replace(/\s+/g, '* AND *') + '*)';
    else if (!hardOneValue) value = '*' + value + '*';
    result += '(' + propName + ':' + value + ')';
    return result;
  }

  private _multipleValue(propName: string, values: string[], strong: boolean = false) {
    let result: string = '';
    values = values.map(v => {
      v = v.trim().replace(/["']/g, '');
      if (v.match(/\s+/)) {
        v = '(' + (strong ? v : '*' + v.replace(/\s+/g, '* AND *') + '*') + ')';
      }
      return v;
    });
    result += '(' + propName + ':' + values.join(' OR ' + propName + ':') + ')';
    return result;
  }
}
