import {IQueryResponseData} from './IQueryResponseData';

export interface IQueryResponse {
  response: IQueryResponseData;
}
