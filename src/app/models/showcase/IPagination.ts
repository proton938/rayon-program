export interface IPagination {
  totalItems: number;
  itemsPerPage: number;
  currentPage: number;
  disable: boolean;
}
