import {BaseSolrDocumentType} from './document-types/BaseSolrDocumentType';
import {IQueryResponseData} from './IQueryResponseData';
import * as _ from 'lodash';
import {SolrDocumentBuilder} from './builders/SolrDocumentBuilder';
import {Observable} from 'rxjs/Rx';

export class QueryResponseDataAsync {
  docs: Observable<BaseSolrDocumentType>[];
  numFound: number;
  pageSize: number;
  start: number;

  constructor(i: IQueryResponseData, solrDocumentBuilder: SolrDocumentBuilder) {
    this.start = i.start;
    this.pageSize = i.pageSize;
    this.numFound = i.numFound;
    this.docs =  (i.docs) ? _.map(i.docs, d => { return solrDocumentBuilder.build(d); }) : [];
  }
}
