import {BaseSolrDocumentType} from './document-types/BaseSolrDocumentType';

export class QueryResponseData {
  docs: BaseSolrDocumentType[];
  numFound: number;
  pageSize: number;
  start: number;

  constructor(start: number, pageSize: number, numFound: number, docs: BaseSolrDocumentType[]) {
    this.start = start;
    this.pageSize = pageSize;
    this.numFound = numFound;
    this.docs = docs;
  }
}
