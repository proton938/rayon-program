import {ShowcaseSorting} from './ShowcaseSorting';

export interface ISortingBuilder {
  build(): ShowcaseSorting[];
}
