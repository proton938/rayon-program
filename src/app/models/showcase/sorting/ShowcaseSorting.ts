export class ShowcaseSorting {
  code: string;
  value: string;
  name: string;
  show: boolean;
  width: any;

  constructor(code: string, value: string, name: string, show: boolean = true, width?: any) {
    this.code = code;
    this.value = value;
    this.name = name;
    this.show = show;
    this.width = width;
  }
}
