import {ISortingBuilder} from '../ISortingBuilder';
import {ShowcaseSorting} from '../ShowcaseSorting';

export class RayonProgramSortingBuilder implements ISortingBuilder {

  build(): ShowcaseSorting[] {
    const result: ShowcaseSorting[] = [];

    result.push(new ShowcaseSorting('prefectsNameObject', '', 'Округ'));
    result.push(new ShowcaseSorting('districtsNameObject', '', 'Район'));
    result.push(new ShowcaseSorting('kindNameObject', '', 'Отрасль и вид'));
    result.push(new ShowcaseSorting('nameObject', 'none', 'Наименование'));
    result.push(new ShowcaseSorting('addressObject', 'none', 'Адрес'));
    result.push(new ShowcaseSorting('organizationNameObject', 'none', 'Ответственный'));
    result.push(new ShowcaseSorting('itemTypeNameObject', 'none', 'Тип элемента', false));
    result.push(new ShowcaseSorting('problemObject', 'none', 'Проблемный', false));
    result.push(new ShowcaseSorting('attractionObject', 'none', 'Место притяжения', false));

    return result;
  }
}
