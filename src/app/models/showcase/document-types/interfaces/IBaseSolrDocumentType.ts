export interface IBaseSolrDocumentType {
  id: string; // Идентификатор документа в Solr
  type: string; // Тип документа
  documentId: string; // Идентификатор документа (внутри конкретного типа)
  linkId: string;	// Идентификатор для ссылки на карточку документа
  subtype: string; // Подтип
  link: string;
  title: string;
}
