import {BaseSolrDocumentType} from './BaseSolrDocumentType';
import {IRayonProgramSolrDocumentType} from "./interfaces/IRayonProgramSolrDocumentType";

export class RayonProgramSolrDocumentType extends BaseSolrDocumentType {
  requestIdObject: string;
  prefectsNameObject: string[];
  districtsNameObject: string[];
  nameObject: string;
  organizationNameObject: string;
  organizationCodeObject: string;
  addressObject: string;
  budgetObject: string;
  executorLoginObject: string;
  executorFioObject: string;
  statusNameObject: string;
  statusCodeObject: string;
  psdObjectStart: string;
  psdObjectFinish: string;
  smrObjectStart: string;
  smrObjectFinish: string;
  inclusionObject: string;
  industryNameObject: string;
  industryCodeObject: string;
  kindNameObject: string;
  kindCodeObject: string;
  objectAreaObject: string;
  sectorAreaObject: string;
  ownershipNameObject: string;
  constructionProgramNameObject: string;
  periodNameObject: string;
  overhaulObject: string;
  typeObjectNameObject: string;
  tepsObject: string[];
  commentObject: string;
  problemObject: string;
  addOrganizationCodeObject: string;
  addOrganizationNameObject: string;
  sampleObject: string;
  itemTypeNameObject: string;

  constructor(i: IRayonProgramSolrDocumentType) {
    super(i);
    this.requestIdObject = i.requestIdObject;
    this.prefectsNameObject = i.prefectsNameObject || [];
    this.districtsNameObject = i.districtsNameObject || [];
    this.nameObject = i.nameObject;
    this.organizationNameObject = i.organizationNameObject;
    this.organizationCodeObject = i.organizationCodeObject;
    this.addressObject = i.addressObject;
    this.budgetObject = i.budgetObject;
    this.executorLoginObject = i.executorLoginObject;
    this.executorFioObject = i.executorFioObject;
    this.statusNameObject = i.statusNameObject;
    this.statusCodeObject = i.statusCodeObject;
    this.psdObjectStart = i.psdObjectStart;
    this.psdObjectFinish = i.psdObjectFinish;
    this.smrObjectStart = i.smrObjectStart;
    this.smrObjectFinish = i.smrObjectFinish;
    this.inclusionObject = i.inclusionObject;
    this.industryNameObject = i.industryNameObject;
    this.industryCodeObject = i.industryCodeObject;
    this.kindNameObject = i.kindNameObject;
    this.kindCodeObject = i.kindCodeObject;
    this.objectAreaObject = i.objectAreaObject;
    this.sectorAreaObject = i.sectorAreaObject;
    this.ownershipNameObject = i.ownershipNameObject;
    this.constructionProgramNameObject = i.constructionProgramNameObject;
    this.periodNameObject = i.periodNameObject;
    this.overhaulObject = i.overhaulObject;
    this.typeObjectNameObject = i.typeObjectNameObject;
    this.tepsObject = i.tepsObject || [];
    this.commentObject = i.commentObject;
    this.problemObject = i.problemObject;
    this.addOrganizationCodeObject = i.addOrganizationCodeObject;
    this.addOrganizationNameObject = i.addOrganizationNameObject;
    this.sampleObject = i.sampleObject;
    this.itemTypeNameObject = i.itemTypeNameObject;
  }
}
