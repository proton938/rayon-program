import {IBaseSolrDocumentType} from './interfaces/IBaseSolrDocumentType';

export class BaseSolrDocumentType {
  protected id: string;
  protected type: string;
  protected documentId: string;
  protected linkId: string;
  protected subtype: string;
  protected link: string;
  protected title: string;

  constructor(i: IBaseSolrDocumentType) {
    this.id = i.id;
    this.type = i.type;
    this.documentId = i.documentId;
    this.linkId = i.linkId;
    this.subtype = i.subtype;
    this.link = i.link;
    this.title = i.title;
  }
}
