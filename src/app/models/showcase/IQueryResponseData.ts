import {IBaseSolrDocumentType} from './document-types/interfaces/IBaseSolrDocumentType';

export interface IQueryResponseData {
  docs: IBaseSolrDocumentType[];
  numFound: number;
  pageSize: number;
  start: number;
}
