import {Observable} from 'rxjs/Rx';
import {of} from 'rxjs/index';
import {Injectable} from '@angular/core';
import {IRayonProgramSolrDocumentType} from "../document-types/interfaces/IRayonProgramSolrDocumentType";
import {RayonProgramSolrDocumentType} from "../document-types/RayonProgramSolrDocumentType";
@Injectable()
export class RayonProgramSolrDocumentBuilder {
  build(value: IRayonProgramSolrDocumentType): Observable<RayonProgramSolrDocumentType> {
    return of(new RayonProgramSolrDocumentType(value));
  }
}
