import {IBaseSolrDocumentType} from '../document-types/interfaces/IBaseSolrDocumentType';
import {BaseSolrDocumentType} from '../document-types/BaseSolrDocumentType';
import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {RayonProgramSolrDocumentBuilder} from "./RayonProgramSolrDocumentBuilder";
import {IRayonProgramSolrDocumentType} from "../document-types/interfaces/IRayonProgramSolrDocumentType";

@Injectable()
export class SolrDocumentBuilder {
  constructor(private rayonProgramSolrDocumentBuilder: RayonProgramSolrDocumentBuilder) {
  }

  build(value: IBaseSolrDocumentType): Observable<BaseSolrDocumentType> {
    switch (value.type) {
      case 'OBJECT':
        return this.rayonProgramSolrDocumentBuilder.build((<IRayonProgramSolrDocumentType>value));
    }
  }
}
