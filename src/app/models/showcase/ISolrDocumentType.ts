export interface ISolrDocumentType {
  code: string;
  description: string;
}
