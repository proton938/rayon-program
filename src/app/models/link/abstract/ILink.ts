import {IDocumentBase} from "@reinform-cdp/core";

export interface ILink extends IDocumentBase { // Решения
  documentId?: string; // Идентификатор решения
  id1?: string; // Тип решения
  id2?: string; // Содержание решения
  type1?: string;  // Адрес
  type2?: string;  // Протокол
  status?: string;
  subsystem1?: string;
  subsystem2?: string;
  system1?: string;
  system2?: string;
}
