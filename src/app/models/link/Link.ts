import {DocumentBase} from "@reinform-cdp/core";
import {ILink} from "./abstract/ILink";

export class Link extends DocumentBase<ILink, Link> {
  documentId: string = '';
  id1?: string = '';
  id2?: string = '';
  type1?: string = '';
  type2?: string = '';
  status?: string = '';
  subsystem1?: string = '';
  subsystem2?: string = '';
  system1?: string = '';
  system2?: string = '';

  build(i?: ILink) {
    super.build(i);
    if (i) {
      if (i.documentId) { this.documentId = i.documentId; }
      if (i.id1) { this.id1 = i.id1; }
      if (i.id2) { this.id2 = i.id2; }
      if (i.type1) { this.type1 = i.type1; }
      if (i.type2) {this.type2 = i.type2; }
      if (i.status) {this.status = i.status; }
      if (i.subsystem1) {this.subsystem1 = i.subsystem1; }
      if (i.subsystem2) {this.subsystem2 = i.subsystem2; }
      if (i.system1) {this.system1 = i.system1; }
      if (i.system2) {this.system2 = i.system2; }
    }
  }
}
