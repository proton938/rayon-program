import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgramObject} from './abstracts/IRayonProgramObject';
import {RayonProgramObjectProgram} from './RayonProgramObjectProgram';

export class RayonProgramObject extends DocumentBase<IRayonProgramObject, RayonProgramObject> {
  industry: NSIDocumentType = null;
  kind: NSIDocumentType = null;
  ownership: NSIDocumentType = null;
  period: NSIDocumentType = null;
  typeObject: NSIDocumentType = null;
  name = '';
  description = '';
  extent = '';
  area = '';
  areaObject = '';
  programm: RayonProgramObjectProgram = new RayonProgramObjectProgram();

  build(i?: IRayonProgramObject) {
    super.build(i);
    if (i) {
      if (i.industry) {
        this.industry = new NSIDocumentType();
        this.industry.build(i.industry);
      }
      if (i.kind) {
        this.kind = new NSIDocumentType();
        this.kind.build(i.kind);
      }
      if (i.ownership) {
        this.ownership = new NSIDocumentType();
        this.ownership.build(i.ownership);
      }
      if (i.period) {
        this.period = new NSIDocumentType();
        this.period.build(i.period);
      }
      if (i.typeObject) {
        this.typeObject = new NSIDocumentType();
        this.typeObject.build(i.typeObject);
      }
      this.programm.build(i.programm);
    }
  }
}
