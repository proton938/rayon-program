import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramParameterZu} from "./abstracts/IRayonProgramParameterZu";

export class RayonProgramParameterZu extends DocumentBase<IRayonProgramParameterZu, RayonProgramParameterZu> {
  cadastralNumber: string[] = [];
  landCategory = '';
  permittedUse = '';
  gpzuNumber = '';
  gpzuDate: Date = null;

  build(i?: IRayonProgramParameterZu) {
    super.build(i);
    if (i) {
      this.cadastralNumber = i.cadastralNumber;
    }
  }
}
