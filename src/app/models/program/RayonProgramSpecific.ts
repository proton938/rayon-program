import {DocumentBase} from '@reinform-cdp/core';
import {IRayonProgramSpecific} from './abstracts/IRayonProgramSpecific';
import {map} from 'lodash';

export class RayonProgramSpecific extends DocumentBase<IRayonProgramSpecific, RayonProgramSpecific> {
  specificCode = '';
  specificName = '';
  specificValue: string = null;
  url = '';

  static buildArray(values: IRayonProgramSpecific[]): RayonProgramSpecific[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonProgramSpecific();
      value.build(u);
      return value;
    }) : [];
    return result;
  }

  build(i?: IRayonProgramSpecific) {
    super.build(i);
    if (i) {
      if (i.specificValue) { this.specificValue = i.specificValue; }
    }
  }
}
