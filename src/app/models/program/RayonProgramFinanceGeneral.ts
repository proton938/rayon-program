import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramFinanceGeneral} from "./abstracts/IRayonProgramFinanceGeneral";
import {IRayonProgramFinanceByYear} from "./abstracts/IRayonProgramFinanceByYear";
import {RayonProgramFinanceByYear} from "./RayonProgramFinanceByYear";

export class RayonProgramFinanceGeneral extends DocumentBase<IRayonProgramFinanceGeneral, RayonProgramFinanceGeneral> {
  general: string = '';
  byYear: IRayonProgramFinanceByYear[] = [];

  build(i?: IRayonProgramFinanceGeneral) {
    super.build(i);
    if (i) {
      this.byYear = RayonProgramFinanceByYear.buildArray(i.byYear);
    }
  }
}
