import {DocumentBase} from '@reinform-cdp/core';
import {IRayonProgramInstruction} from './abstracts/IRayonProgramInstruction';
import {map} from 'lodash';

export class RayonProgramInstruction extends DocumentBase<IRayonProgramInstruction, RayonProgramInstruction> {
  instructionId = '';
  instructionReq = '';

  static buildArray(values: IRayonProgramInstruction[]): RayonProgramInstruction[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonProgramInstruction();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
