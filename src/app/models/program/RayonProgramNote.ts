import {DocumentBase} from '@reinform-cdp/core';
import {IRayonProgramNote} from './abstracts/IRayonProgramNote';
import {map} from 'lodash';

export class RayonProgramNote extends DocumentBase<IRayonProgramNote, RayonProgramNote> {
  task: string = '';
  noteText: string = '';
  noteAuthor: string = '';
  noteAuthorFIO: string = '';
  noteDate: Date = null;

  build(i?: IRayonProgramNote) {
    super.build(i);
  }

  static buildArray(values: IRayonProgramNote[]): RayonProgramNote[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonProgramNote();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
