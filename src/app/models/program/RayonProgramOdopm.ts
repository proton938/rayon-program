import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramOdopm} from "./abstracts/IRayonProgramOdopm";

export class RayonProgramOdopm extends DocumentBase<IRayonProgramOdopm, RayonProgramOdopm> {
  odopmDatasetId: string = '';
  odopmId: string = '';
}
