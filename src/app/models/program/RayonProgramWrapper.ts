import {DocumentBase} from '@reinform-cdp/core';
import {RayonProgram} from './RayonProgram';
import {IRayonProgramWrapper} from './abstracts/IRayonProgramWrapper';

export class RayonProgramWrapper extends DocumentBase<IRayonProgramWrapper, RayonProgramWrapper> {
  obj: RayonProgram = new RayonProgram();

  build(i?: IRayonProgramWrapper) {
    super.build(i);
    if (i) {
      this.obj.build(i.obj);
    }
  }
}
