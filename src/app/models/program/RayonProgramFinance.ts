import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {RayonProgramFinanceCost} from './RayonProgramFinanceCost';
import {IRayonProgramFinance} from './abstracts/IRayonProgramFinance';
import {RayonProgramFinanceGeneral} from "./RayonProgramFinanceGeneral";
import {RayonProgramFinanceAdditional} from "./RayonProgramFinanceAdditional";

export class RayonProgramFinance extends DocumentBase<IRayonProgramFinance, RayonProgramFinance> {
  budget: boolean = null;
  source: NSIDocumentType = null;
  stateProgram: NSIDocumentType = null;
  stateCustomer: NSIDocumentType = null;
  event = '';
  workType = '';
  cost: RayonProgramFinanceCost = new RayonProgramFinanceCost();
  general: RayonProgramFinanceGeneral = new RayonProgramFinanceGeneral();
  additional: RayonProgramFinanceAdditional = new RayonProgramFinanceAdditional();

  build(i?: IRayonProgramFinance) {
    super.build(i);
    if (i) {
      if (i.source) {
        this.source = new NSIDocumentType();
        this.source.build(i.source);
      }
      if (i.stateProgram) {
        this.stateProgram = new NSIDocumentType();
        this.stateProgram.build(i.stateProgram);
      }
      if (i.stateCustomer) {
        this.stateCustomer = new NSIDocumentType();
        this.stateCustomer.build(i.stateCustomer);
      }
      this.general.build(i.general);
      this.additional.build(i.additional);
      this.cost.build(i.cost);
    }
  }
}
