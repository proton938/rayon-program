import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramInfo3D} from "./abstracts/IRayonProgramInfo3D";

export class RayonProgramInfo3D extends DocumentBase<IRayonProgramInfo3D, RayonProgramInfo3D> {
  header: string = '';
  briefInfo: string = '';
  theses: string[] = [];

  build(i?: IRayonProgramInfo3D) {
    super.build(i);
    if (i) {
      this.theses = i.theses;
    }
  }
}
