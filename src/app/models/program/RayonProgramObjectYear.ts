import {DocumentBase, RangeDocumentType} from '@reinform-cdp/core';
import {IRayonProgramObjectYear} from './abstracts/IRayonProgramObjectYear';

export class RayonProgramObjectYear extends DocumentBase<IRayonProgramObjectYear, RayonProgramObjectYear> {
  period: number = null;
  comment: string = '';
  psd: RangeDocumentType = new RangeDocumentType();
  smr: RangeDocumentType = new RangeDocumentType();

  build(i?: IRayonProgramObjectYear) {
    super.build(i);
    if (i) {
      this.psd.build(i.psd);
      this.smr.build(i.smr);
    }
  }
}
