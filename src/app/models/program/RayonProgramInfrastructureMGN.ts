import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramInfo3D} from "./abstracts/IRayonProgramInfo3D";
import {IRayonProgramInfrastructureMGN} from "./abstracts/IRayonProgramInfrastructureMGN";
import {map} from "lodash";

export class RayonProgramInfrastructureMGN extends DocumentBase<IRayonProgramInfrastructureMGN, RayonProgramInfrastructureMGN> {
  name: string = '';
  location: string = '';
  technicalStatus: string = '';
  balanceholder: string = '';
  installationDate: Date = null;
  exploitationDate: Date = null;
  actNumber: string = '';

  build(i?: IRayonProgramInfrastructureMGN) {
    super.build(i);
  }

  static buildArray(values: IRayonProgramInfrastructureMGN[]): RayonProgramInfrastructureMGN[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonProgramInfrastructureMGN();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
