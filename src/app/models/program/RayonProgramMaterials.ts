import {DocumentBase} from '@reinform-cdp/core';
import {IRayonProgramMaterials} from "./abstracts/IRayonProgramMaterials";
import {RayonProgramLocation} from "./RayonProgramLocation";

export class RayonProgramMaterials extends DocumentBase<IRayonProgramMaterials, RayonProgramMaterials> {
  map = '';
  map3d = '';
  panorama = '';
  panorama3d = '';
  video: string[] = [];
  location3D: RayonProgramLocation = new RayonProgramLocation();

  build(i?: IRayonProgramMaterials) {
    super.build(i);
    if (i) {
      this.video = i.video;
      this.location3D.build(i.location3D);
    }
  }
}
