import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramLocation} from "./abstracts/IRayonProgramLocation";

export class RayonProgramLocation extends DocumentBase<IRayonProgramLocation, RayonProgramLocation> {
  EPSG: string = '';
  x: string = '';
  y: string = '';
}
