import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramMos} from "./abstracts/IRayonProgramMos";

export class RayonProgramMos extends DocumentBase<IRayonProgramMos, RayonProgramMos> {
  name: string = '';
  description: string = '';
  'public': boolean = null;
  risk: string = '';
  comment: string = '';
  news: string = '';
}
