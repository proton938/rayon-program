import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramFinanceAdditional} from "./abstracts/IRayonProgramFinanceAdditional";
import {RayonProgramFinanceByYear} from "./RayonProgramFinanceByYear";

export class RayonProgramFinanceAdditional extends DocumentBase<IRayonProgramFinanceAdditional, RayonProgramFinanceAdditional> {
  required: boolean = null;
  additional: string = '';
  byYear: RayonProgramFinanceByYear[] = [];

  build(i?: IRayonProgramFinanceAdditional) {
    super.build(i);
    if (i) {
      this.byYear = RayonProgramFinanceByYear.buildArray(i.byYear);
    }
  }
}
