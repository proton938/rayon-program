import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramFinanceByYear} from "./abstracts/IRayonProgramFinanceByYear";
import {map} from "lodash";

export class RayonProgramFinanceByYear extends DocumentBase<IRayonProgramFinanceByYear, RayonProgramFinanceByYear> {
  year: number = null;
  total: string = '';

  static buildArray(values: IRayonProgramFinanceByYear[] = []): RayonProgramFinanceByYear[] {
    let result = (values && values.length) ? map(values, u => {
      let value = new RayonProgramFinanceByYear();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
