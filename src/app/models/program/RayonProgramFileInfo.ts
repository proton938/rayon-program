import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramFileInfo} from "./abstracts/IRayonProgramFileInfo";
import {map} from 'lodash';

export class RayonProgramFileInfo extends DocumentBase<IRayonProgramFileInfo, RayonProgramFileInfo> {
  id: string = '';
  type: string = '';
  date: string = '';
  unpublished: boolean = null;

  static buildArray(values: IRayonProgramFileInfo[]): RayonProgramFileInfo[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonProgramFileInfo();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
