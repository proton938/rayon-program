import {DocumentBase} from '@reinform-cdp/core';
import {IRayonProgramFinanceCost} from './abstracts/IRayonProgramFinanceCost';

export class RayonProgramFinanceCost extends DocumentBase<IRayonProgramFinanceCost, RayonProgramFinanceCost> {
  main = '';
  additional = '';
}
