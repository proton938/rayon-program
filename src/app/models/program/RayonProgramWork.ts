import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";
import {IRayonProgramWork} from "./abstracts/IRayonProgramWork";
import {map} from "lodash";

export class RayonProgramWork extends DocumentBase<IRayonProgramWork, RayonProgramWork> {
  type: NSIDocumentType = null;
  start: number = null;
  finish: number = null;
  description: string = '';

  build(i?: IRayonProgramWork) {
    super.build(i);
    if (i) {
      if (i.type) {
        this.type = new NSIDocumentType();
        this.type.build(i.type);
      }
    }
  }

  static buildArray(values: IRayonProgramWork[]): RayonProgramWork[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonProgramWork();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
