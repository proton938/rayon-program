import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgram} from './abstracts/IRayonProgram';
import {RayonProgramAddress} from './RayonProgramAddress';
import {RayonProgramObject} from './RayonProgramObject';
import {RayonProgramResponsible} from './RayonProgramResponsible';
import {RayonProgramParameterZu} from './RayonProgramParameterZu';
import {RayonProgramParameterObject} from './RayonProgramParameterObject';
import {RayonProgramMaterials} from './RayonProgramMaterials';
import {RayonProgramOdopm} from './RayonProgramOdopm';
import {RayonProgramSpecific} from './RayonProgramSpecific';
import {RayonProgramMos} from './RayonProgramMos';
import {RayonProgramDominant} from './RayonProgramDominant';
import {RayonProgramInfo3D} from './RayonProgramInfo3D';
import {RayonProgramFileInfo} from './RayonProgramFileInfo';
import {RayonSpecialObject} from './RayonSpecialObject';
import {RayonProgramInfrastructureMGN} from './RayonProgramInfrastructureMGN';
import {RayonProgramNote} from './RayonProgramNote';

export class RayonProgram extends DocumentBase<IRayonProgram, RayonProgram> {
  documentId = '';
  documentDate: Date = null;
  folderId = '';
  aipId = '';
  odopm = new RayonProgramOdopm();
  odsId = '';
  oksId = '';
  outerDate: Date = null;
  outerReason = '';
  odsOdhId = '';
  id2Gis = '';
  requestId = '';
  requestNumber = '';
  itemType: NSIDocumentType = null;
  objectForm: NSIDocumentType = null;
  address: RayonProgramAddress = new RayonProgramAddress();
  object: RayonProgramObject = new RayonProgramObject();
  comment = '';
  responsible: RayonProgramResponsible = new RayonProgramResponsible();
  problem: boolean = null;
  attraction: boolean = null;
  okn: boolean = null;
  'public': boolean = null;
  risk = '';
  wasIs: boolean = null;
  commercialObject: boolean = null;
  masterplanObject: boolean = null;
  materials: RayonProgramMaterials = new RayonProgramMaterials();
  addOrganization: NSIDocumentType = null;
  parameterZu: RayonProgramParameterZu = new RayonProgramParameterZu();
  parameterObject: RayonProgramParameterObject = new RayonProgramParameterObject();
  sample: boolean = null;
  specific: RayonProgramSpecific[] = [];
  mos: RayonProgramMos = new RayonProgramMos();
  dominant: RayonProgramDominant = new RayonProgramDominant();
  rubricator: NSIDocumentType[] = [];
  info3D: RayonProgramInfo3D = new RayonProgramInfo3D();
  files: RayonProgramFileInfo[] = [];
  archive: boolean = null;
  comments: RayonProgramNote[] = [];
  specialObject: RayonSpecialObject = null;
  infrastructureMGN: RayonProgramInfrastructureMGN[] = [];
  correctionReason = '';

  build(i?: IRayonProgram) {
    super.build(i);
    if (i) {
      this.odopm.build(i.odopm);
      this.address.build(i.address);
      this.object.build(i.object);
      this.responsible.build(i.responsible);
      if (i.addOrganization) {
        this.addOrganization = new NSIDocumentType();
        this.addOrganization.build(i.addOrganization);
      }
      if (i.itemType) {
        this.itemType = new NSIDocumentType();
        this.itemType.build(i.itemType);
      }
      if (i.objectForm) {
        this.objectForm = new NSIDocumentType();
        this.objectForm.build(i.objectForm);
      }
      this.parameterZu.build(i.parameterZu);
      this.parameterObject.build(i.parameterObject);
      this.materials.build(i.materials);
      this.specific = RayonProgramSpecific.buildArray(i.specific);
      this.mos.build(i.mos);
      this.dominant.build(i.dominant);
      this.rubricator = NSIDocumentType.buildArray(i.rubricator);
      this.info3D.build(i.info3D);
      this.files = RayonProgramFileInfo.buildArray(i.files);
      if (i.specialObject) {
        this.specialObject = new RayonSpecialObject();
        this.specialObject.build(i.specialObject);
      }
      this.comments = RayonProgramNote.buildArray(i.comments);
      this.infrastructureMGN = RayonProgramInfrastructureMGN.buildArray(i.infrastructureMGN);
    }
  }
}
