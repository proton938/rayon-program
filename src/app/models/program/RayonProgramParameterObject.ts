import {DocumentBase, RangeDocumentType} from '@reinform-cdp/core';
import {IRayonProgramParameterObject} from "./abstracts/IRayonProgramParameterObject";

export class RayonProgramParameterObject extends DocumentBase<IRayonProgramParameterObject, RayonProgramParameterObject> {
  constructionYear = '';
  use = '';
  floor = '';
  wallMaterial = '';
  wearRate = '';
  capacity = '';
}
