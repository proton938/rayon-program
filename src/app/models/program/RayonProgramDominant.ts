import {DocumentBase} from "@reinform-cdp/core";
import {IRayonProgramDominant} from "./abstracts/IRayonProgramDominant";
import {RayonProgramLocation} from "./RayonProgramLocation";

export class RayonProgramDominant extends DocumentBase<IRayonProgramDominant, RayonProgramDominant> {
  dominantIs: boolean = null;
  locationDominant: RayonProgramLocation = new RayonProgramLocation();

  build(i?: IRayonProgramDominant) {
    super.build(i);
    if (i) {
      this.locationDominant.build(i.locationDominant);
    }
  }
}
