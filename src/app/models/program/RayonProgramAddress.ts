import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgramAddress} from './abstracts/IRayonProgramAddress';
import {toString} from "lodash";

export class RayonProgramAddress extends DocumentBase<IRayonProgramAddress, RayonProgramAddress> {
  prefect: NSIDocumentType[] = [];
  district: NSIDocumentType[] = [];
  address: string = '';
  addressId: string = null;

  build(i?: IRayonProgramAddress) {
    super.build(i);
    if (i) {
      this.addressId = toString(i.addressId); // If do not to use this method (toString) ng-select does not show current value
      this.prefect = NSIDocumentType.buildArray(i.prefect);
      this.district = NSIDocumentType.buildArray(i.district);
    }
  }
}
