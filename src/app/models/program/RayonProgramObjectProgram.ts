import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgramObjectProgram} from './abstracts/IRayonProgramObjectProgram';

export class RayonProgramObjectProgram extends DocumentBase<IRayonProgramObjectProgram, RayonProgramObjectProgram> {
  inclusion: boolean = null;
}
