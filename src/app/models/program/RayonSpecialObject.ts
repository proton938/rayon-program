import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonSpecialObject} from "./abstracts/IRayonSpecialObject";

export class RayonSpecialObject extends DocumentBase<IRayonSpecialObject, RayonSpecialObject> {
  code: string = '';
  name: string = '';

  build(i?: IRayonSpecialObject) {
    super.build(i);
    if (i) {
      this.code = i.code;
      this.name = i.name;
    }
  }
}
