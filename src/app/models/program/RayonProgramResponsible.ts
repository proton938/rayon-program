import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgramResponsible} from './abstracts/IRayonProgramResponsible';

export class RayonProgramResponsible extends DocumentBase<IRayonProgramResponsible, RayonProgramResponsible> {
  organization: NSIDocumentType = null;

  build(i?: IRayonProgramResponsible) {
    super.build(i);
    if (i) {
      if (i.organization) {
        this.organization = new NSIDocumentType();
        this.organization.build(i.organization);
      }
    }
  }
}
