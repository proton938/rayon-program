import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';

export interface IRayonProgramObjectProgram extends IDocumentBase {
  inclusion?: boolean; // Включение в программу
}
