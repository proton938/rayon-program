import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramOdopm extends IDocumentBase {
  odopmDatasetId?: string; // Идентификатор набора данных ОДОПМ
  odopmId?: string; // Идентификатор объекта в ОДОПМ
}
