import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramParameterZu extends IDocumentBase {
  cadastralNumber?: string[]; // Кадастровый номер земельного участка
  landCategory?: string; // Категория земель
  permittedUse?: string; // Вид разрешенного использования
  gpzuNumber?: string; // Номер ГПЗУ
  gpzuDate?: Date; // Дата ГПЗУ
}
