import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramParameterObject extends IDocumentBase {
  constructionYear?: string; // Год постройки
  use?: string; // Назначение
  floor?: string; // Этажность
  wallMaterial?: string; // Материал
  wearRate?: string; // Процент износа
  capacity?: string; // Вместимость
}
