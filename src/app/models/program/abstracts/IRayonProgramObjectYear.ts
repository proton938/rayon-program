import {IDocumentBase, IRangeDocumentType} from '@reinform-cdp/core';

export interface IRayonProgramObjectYear extends IDocumentBase {
  period?: number; // Срок реализации задач
  comment?: string; // Информация о ходе реализации задач
  psd?: IRangeDocumentType; // Предлагаемый год ПСД
  smr?: IRangeDocumentType; // Предлагаемый год СМР
}
