import {IDocumentBase} from '@reinform-cdp/core';

export interface IRayonProgramFinanceCost extends IDocumentBase {
  main?: string; // Предусмотренная стоимость, млн. руб
  additional?: string; // Стоимость дополнительного финансирвования, млн. руб
}
