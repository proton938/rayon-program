import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';

export interface IRayonSpecialObject extends IDocumentBase {
  code?: string;
  name?: string;
}
