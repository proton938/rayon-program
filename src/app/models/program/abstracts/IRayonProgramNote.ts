import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramNote extends IDocumentBase {
  task?: string; // Задача
  noteText?: string; // Текст
  noteAuthor?: string; // Автор
  noteAuthorFIO?: string; // Автор ФИО
  noteDate?: string; // Дата
}
