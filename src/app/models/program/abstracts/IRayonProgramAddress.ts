import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';

export interface IRayonProgramAddress extends IDocumentBase {
  prefect?: INSIDocumentType[]; // Округ
  district?: INSIDocumentType[]; // Район
  address?: string; // Адресгый ориентир
  addressId?: string; // Идентификатор адреса БТИ
}
