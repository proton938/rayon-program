import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';

export interface IRayonProgramResponsible extends IDocumentBase {
  organization?: INSIDocumentType; // Ответственная организация
}
