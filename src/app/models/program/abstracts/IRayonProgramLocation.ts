import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramLocation extends IDocumentBase { // Геокоординаты
  EPSG: string; // Система координат
  x: string; // Широта
  y: string; // Долгота
}
