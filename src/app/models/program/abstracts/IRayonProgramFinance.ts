import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgramFinanceCost} from './IRayonProgramFinanceCost';
import {IRayonProgramFinanceGeneral} from "./IRayonProgramFinanceGeneral";
import {IRayonProgramFinanceAdditional} from "./IRayonProgramFinanceAdditional";

export interface IRayonProgramFinance extends IDocumentBase {
  budget?: boolean; // Финансовое обеспечение
  source?: INSIDocumentType; // Источник финансирования
  stateProgram?: INSIDocumentType; // Государственная программа
  stateCustomer?: INSIDocumentType; // Государственный заказчик
  event?: string; // Мероприятие
  workType?: string; // Вид работ
  cost?: IRayonProgramFinanceCost; // Стоимость
  general?: IRayonProgramFinanceGeneral; // Общий объем финансирования
  additional?: IRayonProgramFinanceAdditional; // Объем дополнительного финансирования
}
