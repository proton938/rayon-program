import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgramObjectProgram} from './IRayonProgramObjectProgram';

export interface IRayonProgramObject extends IDocumentBase {
  industry?: INSIDocumentType; // Индустрия объекта
  kind?: INSIDocumentType; // Тип объекта или мероприятия
  ownership?: INSIDocumentType; // Форма собственности
  period?: INSIDocumentType; // Период возведения
  typeObject?: INSIDocumentType; // Вид объекта
  name?: string; // Наименование объекта
  description?: string; // Краткое описание объекта
  extent?: string; // Протяженность, км
  area?: string; // Площадь участка, га
  areaObject?: string; // Площадь объекта, кв.м
  programm?: IRayonProgramObjectProgram; // Программа
}
