import {IDocumentBase} from "@reinform-cdp/core";
import {IRayonProgramFinanceByYear} from "./IRayonProgramFinanceByYear";

export interface IRayonProgramFinanceGeneral extends IDocumentBase {
  general?: string; // Общий объем финансирования, руб
  byYear?: IRayonProgramFinanceByYear[]; // Общий объем финансирования по годам
}
