import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramMos extends IDocumentBase {
  name?: string; // Наименование объекта
  description?: string; // Краткое описание объекта
  'public'?: boolean; // К публикации на открытых информационных ресурсах
  risk?: string; // Риски публикации
  comment?: string; // Примечание
  news?: string; // Ссылки на новости по объекту
}
