import {IDocumentBase} from "@reinform-cdp/core";
import {IRayonProgramFinanceByYear} from "./IRayonProgramFinanceByYear";

export interface IRayonProgramFinanceAdditional extends IDocumentBase {
  required?: boolean; // Требуется дополнительное финансирование
  additional?: string; // Объем дополнительного финансирования, руб
  byYear?: IRayonProgramFinanceByYear[]; // Объем дополнительного финасирования по годам
}
