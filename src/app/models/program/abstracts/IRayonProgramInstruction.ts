import {IDocumentBase} from '@reinform-cdp/core';

export interface IRayonProgramInstruction extends IDocumentBase {
  instructionId?: string; // Идентификатор связанного поручения
  instructionReq?: string; // Реквизиты связанного поручения
}
