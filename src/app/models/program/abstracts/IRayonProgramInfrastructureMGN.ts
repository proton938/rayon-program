import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramInfrastructureMGN extends IDocumentBase {
  name?: string; // Наименование устройства
  location?: string; // Место расположения
  technicalStatus?: string; // Техническое состояние ППИ
  balanceholder?: string; // Балансодержатель
  installationDate?: string; // Дата монтажа
  exploitationDate?: string; // Дата ввода в эксплуатацию
  actNumber?: string; // Номер акта ввода в эксплуатацию
}
