import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';
import {IRayonProgramAddress} from './IRayonProgramAddress';
import {IRayonProgramObject} from './IRayonProgramObject';
import {IRayonProgramResponsible} from './IRayonProgramResponsible';
import {IRayonProgramParameterZu} from "./IRayonProgramParameterZu";
import {IRayonProgramParameterObject} from "./IRayonProgramParameterObject";
import {IRayonProgramMaterials} from "./IRayonProgramMaterials";
import {IRayonProgramOdopm} from "./IRayonProgramOdopm";
import {IRayonProgramSpecific} from "./IRayonProgramSpecific";
import {IRayonProgramMos} from "./IRayonProgramMos";
import {IRayonProgramDominant} from "./IRayonProgramDominant";
import {IRayonProgramInfo3D} from "./IRayonProgramInfo3D";
import {IRayonProgramFileInfo} from "./IRayonProgramFileInfo";
import {IRayonSpecialObject} from "./IRayonSpecialObject";
import {IRayonProgramInfrastructureMGN} from "./IRayonProgramInfrastructureMGN";
import {IRayonProgramNote} from "./IRayonProgramNote";

export interface IRayonProgram extends IDocumentBase {
  documentId?: string; // Идентификатор документа
  documentDate?: Date; // Дата документа
  folderId?: string; // Идентификатор папки в Alfresco
  aipId?: string; // Идентификатор объекта АИП
  odopm?: IRayonProgramOdopm; // Источник объекта в ОДОПМ
  odsId?: string; // Идентификатор объекта в АСУ ОДС
  oksId?: string; // Идентификатор ОКС
  outerDate?: string; // Дата добавления/изменения/удаления во внешней ИС
  outerReason?: string; // Причина добавления/изменения/удаления во внешней ИС
  odsOdhId?: string; // Идентификатор объекта в АСУ ОДС (ОДХ)
  id2Gis?: string; // Идентификатор филиала 2GIS
  requestId?: string; // Идентификатор заявки
  requestNumber?: string; // Порядковый номер заявки
  itemType?: INSIDocumentType; // Тип элемента
  objectForm?: INSIDocumentType; // Тип объекта
  address?: IRayonProgramAddress; // Адресные данные
  object?: IRayonProgramObject;
  comment?: string; // Примечание
  responsible?: IRayonProgramResponsible; // Ответственные
  problem?: boolean; // Проблемный объект
  attraction?: boolean; // Место притяжения
  okn?: boolean; // Объект культурного наследия
  'public'?: boolean; // К публикации на открытых информационных ресурсах
  risk?: string; // Риски публикации
  wasIs?: boolean; // Признак возможности отображения  на карте "было-стало"
  commercialObject?: boolean; // Признак "Коммерческий объект"
  masterplanObject?: boolean; // Признак "Объект мастер-плана"
  materials?: IRayonProgramMaterials; // Материалы
  addOrganization?: INSIDocumentType; // Организация, добавившая объект
  parameterZu?: IRayonProgramParameterZu; // Характеристики земельного участка
  parameterObject?: IRayonProgramParameterObject; // Характеристики объекта
  sample?: boolean; // Образовый объект
  specific?: IRayonProgramSpecific[]; // Специфический атрибут документа
  mos?: IRayonProgramMos; // Дополнительные поля для публикации объекта на внешнем контуре
  dominant?: IRayonProgramDominant; // Уникальный объект (доминант)
  rubricator?: INSIDocumentType[]; // Рубрикатор УКК
  info3D?: IRayonProgramInfo3D; // Описание объекта для 3D - инфографики
  files?: IRayonProgramFileInfo[]; // Файлы
  archive?: boolean; // Архивный объект
  comments?: IRayonProgramNote[]; //Комментарии к задачам
  specialObject?: IRayonSpecialObject; // Особый объект
  infrastructureMGN?: IRayonProgramInfrastructureMGN[]; // Устройства для маломобильных групп населения
  correctionReason?: string; // Причина изменения
}
