import {IDocumentBase, NSIDocumentType} from "@reinform-cdp/core";

export interface IRayonProgramWork extends IDocumentBase {
  type?: NSIDocumentType; // Вид работ
  start?: number; // Начало периода проведения работ
  finish?: number; // Окончание периода проведения работ
  description?: string; // Краткое описание выполненных рабт
}
