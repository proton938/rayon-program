import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramFinanceByYear extends IDocumentBase {
  year?: number; // Год
  total?: string; // Объем финансирования, руб
}
