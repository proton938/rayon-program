import {IDocumentBase} from '@reinform-cdp/core';
import {IRayonProgram} from './IRayonProgram';

export interface IRayonProgramWrapper extends IDocumentBase {
  obj: IRayonProgram; // Корневой элемент
}
