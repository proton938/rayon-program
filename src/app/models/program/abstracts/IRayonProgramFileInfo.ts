import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramFileInfo extends IDocumentBase {
  id?: string; // Идентификатор файла
  type?: string; //Класс файла
  date?: string; // Дата и время обновления
  unpublished?: boolean; // Непубликуемый документ
}
