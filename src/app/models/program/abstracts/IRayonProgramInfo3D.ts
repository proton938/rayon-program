import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonProgramInfo3D extends IDocumentBase {
  header?: string; // Заголовок
  briefInfo?: string; // Краткая информация
  theses?: string[]; // Тезис
}
