import {IDocumentBase} from "@reinform-cdp/core";
import {IRayonProgramLocation} from "./IRayonProgramLocation";

export interface IRayonProgramDominant extends IDocumentBase {
  dominantIs?: boolean; // Признак "Доминантный объект"
  locationDominant?: IRayonProgramLocation; // Геокоординаты центра картинки доминантного объекта
}
