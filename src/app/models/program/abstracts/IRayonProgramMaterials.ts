import {IDocumentBase} from '@reinform-cdp/core';
import {IRayonProgramLocation} from "./IRayonProgramLocation";

export interface IRayonProgramMaterials extends IDocumentBase {
  map?: string; // Карта
  map3d?: string; // 3D-карта
  panorama?: string; // Панорама URL было-стало
  panorama3d?: string; // Панорама URL 3d
  video?: string[]; // Видео URL
  location3D?: IRayonProgramLocation; // Геокоординаты центра изображения 3D-объекта
}
