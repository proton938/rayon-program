import {IRayonSchema, IRayonSchemaAttribute} from "./abstracts/IRayonSchema";

export class RayonSchemaAttribute {
  name: string = '';
  description: string = '';
  isComplex: boolean = null;
  arity: string = '';
  type: string = '';
  attributes: RayonSchemaAttribute[] = [];

  constructor(i?: IRayonSchemaAttribute) {
    if (i) {
      ['name', 'description', 'arity', 'type'].forEach(p => {
        if (i[p]) this[p] = i[p];
      });
      this.isComplex = i.isComplex || null;
      if (i.attributes) this.attributes = i.attributes.map(attr => new RayonSchemaAttribute(attr));
    }
  }
}

export class RayonSchema {
  name: string = '';
  description: string = '';
  isComplex: boolean = null;
  arity: string = '';
  attributes: RayonSchemaAttribute[] = [];

  constructor(i?: IRayonSchema) {
    if (i) {
      ['name', 'description', 'arity'].forEach(p => {
        if (i[p]) this[p] = i[p];
      });
      this.isComplex = i.isComplex || null;
      if (i.attributes) {
        this.attributes = i.attributes.map(attr => new RayonSchemaAttribute(attr));
      }
    }
  }
}
