export interface IRayonSchemaAttribute {
  name?: string;
  description?: string;
  isComplex?: boolean;
  arity?: string;
  type?: string;
  attributes?: IRayonSchemaAttribute[];
}

export interface IRayonSchema {
  name?: string;
  description?: string;
  isComplex?: boolean;
  arity?: string;
  attributes?: any[];
}
