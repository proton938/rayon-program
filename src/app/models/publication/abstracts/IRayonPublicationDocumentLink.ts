import {IDocumentBase} from "@reinform-cdp/core";

export interface IRayonPublicationDocumentLink extends IDocumentBase {
  idLink?: string; // Идентификатор связанного документа
  typeLink?: string; // Тип связанного документа
  geoLink?: boolean; // Наличие геометрии
  checkedLink?: boolean; // Признак "Проверено" модератором АНО
  checkedMosLink?: boolean; // Признак "Проверено" модератором mos.ru
  agreedLink?: boolean; // Признак "Согласовано АНО"
  agreedMosLink?: boolean; // Признак "Согласовано mos.ru"
}
