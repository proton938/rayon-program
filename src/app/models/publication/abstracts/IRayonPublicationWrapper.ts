import {IDocumentBase} from "@reinform-cdp/core";
import {IRayonPublication} from "./IRayonPublication";

export interface IRayonPublicationWrapper extends IDocumentBase {
  publication: IRayonPublication;
}
