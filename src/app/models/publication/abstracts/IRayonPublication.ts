import {IDocumentBase, INSIDocumentType} from "@reinform-cdp/core";
import {IRayonPublicationExecutor} from "./IRayonPublicationExecutor";
import {IRayonPublicationRequestContent} from "./IRayonPublicationRequestContent";
import {IRayonPublicationDocument} from "./IRayonPublicationDocument";
// Заявка на публикацию данных
export interface IRayonPublication extends IDocumentBase {
  documentId?: string; // Идентификатор документа
  folderId?: string; // Идентификатор папки в Alfresco
  requestDate?: string; // Дата заявки
  selection?: boolean; // Признак заявки, созданной через форму
  requestContent?: IRayonPublicationRequestContent; // Содержание запроса (для пакетов)
  document?: IRayonPublicationDocument[]; // Документ для модерации
  status?: INSIDocumentType; // Статус заявки
  executors?: IRayonPublicationExecutor[]; // Исполнители
}
