import {IDocumentBase, INSIDocumentType} from "@reinform-cdp/core";
import {IRayonPublicationDocumentLink} from "./IRayonPublicationDocumentLink";

export interface IRayonPublicationDocument extends IDocumentBase {
  id?: string; // Идентификатор документа
  type?: string; // Тип документа
  name?: string; // Наименование объекта / работы
  address?: string; // Адрес
  workType?: INSIDocumentType; // Вид работы
  geo?: boolean; // Наличие геометрии
  checked?: boolean; // Признак "Проверено" модератором АНО
  checkedMos?: boolean; // Признак "Проверено" модератором mos.ru
  agreed?: boolean; // Признак "Согласовано АНО"
  agreedMos?: boolean; // Признак "Согласовано mos.ru"
  documentLink?: IRayonPublicationDocumentLink[]; // Связанный документ
}
