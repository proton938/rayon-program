import {IDocumentBase, IUserDocumentType} from "@reinform-cdp/core";

export interface IRayonPublicationExecutor extends IDocumentBase {
  role?: string; // Роль
  executor?: IUserDocumentType; // Исполнитель
}
