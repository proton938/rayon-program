import {IDocumentBase, ITepDocumentType, IUserDocumentType} from "@reinform-cdp/core";

export interface IRayonPublicationRequestContent extends IDocumentBase {
  parameter?: ITepDocumentType[]; // Параметр отбора
  requestAuthor?: IUserDocumentType; // Автор заявки
}
