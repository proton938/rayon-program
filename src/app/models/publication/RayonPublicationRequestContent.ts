import {DocumentBase, TepDocumentType, UserDocumentType} from "@reinform-cdp/core";
import {IRayonPublicationRequestContent} from "./abstracts/IRayonPublicationRequestContent";

export class RayonPublicationRequestContent extends DocumentBase<IRayonPublicationRequestContent, RayonPublicationRequestContent> {
  parameter: TepDocumentType[] = [];
  requestAuthor: UserDocumentType = null;

  build(i?: IRayonPublicationRequestContent) {
    super.build(i);
    if (i) {
      this.parameter = TepDocumentType.buildArray(i.parameter);
      if (i.requestAuthor) {
        this.requestAuthor = new UserDocumentType();
        this.requestAuthor.build(i.requestAuthor);
      }
    }
  }
}
