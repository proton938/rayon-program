import {DocumentBase} from "@reinform-cdp/core";
import {IRayonPublicationDocumentLink} from "./abstracts/IRayonPublicationDocumentLink";
import {map} from 'lodash';

export class RayonPublicationDocumentLink extends DocumentBase<IRayonPublicationDocumentLink, RayonPublicationDocumentLink> {
  idLink: string = '';
  typeLink: string = '';
  geoLink: boolean = null;
  checkedLink: boolean = null;
  checkedMosLink: boolean = null;
  agreedLink: boolean = null;
  agreedMosLink: boolean = null;

  static buildArray(values: IRayonPublicationDocumentLink[]): RayonPublicationDocumentLink[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonPublicationDocumentLink();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
