import {DocumentBase} from "@reinform-cdp/core";
import {IRayonPublicationWrapper} from "./abstracts/IRayonPublicationWrapper";
import {RayonPublication} from "./RayonPublication";

export class RayonPublicationWrapper extends DocumentBase<IRayonPublicationWrapper, RayonPublicationWrapper> {
  publication: RayonPublication = new RayonPublication();

  build(i?: IRayonPublicationWrapper) {
    super.build(i);
    if (i) {
      this.publication.build(i.publication);
    }
  }
}
