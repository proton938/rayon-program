import {DocumentBase, UserDocumentType} from "@reinform-cdp/core";
import {IRayonPublicationExecutor} from "./abstracts/IRayonPublicationExecutor";
import {map} from "lodash";

export class RayonPublicationExecutor extends DocumentBase<IRayonPublicationExecutor, RayonPublicationExecutor> {
  role: string = '';
  executor: UserDocumentType = null;

  build(i?: IRayonPublicationExecutor) {
    super.build(i);
    if (i) {
      if (i.executor) {
        this.executor = new UserDocumentType();
        this.executor.build(i.executor);
      }
    }
  }

  static buildArray(values: IRayonPublicationExecutor[]): RayonPublicationExecutor[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonPublicationExecutor();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
