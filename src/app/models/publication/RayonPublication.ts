import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";
import {IRayonPublication} from "./abstracts/IRayonPublication";
import {RayonPublicationExecutor} from "./RayonPublicationExecutor";
import {RayonPublicationRequestContent} from "./RayonPublicationRequestContent";
import {RayonPublicationDocument} from "./RayonPublicationDocument";
import {fromJson, toJson} from "@uirouter/core";
import {copy} from 'angular';

export class RayonPublication extends DocumentBase<IRayonPublication, RayonPublication> {
  documentId: string = '';
  folderId: string = '';
  requestDate: Date = null;
  selection: boolean = null;
  requestContent: RayonPublicationRequestContent = new RayonPublicationRequestContent();
  document: RayonPublicationDocument[] = [];
  status: NSIDocumentType = null;
  executors: RayonPublicationExecutor[] = [];

  build(i?: IRayonPublication) {
    super.build(i);
    if (i) {
      this.requestContent.build(i.requestContent);
      this.document = RayonPublicationDocument.buildArray(i.document);
      if (i.status) {
        this.status = new NSIDocumentType();
        this.status.build(i.status);
      }
      this.executors = RayonPublicationExecutor.buildArray(i.executors);
    }
  }

  convert(): IRayonPublication {
    let max = copy(this), min = max.min(), result = fromJson(toJson(max));
    result.requestDate = min.requestDate;
    return result;
  }
}
