import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";
import {IRayonPublicationDocument} from "./abstracts/IRayonPublicationDocument";
import {RayonPublicationDocumentLink} from "./RayonPublicationDocumentLink";
import {map} from 'lodash';

export class RayonPublicationDocument extends DocumentBase<IRayonPublicationDocument, RayonPublicationDocument> {
  id: string = '';
  type: string = '';
  name: string = '';
  address: string = '';
  workType: NSIDocumentType = null;
  geo: boolean = null;
  checked: boolean = null;
  checkedMos: boolean = null;
  agreed: boolean = null;
  agreedMos: boolean = null;
  documentLink: RayonPublicationDocumentLink[] = [];

  build(i?: IRayonPublicationDocument) {
    super.build(i);
    if (i) {
      if (i.workType) {
        this.workType = new NSIDocumentType();
        this.workType.build(i.workType);
      }
      this.documentLink = RayonPublicationDocumentLink.buildArray(i.documentLink);
    }
  }

  static buildArray(values: IRayonPublicationDocument[]): RayonPublicationDocument[] {
    const result = (values && values.length) ? map(values, (u) => {
      const value = new RayonPublicationDocument();
      value.build(u);
      return value;
    }) : [];
    return result;
  }
}
