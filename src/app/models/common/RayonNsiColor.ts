import {DocumentBase} from "@reinform-cdp/core";
import {IRayonNsiColor} from "./abstracts/IRayonNsiColor";

export class RayonNsiColor extends DocumentBase<IRayonNsiColor, RayonNsiColor> {
  code = '';
  name = '';
  color = '';

  build(i?: IRayonNsiColor) {
    super.build(i);
  }
}
