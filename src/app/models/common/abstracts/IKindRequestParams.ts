export interface IKindRequestParams {
  kindObject: string[];
  kindWork: string[],
  page: number,
  pageSize: number,
  sortType: string
}
