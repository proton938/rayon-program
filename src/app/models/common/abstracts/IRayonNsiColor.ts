import {INSIDocumentType} from "@reinform-cdp/core";

export interface IRayonNsiColor extends INSIDocumentType {
  color?: string; // Стиль для цвета
}
