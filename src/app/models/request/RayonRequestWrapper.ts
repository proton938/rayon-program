import {DocumentBase} from '@reinform-cdp/core';
import {IRayonRequestWrapper} from './abstracts/IRayonRequestWrapper';
import {RayonRequest} from './RayonRequest';

export class RayonRequestWrapper extends DocumentBase<IRayonRequestWrapper, RayonRequestWrapper> {
  request: RayonRequest = new RayonRequest();

  build(i?: IRayonRequestWrapper) {
    super.build(i);
    if (i) {
      this.request.build(i.request);
    }
  }
}
