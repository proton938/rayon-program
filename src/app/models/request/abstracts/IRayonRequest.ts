import {IDocumentBase, INSIDocumentType, IUserDocumentType} from '@reinform-cdp/core';
import {IRayonRequestReview} from './IRayonRequestReview';

export interface IRayonRequest extends IDocumentBase {
  documentId?: string;
  documentDate?: string;
  district?: INSIDocumentType;
  industry?: INSIDocumentType;
  organization?: INSIDocumentType;
  userStartProcess?: IUserDocumentType; // Пользователь, инициировавший процесс
  done?: boolean;
  review?: IRayonRequestReview; // Замечание
  prefecture?: boolean;
}
