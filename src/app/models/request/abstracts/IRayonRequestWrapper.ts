import {IDocumentBase} from '@reinform-cdp/core';
import {IRayonRequest} from './IRayonRequest';

export interface IRayonRequestWrapper extends IDocumentBase {
  request: IRayonRequest; // Корневой элемент
}
