import {IDocumentBase, INSIDocumentType, IUserDocumentType} from '@reinform-cdp/core';

export interface IRayonRequestReview extends IDocumentBase {
  comment?: string;
  date?: string;
  login?: string;
  fio?: string;
}
