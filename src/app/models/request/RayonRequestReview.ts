import {DocumentBase, NSIDocumentType, UserDocumentType} from '@reinform-cdp/core';
import {IRayonRequestReview} from './abstracts/IRayonRequestReview';

export class RayonRequestReview extends DocumentBase<IRayonRequestReview, RayonRequestReview> {
  comment = '';
  date: Date = null;
  login = '';
  fio = '';
}
