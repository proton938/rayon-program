import {DocumentBase, NSIDocumentType, UserDocumentType} from '@reinform-cdp/core';
import {IRayonRequest} from './abstracts/IRayonRequest';
import {RayonRequestReview} from './RayonRequestReview';

export class RayonRequest extends DocumentBase<IRayonRequest, RayonRequest> {
  documentId = '';
  documentDate: Date = null;
  district: NSIDocumentType = new NSIDocumentType();
  industry: NSIDocumentType = new NSIDocumentType();
  organization: NSIDocumentType = new NSIDocumentType();
  userStartProcess: UserDocumentType = new UserDocumentType();
  done: boolean = null;
  review: RayonRequestReview = new RayonRequestReview();
  prefecture: boolean = null;

  build(i?: IRayonRequest) {
    super.build(i);
    if (i) {
      this.district.build(i.district);
      this.industry.build(i.industry);
      this.organization.build(i.organization);
      this.userStartProcess.build(i.userStartProcess);
      this.review.build(i.review);
    }
  }
}
