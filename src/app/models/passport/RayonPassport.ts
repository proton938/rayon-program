import {DocumentBase, NSIDocumentType} from '@reinform-cdp/core';
import {IRayonPassport} from './abstracts/IRayonPassport';

export class RayonPassport extends DocumentBase<IRayonPassport, RayonPassport> {
  documentId = '';
  documentDate: Date = null;
  district: NSIDocumentType = new NSIDocumentType();

  build(i?: IRayonPassport) {
    super.build(i);
    if (i) {
      this.district.build(i.district);
    }
  }
}
