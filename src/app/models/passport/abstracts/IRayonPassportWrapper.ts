import {IDocumentBase} from '@reinform-cdp/core';
import {IRayonPassport} from './IRayonPassport';

export interface IRayonPassportWrapper extends IDocumentBase {
  passport: IRayonPassport; // Корневой элемент
}
