import {IDocumentBase, INSIDocumentType} from '@reinform-cdp/core';

export interface IRayonPassport extends IDocumentBase {
  documentId?: string;
  documentDate?: string;
  district?: INSIDocumentType;
}
