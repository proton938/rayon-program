import {DocumentBase} from '@reinform-cdp/core';
import {IRayonPassportWrapper} from './abstracts/IRayonPassportWrapper';
import {RayonPassport} from './RayonPassport';

export class RayonPassportWrapper extends DocumentBase<IRayonPassportWrapper, RayonPassportWrapper> {
  passport: RayonPassport = new RayonPassport();

  build(i?: IRayonPassportWrapper) {
    super.build(i);
    if (i) {
      this.passport.build(i.passport);
    }
  }
}
