import {DocumentBase, NSIDocumentType} from "@reinform-cdp/core";
import {IRayonPlan} from "./abstracts/IRayonPlan";
import {RayonNsiColor} from "../common/RayonNsiColor";

export class RayonPlan extends DocumentBase<IRayonPlan, RayonPlan> {
  documentId: string = '';
  documentDate: Date = null;
  folderId: string = '';
  planVersion: number = null;
  planNumber: string = '';
  planApproveDate: Date = null;
  planYear: number = null;
  planName: string = '';
  comment: string = '';
  prefect: NSIDocumentType = null;
  statusId: RayonNsiColor = null;
  files: string[] = [];

  build(i?: IRayonPlan) {
    super.build(i);
    if (i) {
      if (i.prefect) {
        this.prefect = new NSIDocumentType();
        this.prefect.build(i.prefect);
      }
      if (i.statusId) {
        this.statusId = new RayonNsiColor();
        this.statusId.build(i.statusId);
      }
      this.files = i.files || [];
    }
  }
}
