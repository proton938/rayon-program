import {DocumentBase} from "@reinform-cdp/core";
import {IRayonPlanWrapper} from "./abstracts/IRayonPlanWrapper";
import {RayonPlan} from "./RayonPlan";

export class RayonPlanWrapper extends DocumentBase<IRayonPlanWrapper, RayonPlanWrapper> {
  plan: RayonPlan = new RayonPlan();

  build(i?: IRayonPlanWrapper) {
    super.build(i);
    if (i) {
      this.plan.build(i.plan);
    }
  }
}
