import {IDocumentBase} from "@reinform-cdp/core";
import {IRayonPlan} from "./IRayonPlan";

export interface IRayonPlanWrapper extends IDocumentBase {
  plan: IRayonPlan; // Мастер-план
}
