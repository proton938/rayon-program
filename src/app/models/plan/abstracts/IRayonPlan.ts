import {IDocumentBase, INSIDocumentType} from "@reinform-cdp/core";
import {IRayonNsiColor} from "../../common/abstracts/IRayonNsiColor";

export interface IRayonPlan extends IDocumentBase {
  documentId?: string; // Идентификатор документа
  documentDate?: string; // Дата документа
  folderId?: string; // Идентификатор папки в Alfresco
  planVersion?: number; // Версия мастер-плана
  planNumber?: string; // Номер мастер-плана
  planApproveDate?: string; // Дата утверждения мастер-плана
  planYear?: number; // Год выполнения мастер-плана
  planName?: string; // Наименование мастер-плана
  comment?: string; // Примечание
  prefect?: INSIDocumentType; // Округ
  statusId?: IRayonNsiColor; // Статус мастер-плана
  files?: string[]; // Файлы
}
