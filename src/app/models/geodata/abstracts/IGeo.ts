import {IDocumentBase} from "@reinform-cdp/core";

export interface IGeo extends IDocumentBase {
  docType?: string;
  docId?: string;
  esriJson?: string;
  esriJsonEhd?: string;
  geoJson?: string;
  srid?: number;
  subsystemType?: string;
  wkt?: string;
  x?: string;
  xy?: string;
  y?: string;
}
