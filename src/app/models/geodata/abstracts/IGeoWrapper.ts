import {IDocumentBase} from "@reinform-cdp/core";
import {IGeo} from "./IGeo";

export interface IGeoWrapper extends IDocumentBase {
  json: IGeo;
}
