import {DocumentBase} from "@reinform-cdp/core";
import {IGeo} from "./abstracts/IGeo";

export class Geo extends DocumentBase<IGeo, Geo> {
  docType: string = '';
  docId: string = '';
  esriJson: string = '';
  esriJsonEhd: string = '';
  geoJson: string = '';
  srid: number = null;
  subsystemType: string = '';
  wkt: string = '';
  x: string = '';
  xy: string = '';
  y: string = '';
}
