import {DocumentBase} from "@reinform-cdp/core";
import {IGeoWrapper} from "./abstracts/IGeoWrapper";
import {Geo} from "./Geo";

export class GeoWrapper extends DocumentBase<IGeoWrapper, GeoWrapper> {
  json: Geo = new Geo();

  build(i?: IGeoWrapper) {
    super.build(i);
    if (i) {
      this.json.build(i.json);
    }
  }
}
