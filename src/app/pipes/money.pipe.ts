import {Pipe, PipeTransform} from "@angular/core";
import {isNumber, toString} from 'lodash';

@Pipe({
  name: 'money'
})
export class MoneyPipe implements PipeTransform {
  transform(value: string, options?: any): string {
    if (!value) return '';
    let result = '', parts = value.split('.'), decimal = (parts[1] || '').replace(/[^\d]/g, '');
    let decimalSize: number = options && isNumber(options.decimalSize) ? options.decimalSize : 2;
    value = parts[0];
    value.replace(/[^\d]/g, '').split('').reverse()
      .forEach((item, index) => {
      if (index && index % 3 === 0) result = ' ' + result;
      result = item + result;
    });
    if (decimal) result += '.' + toString(decimal).substr(0, decimalSize);
    return result;
  }
}
