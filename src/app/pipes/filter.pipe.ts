import {Pipe, PipeTransform} from "@angular/core";
import {isFunction} from 'lodash';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(list: any[], method: any): any[] {
    if (!list.length) return [];

    if (isFunction(method)) return list.filter(method);
    else return list;
  }
}
