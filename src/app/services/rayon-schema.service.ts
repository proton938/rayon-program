import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {map} from 'rxjs/internal/operators';
import {RayonSchema, RayonSchemaAttribute} from '../models/schema/RayonSchema';
import {fromJson, toJson} from '@uirouter/core';
import {copy} from 'angular';
import * as _ from 'lodash';

@Injectable()
export class RayonSchemaService {
  baseUrl: string = '/app/mr/program/schema';

  constructor(private http: HttpClient) {}

  getSchema(docType: string): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/jsonRepresentationByDocType/' + docType)
      .pipe(map((result: any) => new RayonSchema(fromJson(toJson(result)))));
  }

  getModel(schema: RayonSchema, doc: any) {
    let r: any = {};
    let addNode = (parent: any, source: any, d: any) => {
      if (source.name) parent[source.name] = makeValue(source, d ? d[source.name] : null);
    };
    let makeValue = (source: any, d: any) => {
      let value: any;
      let arty: any[] = source.arity.match(/\d-(.+)/);
      let isMultiple: boolean = arty && arty[1] && arty[1].length > 1;
      if (source.isComplex) {
        return isMultiple
          ? (d && d.length ? d.map(i => makeComplex(source.attributes, i)) : [makeComplex(source.attributes)])
          : (d ? makeComplex(source.attributes, d) : makeComplex(source.attributes));
      } else {
        return isMultiple
          ? (d && d.length ? d.map(i => makeSimple(source.type, i)) : [makeSimple(source.type)])
          : (d ? makeSimple(source.type, d) : makeSimple(source.type));
      }
    };
    let makeComplex = (attrs: RayonSchemaAttribute[], d?: any) => {
      let value: any = {};
      attrs.forEach(at => addNode(value, at, d));
      return value;
    };
    let makeSimple = (type: string, d?) => {
      let value: any;
      switch(type) {
        case 'string':
          value = d || '';
          break;
        default:
          value = d || null;
          break;
      }
      return value;
    };
    addNode(r, schema, doc);
    return r;
  }

  min(source: any): any {
    const result = copy(source);
    _.forIn(result, (value, key) => {
      if (_.isFunction(value)) {
      } else if (_.isNull(value) || _.isUndefined(value)) {
        delete result[key];
      } else if (_.isString(value) && value.trim() === '') {
        delete result[key];
      } else if (_.isNumber(value) || _.isBoolean(value) || _.isString(value)) {
        result[key] = value;
      } else if (_.isArray(value)) {
        if (value.length === 0) {
          delete result[key];
        } else {
          const minArr = [];
          value.forEach(r => {
            if (_.isNull(r) || _.isUndefined(r)) {
            } else if (_.isString(r) && r.trim() === '') {
            } else if (_.isNumber(r) || _.isBoolean(r) || _.isString(r)) {
              minArr.push(r);
            } else {
              const minObj = this.min(r);
              if (_.isEmpty(minObj)) {
              } else minArr.push(minObj);
            }
          });
          if (minArr.length === 0) delete result[key];
          else result[key] = minArr;
        }
      } else {
        const minObj = this.min(<any>value);
        if (_.isEmpty(minObj)) delete result[key];
        else result[key] = minObj;
      }
    });
    return <any>result;
  }
}
