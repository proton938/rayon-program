import {Injectable} from '@angular/core';
import {RayonWork} from '../models/work/RayonWork';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {RayonWorkWrapper} from '../models/work/RayonWorkWrapper';
import {fromJson, toJson} from '@uirouter/core';
import {IRayonWork} from '../models/work/abstract/IRayonWork';
import {isArray, last, some} from 'lodash';
import {compare, Operation} from 'fast-json-patch';

@Injectable()
export class RayonWorkTypeService {
  work: RayonWork;
  baseUrl = '/app/mr/program/work';

  constructor(private http: HttpClient) {}

  create(data: IRayonWork): Observable<RayonWorkWrapper> {
    return this.http.post<RayonWorkWrapper>(this.baseUrl + '/create', {work: data});
  }

  get(id: string): Observable<RayonWorkWrapper> {
    return this.http.get<RayonWorkWrapper>(this.baseUrl + '/' + id);
  }

  getList(ids: string[]): Observable<RayonWorkWrapper[]> {
    return this.http.post<RayonWorkWrapper[]>(this.baseUrl + '/documents', ids);
  }

  patch(id: string, data, notes?: string) {
    let url: string = this.baseUrl + '/' + id;
    if (notes) { url += '?notes=' + notes; }
    return this.http.patch(url, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
  }

  delete(id: string | string[]): Observable<void> {
    const options = {
      headers: {'Content-Type': 'application/json'},
      body: id
    };
    return isArray(id)
      ? this.http.delete<void>(`${this.baseUrl}/remove`, options) // Массовое удаление работ и связанных документов
      : this.http.delete<void>(`${this.baseUrl}/${id}`); // Удаление документа
  }

  linkWorkIsogd(id: string | string[]): Observable<any> {
    return this.http.post(this.baseUrl + '/linkWorkIsogd', isArray(id) ? id : [id]);
  }

  updateIsogdSpecific(work: RayonWork): Observable<any> {
    const isIsogdSpec = s => some(['gpzunumber', 'rvnumber', 'rsnumber'], i => i === s.specificCode);
    if (some(work.specific, s => isIsogdSpec(s) && !s.url)) {
      return this.linkWorkIsogd(work.documentId);
    } else return of('');
  }

  init(id?: string): Observable<RayonWork> {
    return Observable.create(observer => {
      if (id) {
        this.get(id).pipe().subscribe(response => {
          const workWrapper: RayonWorkWrapper = new RayonWorkWrapper();
          workWrapper.build(fromJson(toJson(response)));
          this.work = workWrapper.work;
          observer.next(this.work);
          observer.complete();
        });
      } else {
        this.work = new RayonWork();
        observer.next(this.work);
        observer.complete();
      }
    });
  }

  list(ids: string[]): Observable<RayonWork[]> {
    return Observable.create(observer => {
      if (ids && ids.length) {
        this.getList(ids).subscribe(response => {
          const items = response.map(i => {
            const doc = new RayonWork();
            doc.build(i.work);
            return doc;
          });
          observer.next(items);
          observer.complete();
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  diff(q1: RayonWork, q2: RayonWork): Operation[] {
    return compare(
      {work: q1.min()},
      {work: q2.min()}
    );
  }

  getInstance(result: any): RayonWork {
    const doc = new RayonWork();
    if (result && result.work) { doc.build(result.work); }
    return doc;
  }

  // Resolve progress before patch
  resolveProgress(old: RayonWork, next: RayonWork, actual: RayonWork) {
    if (old.status && next.status && old.status.code === next.status.code) {
      // Если значение текущей стадии не изменилось - не меняем его (old = next)
      next.progress = actual.progress.slice();
      next.status = actual.status;
    } else if (actual.progress && actual.progress.length && next.progress && next.progress.length) {
      next.progress = [
        ...actual.progress.slice(0, last(actual.progress).stage.code === last(next.progress).stage.code ? -1 : actual.progress.length),
        last(next.progress)
      ];
    }
  }
}
