import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RayonProgram} from "../models/program/RayonProgram";
import {RayonProgramWrapper} from "../models/program/RayonProgramWrapper";
import {IRayonProgram} from "../models/program/abstracts/IRayonProgram";
import {fromJson, toJson} from "angular";
import {compare, Operation} from "fast-json-patch";
import {IKindRequestParams} from "../models/common/abstracts/IKindRequestParams";

@Injectable()
export class RayonProgramResourceService {
  baseUrl: string = '/app/mr/program';
  document: RayonProgram;
  address: string;

  constructor(private http: HttpClient) {}

  create(data: IRayonProgram, noindex?: boolean): Observable<RayonProgramWrapper> {
    return this.http.post<RayonProgramWrapper>(this.baseUrl + '/document/create' + (noindex ? '/noindex' : ''), {obj: data});
  }

  get(id: string): Observable<RayonProgramWrapper> {
    return this.http.get<RayonProgramWrapper>(this.baseUrl + '/document/' + id);
  }

  patch(id: string, data, notes?: string) {
    let url = this.baseUrl + '/document/' + id;
    if (notes) url += '?notes=' + notes;
    return this.http.patch(url, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
  }

  // Единичное удаление объекта
  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/document/${id}`);
  }

  // Массовое удаление объектов и связанных документов
  remove(ids: string[]): Observable<void> {
    return this.http.request<void>('DELETE', `${this.baseUrl}/document/remove`, {body: ids});
  }

  init(id?: string): Observable<RayonProgram> {
    return Observable.create(observer => {
      if (id) {
        this.get(id).pipe().subscribe(response => {
          let documentWrapper: RayonProgramWrapper = new RayonProgramWrapper();
          documentWrapper.build(fromJson(toJson(response)));
          this.document = documentWrapper.obj;
          observer.next(this.document);
          observer.complete();
        });
      } else {
        this.document = new RayonProgram();
        observer.next(this.document);
        observer.complete();
      }
    });
  }

  log(id: string, sort: string = 'desc'): Observable<any> {
    return this.http.get(this.baseUrl + '/log/' + id + '/allTypes/' + sort);
  }

  diff(q1: RayonProgram, q2: RayonProgram): Operation[] {
    return compare(
      {obj: q1.min()},
      {obj: q2.min()}
    );
  }

  changeNotify(id: string, type: string): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/changeNotify/' + id + '/' + type);
  }

  changeNotifyDocs(docs: {[doc: string]: string}): Observable<any> {
    return this.http.post<any>(this.baseUrl + '/changeNotify/docs', docs);
  }

  updateObjRubricators(docs: {[doc: string]: string}): Observable<any> {
    return this.http.post<any>(this.baseUrl + '/document/updateObjRubricators', docs);
  }

  getInstance(result: any): RayonProgram {
    let doc = new RayonProgram();
    if (result && result.obj) doc.build(result.obj);
    return doc;
  }

  findByKinds(params: IKindRequestParams): Observable<any> {
    return this.http.post(this.baseUrl + '/document/objectKindWorkKind', params);
  }
}
