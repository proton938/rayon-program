import {Injectable} from '@angular/core';
import {forkJoin, Observable} from 'rxjs';
import {RayonRequestResourceService} from './rayon-request-resource.service';
import {RayonRequestWrapper} from '../models/request/RayonRequestWrapper';
import {ToastrService} from 'ngx-toastr';
import {SessionStorage} from '@reinform-cdp/security';
import {UserDocumentType} from '@reinform-cdp/core';
import * as _ from 'lodash';
import {ActivityResourceService} from '@reinform-cdp/bpm-components';
import {SolrMediatorService} from "./solr-mediator.service";

@Injectable()
export class RayonRequestService {
  constructor(private rayonRequestResourceService: RayonRequestResourceService,
              private toastrService: ToastrService,
              private solrMediator: SolrMediatorService,
              private activityResourceService: ActivityResourceService,
              private session: SessionStorage) {}

  public createRequests(params: any[]): Observable<boolean> {
    return Observable.create(observer => {
      const requests: Observable<boolean>[] = [];
      params.forEach(p => {
        requests.push(this.findRequestByParamsAndCreateIfNeeded(p.district, p.industry, p.organization, p.prefecture));
      });
      forkJoin(requests).subscribe(response => {
        const result = _.find(response, r => {
          return r;
        });
        observer.next(!!result);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  private startProcess(id: string, industry, organization, prefecture): Observable<void> {
    return Observable.create(observer => {
      this.activityResourceService.getProcessDefinitions({
        key: 'mrrequest_formationDistrictsPassport',
        latest: true
      }).then(response => {
        let vars = [];
        vars.push({name: 'EntityIdVar', value: id});
        if (prefecture) {
          vars.push({name: 'IndustryDistrictVar', value: organization.group});
          vars.push({name: 'OrgVar', value: null});
        } else {
          vars.push({name: 'IndustryDistrictVar', value: industry.group});
          vars.push({name: 'OrgVar', value: organization.group});
        }
        return this.activityResourceService.initProcess({
          processDefinitionId: response.data[0].id,
          variables: vars
        });
      }).then(response => {
        this.toastrService.success('Процесс успешно запущен!');
        observer.next();
        observer.complete();
      }).catch(error => {
        observer.error(error);
        this.toastrService.error('Ошибка при запуске процесса!');
        console.log(error);
        observer.error(error);
        observer.complete();
      });
    });
  }

  private findRequestByParamsAndCreateIfNeeded(district, industry, organization, prefecture): Observable<boolean> {
    return Observable.create(observer => {
      this.hasRequestByParams(district, industry, organization, prefecture).subscribe(hasRequest => {
        if (!hasRequest) {
          const request = this.createRayonRequestWrapper(district, industry, organization, prefecture);
          this.rayonRequestResourceService.create(request.min()).subscribe(response => {
            let message = `Заявка по району '${district.name}', `;
            message += `отрасли '${industry.name}' и организации '${organization.name}' успешно создана!`;
            this.toastrService.success(message);
            this.startProcess(response.request.documentId, industry, organization, prefecture).subscribe(response => {
              observer.next(true);
              observer.complete();
            }, error => {
              observer.error(error);
              observer.complete();
            });
          }, error => {
            if (error.error.userMessages) {
              this.toastrService.error(error.error.userMessages + '!');
            } else {
              let message = `Ошибка при создании заявки по району '${district.name}', `;
              message += `отрасли '${industry.name}' и организации '${organization.name}'!`;
              this.toastrService.error(message);
            }
            observer.error(error);
            console.log(error);
            observer.complete();
          });
        } else {
          observer.next(false);
          let message = `Заявка по району '${district.name}', `;
          message += `отрасли '${industry.name}' и организации '${organization.name}' создана ранее, `;
          message += `работа по заявке не завершена. Создание новой заявки невозможно!`;
          this.toastrService.warning(message);
          observer.complete();
        }
      });
    });
  }

  private hasRequestByParams(district, industry, organization, prefecture): Observable<boolean> {
    return Observable.create(observer => {
      this.solrMediator.query({
        page: 0,
        pageSize: 1000,
        types: ['REQUEST'],
        query: this.buildQueryForHasRequestByParams(district, industry, organization)
      }, false).subscribe(response => {
        observer.next((response.numFound > 0));
        observer.complete();
      }, error => {
        observer.error(error);
        let message = `Ошибка при поиске информации о заявке по району '${district.name}', `;
        message += `отрасли '${industry.name}' и `;
        message += (prefecture) ? `префектуре '${organization.name}'!` : `организации '${organization.name}'!`;
        this.toastrService.error(message);
        observer.complete();
      });
    });
  }

  private buildQueryForHasRequestByParams(district, industry, organization): string {
    const results = [`(districtCodeRequest:${district.code})`,
      `(organizationCodeRequest:${organization.code})`,
      `(industryCodeRequest:${industry.code})`,
      `(doneRequest:false)`];
    return results.join(' AND ');
  }

  private createRayonRequestWrapper(district, industry, organization, prefecture): RayonRequestWrapper {
    const request = new RayonRequestWrapper();
    request.request.district.code = district.code;
    request.request.district.name = district.name;
    request.request.industry.code = industry.code;
    request.request.industry.name = industry.name;
    request.request.organization.code = organization.code;
    request.request.organization.name = organization.name;
    request.request.userStartProcess = new UserDocumentType();
    request.request.userStartProcess.login = this.session.login();
    request.request.userStartProcess.fio = this.session.fullName();
    request.request.userStartProcess.post = this.session.post();
    request.request.done = false;
    request.request.prefecture = prefecture;
    return request;
  }
}
