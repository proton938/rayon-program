import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {fromJson, toJson} from "angular";
import {RayonPublication} from "../models/publication/RayonPublication";
import {IRayonPublication} from "../models/publication/abstracts/IRayonPublication";
import {compare, Operation} from "fast-json-patch";
import {map} from "rxjs/internal/operators";


@Injectable()
export class RayonPublicationService {
  baseHref: string = '/app/mr/program/publication';
  baseRoot: string = 'publication';
  document: RayonPublication;

  constructor(private http: HttpClient) {}

  create(data: IRayonPublication): Observable<RayonPublication> {
    return this.http.post<any>(this.baseHref + '/create', {[this.baseRoot]: data})
      .pipe(map((result: any) => {
        let doc = new RayonPublication();
        if (result && result[this.baseRoot]) doc.build(result[this.baseRoot]);
        return doc;
      }));
  }

  get(id: string): Observable<RayonPublication> {
    return this.http.get<any>(this.baseHref + '/' + id)
      .pipe(map((result: any) => {
        let doc = new RayonPublication();
        if (result && result[this.baseRoot]) doc.build(result[this.baseRoot]);
        return doc;
      }));
  }

  diff(old: RayonPublication, next: RayonPublication): any[] {
    return compare(
      { [this.baseRoot]: old.min() },
      { [this.baseRoot]: next.min() }
    );
  }

  patch(id: string, data: Operation[]) {
    return this.http.patch(this.baseHref + '/' + id, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    }).pipe(map((result: any) => {
      let doc = new RayonPublication();
      if (result && result[this.baseRoot]) doc.build(result[this.baseRoot]);
      return doc;
    }));
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.baseHref}/${id}`);
  }

  init(id?: string): Observable<RayonPublication> {
    return Observable.create(observer => {
      this.document = new RayonPublication();
      if (id) {
        this.get(id).subscribe(response => {
          this.document = response;
          observer.next(this.document);
          observer.complete();
        });
      } else {
        observer.next(this.document);
        observer.complete();
      }
    });
  }
}
