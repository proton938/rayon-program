import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs/index';
import {HttpClient} from "@angular/common/http";
import {Link} from "../models/link/Link";
import {compare, Operation} from "fast-json-patch";
import {isArray} from 'lodash';

@Injectable()
export class RayonLinkResourceService {
  url: string = '/app/mr/link/link';

  constructor(private http: HttpClient) {}

  create(data: any): Observable<any> {
    return this.http.post<any>(this.url + '/create', data);
  }

  find(id: string | string[]): Observable<any> {
    if (isArray(id)) return this.http.post<any>(this.url + '/rayon/find', id);
    return this.http.get<any>(this.url + '/rayon/' + id + '/find');
  }

  get(id: string): Observable<any> {
    return this.http.get<any>(this.url + '/' + id);
  }

  delete(id: string): Observable<any> {
    return this.http.delete<void>(this.url + '/' + id);
  }

  deleteLink(linkId: string | string[]): Observable<any> {
    let options = {
      headers: {'Content-Type': 'application/json'},
      body: {ids: isArray(linkId) ? linkId : [linkId]}
    };
    return this.http.delete<any>(this.url + '/delete/ids', options);
  }

  diff(q1: Link, q2: Link): Operation[] {
    return compare(
      {link: q1.min()},
      {link: q2.min()}
    );
  }

  patch(id: string, data): Observable<any> {
    return this.http.patch<any>(this.url + '/' + id, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
  }

  changeStatus(link: Link, status: string): Observable<any> {
    let currentLink: Link = new Link();
    currentLink.build(link);
    link.status = status;
    let modifyLink: Link = new Link();
    modifyLink.build(link);
    return this.patch(currentLink.documentId, this.diff(currentLink, modifyLink));
  }

  cloneLinks(idFrom: string, idTo: string, note?: string): Observable<any> {
    return this.http.post<any>(this.url + '/copyLink/' + idFrom + '/' + idTo, {});
  }
}
