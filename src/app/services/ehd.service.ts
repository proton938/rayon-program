import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";

@Injectable()
export class EhdService {
  url: string = '/app/mr/program';

  constructor(private http: HttpClient) {
  }

  loadCatalogEntries(catalogCode: string, odopmIds: string[]): Observable<any> {
    let param = odopmIds.map(id => `odopmIds=${id}`).join("&");
    return this.http.post<any>(`${this.url}/ehd/catalog/${catalogCode}/entries/load?${param}`, {});
  }

}
