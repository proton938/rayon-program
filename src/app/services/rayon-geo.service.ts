import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {fromJson, toJson, identity} from "angular";
import {IGeo} from "../models/geodata/abstracts/IGeo";
import {Geo} from "../models/geodata/Geo";
import {map} from "rxjs/internal/operators";
import {compare, Operation} from "fast-json-patch";

@Injectable()
export class RayonGeoService {
  baseHref: string = '/app/mr/geodata';
  baseRoot: string = 'json';

  constructor(private http: HttpClient) {}

  create(data: IGeo): Observable<Geo> {
    return this.http.post<Geo>(this.baseHref + '/geo/create', data)
      .pipe(map((result: any) => {
        let doc = new Geo();
        if (result && result[this.baseRoot]) doc.build(result[this.baseRoot]);
        return doc;
      }));
  }

  get(id: string): Observable<Geo> {
    return this.http.get<Geo>(this.baseHref + '/geo/' + id)
      .pipe(map((result: any) => {
        let doc = new Geo();
        if (result && result[this.baseRoot]) doc.build(result[this.baseRoot]);
        return doc;
      }));
  }

  diff(old: Geo, next: Geo): any[] {
    return compare(
      { [this.baseRoot]: old.min() },
      { [this.baseRoot]: next.min() }
    );
  }

  patch(id: string, data: Operation[]) {
    return this.http.patch(this.baseHref + '/geo/' + id, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    }).pipe(map((result: any) => {
      let doc = new Geo();
      if (result && result[this.baseRoot]) doc.build(result[this.baseRoot]);
      return doc;
    }));
  }

  patchFromDocument(documentId: string, data: Operation[], documentType: string = 'MR_PROGRAM_OBJ', subsystemType: string = 'MR_PROGRAM') {
    let url: string  = this.baseHref + '/geo/document/' + documentType + '/' + documentId;
    url += '?subsystemType=' + subsystemType;
    return this.http.patch(url, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    }).pipe(map((result: any) => {
      let doc = new Geo();
      if (result && result[this.baseRoot]) doc.build(result[this.baseRoot]);
      return doc;
    }));
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(this.baseHref + '/geo/' + id);
  }

  deleteFromDocument(documentId: string, documentType: string = 'MR_PROGRAM_OBJ', subsystemType: string = 'MR_PROGRAM'): Observable<void> {
    let url: string  = this.baseHref + '/geo/document/' + documentType + '/' + documentId;
    url += '?subsystemType=' + subsystemType;
    return this.http.delete<void>(url);
  }

  find(docId: string, docType: string, subSystemType: string, axDecimalDigits?: string): Observable<void> {
    let url: string = this.baseHref + '/geo/find';
    let queryParams = [];
    if (docId) queryParams.push('docId=' + docId);
    if (docType) queryParams.push('docType=' + docType);
    if (subSystemType) queryParams.push('subsystemType=' + subSystemType);
    if (axDecimalDigits) queryParams.push('axDecimalDigits=' + axDecimalDigits);
    if (queryParams.length) url += '?' + queryParams.join('&');
    return this.http.get<any>(url);
  }

  findLink(docId: string, docType: string, subSystemType: string, axDecimalDigits?: string): Observable<any> {
    let url: string = this.baseHref + '/geo/findLink';
    let queryParams = [];
    if (docId) queryParams.push('docId=' + docId);
    if (docType) queryParams.push('docType=' + docType);
    if (subSystemType) queryParams.push('subsystemType=' + subSystemType);
    if (axDecimalDigits) queryParams.push('axDecimalDigits=' + axDecimalDigits);
    if (queryParams.length) url += '?' + queryParams.join('&');
    return this.http.get<any>(url);
  }

  createVector(file: any, data: any): Observable<any> {
    let url: string = this.baseHref + '/geo/vector/create';
    let queryArray: string[] = [];
    if (data) {
      if (data.docId) queryArray.push('docId=' + data.docId);
      if (data.docType) queryArray.push('docType=' + data.docType);
      if (data.subsystemType) queryArray.push('subsystemType=' + data.subsystemType);
      if (data.extension) queryArray.push('extension=' + data.extension);
      if (data.srid) queryArray.push('srid=' + data.srid);
    }
    if (queryArray) url += '?' + queryArray.join('&');

    let fd = new FormData();
    fd.append('file', file);

    return this.http.post<any>(url, fd);
  }

  exists(ids: string[] = []): Observable<any> {
    let url: string = this.baseHref + '/geo/geometryExists';
    return this.http.post<any>(url,{docIds: ids});
  }
}
