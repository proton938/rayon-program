import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IRayonRequestWrapper} from '../models/request/abstracts/IRayonRequestWrapper';

@Injectable()
export class RayonRequestResourceService {
  constructor(private http: HttpClient) {}

  create(data: IRayonRequestWrapper): Observable<IRayonRequestWrapper> {
    return this.http.post<IRayonRequestWrapper>('/app/mr/program/request/create', data);
  }

  get(id: string): Observable<IRayonRequestWrapper> {
    return this.http.get<IRayonRequestWrapper>('/app/mr/program/request/' + id);
  }

  patch(id: string, data) {
    return this.http.patch('/app/mr/program/request/' + id, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
  }
}
