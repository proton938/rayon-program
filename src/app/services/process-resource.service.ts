import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {RayonPlan} from "../models/plan/RayonPlan";
import {Observable} from "rxjs/Rx";

@Injectable()
export class ProcessResourceService {
  url: string = '/app/mr/program/process';
  document: RayonPlan;

  constructor(private http: HttpClient) {
  }

  getInfo(processId: string): Observable<ProcessInfo> {
    return <Observable<ProcessInfo>>this.http.get(this.url + '/info/' + processId);
  }

}

export interface ProcessInfo {
  donePercent: number;
  processId: string;
  resultMsg: string;
  status: "RUNNING" | "ERROR";
}
