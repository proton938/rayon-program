import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {RayonPlan} from "../models/plan/RayonPlan";
import {Observable} from "rxjs/Rx";
import {map} from "rxjs/internal/operators";
import {compare, Operation} from "fast-json-patch";
import {ExFileType} from "@reinform-cdp/widgets";
import {from} from "rxjs/index";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {compact, last} from 'lodash';
import {SolrMediatorService} from "./solr-mediator.service";

@Injectable()
export class PlanResourceService {
  url: string = '/app/mr/program/plan';
  document: RayonPlan;

  constructor(private http: HttpClient,
              private filestore: FileResourceService,
              private solrMediator: SolrMediatorService) {}

  get(id: string): Observable<RayonPlan> {
    return <Observable<any>>this.http.get(this.url + '/' + id)
      .pipe(map(this.planInstance));
  }

  getList(ids: string[]): Observable<RayonPlan[]> {
    return this.http.post<RayonPlan[]>(this.url + '/ids', ids)
      .pipe(map((items: any[]) => {
        return items.map(item => this.planInstance(item));
      }));
  }

  create(json: any): Observable<RayonPlan> {
    return this.http.post<any>(this.url + '/create', {plan: json})
      .pipe(map(this.planInstance));
  }

  diff(q1: RayonPlan, q2: RayonPlan) {
    return compare(
      {plan: q1.min()},
      {plan: q2.min()}
    );
  }

  update(id: string, diff: Operation[]): Observable<any> {
    return <Observable<any>>this.http.patch(this.url + '/' + id, diff)
      .pipe(map(this.planInstance));
  }

  delete(id: string): Observable<any> {
    return <Observable<any>>this.http.delete(this.url + '/' + id);
  }

  getAllFiles(folderId: string): Observable<any[]> {
    return Observable.create(observer => {
      if (folderId) {
        from(this.filestore.getFolderContent(folderId, false)).subscribe(response => {
          if (response) {
            let files: ExFileType[] = response.map(file => ExFileType.create(file.versionSeriesGuid, file.fileName, file.fileSize,
              false, file.dateCreated, file.fileType, file.mimeType));
            observer.next(files);
          }
        }, error => observer.error(error), () => observer.complete());
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  planInstance(result: any): RayonPlan {
    let doc = new RayonPlan();
    if (result && result.plan) doc.build(result.plan);
    return doc;
  }

  init(id?: string): Observable<RayonPlan> {
    return Observable.create(observer => {
      if (id) {
        this.get(id).subscribe(response => {
          this.document = response;
          observer.next(this.document);
          observer.complete();
        });
      } else {
        this.document = new RayonPlan();
        observer.next(this.document);
        observer.complete();
      }
    });
  }

  getVersionsFromSolr(prefectCode: string, year: number): Observable<any> {
    return this.solrMediator.query({
      query: 'prefectCode:' + prefectCode + ' AND planYear:' + year,
      page: 0,
      pageSize: 999,
      types: [this.solrMediator.types.plan],
      sort: 'planVersion desc'
    });
  }

  getNextVersionValue(prefectCode: string, year: number): Observable<number> {
    return Observable.create(observer => {
      this.getVersionsFromSolr(prefectCode, year).subscribe((r: any) => {
        if (r && r.docs && r.docs.length) {
          let lastVersion: number = last(compact(r.docs.map(i => i.planVersion))) || 0;
          observer.next(lastVersion + 1);
        } else observer.next(1);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  makePlanNumber(doc: RayonPlan): string {
    return [doc.prefect.name, doc.planYear, doc.planVersion].join('/');
  }

  updateMasterPlanMark(id: string, mark: boolean = true): Observable<any> {
    return this.http.post<any>(this.url + '/' + id + '/masterPlan/' + mark, {});
  }

  approve(id: string, notes?: string): Observable<void> {
    let data: any = notes ? {notes: notes} : {};
    return this.http.post<void>(this.url + '/approve/' + id, data);
  }

  updateMasterPlanMarks(idsToApprove: string[] = [], idsToRemove: string[] = []): Observable<any> {
    return this.http.post<any>(this.url + '/masterPlan', {
      linksMasterplanFalse: idsToRemove,
      linksMasterplanTrue: idsToApprove
    });
  }

  linkPlanWithWorkObjectByIds(isLinked: boolean, planNumber: string, workIds: string[]): Observable<string> {
    return this.http.post(this.url + `/linkPlanWithWorkObjectByIds?isLinked=${isLinked}&planNumber=${planNumber}`, workIds, {responseType: 'text'});
  }
}
