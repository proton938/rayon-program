import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ChangeRequest} from "../models/change-request/ChangeRequest";
import {Observable} from "rxjs/Rx";
import {map} from "rxjs/internal/operators";
import {ChangeRequestWrapper} from "../models/change-request/ChangeRequestWrapper";
import {forIn, isObject} from 'lodash';
import {compare, Operation} from "fast-json-patch";

@Injectable()
export class ChangeRequestService {
  private wrapper: string;
  private docType: string = 'CHANGEREQUEST';
  url: string = '/app/mr/program';
  document: ChangeRequest;

  constructor(private http: HttpClient) {
    this.wrapper = this.instanceName;
  }

  get(id: string): Observable<ChangeRequest> {
    return this.http.get<ChangeRequest>(this.url + '/crud/fetch/' + this.docType + '/' + id)
      .pipe(map(data => this.getInstance(data)));
  }

  create(data): Observable<ChangeRequest> {
    return this.http.post<ChangeRequest>(this.url + '/crud/create/' + this.docType, {[this.wrapper]: data})
      .pipe(map(data => this.getInstance(data)));
  }

  patch(id: string, data) {
    return this.http.patch(this.url + '/crud/patch/' + this.docType + '/' + id, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
  }

  getInstance(result: any): ChangeRequest {
    let doc = new ChangeRequest();
    if (result && result[this.wrapper]) doc.build(result[this.wrapper]);
    return doc;
  }

  diff(q1: ChangeRequest, q2: ChangeRequest): Operation[] {
    return compare(
      {[this.wrapper]: q1.min()},
      {[this.wrapper]: q2.min()}
    );
  }

  init(id?: string): Observable<ChangeRequest> {
    return Observable.create(observer => {
      if (id) {
        this.get(id).pipe().subscribe(res => {
          this.document = res;
          observer.next(this.document);
          observer.complete();
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else {
        this.document = new ChangeRequest();
        observer.next(this.document);
        observer.complete();
      }
    });
  }

  get instanceName(): string {
    let wrapper: ChangeRequestWrapper = new ChangeRequestWrapper();
    let r: string = '';
    forIn(wrapper, (item, name) => r = wrapper.hasOwnProperty(name) ? name : r);
    return r;
  }
}
