import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";

@Injectable()
export class RayonBtiService {
  buildingUrl: string = '/addresses/building';

  constructor (private http: HttpClient) {}

  getBuilding(id: string): Observable<any> {
    return this.http.get<any>(this.buildingUrl + '/' + id);
  }

  patchBuilding(id: string, data) {
    return this.http.patch(this.buildingUrl + '/' + id, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
  }

  deleteBuilding(id: string): Observable<void> {
    return this.http.delete<void>(this.buildingUrl + '/' + id);
  }

  findBuildings(unoms: string[]): Observable<any[]> {
    return this.http.post<any[]>(this.buildingUrl + '/findByUnoms', unoms);
  }

}
