import {Injectable} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {SessionStorage} from "@reinform-cdp/security";
import {from, forkJoin} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {assign, find, concat, compact, some, every, isNull, isArray, isObject} from 'lodash';

@Injectable()
export class ObjectMatrixService {
  matrixDictionaryName: string = 'mr_program_ObjectMatrix';
  matrix: any[] = [];

  constructor(private nsi: NsiResourceService,
              private session: SessionStorage) {}

  /**
   * Before using this service need to call method init() and wait its end
   */
  init(): Observable<any[]> {
    return Observable.create(observer => {
      from(this.nsi.getDictsFromCache([this.matrixDictionaryName])).subscribe(res => {
        this.matrix = res[this.matrixDictionaryName];
        observer.next(this.matrix);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  /**
   * Filter dictionary values by current user groups using role matrix
   * @param list - dictionary witch need to filter
   * @param type - property name from matrix dictionary where need to find available values
   * @param params
   * @returns - filtered list from first argument
   */
  filter(list: any[], type: string, params?: any): any[] {
    let result: any[] = [];
    let cfg: any = {
      accord: 'code',
      add: false
    };
    if (isObject(params)) cfg = assign(cfg, params);
    if (cfg.add) type += 'Add';
    let dict: any[] = this._getDictByAllRoles(type);
    result = dict.length ? list.filter(i => some(dict, d => {
      return d[cfg.accord] === i[cfg.accord]
        && (cfg.add && d[cfg.accord].extra ? d[cfg.accord].extra.add : true);
    })) : list;
    return result;
  }

  /**
   * Check dictionary value by current user groups using role matrix
   * @param value - code (or codes array) from dictionary value for check
   * @param type - property name from matrix dictionary where need to find available values
   * @param role - if need to check by one role mey use this parameter
   * @param accord - main property for comparison dictionary values
   * @returns - true if value is suitable
   */
  check(value: string | string[], type: string, role: string = null, accord: string = 'code'): boolean {
    let result: boolean = true;
    let dict: any[] = role ? this._getDictByRole(type, role) : this._getDictByAllRoles(type);

    if (dict.length) {
      if (isArray(value)) {
        result = every(value, v => some(dict, i => i[accord] === v));
      } else {
        result = some(dict, i => i[accord] === value);
      }
    }

    return result;
  }

  private getUserMatrixRows(): any[] {
    let groups: string[] = (this.session.groups() || [])
      .filter(g => g.indexOf('MR_ORG') === 0 || g.indexOf('MR_PREFECT') === 0);
    return this.matrix.filter(m => some(groups, g => g === m.code));
  }

  private _getDictByRole(type: string, role: string) : any[] {
    let result: any[] = [];
    let matrixRow = find(this.matrix, i => i.code === role);
    if (matrixRow) result = matrixRow[type] || [];
    return result;
  }

  private _getDictByAllRoles(type: string, params?: any): any[] {
    let cfg: any = {
      accord: 'code',
      extra: true
    };
    if (isObject(params)) cfg = assign(cfg, params);
    let matrixRows = this.getUserMatrixRows();
    if (some(matrixRows, i => !i[type] || !i[type].length)) return [];
    return concat.apply([], matrixRows.map(i => i[type] ? i[type].map(t => assign({}, t, cfg.extra ? {extra: i} : null)) : []));
  }
}
