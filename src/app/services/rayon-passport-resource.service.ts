import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IRayonPassportWrapper} from '../models/passport/abstracts/IRayonPassportWrapper';

@Injectable()
export class RayonPassportResourceService {
  root = '/app/mr/program/passport/';

  constructor(private http: HttpClient) {}

  create(data: IRayonPassportWrapper): Observable<IRayonPassportWrapper> {
    return this.http.post<IRayonPassportWrapper>(`${this.root}create`, data);
  }

  get(id: string): Observable<IRayonPassportWrapper> {
    return this.http.get<IRayonPassportWrapper>(`${this.root}${id}`);
  }

  patch(id: string, data) {
    return this.http.patch(`${this.root}${id}`, data, {
      headers: {'Content-Type': 'application/json; charset=UTF-8'}
    });
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.root}${id}`);
  }
}
