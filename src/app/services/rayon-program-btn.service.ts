import {Injectable} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {RayonWorkTypeService} from "./rayon-work-type.service";
import {SessionStorage} from "@reinform-cdp/security";
import {from, forkJoin} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {find, compact, some, every, isNull, isArray} from 'lodash';
import {RayonProgram} from "../models/program/RayonProgram";
import {RayonWork} from "../models/work/RayonWork";
import {SolrMediatorService} from "./solr-mediator.service";

@Injectable()
export class RayonProgramBtnService {
  debug: boolean = false;

  constructor(private nsi: NsiResourceService,
              private solrMediator: SolrMediatorService,
              private workTypeService: RayonWorkTypeService,
              private session: SessionStorage) {
    this.switchDebug();
  }

  isShowEditObjectBtn(document): Observable<boolean> {
    return Observable.create(observer => {
      let params: any = {
        document: document,
        dictionaries: {},
        works: []
      };
      let workDocs: any[] = [];
      let dictionaries$ = from(this.nsi.getDictsFromCache(['mr_program_ObjectMatrix']));
      let works$ = this.solrMediator.query({page: 0, pageSize: 50, types: [this.solrMediator.types.work],
        query: 'objectIdWork:' + params.document.documentId});

      forkJoin([dictionaries$, works$]).subscribe(res => {
        params.dictionaries = res[0];
        params.dictionaries.objectMatrix = params.dictionaries['mr_program_ObjectMatrix'];
        workDocs = res[1] ? (res[1].docs || []) : [];
        if (workDocs.length) {
          this.workTypeService.list(workDocs.map(i => i.documentId)).subscribe((res: any[]) => {
            params.works = res.filter(i => i.type) || [];
            observer.next(this.checkDocumentValuesForEdit(params));
            observer.complete();
          }, error => {
            observer.error(error);
            observer.complete();
          });
        } else {
          observer.next(this.checkDocumentValuesForEdit(params));
          observer.complete();
        }
      });
    });
  }

  checkDocumentValuesForEdit(params: any): boolean {
    let r: boolean = false;
    (this.session.groups() || []).filter(g => g.indexOf('MR_ORG') === 0 || g.indexOf('MR_PREFECT') === 0).forEach(g => {
      if (!r) r = this.checkDocumentValuesByRole(params, g);
    });
    return r;
  }

  isShowAddWorkBtn(obj: RayonProgram): Observable<boolean> {
    return Observable.create(observer => {
      let result: boolean = false;
      let groups: string[] = (this.session.groups() || []).filter(g => g.indexOf('MR_ORG') === 0 || g.indexOf('MR_PREFECT') === 0);
      let dictionaries: any = {};
      let dictionaries$ = from(this.nsi.getDictsFromCache(['mr_program_WorkMatrix']));
      forkJoin([dictionaries$]).subscribe(res => {
        dictionaries = res[0];
        dictionaries.workMatrix = dictionaries['mr_program_WorkMatrix'] || [];
        let matrixRows: any[] = dictionaries.workMatrix.filter(m => some(groups, g => g === m.role && m.add));
        result = some(matrixRows, i => {
          let r: boolean = true;
          if (r) r = this.checkField({}, 'obj/responsible/organization/code', i.objectorg, obj);
          if (r) r = this.checkField({}, 'obj/address/prefect/code', i.prefect, obj);
          if (r) r = this.checkField({}, 'obj/object/industry/code', i.industry, obj);
          if (r) r = isNull(i.okn) || !obj.okn == !i.okn;
          return r;
        });
        observer.next(result);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  isShowEditWorkBtn(obj: RayonProgram, work: RayonWork) : Observable<boolean> {
    return Observable.create(observer => {
      let result: boolean = false;
      let groups: string[] = (this.session.groups() || []).filter(g => g.indexOf('MR_ORG') === 0 || g.indexOf('MR_PREFECT') === 0);
      let dictionaries: any = {};
      let dictionaries$ = from(this.nsi.getDictsFromCache(['mr_program_WorkMatrix']));
      forkJoin([dictionaries$]).subscribe(res => {
        dictionaries = res[0];
        dictionaries.workMatrix = dictionaries['mr_program_WorkMatrix'] || [];
        let matrixRows: any[] = dictionaries.workMatrix.filter(m => some(groups, g => g === m.role));
        result = some(matrixRows, i => {
          let r: boolean = true;
          if (r) r = this.checkField({}, 'work/organization/code', i.organization, work);
          if (r) r = this.checkField({}, 'obj/responsible/organization/code', i.objectorg, obj);
          if (r) r = this.checkField({}, 'obj/address/prefect/code', i.prefect, obj);
          if (r) r = this.checkField({}, 'obj/object/industry/code', i.industry, obj);
          if (r) r = this.checkField({}, 'work/type/code', i.type, work);
          if (r) r = isNull(i.okn) || !obj.okn == !i.okn;
          return r;
        });
        observer.next(result);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  private checkDocumentValuesByRole(params: any, role: string): boolean {
    let r: boolean = true;
    let dictRow: any = find(params.dictionaries.objectMatrix, i => i.code === role);
    if (dictRow) {
      // if (this.debug) console.log('=== CHECK ROLE:', role);
      if (r) r = this.checkField(params, 'obj/responsible/organization/code', dictRow['organization']);
      if (r) r = this.checkField(params, 'obj/address/prefect/code', dictRow['prefect']);
      if (r) r = this.checkField(params, 'obj/object/industry/code', dictRow['industry']);
      if (r) r = this.checkField(params, 'obj/object/kind/code', dictRow['kind']);
      if (r && params.works.length) r = this.checkField(params, 'work/type/code', dictRow['type'], params.works);
      if (r) {
        r = isNull(dictRow.okn) || !dictRow.okn === !params.document.okn;
      }
    } else r = false;
    return r;
  }

  private checkField(params: any, path: string, list: any[] = [], source?: any): boolean {
    let r: boolean = true;
    if (list && list.length) {
      let field: any = source || params.document;
      compact(path.replace(/^obj|work\//, '').split('/')).forEach(name => {
        if (field) field = isArray(field) ? field.map(i => i[name]) : field[name];
      });
      // if (this.debug) console.log('Field:', path, field);
      if (isArray(field) && field.length) {
        r = every(field, f => some(list, i => i.code === f));
        // if (this.debug) console.log('Every:', r, list)
      } else {
        r = some(list, i => i.code === field);
        // if (this.debug) console.log('Some:', r, list);
      }
    }
    return r;
  }

  private switchDebug() {
    let url: string = (<any>window).location.href;
    let match = url.match(/[&\?]debug=(.*)?/);
    if (match) {
      switch(match[1]) {
        case 'btn':
        case 'true':
          this.debug = true;
          break;

        default:
          this.debug = false;
          break;
      }
    }
  }
}
