import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class DroneService {
  url: string = '/app/drone/drone';

  constructor(private http: HttpClient) {}

  getFlyBy(id: string): Observable<any> {
    return this.http.get<any>(this.url + '/flyby/' + id);
  }

  getFlyByList(ids: string[]): Observable<any> {
    return this.http.post<any>(this.url + '/flyby/listById', ids);
  }

  getFlyObject(id: string): Observable<any> {
    return this.http.get<any>(this.url + '/flyObject/' + id);
  }

  getFlyContract(id: string): Observable<any> {
    return this.http.get<any>(this.url + '/contract/' + id);
  }

  getRequest(id: string): Observable<any> {
    return this.http.get<any>(this.url + '/droneRequest/' + id);
  }

}
