import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RayonPassportResourceService} from './rayon-passport-resource.service';
import {RayonPassportWrapper} from '../models/passport/RayonPassportWrapper';
import {ToastrService} from 'ngx-toastr';
import {SolrMediatorService} from "./solr-mediator.service";

@Injectable()
export class RayonPassportService {
  constructor(private rayonPassportResourceService: RayonPassportResourceService,
              private toastrService: ToastrService,
              private solrMediator: SolrMediatorService) {}

  findPassportByDistrictAndCreateIfNeeded(district): Observable<void> {
    return Observable.create(observer => {
      this.hasPassportByDistrict(district).subscribe(hasPassport => {
        if (!hasPassport) {
          const passport = this.createRayonPassportWrapper(district);
          this.rayonPassportResourceService.create(passport.min()).subscribe(response => {
            this.toastrService.success(`Паспорт по району '${district.name}' успешно создан!`);
            observer.next();
            observer.complete();
          }, error => {
            if (error.error.userMessages) {
              this.toastrService.error(error.error.userMessages + '!');
            } else {
              this.toastrService.error(`Ошибка при создании паспорта по району '${district.name}'!`);
            }
            console.log(error);
            observer.complete();
          });
        } else {
          observer.next();
          observer.complete();
        }
      });
    });
  }

  private hasPassportByDistrict(district): Observable<boolean> {
    return Observable.create(observer => {
      this.solrMediator.query({
        page: 0,
        pageSize: 1000,
        types: ['PASSPORT'],
        query: `districtCodePassport:${district.code}`
      }, false).subscribe(response => {
        observer.next((response.numFound > 0));
        observer.complete();
      }, error => {
        this.toastrService.error(`Ошибка при поиске информации о паспорте района '${district.name}'!`);
        observer.complete();
      });
    });
  }

  private createRayonPassportWrapper(district): RayonPassportWrapper {
    const passport = new RayonPassportWrapper();

    passport.passport.district.code = district.code;
    passport.passport.district.name = district.name;

    return passport;
  }
}
