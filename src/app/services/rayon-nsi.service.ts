import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';

@Injectable()
export class RayonNsiService {
  constructor(private nsiResource: NsiResourceService) {
  }

  getDicts(): Observable<any> {
    return from(this.nsiResource.getDictsFromCache(['District',
      'mr_program_ObjectTypes', 'mr_program_DistrictResponsible', 'mr_program_ObjectCategories']));
  }

  getDictsForCreateNewRequest(): Observable<any> {
    return from(this.nsiResource.getDictsFromCache(['District',
      'mr_program_ObjectIndustries',
      'mr_program_ResponsibleOrganizations',
      'mr_program_Prefectures']));
  }
}
