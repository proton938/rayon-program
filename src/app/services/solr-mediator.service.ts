import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {CdpSearchByIds, CdpSolrResourceService, SolarResourceService} from "@reinform-cdp/search-resource";
import {from} from "rxjs/index";
import {every, isString, uniq, isArray} from 'lodash';
import {CommonQueryService} from "./common-query.service";
import {map} from "rxjs/internal/operators";

@Injectable()
export class SolrMediatorService {
  types: any = {
    obj: 'MR_PROGRAM_OBJ',
    instruction: 'MR_PROGRAM_INSTRUCTION',
    work: 'MR_PROGRAM_WORK',
    plan: 'MR_PROGRAM_PLAN',
    decision: 'MR_MEETING_DECISION',
    address: 'SYS_ADDRESSES_ADDRESS',
    droneRequest: 'DRONE_DRONE_REQUEST'
  };

  constructor(private solr: SolarResourceService,
              private cdpSolr: CdpSolrResourceService,
              private commonQuery: CommonQueryService) {}

  query(params: any, isNew: boolean = true): Observable<any> {
    if (isNew) {
      if (params.common) this.commonToQuery(params.common, params, isNew);
      else delete params.common;
      if (params.query) params.query = params.query.replace(/documentId:|sys_documentId:/g, 'sys_documentId:');
      else delete params.query;
      return this.cdpSolr.query(params).pipe(map(this.modifyDocs.bind(this)));
    } else {
      return from(this.solr.query(params)).pipe(map(this.modifyDocs.bind(this)));
    }
  }

  getByIds(data: CdpSearchByIds[] | string[], isNew = true): Observable<any> {
    if (every(data, i => isString(i))) isNew = false;
    if (isNew) {
      return this.cdpSolr.getByIds(<CdpSearchByIds[]>data).pipe(map(this.modifyDocs.bind(this)));
    } else {
      return from(this.solr.getByIds({ids: <string[]>data})).pipe(map(this.modifyDocs.bind(this)));
    }
  }

  modifyDocs(response: any): any {
    let modifyItem = item => {
      if (!item.documentId && item.sys_documentId) item.documentId = item.sys_documentId;
      return item;
    };
    let modify = r => {
      if (r && r.docs && r.docs.length) r.docs.forEach(i => modifyItem(i));
      return r;
    };
    return isArray(response) ? response.map(i => modify(i)) : modify(response);
  }

  commonToQuery(common: string, params: any, isNew: boolean = true) {
    let fieldCollection: any = {
      [this.types.obj]: [
        {code: 'addressObject'},
        {code: 'nameObject'},
        {code: 'kindNameObject'},
        {code: 'organizationNameObject'}
      ],
      [this.types.droneRequest]: [
        {code: 'flyby_date', type: 'date'},
        {code: 'flyobject_address'},
        {code: 'flyobject_name'}
      ]
    };
    let fields: string[] = [];
    params.types.forEach(t => fields.push(...fieldCollection[t]));
    let commonQuery: string = this.commonQuery.commonToQuery(common, uniq(fields), isNew);
    if (commonQuery) {
      params.query = params.query ? params.query + ' AND (' + commonQuery + ')' : commonQuery;
    }
    delete params.common;
  }
}
