import {Injectable} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {SessionStorage} from "@reinform-cdp/security";
import {from, forkJoin} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {assign, find, concat, compact, some, every, isObject, isNull, isFunction, isArray} from 'lodash';

@Injectable()
export class WorkMatrixService {
  matrixDictionaryName: string = 'mr_program_WorkMatrix';
  matrix: any[] = [];

  constructor(private nsi: NsiResourceService,
              private session: SessionStorage) {}

  /**
   * Before using this service need to call method init() and wait its end
   */
  init(): Observable<any[]> {
    return Observable.create(observer => {
      from(this.nsi.getDictsFromCache([this.matrixDictionaryName])).subscribe(res => {
        this.matrix = res[this.matrixDictionaryName];
        observer.next(this.matrix);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  /**
   * Filter dictionary values by current user groups using role matrix
   * @param list - dictionary witch need to filter
   * @param type - property name from matrix dictionary where need to find available values
   * @param params - other settings:
   *        - accord - main property for comparison dictionary values
   *        - filter - additional filter method - (item) => boolean
   * @returns - filtered list from first argument
   */
  filter(list: any[], type: string, params?: any): any[] {
    let result: any[] = [];
    let cfg: any = {
      accord: 'code',
      filter: null
    };
    if (isObject(params)) cfg = assign(cfg, params);
    let dict: any[] = this.getDictByAllRoles(type);
    result = dict.length ? list.filter(i => some(dict, d => d[cfg.accord] === i[cfg.accord])) : list;
    if (isFunction(cfg.filter)) result = result.filter(cfg.filter);
    return result;
  }

  /**
   * Check dictionary value by current user groups using role matrix
   * @param value - code (or codes array) from dictionary value for check
   * @param type - property name from matrix dictionary where need to find available values
   * @param params:
   *        - accord - main property for comparison dictionary values
   * @returns - true if value is suitable
   */
  check(value: string | string[], type: string, params?: any): boolean {
    // TODO не использовать (пока можно использовать checkByList)
    let result: boolean = true;
    let dict: any[] = [];
    let cfg: any = {
      accord: 'code',
      role: null,
      extra: true,
      source: []
    };
    if (isObject(params)) cfg = assign(cfg, params);
    if (cfg.source && cfg.source.length) {
      dict = concat.apply([], cfg.source.map(i => {
        if (cfg.extra) return i.extra[type] ? i.extra[type].map(t => assign({}, t, cfg.extra ? {extra: i} : null)) : [];
        return i[type] ? i[type].map(t => assign({}, t, i)) : [];
      }));
    } else if (cfg.role) {
      dict = this.getDictByRole(type, cfg.role);
    } else {
      dict = this.getDictByAllRoles(type);
    }

    if (dict.length) {
      if (isArray(value)) {
        result = every(value, v => some(dict, i => i[cfg.accord] === v));
      } else {
        result = some(dict, i => i[cfg.accord] === value);
      }
    }

    return result;
  }

  checkByList(value: string | string[], type: string, params?: any): boolean {
    let result: boolean = true;
    let dict: any[] = [];
    let cfg: any = {
      accord: 'code',
      source: [] // массив записей справочника матрицы
    };
    if (isObject(params)) cfg = assign(cfg, params);
    if (cfg.source && cfg.source.length) {
      result = some(cfg.source, s => {
        if (!s || !s[type] || !s[type].length) return true;
        if (isArray(value)) {
          return every(value, v => some(s[type], i => i[cfg.accord] === v));
        } else {
          return some(s[type], i => i[cfg.accord] === value);
        }
      });
    }
    return result;
  }

  getUserMatrixRows(): any[] {
    let groups: string[] = (this.session.groups() || []).filter(g => g.indexOf('MR_ORG') === 0 || g.indexOf('MR_PREFECT') === 0);
    return this.matrix.filter(m => some(groups, g => g === m.role));
  }

  getDictByRole(type: string, role: string) : any[] {
    let result: any[] = [];
    let matrixRow = find(this.matrix, i => i.code === role);
    if (matrixRow) result = matrixRow[type] || [];
    return result;
  }

  getDictByAllRoles(type: string, params?: any): any[] {
    let cfg: any = {
      accord: 'code',
      extra: true
    };
    if (isObject(params)) cfg = assign(cfg, params);
    let matrixRows = this.getUserMatrixRows();
    if (every(matrixRows, i => !i[type] || !i[type].length)) return [];
    return concat.apply([], matrixRows.map(i => i[type] ? i[type].map(t => assign({}, t, cfg.extra ? {extra: i} : null)) : []));
  }
}
