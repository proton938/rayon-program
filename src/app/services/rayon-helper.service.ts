import {Inject, Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {copy} from 'angular';
import {isString, isArray, compact, isObject, isEmpty, hasIn} from 'lodash';
import {HttpClient} from '@angular/common/http';
import {Observable, from} from 'rxjs';
import {IRootScopeService} from 'angular';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {RayonProgramFileInfo} from '../models/program/RayonProgramFileInfo';
import { formatDate } from '@angular/common';
import {map} from 'rxjs/internal/operators';
import {StateService} from '@uirouter/core';
import {NSIDocumentType} from '@reinform-cdp/core';
import {RayonProgram} from '../models/program/RayonProgram';
import {SettingsService} from '@reinform-cdp/skeleton';
import {SolrMediatorService} from './solr-mediator.service';

@Injectable()
export class RayonHelperService {
  registersUrl = '/main/#/app/catalog-registers';
  urlSettingName = 'registersCatalogUrl';

  constructor(protected toastr: ToastrService,
              private http: HttpClient,
              private solrMediator: SolrMediatorService,
              private state: StateService,
              private fileService: FileResourceService,
              private settings: SettingsService,
              @Inject('$rootScope') private rootScope: IRootScopeService) {
    const url = settings.settings[this.urlSettingName];
    if (url) { this.registersUrl = url; }
  }

  error(error: any): void {
    const err = error ? error.error : '';
    if (err && err.userMessages && err.userMessages.length) {
      err.userMessages.forEach(msg => this.toastr.error(msg));
    } else if (isString(error)) {
      this.toastr.error(error);
    }
  }

  success(message: string): void {
    this.toastr.success(message);
  }

  warning(message: string): void {
    this.toastr.warning(message);
  }

  /* LDAP find users */
  findLdapUsers(params?: {
    fio?: string,
    department?: string[],
    group?: string[]
  }): Observable<any[]> {
    return Observable.create(observer => {
      this.http.post('/nsi-api/v1/users', params || {}).subscribe(response => {
        observer.next(response);
        observer.complete();
      });
    });
  }

  /**
   * [textMask]
   * Generate mask for number fields (\d+)
   */
  maskNumber(t: string): any[] {
    const result = [];
    let parts: any[] = [];

    if (t) { parts = t.split(''); }
    parts.forEach(i => {
      if (/\d/.test(i)) { result.push(/\d/); }
    });
    return result;
  }

  /**
   * [textMask]
   * Generate mask for money fields (\d+)
   */
  maskMoney(t: string): any[] {
    const result = [];
    let parts: any[] = [],
      hasDot = false,
      wholePart = '';

    if (t) {
      const surround = t.split('.');
      if (surround.length > 1) { hasDot = true; }
      wholePart = surround.splice(0, 1).join('');
      parts = wholePart.replace(/\D/g, '').split('');
    }
    parts.reverse().forEach((item, index) => {
      if (index && index % 3 === 0) { result.push(' '); }
      result.push(/\d/);
    });
    result.reverse();
    if (hasDot) { result.push('.', /\d/, /\d/); }
    return result;
  }

  /**
   * [textMask]
   * Generate mask for decimal fields (\d+\.\d*)
   */
  maskDecimal(t: string): any[] {
    const result = [];
    let parts: any[] = [],
        decimal = false;
    if (t) { parts = t.split(''); }
    parts.forEach((i, index) => {
      if (!index && i === '-') { result.push(/-/); }
      if (/\d/.test(i)) {
        result.push(/\d/);
      } else if (i === '.' && !decimal) {
        result.push('.');
        decimal = true;
      }
    });
    if (result.length && result[result.length - 1] === '.') { result.push(/\d/); }
    return result;
  }

  /**
   * [textMask]
   * Generate mask for cadastral number
   * АА:ВВ:CCCCСCC:КК
   * АА-2 цифры
   * ВВ-2 цифры
   * CCCCСCC - 6-7 цифр
   * КК 2-3 цифры
   * Пример: 77:123:222444:22
   */
  maskCadastral(t: string): any[] {
    const result = [];
    let parts: any[] = [];
    const addResult = (str: string, max: number = 0, min?: number) => {
      if (result.length) { result.push(/:/); }
      const symbols: string[] = str.split('');
      symbols.forEach((i, index) => {
        if (index < max && /\d/.test(i)) { result.push(/\d/); }
      });
    };
    if (t) { parts = t.split(':'); }
    parts.forEach((i, index) => {
      switch (index) {
        case 0: addResult(i, 2); break;
        case 1: addResult(i, 2); break;
        case 2: addResult(i, 7); break;
        case 3: addResult(i, 3); break;
        default: return false;
      }
    });
    return result;
  }

  convertToNsi(value: any): NSIDocumentType {
    const r: NSIDocumentType = new NSIDocumentType();
    r.build(value);
    return r;
  }

  convertDocToProgram(doc: any): RayonProgram {
    const r: RayonProgram = new RayonProgram();
    r.build(doc);
    return r;
  }

  getBreadcrumbs(): any[] {
    return this.rootScope['breadcrumbs'];
  }

  getShowcaseUrl(showcaseCode: string, documentCode: string = 'OBJECT', subsystemCode: string = 'MR_PROGRAM', systemCode: string = 'MR') {
    return this.registersUrl + `/showcase/${systemCode}/${subsystemCode}/${documentCode}/${showcaseCode}`;
  }

  cardBreadcrumbs(showcase: any, cardTitle: string) {
    const r: any[] = [{ title: 'Реестры', url: this.registersUrl }];
    if (showcase) {
      r.push({
        title: showcase.name,
        url: this.getShowcaseUrl(showcase.code, showcase.documentCode) + '?showcaseName=' + showcase.name
      });
    }

    r.push({
      title: cardTitle || '',
      url: null
    });

    return r;
  }

  setBreadcrumbs(breadcrumbs: any[] = []): void {
    if (!this.rootScope['breadcrumbs']) { this.rootScope['breadcrumbs'] = []; }
    this.rootScope['breadcrumbs'].length = 0;
    breadcrumbs.forEach(b => {
      this.rootScope['breadcrumbs'].push(b);
    });
  }

  getFullAddress(addressId: string): Observable<string> {
    return Observable.create(observer => {
      const ok = (text: string = '') => {
        observer.next(text);
        observer.complete();
      };
      if (addressId) {
        this.solrMediator.query({page: 0, pageSize: 1, types: [this.solrMediator.types.address],
          query: 'documentId:' + addressId}).subscribe((addRes: any) => {
          if (addRes && addRes.docs && addRes.docs.length && addRes.docs.length === 1) {
            ok(addRes.docs[0].fullAddress);
          } else { ok(); }
        }, error => {
          observer.error(error);
          observer.complete();
        });
      } else { ok(); }
    });
  }

  getFiles(folderId: string): Observable<any[]> {
    return Observable.create(observer => {
      from(this.fileService.getFolderContent(folderId, false)).subscribe(response => {
        observer.next(response || []);
      }, error => observer.error(error), () => observer.complete());
    });
  }

  getShortFileInfo(file: any, unpublished: boolean = false): RayonProgramFileInfo {
    const info = new RayonProgramFileInfo();
    info.build({
      id: file.versionSeriesGuid || file.idFile,
      type: file.fileType || file.typeFile,
      date: file.dateCreated || (file.dateFile ? formatDate(new Date(file.dateFile), 'yyyy-MM-ddTHH:mm:ss', 'en') : ''),
      unpublished: unpublished
    });
    return info;
  }

  getNumberOfSimilarFile(name: string, files: any[] = []): number {
    let num = 0;
    files.forEach(f => {
      if (f.nameFile.indexOf(name.split('.').slice(0, -1).join('.')) + 1) {
        const fName = f.nameFile.split('.').slice(0, -1).join('.');
        const numberOfFile: number = fName.match(/\((\d+\)$)/) ?
          +fName.match(/\((\d+\)$)/)[0].slice(1, -1) + 1 : 1;
        num = (num > numberOfFile) ? num : numberOfFile;
      }
    });
    return num;
  }

  getWorks(workIds: string[] = [], objectIds: string[] = []): Observable<any[]> {
    const queryParts: string[] = [];
    workIds.forEach(i => queryParts.push('documentId:' + i));
    objectIds.forEach(i => queryParts.push('objectIdWork:' + i));
    const query = {page: 0, pageSize: 999, types: [this.solrMediator.types.work], query: queryParts.join(' OR ')};
    return this.solrMediator.query(query).pipe(map((r: any) => r && r.docs ? r.docs : []));
  }

  getCommonQuery(searchString: string = '', fieldNames: string[] = []) {
    let r = '';
    if (searchString !== '') {
      fieldNames.forEach((item: string, index: number) => {
        const queryItem: string = this.getCommonQueryItem(item, searchString.trim());
        if (queryItem) { r += (index ? ' OR ' : '') + queryItem; }
      });
    }
    return r;
  }

  getCommonQueryItem(itemName: string, searchString: string = '') {
    let r = '';
    if (itemName.indexOf('Date') > 0) {
      if (/^\d{2}\.\d{2}\.\d{4}$/.test(searchString)) {
        const date = searchString.split('.').reverse().join('-');
        r = `(${itemName}:[${date}T00:00:00Z TO ${date}T23:59:59Z])`;
      }
    } else if (searchString) {
      r = `(${itemName}:(*${searchString.split(/\s+/).join('* AND *')}*))`;
    } else {
      r = `(${itemName}:*${searchString}*)`;
    }
    return r;
  }

  goToTask(stateName: string, taskId: string, system: string = 'mrobject', systemCode: string = 'MR') {
    (<any>window).location.href = this.state.href(stateName, {
      system: system,
      taskId: taskId,
      systemCode: systemCode
    });
  }

  getField(context: any, path: string = ''): any {
    if (!context) { return null; }
    let field: any = copy(context);
    const pathParts: string[] = path ? compact(path.split(/[\/\.]/)) : [];
    const transformArray = f => {
      const r: any[] = [];
      f.forEach(i => {
        if (isArray(i)) {
          r.push(...i);
        } else {
          r.push(i);
        }
      });
      return r;
    };
    pathParts.forEach(i => {
      if (isArray(field)) {
        field = transformArray(field).map(f => f && f[i] ? f[i] : null);
      } else if (field) {
        field = field[i];
      }
    });
    return field;
  }

  isEmptyObject(field): boolean {
    let r = false;
    if (isObject(field)) {
      if (isEmpty(field) || (hasIn(field, 'code') && !field.code)) {
        r = true;
      }
    }
    return r;
  }

  makeYearDictionary() {
    const year = (new Date()).getFullYear();
    const min = year - 10;
    const max = year + 10;
    let startYear = min;
    const r: any[] = [startYear];
    for (let i = 0; startYear < max; i++) {
      r.push(++startYear);
    }
    return r.map(i => {
      return {code: i, name: i};
    });
  }
}
